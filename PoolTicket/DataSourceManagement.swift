//
//  DataSourceManagement.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/11/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation

class DataSourceManagement : NSObject {
    
        static var searchPoolList = [poolsListModel]()
        public static let SelectedCityAndState = "selected_city_and_state";
        static var favoritePoolList = [poolsListModel]()
        static var increaseUrl = ""
        static var searchPoolName = ""
        static var openNearLocationPools = false
        static var allPools = false
        static var lat = ""
        static var lng = ""
        static var poolSortList = [sort_list]()
        static var selectedPoolID = ""
        static var selectedDrawerState = "-0"
        static var commentsList = [poolsComments]()
        static var poolCommentSortList = [sort_list]()
        static var pAttributes = [poolsAttributes]()
        static var pSpecification = [poolsSpecification]()
        static var userPurchaseHistory = [purchasesModel]()
        static var fromInbox = false
        static var filterList: [filterObject] = []
    
        static func addSelectedDrawerState(_ state: String) -> Void{
            self.selectedDrawerState = state
        }

       static func addPoolsList(_ rest:poolsListModel) -> Void{
            searchPoolList.append(rest)
        }
    
        static func cleanPoolsList()->Void {
            searchPoolList.removeAll()
        }
    
        static func addFavoritePoolsList(_ rest:poolsListModel) -> Void{
            favoritePoolList.append(rest)
        }
    
        static func cleanFavoritePoolsList()->Void {
            favoritePoolList.removeAll()
        }
    
        static func addTheIncreaseURL(_ url: String){
          self.increaseUrl = url
        }
    
        static func addSearchPoolName(_ pName: String){
            self.searchPoolName = pName
        }
    
        static func setOpenNearLocationPool(_ status: Bool){
            self.openNearLocationPools = status
        }
    
        static func allPools(_ status: Bool){
            self.allPools = status
        }
    
        static func setDeviceLng(_ lng: String){
            self.lng = lng
        }
    
        static func setDeviceLat(_ lat: String){
            self.lat = lat
        }
    
        static func addSortList(_ rest:sort_list) -> Void{
            poolSortList.append(rest)
        }
    
        static func cleanSortList()->Void {
            poolSortList.removeAll()
        }
    
        static func setSelectedPoolId(_ id: String){
            self.selectedPoolID = id
        }
    
    
        static func addPoolsComment(_ rest:poolsComments) -> Void{
            commentsList.append(rest)
        }
    
        static func cleanPoolsComment()->Void {
            commentsList.removeAll()
        }
    
        static func addCommentSortList(_ rest:sort_list) -> Void{
            poolCommentSortList.append(rest)
        }
    
        static func cleanCommentSortList()->Void {
            poolCommentSortList.removeAll()
        }
    
        static func addPoolAttribute(_ rest:poolsAttributes) -> Void{
            pAttributes.append(rest)
        }
    
        static func cleanPoolAttribute()->Void {
            pAttributes.removeAll()
        }
    
        static func addPoolSpecification(_ rest:poolsSpecification) -> Void{
            pSpecification.append(rest)
        }
    
        static func cleanPoolSpecification()->Void {
            pSpecification.removeAll()
        }
    
        static func addPurchasesHistory(_ rest:purchasesModel) -> Void{
            userPurchaseHistory.append(rest)
        }
    
        static func cleanPurchasesHistory()->Void {
            userPurchaseHistory.removeAll()
        }
    
        static func setFromInboxStatus(_ status: Bool){
            fromInbox = status
        }
    
    
        // setter and getter access token
        public static func setSelectedStateAndCity(_ value: cityModel) -> Void{
           
            let encodedData = NSKeyedArchiver.archivedData(withRootObject: value)
            let defaults = UserDefaults.standard
            shareData.shared.selectedCityAndState = value
            defaults.set(encodedData, forKey: DataSourceManagement.SelectedCityAndState)
       
    }
    
        public static func getSelectedStateAndCity()->cityModel?{
            let defaults = UserDefaults.standard
            // retrieving a value for a key
            if let data = defaults.data(forKey: DataSourceManagement.SelectedCityAndState),
                let object = NSKeyedUnarchiver.unarchiveObject(with: data) as? cityModel {
                shareData.shared.selectedCityAndState = object
                return object
                //myPeopleList.forEach({print( $0.name, $0.age)})  // Joe 10
            } else {
                print("There is an issue")
            }
            return getDefaultSelectedCity()
        }
    
        // set default city to tehran
        static func getDefaultSelectedCity()->cityModel{
            return cityModel.prepareDefault()
        }
    
    
    
    
    
//    static var mainCategory = [categoryMainModel]()
//    static var mainBanner = [bannerMainModel]()
//    static var mainCategoryProductRow01 = [productMainModel]()
//    static var mainCategoryProductRow02 = [productMainModel]()
//    static var mainCategoryProductRow03 = [productMainModel]()
//    static var relatedProduct = [productMainModel]()
//    static var bestSeller = [productMainModel]()
//    static var latestProducts = [productMainModel]()
//    static var productDetail = [singleProductModel]()
//    static var category = [categoryModel]()
//    static var categoryByIdProducts = [productMainModel]()
//    static var searchProduct = [productMainModel]()
//    static var userProfile = [userProfileModel]()
//    static var wishlistProduct = [wishlistModel]()
//    static var profileAddresses = [addressMainModel]()
//    static var profileAddressId = [addressMainModel]()
//    static var loginDetail:userLoginModel?
//    static var reviewOrdersList = [reviewOrdersModel]()
//    static var reviewOrder:reviewOrderByIdModel?
//    static var paymentAddresses:paymentAddresses?
//    static var pBrandsList = [brandsModel]()
//    static var pBrandsChildList = [brandChildModel]()
//
//
//    static func addMainCategory(_ rest:categoryMainModel) -> Void{
//        mainCategory.append(rest)
//    }
//
//    static func cleanMainCategory()->Void {
//        mainCategory.removeAll()
//    }
//
//    static func addMainBanner(_ rest:bannerMainModel) -> Void{
//        mainBanner.append(rest)
//    }
//
//    static func cleanMainBanner()->Void {
//        mainBanner.removeAll()
//    }
//
//    static func addMainCategoryProductRow1(_ rest:productMainModel) -> Void{
//        mainCategoryProductRow01.append(rest)
//    }
//
//    static func cleanMainCategoryProductRow1()->Void {
//        mainCategoryProductRow01.removeAll()
//    }
//
//    static func addMainCategoryProductRow2(_ rest:productMainModel) -> Void{
//        mainCategoryProductRow02.append(rest)
//    }
//
//    static func cleanMainCategoryProductRow2()->Void {
//        mainCategoryProductRow02.removeAll()
//    }
//
//    static func addMainCategoryProductRow3(_ rest:productMainModel) -> Void{
//        mainCategoryProductRow03.append(rest)
//    }
//
//    static func cleanMainCategoryProductRow3()->Void {
//        mainCategoryProductRow03.removeAll()
//    }
//
//    static func addBestSeller(_ rest:productMainModel) -> Void{
//        bestSeller.append(rest)
//    }
//
//    static func cleanBestSeller()->Void {
//        bestSeller.removeAll()
//    }
//
//    static func addLatestProducts(_ rest:productMainModel) -> Void{
//        latestProducts.append(rest)
//    }
//
//    static func cleanLatestProducts()->Void {
//        latestProducts.removeAll()
//    }
//
//    static func addProductDetail(_ rest:singleProductModel) -> Void{
//        productDetail.append(rest)
//    }
//
//    static func cleanProductDetail()->Void {
//        productDetail.removeAll()
//    }
//
//    static func addRelatedProduct(_ rest:productMainModel) -> Void{
//        relatedProduct.append(rest)
//    }
//
//    static func cleanRelatedProduct()->Void {
//        relatedProduct.removeAll()
//    }
//
//    static func addCategory(_ rest:categoryModel) -> Void{
//        category.append(rest)
//    }
//
//    static func cleanCategory()->Void {
//        category.removeAll()
//    }
//
//    static func addcategoryByIdProducts(_ rest:productMainModel) -> Void{
//        categoryByIdProducts.append(rest)
//    }
//
//    static func cleancategoryByIdProducts()->Void {
//        categoryByIdProducts.removeAll()
//    }
//
//    static func addSearchProducts(_ rest:productMainModel) -> Void{
//        searchProduct.append(rest)
//    }
//
//    static func cleanSearchProducts()->Void {
//        searchProduct.removeAll()
//    }
//
//    static func addUserProfile(_ rest:userProfileModel) -> Void{
//        userProfile.append(rest)
//    }
//
//    static func cleanUserProfile()->Void {
//        userProfile.removeAll()
//    }
//
//    static func addWishList(_ rest:wishlistModel) -> Void{
//        wishlistProduct.append(rest)
//    }
//
//    static func cleanWishlist()->Void {
//        wishlistProduct.removeAll()
//    }
//
//    static func addProfileAddresses(_ rest:addressMainModel) -> Void{
//        profileAddresses.append(rest)
//    }
//
//    static func cleanProfileAddresses()->Void {
//        profileAddresses.removeAll()
//    }
//
//    static func addProfileAddressById(_ rest:addressMainModel) -> Void{
//        profileAddressId.append(rest)
//    }
//
//    static func cleanProfileAddressById()->Void {
//        profileAddressId.removeAll()
//    }
//
//    static func addLoginDetail(_ rest:userLoginModel) -> Void{
//        loginDetail =  rest
//    }
//
//    static func cleanLoginDetail()->Void {
//        loginDetail = nil
//    }
//
//    static func addReviewOrders(_ rest:reviewOrdersModel) -> Void{
//        reviewOrdersList.append(rest)
//    }
//
//    static func cleanReviewOrders()->Void {
//        reviewOrdersList.removeAll()
//    }
//
//    static func addReviewOrderById(_ rest:reviewOrderByIdModel) -> Void{
//        reviewOrder =  rest
//    }
//
//    static func cleanReviewOrderById()->Void {
//        reviewOrder = nil
//    }
//
//    static func addPaymentAddresses(_ rest:paymentAddresses) -> Void{
//        paymentAddresses =  rest
//    }
//
//    static func cleanPaymentAddresses()->Void {
//        paymentAddresses = nil
//    }
//    static func addBrands(_ rest:brandsModel) -> Void{
//        pBrandsList.append(rest)
//    }
//
//    static func cleanBrands()->Void {
//        pBrandsList.removeAll()
//    }
//
//    static func addBrandsChild(_ rest:brandChildModel) -> Void{
//        pBrandsChildList.append(rest)
//    }
//
//    static func cleanBrandsChild()->Void {
//        pBrandsChildList.removeAll()
//    }
//
    
}
