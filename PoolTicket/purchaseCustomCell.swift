//
//  purchase_custom_cell.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/3/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit

class purchaseCustomCell: UITableViewCell {
    
    
    @IBOutlet weak var pool_name: UILabel!
    @IBOutlet weak var voucher: UILabel!
    @IBOutlet weak var start_date: UILabel!
    @IBOutlet weak var time: UILabel!
    
    @IBOutlet weak var ticket_count: UILabel!
    @IBOutlet weak var total_price: UILabel!

    @IBOutlet weak var show_specification: UIButton!
    @IBOutlet weak var status: UIButton!
    @IBOutlet weak var saens_name: UILabel!
}
