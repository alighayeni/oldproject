//
//  ShowTransactionSpecificationVC.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/7/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import WebKit


class  ShowTransactionSpecificationVC: UIViewController, WKUIDelegate, WKNavigationDelegate {
    
    @IBOutlet weak var contentView: UIView!
    var webView: WKWebView!

    
    func closeView(_ Sender: Any){
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage() //remove pesky 1 pixel line
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor.white
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.webView = WKWebView(frame: self.contentView.bounds, configuration: WKWebViewConfiguration())
        self.webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
       
        //self.webView.translatesAutoresizingMaskIntoConstraints = false
        self.webView.navigationDelegate = self
        self.contentView.addSubview(self.webView)
        
        if let value = shareData.shared.purchase_id as? String {
            let client_id = Constants.clientId
            let access_token = SwiftUtil.getUserAccessToken()
            var rURL = URL (string: Constants.PURCHASE_BY_ID+value+"?client_id="+client_id+"&access_token="+access_token)
            self.loadWebPage(rURL!)
        }
        
    }
    
    func loadWebPage(_ url: URL)  {
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        var customRequest = URLRequest(url: url)
        customRequest.setValue(client_id, forHTTPHeaderField: "client_id")
        customRequest.setValue(access_token, forHTTPHeaderField: "access_token")
        webView!.load(customRequest)
    }
    
    
    @IBAction func closeView(){
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
}
