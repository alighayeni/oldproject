//
//  refreshTokenUsersModel.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/12/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
class refreshTokenUsersModel: NSObject {
    

    
    var credit:String?
    var city_id:String?
    var email:String?
    var mobile:String?
    var name:String?
    var state_id:String?

    
    
    init(credit:String, city_id:String, email:String, mobile:String, name:String,  state_id:String) {
        self.credit=credit
        self.city_id=city_id
        self.email=email
        self.mobile=mobile
        self.name=name
        self.state_id=state_id


    }
    
    static func prepare(_ item:AnyObject) -> refreshTokenUsersModel{
        
        
        
        let credit = String(describing: item["credit"] as AnyObject)
        let city_id = String(describing: item["city_id"] as AnyObject)
        let email = String(describing: item["email"] as AnyObject)
        let mobile = String(describing: item["mobile"] as AnyObject)
        let name = String(describing: item["name"] as AnyObject)
        let state_id = String(describing: item["state_id"] as AnyObject)

        
        let instance = refreshTokenUsersModel(credit:credit, city_id:city_id, email:email, mobile:mobile, name:name, state_id:state_id)
        return instance
    }

    
    
    
    
}
