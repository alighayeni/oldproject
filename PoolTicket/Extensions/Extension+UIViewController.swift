//
//  UIViewController.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/11/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    //MARK: NAvigation bar
    func setNavigationBarItem() {
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 254/255.0, green: 210/255.0, blue: 15.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
        self.slideMenuController()?.addLeftGestures()
        self.slideMenuController()?.addRightGestures()
    }
    
    func removeNavigationBarItem() {
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = nil
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
    }
    
    //MARK: Refresh Token Set And Get
    func setUserRefreshToken(_ value: String){
        let defaults = UserDefaults.standard
        Constants.Refresh_Token = value
        defaults.set(value, forKey: UserDefaults.Keys.RefreshTokenTitle)
    }
    
    func getUserRefreshToken()->String{
        let defaults = UserDefaults.standard
        if let value = defaults.string(forKey: UserDefaults.Keys.RefreshTokenTitle) {
            Constants.Refresh_Token = value
            return value
        }
        return ""
    }
    
    
    //MARK: User Access Token Set And Get
    func setUserAccessToken(_ value: String){
        let defaults = UserDefaults.standard
        Constants.Access_Token = value
        defaults.set(value, forKey: UserDefaults.Keys.AccessTokenTitle)
    }
    
    func getUserAccessToken()->String{
        let defaults = UserDefaults.standard
        if let value = defaults.string(forKey: UserDefaults.Keys.AccessTokenTitle) {
            if value != "" {
                Constants.Access_Token = value
                return value
            }
        }
        return ""
    }
    
    //MARK: User Name Set And Get
    func setUserName(_ value: String){
        let defaults = UserDefaults.standard
        Constants.UserName = value
        defaults.set(value, forKey: UserDefaults.Keys.UserNameTitle)
    }
    
    func getUserName()->String{
        let defaults = UserDefaults.standard
        if let value = defaults.string(forKey: UserDefaults.Keys.UserNameTitle) {
            if value != "" {
                Constants.UserName = value
                return value
            }
        }
        return ""
    }
    
    //MARK: All Cities Update Set And Get
    func setAllCitiesUpdate(_ value: String) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: UserDefaults.Keys.AllCitiesUpdate)
    }
    
    func getAllCitiesUpdate()->String{
        let defaults = UserDefaults.standard
        if let value = defaults.string(forKey: UserDefaults.Keys.AllCitiesUpdate) {
            if value != "" {
                return value
            }
        }
        return ""
    }
    
    //MARK: Set and Get time to get update Flag
    func setNextTimeUpdate(_ value: String) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: UserDefaults.Keys.NextTimeUpdate)
    }
    
    func getNextTimeUpdate() -> String {
        let defaults = UserDefaults.standard
        if let value = defaults.string(forKey: UserDefaults.Keys.NextTimeUpdate) {
            if value != "" {
                return value
            }
        }
        return ""
    }
    
    //MARK: set The USER ID
    func setUserId(_ value: String) {
        let defaults = UserDefaults.standard
        Constants.User_Id = value
        defaults.set(value, forKey: Constants.UserIdTitle)
    }
 
   
    //MARK: Custom URL
    func openCustomURLScheme(customURLScheme: String) -> Bool {
        let customURL = URL(string: customURLScheme)!
        if UIApplication.shared.canOpenURL(customURL) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(customURL)
            } else {
                UIApplication.shared.openURL(customURL)
            }
            return true
        }
        
        return false
    }
}
