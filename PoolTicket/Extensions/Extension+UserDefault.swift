//
//  Extension+UserDefault.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 9/27/19.
//  Copyright © 2019 OneSignal. All rights reserved.
//

import Foundation

extension UserDefaults {
    enum Keys {
        static var Access_Token = "";
        static var AccessTokenTitle = "Access_Token";
        static var UserName = "";
        static var UserNameTitle = "Username";
        static var NextTimeUpdate =  "next_time_update"
        static var AllCitiesUpdate =  "all_cities_update"
        static var Refresh_Token = "";
        static var RefreshTokenTitle = "Refresh_Token";
        static var PTracking = "p-tracking";
        static var User_Id = "";
        static var UserIdTitle = "User_Id";
        static var PushNotificationToken = "pushNotificationToken"
        static var drawerLogoUrl = "drawerLogoUrl"
        static var profileName = "profileName"
        static var profileMobile = "profileMobile"
        static var profileEmail = "profileEmail"
        static var facebook = "facebook"
        static var instagram = "instagram"
        static var telegram = "telegram"
        static var twitter = "twitter"
    }
}
