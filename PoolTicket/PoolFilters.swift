//
//  PoolFilters.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/14/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyDrop

class PoolFilters: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
 
    @IBOutlet weak var tableview:UITableView!
    @IBOutlet weak var centerView:UIView!

    var list : [areasModel] = []
    var list01 : [String] = []
    var jensiatList : JensiatExpandable = JensiatExpandable.init(isExpanded: false, jensiatObjects: [])
    var poolTypeList : PoolTypeExpandable = PoolTypeExpandable.init(isExpanded: false, poolTypeObject: [])
    var serviceTypeList : ServiceTypeExpandable = ServiceTypeExpandable .init(isExpanded: false, serviceTypeObject: [])

    static var poolName = ""
    
    static var selectedAreas = ""
    static var selectedAreaTitle = ""
    
    static var selectedGenderType = "2"
    static var selectedGenderTypePosition = 0
    
    static var selectedPoolTypeId = ""
    
    static var selectedServiceType = "0"
    static var selectedServiceTypePosition = 0

    var isSale = "true"
    var isSaleTicket = "false"
    var isSaleCourse = "false"
    var isSalePack = "false"
    var selectedCityId = ""

    @objc func openDropDown(_ sender:Any) {
        
        if let value = sender as? UIButton {
            self.showDurationAlert(sender,self.list01, value, "همه مناطق")
        }
        
    }

    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 5
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?  {
        
        switch section {
        case 0:
            let infoView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "FilterHeaderSection") as! FilterHeaderSection
            infoView.itemBtn.setTitle("   نام مجموعه", for: .normal)
            infoView.itemBtn.backgroundColor = .white
            infoView.itemBtn.tag = 0
            infoView.itemIcon.image = nil
            return infoView
            
        case 1:
            if let infoView = GetAllAreasHeaderSection.instanceFromNib() as? GetAllAreasHeaderSection {
                
                infoView.areasName.addTarget(self, action: #selector(openDropDown), for: .touchUpInside)
                if PoolFilters.selectedAreaTitle.count > 0 {
                    infoView.areasName.setTitle(PoolFilters.selectedAreaTitle, for: .normal)
                }else{
                    infoView.areasName.setTitle("همه مناطق", for: .normal)
                    PoolFilters.selectedAreas = ""
                    PoolFilters.selectedAreaTitle = ""
                }
                return infoView
            }
        case 2:
            
            let infoView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "FilterHeaderSection") as! FilterHeaderSection
            infoView.itemBtn.setTitle("      جنسیت   ", for: .normal)
            infoView.itemBtn.tag = section
            infoView.itemBtn.addTarget(self, action: #selector(handleExpandClose), for: .touchUpInside)
            if let isExpanded = self.jensiatList.isExpanded as? Bool{
                if !isExpanded {
                    infoView.itemIcon.image = UIImage(named: "ic_arrow_left_gray")
                }else{
                    infoView.itemIcon.image = UIImage(named:"ic_arrow_down_blue")
                }
            }
            infoView.clipsToBounds = true
            return infoView
        case 3:
            let infoView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "FilterHeaderSection") as! FilterHeaderSection
            infoView.itemBtn.setTitle("      نوع مجموعه   ", for: .normal)
            infoView.itemBtn.tag = section
            infoView.itemBtn.addTarget(self, action: #selector(handleExpandClose), for: .touchUpInside)
            if let isExpanded = self.poolTypeList.isExpanded as? Bool{
                if !isExpanded {
                    infoView.itemIcon.image = UIImage(named: "ic_arrow_left_gray")
                }else{
                    infoView.itemIcon.image = UIImage(named:"ic_arrow_down_blue")
                }
            }
            infoView.clipsToBounds = true
            return infoView
        case 4:
            let infoView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "FilterHeaderSection") as! FilterHeaderSection
            infoView.itemBtn.setTitle("      خدمات   ", for: .normal)
            infoView.itemBtn.tag = section
            infoView.itemBtn.addTarget(self, action: #selector(handleExpandClose), for: .touchUpInside)
            if let isExpanded = self.serviceTypeList.isExpanded as? Bool{
                if !isExpanded {
                    infoView.itemIcon.image = UIImage(named: "ic_arrow_left_gray")
                }else{
                    infoView.itemIcon.image = UIImage(named:"ic_arrow_down_blue")
                }
            }
            infoView.clipsToBounds = true
            return infoView
            
        default:
            print("")
        }
        
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     
        switch section {
        case 0:
            return 1
        case 1:
            return 0
        case 2:
            if !self.jensiatList.isExpanded {
                return 0
            }
            return self.jensiatList.jensiatObjects.count ?? 0
        case 3:
            if !self.poolTypeList.isExpanded {
                return 0
            }
            return self.poolTypeList.poolTypeObject.count ?? 0
        case 4:
            if !self.serviceTypeList.isExpanded {
                return 0
            }
            return self.serviceTypeList.serviceTypeObject.count ?? 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        switch indexPath.section {
        case 0:
            let identifier = "FilterCustomCellTextInput"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! FilterCustomCellTextInput
            cell.poolName.delegate = self
            cell.poolName.text = PoolFilters.poolName
            doneWithKeyboard(cell.poolName)
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        case 2:
            let identifier = "FilterCustomCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! FilterCustomCell
            cell.name?.text = self.jensiatList.jensiatObjects[indexPath.row].name
            if !self.jensiatList.jensiatObjects[indexPath.row].status {
                cell.selectedItemImage.image = UIImage(named: "ic_radio_button_dark")
            }else{
                cell.selectedItemImage.image = UIImage(named: "ic_radio_button_green")
            }
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        case 3:
            let identifier = "FilterCustomCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! FilterCustomCell
            cell.name?.text = self.poolTypeList.poolTypeObject[indexPath.row].name
            if (self.poolTypeList.poolTypeObject[indexPath.row].type_id)! != PoolFilters.selectedPoolTypeId {
                cell.selectedItemImage.image = UIImage(named: "ic_radio_button_dark")
            }else{
                cell.selectedItemImage.image = UIImage(named: "ic_radio_button_green")
            }
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        case 4:
            let identifier = "FilterCustomCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! FilterCustomCell
            cell.name?.text = self.serviceTypeList.serviceTypeObject[indexPath.row].name
            if (self.serviceTypeList.serviceTypeObject[indexPath.row].type_id)! != PoolFilters.selectedServiceType {
                cell.selectedItemImage.image = UIImage(named: "ic_radio_button_dark")
            }else{
                cell.selectedItemImage.image = UIImage(named: "ic_radio_button_green")
            }
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        default:
            print("")
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       
        switch section {
        case 0:
            return 56
        case 1:
            return 100
        case 2:
            return 56
        case 3:
            return 56
        case 4:
            return 56
        default:
            return 48
        }
        return 48
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        switch indexPath.section {
        case 2:
            switch indexPath.row{
            case 0:
                PoolFilters.selectedGenderTypePosition = 0
                PoolFilters.selectedGenderType = "2"
                self.jensiatList.jensiatObjects[0].status = true
                self.jensiatList.jensiatObjects[1].status = false
                self.jensiatList.jensiatObjects[2].status = false
                 tableView.reloadData()
            break
            case 1:
                PoolFilters.selectedGenderTypePosition = 1
                PoolFilters.selectedGenderType = "0"
                self.jensiatList.jensiatObjects[0].status = false
                self.jensiatList.jensiatObjects[1].status = true
                self.jensiatList.jensiatObjects[2].status = false
                tableView.reloadData()
            break
            case 2:
                PoolFilters.selectedGenderTypePosition = 2
                PoolFilters.selectedGenderType = "1"
                self.jensiatList.jensiatObjects[0].status = false
                self.jensiatList.jensiatObjects[1].status = false
                self.jensiatList.jensiatObjects[2].status = true
                tableView.reloadData()
            break
            default:
                print("")
            }
            
       break
      
       case 3:
            PoolFilters.selectedPoolTypeId = self.poolTypeList.poolTypeObject[indexPath.row].type_id!
            tableView.reloadData()
            break
       
       case 4:
            PoolFilters.selectedServiceType = self.serviceTypeList.serviceTypeObject[indexPath.row].type_id!
            serviceTypeSelected(indexPath.row)
            PoolFilters.selectedServiceTypePosition = indexPath.row
            tableView.reloadData()
            break
       default:
            print("")
        }
        
    }
    
    func serviceTypeSelected(_ row: Int){

        switch row{
        case 0:
            isSale = "true"
            isSaleTicket = "false"
            isSaleCourse = "false"
            isSalePack = "false"
            break
        case 1:
            isSaleTicket = "true"
            isSale = "false"
            isSaleCourse = "false"
            isSalePack = "false"
            break
        case 2:
            isSaleCourse = "true"
            isSale = "false"
            isSaleTicket = "false"
            isSalePack = "false"
            break
        case 3:
            isSalePack = "true"
            isSale = "false"
            isSaleTicket = "false"
            isSaleCourse = "false"
            break
        default:
            print("")
        }
        
    }
    
    @objc func handleExpandClose(button: UIButton){
        
        let section = button.tag
        
        switch section {
        case 2:
            var indexPaths = [IndexPath]()
            for row in self.jensiatList.jensiatObjects.indices {
            let indexPath = IndexPath(row: row, section : section)
            indexPaths.append(indexPath)
            }
            if let isExpanded = self.jensiatList.isExpanded as? Bool{
            self.jensiatList.isExpanded = !isExpanded
            if !isExpanded {
                    tableview.insertRows(at: indexPaths, with: .fade)
                }else{
                    tableview.deleteRows(at: indexPaths, with: .fade)
            }
            
           if let header = self.tableview.headerView(forSection: section) {
                if let sectionHeader = header as? FilterHeaderSection {
                    if !isExpanded {
                    
                    sectionHeader.itemIcon?.image = UIImage(named:"ic_arrow_down_blue")
                    //                    infoView.theTitle.textColor = self.setBgColor( 34, 169, 228)
                    //                    infoView.arrow.image = UIImage(named:"ic_arrow_down_blue")
                }else{
                    sectionHeader.itemIcon?.image = UIImage(named:"ic_arrow_left_gray")

                    }
                }
            }
            }
            printLog(m: "")
        case 3:
            var indexPaths = [IndexPath]()
            for row in self.poolTypeList.poolTypeObject.indices {
                let indexPath = IndexPath(row: row, section : section)
                indexPaths.append(indexPath)
            }
            if let isExpanded = self.poolTypeList.isExpanded as? Bool{
                self.poolTypeList.isExpanded = !isExpanded
                if !isExpanded {
                    tableview.insertRows(at: indexPaths, with: .fade)
                }else{
                    tableview.deleteRows(at: indexPaths, with: .fade)
                }
            
                if let header = self.tableview.headerView(forSection: section) {
                    if let sectionHeader = header as? FilterHeaderSection {
                        if !isExpanded {
                            sectionHeader.itemIcon?.image = UIImage(named:"ic_arrow_down_blue")
                            //                    infoView.theTitle.textColor = self.setBgColor( 34, 169, 228)
                            //                    infoView.arrow.image = UIImage(named:"ic_arrow_down_blue")
                        }else{
                            sectionHeader.itemIcon?.image = UIImage(named:"ic_arrow_left_gray")
                            
                        }
                    }
                }
            }
            
            printLog(m: "")
        case 4:
            var indexPaths = [IndexPath]()
            for row in self.serviceTypeList.serviceTypeObject.indices {
                let indexPath = IndexPath(row: row, section : section)
                indexPaths.append(indexPath)
            }
            if let isExpanded = self.serviceTypeList.isExpanded as? Bool{
                self.serviceTypeList.isExpanded = !isExpanded
                if !isExpanded {
                    tableview.insertRows(at: indexPaths, with: .fade)
                }else{
                    tableview.deleteRows(at: indexPaths, with: .fade)
                }
                if let header = self.tableview.headerView(forSection: section) {
                    if let sectionHeader = header as? FilterHeaderSection {
                        if !isExpanded {
                            sectionHeader.itemIcon?.image = UIImage(named:"ic_arrow_down_blue")
                            //                    infoView.theTitle.textColor = self.setBgColor( 34, 169, 228)
                            //                    infoView.arrow.image = UIImage(named:"ic_arrow_down_blue")
                        }else{
                            sectionHeader.itemIcon?.image = UIImage(named:"ic_arrow_left_gray")
                            
                        }
                    }
                }
            }
            printLog(m: "")
        default:
            print("")
        }
    }
    
    @objc func setSale(button: UIButton){

        switch button.tag {
        case 0:
            if self.isSale == "false" {
                self.isSale = "true"
            }else{
                self.isSale = "false"
            }
            break
        case 1:
            if self.isSaleTicket == "false" {
                self.isSaleTicket = "true"
            }else{
                self.isSaleTicket = "false"
            }
            break
        case 2:
            if self.isSaleCourse == "false" {
                self.isSaleCourse = "true"
            }else{
                self.isSaleCourse = "false"
            }
            break
        case 3:
            if self.isSalePack == "false" {
                self.isSalePack = "true"
            }else{
                self.isSalePack = "false"
            }
            break
        default:
            print("")
        }
        
        self.tableview.reloadData()
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.delegate = self
        tableview.dataSource = self
        setUp();
        centerView.dropShadow()
        setTheSelectedCityAndState()
        getPoolTypes()
        
        if DataSourceManagement.searchPoolName.count > 0 {
            PoolFilters.poolName = DataSourceManagement.searchPoolName
        }
        
        tableview.reloadData()

        tableview.register(UINib(nibName: "FilterHeaderSection", bundle: nil), forHeaderFooterViewReuseIdentifier: "FilterHeaderSection")
        
    }
    
    @IBAction func close(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func setFilters()->Void {
       
        let ndx = IndexPath(row:0, section: 0)
        if let cell = tableview.cellForRow(at:ndx) as? FilterCustomCellTextInput {
            if let value = cell.poolName.text {
                DataSourceManagement.addSearchPoolName(value)
                PoolFilters.poolName = value
            }
        }
        
        
        var tQuery = ""
        let client_id = Constants.clientId
            tQuery = "limit=10"
            // tQuery = "city_id=\(selectedCityId)&area_id=\(selectedAreas)&gender=\(selectedGenderType)&just_active_sell_ticket=\(isSaleTicket)&just_active_sell_course=\(isSaleCourse)&just_active_sell_pack=\(isSalePack)&pool_type=\(selectedPoolTypeId)&pool_name=\(poolName)&limit=100&client_id=\(client_id)"
            if selectedCityId != "" {
               tQuery = tQuery + "&" + "city_id=\(selectedCityId)"
            }
        if PoolFilters.selectedAreas != "" {
            tQuery = tQuery + "&" + "area_id=\(PoolFilters.selectedAreas)"
            }
        if PoolFilters.selectedGenderType != "" {
            tQuery = tQuery + "&" + "gender=\(PoolFilters.selectedGenderType)"
            }
            if isSale == "true" {
                 tQuery = tQuery + "&" + "just_active_sell=\(isSale)"
            }else{
                if isSaleTicket == "true" {
                    tQuery = tQuery + "&" + "just_active_sell_ticket=\(isSaleTicket)"
                }
                if isSaleCourse == "true" {
                    tQuery = tQuery + "&" + "just_active_sell_course=\(isSaleTicket)"
                }
                if isSalePack == "true" {
                    tQuery = tQuery + "&" + "just_active_sell_pack=\(isSaleTicket)"
                }
            }
        if PoolFilters.selectedPoolTypeId != "" {
            tQuery = tQuery + "&" + "pool_type=\(PoolFilters.selectedPoolTypeId)"
            }
//        if PoolFilters.poolName != "" {
//            tQuery = tQuery + "&" + "pool_name=\(PoolFilters.poolName)"
//            }
            if client_id != "" {
                tQuery = tQuery + "&" + "client_id=\(client_id)"
            }
            
            //gender=\(selectedGenderType)
            
        
        shareData.shared.filterPoolListEnable = true
        shareData.shared.filterPoolListQuery = tQuery
        
        AppDelegate.changeFilterValue = true
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func removeFilters()->Void {
       
        self.isSale = "true"
        self.isSaleTicket = "false"
        self.isSaleCourse = "false"
        self.isSalePack = "false"
        AppDelegate.changeFilterValue = true
        PoolFilters.poolName = ""
        self.tableview.reloadData()
        shareData.shared.filterPoolListEnable = false
        shareData.shared.filterPoolListQuery = nil
        DataSourceManagement.searchPoolName = ""
        PoolFilters.clearFiltersView()
        self.dismiss(animated: true, completion: nil)

    }
    
    static func clearFiltersView(){
     
        PoolFilters.selectedAreas = ""
        PoolFilters.selectedAreaTitle = ""
        PoolFilters.selectedGenderType = "2"
        PoolFilters.selectedGenderTypePosition = 0
        PoolFilters.selectedPoolTypeId = ""
        PoolFilters.selectedServiceType = "0"
        PoolFilters.selectedServiceTypePosition = 0
        
    }
    
    /*  get list of states */
    func getALLAreas(_ city_id:String) -> Void {
        let client_id = Constants.clientId
        let just_active_states = true
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.GET_ALL_AREA,"client_id=\(client_id)&city_id=\(city_id)&access_token=\(access_token)")
        index.start{ (indexData) in
            
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                
                                if let items = item["areas"] as? [[String: AnyObject]] {
                                    
                                    self.list.append(areasModel.init("-1", "همه مناطق"))
                                    self.list01.append("همه مناطق")
                                    for itemValue in items {
                                        if let value = areasModel.prepare(itemValue as AnyObject) as? areasModel{
                                            self.list.append(value)
                                            self.list01.append(value.area_name!)
                                        }
                                    }
                                }
                                
                            }
                        }
                        
                        
                    }
                }
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                
            })
            
        }
    }
    
    /*  get list of states */
    func getPoolTypes() -> Void {
        let client_id = Constants.clientId
       // let just_active_states = true
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.GET_POOL_TYPE,"client_id=\(client_id)&access_token=\(access_token)")
        index.start{ (indexData) in
            
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                
                                if let items = item["types"] as? [[String: AnyObject]] {
                                    
                                    var tPoolType : [PoolType] = []
                                    tPoolType.append(PoolType.prepareDefault())
                                    for itemValue in items {
                                        if let value = PoolType.prepare(itemValue as AnyObject) as? PoolType{
                                            tPoolType.append(value)
                                        }
                                    }
                                    self.poolTypeList = PoolTypeExpandable.init(isExpanded: false, poolTypeObject: tPoolType)
                                    
                                }
                                
                            }
                        }
                        
                        
                    }
                }
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                 self.tableview.reloadData()
            })
            
        }
    }
    
    func setUp() {
    
        var t1 = JensiatExpandable.init(isExpanded: false, jensiatObjects: [Jensiat.init(status: false, id: "2", name: "بانوان و آقایان"), Jensiat.init(status: false, id: "0", name: "آقایان"), Jensiat.init(status: false, id: "1", name: "بانوان")])
        t1.jensiatObjects[PoolFilters.selectedGenderTypePosition].status = true
        jensiatList = t1
        
        let t2 = ServiceTypeExpandable.init(isExpanded: false, serviceTypeObject: [ServiceType.init("0", "همه"), ServiceType.init("1", "رزرو بلیت"), ServiceType.init("2", "کلاس شنا"), ServiceType.init("3", "پک")])
        serviceTypeSelected(PoolFilters.selectedServiceTypePosition)
        serviceTypeList = t2
        

        
    
    
    }
    
    // getAllAreas
    func setTheSelectedCityAndState(){
        if !DataSourceManagement.openNearLocationPools {
            if let value = DataSourceManagement.getSelectedStateAndCity()?.city_id as? String {
                self.getALLAreas(value)
                self.selectedCityId = value
            }
        }
    }
    
    func keyboardWillShow(_ notification:Notification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            tableview.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
        }
    }
    
    func keyboardWillHide(_ notification:Notification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            tableview.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    //*************** drop down **************************//
    func showDurationAlert(_ sender:Any, _ strArray: [String], _ btn: UIButton, _ title: String) {
        // var durations = ["bank", "doctor", "restaurant"]
        let actions = strArray.map { seconds in
            return UIAlertAction(title: "\(seconds)", style: .default) { _ in
                let tPosition: Int = strArray.firstIndex(of: seconds) ?? 0
                if "-1" != self.list[tPosition].area_id {
                PoolFilters.selectedAreas = self.list[tPosition].area_id ?? "0"
                PoolFilters.selectedAreaTitle = seconds
                }else{
                PoolFilters.selectedAreas = ""
                PoolFilters.selectedAreaTitle = ""
                }
                btn.setTitle( seconds, for: .normal)
            }
        }
        
        
        let cancelAction = UIAlertAction(title: "بستن", style: .cancel, handler: nil)
        
        let controller = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        for action in [cancelAction] + actions {
            controller.addAction(action)
            PoolFilters.selectedAreas = ""
            PoolFilters.selectedAreaTitle = ""
        }
        showAlert(controller, sourceView: sender as? UIView)
    }
    
    func showAlert(_ controller: UIAlertController, sourceView: UIView? = nil) {
        
        if UIDevice.current.userInterfaceIdiom == .pad  {
            if let sourceView = sourceView {
                let rect = sourceView.convert(sourceView.bounds, to: view)
                controller.popoverPresentationController?.sourceView = view
                controller.popoverPresentationController?.sourceRect = rect
            }
        }
        
        present(controller, animated: true, completion: nil)
    }
    
    func upAllDrops(_ sender: AnyObject) {
        if let hidden = navigationController?.isNavigationBarHidden {
            navigationController?.setNavigationBarHidden(!hidden, animated: true)
        }
        
        Drop.upAll()
    }
    
    //************** softkeyboard tool bar **********************//
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField
        {
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
    func doneWithKeyboard(_ sender: UITextField){
        
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        keyboardToolbar.isTranslucent = true
        keyboardToolbar.barTintColor = UIColor.white
        
        let button = UIButton(type: .system)
        button.setTitle("بستن", for: .normal)
        button.sizeToFit()
        button.titleLabel?.font = UIFont(name: "IRANSansMobile", size: 12)
        button.tintColor = UIColor.black
        button.addTarget(self, action: #selector(closeKeyboardFilterSection(_:)), for: .touchUpInside)
        
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        keyboardToolbar.items = [flexBarButton,UIBarButtonItem(customView: button)]
        
        if let value = sender as? UITextField {
            value.inputAccessoryView = keyboardToolbar
        }else if let value = sender as? UITextView {
            value.inputAccessoryView = keyboardToolbar
        }
        
    }
    
    @objc func closeKeyboardFilterSection(_ sender:UITextField){
        
        let ndx = IndexPath(row:0, section: 0)
        if let cell = tableview.cellForRow(at:ndx) as? FilterCustomCellTextInput {
            if let value = cell.poolName.text {
                DataSourceManagement.addSearchPoolName(value)
                PoolFilters.poolName = value
            }
        }
        self.dismissKeyboard()
    }
    
}

struct Jensiat {
    var status : Bool
    var id : String
    var name :  String
}

struct JensiatExpandable {
     var isExpanded : Bool
    var jensiatObjects: [Jensiat]
}

struct PoolTypeExpandable {
    var isExpanded : Bool
    var poolTypeObject: [PoolType]
}

struct ServiceTypeExpandable {
    var isExpanded : Bool
    var serviceTypeObject: [ServiceType]
}
