//
//  courseCartModel.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/12/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
class courseCartModel: NSObject {
    
    var course_id:String?
    var name:String?
    var gender:String?
    var session_num:String?
    var total_capacity:String?
    var remain_capacity:String?
    var cDescription:String?
    var total_sale:String?
    var total_market:String?
    var discount_chosen:String?
    var count:String?
    var start_date:String?
    var finish_date:String?
    var start_register_datet:String?
    var finish_register_date:String?
    var coupon:String?
    var coupon_message:String?
    var coupon_credit:String?

    
    init(_ course_id:String,_ name:String, _ gender:String, _ session_num:String, _ total_capacity:String, _ remain_capacity:String, _ cDescription: String,_ total_sale:String, _ total_market:String, _ discount_chosen:String, _ count:String, _ start_date:String, _ finish_date:String, _ start_register_datet:String, _ finish_register_date:String, _ coupon:String, _ coupon_message:String, _ coupon_credit:String) {
        
        self.course_id = course_id
        self.name = name
        self.gender = gender
        self.session_num = session_num
        self.total_capacity = total_capacity
        self.remain_capacity = remain_capacity
        self.cDescription =  cDescription
        self.total_sale = total_sale
        self.total_market = total_market
        self.discount_chosen = discount_chosen
        self.count = count
        self.start_date = start_date
        self.finish_date = finish_date
        self.start_register_datet = start_register_datet
        self.finish_register_date = finish_register_date
        self.coupon = coupon
        self.coupon_message = coupon_message
        self.coupon_credit = coupon_credit

    }
    
    static func prepare(_ item:AnyObject) -> courseCartModel{
        
        let course_id = String(describing: item["course_id"] as AnyObject)
        let name = String(describing: item["name"] as AnyObject)
        let gender = String(describing: item["gender"] as AnyObject)
        let session_num = String(describing: item["session_num"] as AnyObject)
        let total_capacity = String(describing: item["total_capacity"] as AnyObject)
        let remain_capacity = String(describing: item["remain_capacity"] as AnyObject)
        let cDescription = String(describing: item["description"] as AnyObject)
        let total_sale = String(describing: item["total_sale"] as AnyObject)
        let total_market = String(describing: item["total_market"] as AnyObject)
        let discount_chosen = String(describing: item["discount_chosen"] as AnyObject)
        let count = String(describing: item["count"] as AnyObject)
        let start_date = String(describing: item["start_date"] as AnyObject)
        let finish_date = String(describing: item["finish_date"] as AnyObject)
        let start_register_datet = String(describing: item["start_register_datet"] as AnyObject)
        let finish_register_date = String(describing: item["finish_register_date"] as AnyObject)
        let coupon = String(describing: item["coupon"] as AnyObject)
        let coupon_message = String(describing: item["coupon_message"] as AnyObject)
        let coupon_credit = String(describing: item["coupon_credit"] as AnyObject)

        let instance = courseCartModel(course_id, name, gender, session_num, total_capacity, remain_capacity, cDescription, total_sale, total_market, discount_chosen, count, start_date, finish_date, start_register_datet, finish_register_date, coupon, coupon_message, coupon_credit)
        return instance
    }
}
