//
//  CustomCellButtonInsie.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/25/19.
//  Copyright © 2019 OneSignal. All rights reserved.
//

import UIKit

class CustomCellButtonInside: UITableViewCell {

    @IBOutlet weak var selectBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
