//
//  GetAllAreasHeaderSection.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/15/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit

class GetAllAreasHeaderSection: UIView {
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    @IBOutlet weak var areasName: UIButton!
    
    class func instanceFromNib() -> GetAllAreasHeaderSection {
        return UINib(nibName: "GetAllAreasHeaderSection", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! GetAllAreasHeaderSection
    }
    
}
