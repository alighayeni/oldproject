//
//  CustomSliderRangeHeader.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/29/19.
//  Copyright © 2019 OneSignal. All rights reserved.
//

import UIKit
import RangeSeekSlider


class CustomSliderRangeHeader: UITableViewHeaderFooterView {

    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var sliderRange: RangeSeekSlider!
    @IBOutlet weak var minimumPrice: UILabel!
    @IBOutlet weak var maximumPrice: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
