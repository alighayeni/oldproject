//
//  PoolNameHeaderSection.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/15/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit

class PoolNameHeaderSection: UIView, UITextFieldDelegate {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBOutlet weak var poolName: UITextField!

    class func instanceFromNib() -> PoolNameHeaderSection {
        return UINib(nibName: "PoolNameHeaderSection", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PoolNameHeaderSection
    }
    
}
