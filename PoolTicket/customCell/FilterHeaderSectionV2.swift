//
//  FilterHeaderSectionV2.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/15/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit

class FilterHeaderSectionV2: UIView {
    
    
    @IBOutlet weak var sectionName: UIButton!
    @IBOutlet weak var selectItemImage: UIImageView!

    
    class func instanceFromNib() -> FilterHeaderSectionV2 {
        return UINib(nibName: "FilterHeaderSectionV2", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! FilterHeaderSectionV2
    }
    
}
