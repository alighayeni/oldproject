//
//  GenderHeaderSection.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/15/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import UIKit

class FilterHeaderSection: UITableViewHeaderFooterView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    @IBOutlet weak var rowIcon: UIImageView!
    @IBOutlet weak var itemBtn: UIButton!
    @IBOutlet weak var itemIcon: UIImageView!

    class func instanceFromNib() -> FilterHeaderSection {
        return UINib(nibName: "FilterHeaderSection", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! FilterHeaderSection
    }
    
}
