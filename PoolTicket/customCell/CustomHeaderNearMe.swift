//
//  CustomHeaderNearMe.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/25/19.
//  Copyright © 2019 OneSignal. All rights reserved.
//

import UIKit

class CustomHeaderNearMe: UITableViewHeaderFooterView {

    @IBOutlet weak var nearMeSwitch: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
