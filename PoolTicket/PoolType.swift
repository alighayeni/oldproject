//
//  PoolType.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/15/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit

class PoolType: NSObject {
    
    var type_id:String?
    var name:String?
    
    init(_ type_id:String,_ name:String) {
        self.type_id = type_id
        self.name = name
        
    }
    
    static func prepare(_ item:AnyObject) -> PoolType{
        
        let type_id = String(describing: item["type_id"] as AnyObject)
        let name = String(describing: item["name"] as AnyObject)
        
        let instance = PoolType(type_id, name)
        return instance
    }
    
    static func prepareDefault() -> PoolType{
        
        let type_id = ""
        let name = "همه"
        
        let instance = PoolType(type_id, name)
        return instance
        
    }
    
    
}
