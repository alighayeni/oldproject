//
//  areasءخیثم.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/15/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation


class areasModel: NSObject {
    
    
    
    var area_id:String?
    var area_name:String?
    
    
    
    init(_ area_id:String,_ area_name:String) {
        self.area_id = area_id
        self.area_name = area_name
        
    }
    
    static func prepare(_ item:AnyObject) -> areasModel{
        
        let area_id = String(describing: item["area_id"] as AnyObject)
        let area_name = String(describing: item["area_name"] as AnyObject)
        
        let instance = areasModel(area_id, area_name)
        return instance
    }
    
    
    
}
