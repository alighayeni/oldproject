//
//  ButtonWithPadding.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 5/30/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit

class ButtonWithPadding: UIButton {
    
    let padding = UIEdgeInsets(top: 0, left: 40, bottom: 0, right: 8);
    
    override func titleRect(forContentRect contentRect: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
   
}
