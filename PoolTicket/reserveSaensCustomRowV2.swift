//
//  reserveSaensCustomRowV2.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/6/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
class reserveSaensCustomRowV2: UITableViewCell {
    
    
    
    @IBOutlet weak var dateAndDay: UILabel!
    @IBOutlet weak var times: UILabel!
    
    @IBOutlet weak var descriptionTitle: UILabel!
    @IBOutlet weak var descriptionContent: UILabel!
    
    @IBOutlet weak var titlePrice1: UILabel!
    @IBOutlet weak var price1WithDiscount: UILabel!
    @IBOutlet weak var price1: UILabel!
    @IBOutlet weak var discountRate1: UILabel!

 
    
    @IBOutlet weak var mainView1: UIView!
    
    @IBOutlet weak var reserveSaens: UIButton!
    
    override func awakeFromNib() {
        mainView1.layer.borderWidth = 1.0
        mainView1.layer.borderColor = UIColor.lightGray.cgColor
        mainView1.clipsToBounds = true
        
       
    }
    
}
