//
//  File.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/11/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
class saensCartModel: NSObject {
    
    var saens_row_id:String?
    var total_sale:String?
    var total_market:String?
    var coupon:String?
    var coupon_message:String?
    var coupon_credit:String?
    var discount_chosen:String?
    var count_adult:String?
    var count_child:String?
    var start_date:String?
    var start_day:String?
    var time:String?

    init(_ saens_row_id:String,_ total_sale:String, _ total_market:String, _ coupon:String, _ coupon_message:String, _ coupon_credit:String, _ discount_chosen:String, _ count_adult:String, _ count_child:String, _ start_date:String, _ start_day:String, _ time:String) {
       
        self.saens_row_id = saens_row_id
        self.total_sale = total_sale
        self.total_market = total_market
        self.coupon = coupon
        self.coupon_message = coupon_message
        self.coupon_credit = coupon_credit
        self.discount_chosen = discount_chosen
        self.count_adult = count_adult
        self.count_child = count_child
        self.start_date = start_date
        self.start_day = start_day
        self.time = time
        
    }
    
    static func prepare(_ item:AnyObject) -> saensCartModel{
       
        let saens_row_id = String(describing: item["saens_row_id"] as AnyObject)
        let total_sale = String(describing: item["total_sale"] as AnyObject)
        let total_market = String(describing: item["total_market"] as AnyObject)
        let coupon = String(describing: item["coupon"] as AnyObject)
        let coupon_message = String(describing: item["coupon_message"] as AnyObject)
        let coupon_credit = String(describing: item["coupon_credit"] as AnyObject)
        let discount_chosen = String(describing: item["discount_chosen"] as AnyObject)
        let count_adult = String(describing: item["count_adult"] as AnyObject)
        let count_child = String(describing: item["count_child"] as AnyObject)
        let start_date = String(describing: item["start_date"] as AnyObject)
        let start_day = String(describing: item["start_day"] as AnyObject)
        let time = String(describing: item["time"] as AnyObject)

        let instance = saensCartModel(saens_row_id, total_sale, total_market, coupon, coupon_message, coupon_credit, discount_chosen, count_adult, count_child, start_date, start_day, time)
        return instance
    }
}
