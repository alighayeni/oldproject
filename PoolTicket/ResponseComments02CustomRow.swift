//
//  ResponseComments02CustomRow.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/18/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//


import Foundation
import UIKit

class ResponseComments02CustomRow: UITableViewCell, UITextViewDelegate {
    
    
    @IBOutlet weak var content: UITextView!
    var delegate: UITableViewCellUpdateDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        content.delegate = self
        content.target(forAction: #selector(didCHangeTextFieldValue), withSender: self)
        //content.target(self, action: Selector(didCHangeTextFieldValue), forControlEvents: UIControlEvents.editingChanged)
    }
    
    @objc func didCHangeTextFieldValue(sender: AnyObject?) {
        self.delegate?.cellDidChangeValue(cell: self)
    }
    
    func textViewDidChange(textView: UITextView) { //Handle the text changes here
        print(textView.text); //the textView parameter is the textView where text was changed
        self.delegate?.cellDidChangeValue(cell: self)
    }
    
    public func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        
        self.delegate?.cellDidChangeValue(cell: self)
        return true
    }
    
    
}
