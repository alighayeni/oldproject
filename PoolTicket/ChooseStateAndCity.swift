//
//  ChooseStateAndCity.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/21/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import Firebase

class ChooseStateAndCity: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
   
    @IBAction func nearLocation(_ sender: Any)->Void{
        DataSourceManagement.setOpenNearLocationPool(true)
        shareData.shared.topHeaderLabelPoolList = "مجموعه های نزدیک من"
        openNewClass(classIdentifier: "PoolsListVC")
    }
    
    @IBOutlet weak var topView:UIView!
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    
    @IBAction func searchTheCityAndState(_ sender: Any)->Void{
        
        guard let search = searchTextField.text else {
            self.showDropMessage("فیلد جستجو خالی می باشد.")
            return
        }
        
        if search.count > 0 {
            if DataSourceManagement.fromInbox {
                if let value = self.searchList(search) as? [cityModel]{
                    list = value
                    tableview.reloadData()
                    searchTextField.resignFirstResponder()
                }else{
                    self.showDropMessage("شهر مورد نظر شما، یافت نشد.")
                }
            }else{
                if let value = self.searchListV2(search) as? [cityModel]{
                    list = value
                    tableview.reloadData()
                    searchTextField.resignFirstResponder()
                }else{
                    self.showDropMessage("شهر مورد نظر شما، یافت نشد.")
                }
            }
        }else{
            
            getData()
        }
        
        
    }
    
    var dataController: DataController!
    var list : [cityModel] = []
  
    override func viewDidLoad() {
        super.viewDidLoad()
        AppDelegate()._manager?.track("change_city", data: [:])
        Analytics.logEvent("change_city", parameters: nil)
        
        dataController = DataController.init()
        tableview.delegate = self
        tableview.dataSource = self
        tableview.allowsSelection = true
        searchTextField.delegate = self
        searchTextField.addTarget(self, action: #selector(setOnTextChangeEvent) , for: .editingChanged)
        self.doneWithKeyboard(searchTextField)
    }
    
    @objc func setOnTextChangeEvent(textfieldItem : UITextField){

        if var search = textfieldItem.text as? String {
            
            search = search.trimmingCharacters(in: .whitespaces)
            if search.count > 0 , search != ""{
                if DataSourceManagement.fromInbox {
                    if let value = self.searchList(search) as? [cityModel]{
                        list = value
                        tableview.reloadData()
                    }else{
                        self.showDropMessage("شهر مورد نظر شما، یافت نشد.")
                    }
                }else{
                    if let value = self.searchListV2(search) as? [cityModel]{
                        list = value
                        tableview.reloadData()
                    }else{
                        self.showDropMessage("شهر مورد نظر شما، یافت نشد.")
                    }
                }
            }else{
                
                getData()
            }
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // load data from database
        getData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        DataSourceManagement.fromInbox = false
    }
    
    func getData(){
        
        if DataSourceManagement.fromInbox {
            if let value = self.getAllCityItem() as? [cityModel]{
                list = value
                tableview.reloadData()
            }
        }else{
            if let value = self.getAllCityItemV2() as? [cityModel]{
                list = value
                tableview.reloadData()
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let data = self.list[indexPath.row]

        if let value = data.city_id {
            if value != "-1", value != "-2" {
                let cellIdentifier =  "stateAndCityCellV2"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! stateAndCityCell
                cell.stateName.text = data.city_name! + " (" + data.state_name! + ")"
                cell.chooseItem.tag = indexPath.row
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                return cell
            }else if value == "-1"{
                let cellIdentifier =  "stateAndCityCellV3"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! stateAndCityCell
                cell.stateName.text = data.city_name
                cell.chooseItem.tag = indexPath.row
                cell.cellTitle.text = data.state_name
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                return cell
            }else if value == "-2"{
                let cellIdentifier =  "stateAndCityCellV2"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! stateAndCityCell
                cell.stateName.text = data.city_name! //+ " (" + data.state_name! + ")"
                cell.chooseItem.tag = indexPath.row
                cell.cellTitle.text = data.state_name
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                return cell
            }
        }
        
        
        return UITableViewCell()
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString rString: String) -> Bool {
        
      
        
        return true
    }
    
    @IBAction func closeclass(){
        closeController()
    }
    
    func closeController (){
        self.dismiss(animated: true, completion: nil)
    }
    
    func getAllCityItem()->[cityModel]?{
        let moc = dataController.managedObjectContext
        var tData : [cityModel] = []
        let fetch = NSFetchRequest<CitiesV2>(entityName: "CitiesV2")
        let sort = NSSortDescriptor(key: #keyPath(CitiesV2.order_id), ascending: true)
        fetch.sortDescriptors = [sort]
        do {
            let items =  try moc.fetch(fetch) as [CitiesV2]
          
            //near me
//            let value = cityModel.init("-1", "نزدیک من", "-1", "براساس موقعیت")
//            tData.append(value as cityModel)
           
            //show all result
//            let value01 = cityModel.init("-2", "نمایش همه" , "" , "تمام شهر ها")
//            tData.append(value01 as cityModel)
            
            for item in items {
                let value = cityModel.init(item.city_id!, item.city_name! ,item.state_id!, item.state_name!)
                tData.append(value as cityModel)
            }
            return tData;
        }catch{
            fatalError("Failure to retrieve context: \(error)")
        }
        return nil
    }
    
    func getAllCityItemV2()->[cityModel]?{
        let moc = dataController.managedObjectContext
        var tData : [cityModel] = []
        let fetch = NSFetchRequest<AllCities>(entityName: "AllCities")
        
        do {
            let items =  try moc.fetch(fetch) as [AllCities]
            
            
            for item in items {
                let value = cityModel.init(item.city_id as! String, item.city_name as! String ,item.state_id as! String, item.state_name as! String)
                tData.append(value as cityModel)
            }
            return tData;
        }catch{
            fatalError("Failure to retrieve context: \(error)")
        }
        return nil
    }
    
    func searchList(_ cityORState: String)->[cityModel]?{
        let moc01 = dataController.managedObjectContext
        var tData : [cityModel] = []
        let fetch = NSFetchRequest<CitiesV2>(entityName: "CitiesV2")
        fetch.predicate = NSPredicate(format: "city_name CONTAINS[cd] %@", cityORState)
        do {
            let items =  try moc01.fetch(fetch) as [CitiesV2]
            for item in items {
                let value = cityModel.init(item.city_id as! String, item.city_name as! String ,item.state_id as! String, item.state_name as! String)
                tData.append(value as cityModel)
            }
            return tData;
        }catch{
            fatalError("Failure to retrieve context: \(error)")
        }
        return nil
    }
    
    func searchListV2(_ cityORState: String)->[cityModel]?{
        let moc01 = dataController.managedObjectContext
        var tData : [cityModel] = []
        let fetch = NSFetchRequest<AllCities>(entityName: "AllCities")
        fetch.predicate = NSPredicate(format: "city_name CONTAINS[cd] %@", cityORState)
        do {
            let items =  try moc01.fetch(fetch) as [AllCities]
            for item in items {
                let value = cityModel.init(item.city_id!, item.city_name! ,item.state_id!, item.state_name!)
                tData.append(value as cityModel)
            }
            return tData;
        }catch{
            fatalError("Failure to retrieve context: \(error)")
        }
        return nil
    }
    
    @IBAction func chooseCity(button: UIButton){
        
        AppDelegate.changeFilterValue = true

        if let tId = self.list[button.tag].city_id {
        if tId != "-1", tId != "-2" {
        DataSourceManagement.allPools(false)
        DataSourceManagement.setOpenNearLocationPool(false)
        if DataSourceManagement.fromInbox {
            shareData.shared.topHeaderLabelPoolList =  self.list[button.tag].city_name! + " (" + self.list[button.tag].state_name! + ")"
            DataSourceManagement.setSelectedStateAndCity(self.list[button.tag])
        }else{
            shareData.shared.selectedBirthModel = self.list[button.tag]
        }
        addCityToFilterIfNeeded()
        closeController()
            
        }else if tId == "-1" {
            DataSourceManagement.allPools(false)
            DataSourceManagement.setOpenNearLocationPool(true)
            shareData.shared.topHeaderLabelPoolList = "مجموعه های نزدیک من"
            openNewClass(classIdentifier: "PoolsListVC")
            DataSourceManagement.setFromInboxStatus(true)
        }else if tId == "-2" {
            DataSourceManagement.setOpenNearLocationPool(false)
            DataSourceManagement.allPools(true)
            if DataSourceManagement.fromInbox {
                shareData.shared.topHeaderLabelPoolList =  self.list[button.tag].city_name! + " (" + self.list[button.tag].state_name! + ")"
                DataSourceManagement.setSelectedStateAndCity(self.list[button.tag])
            }else{
                shareData.shared.selectedBirthModel = self.list[button.tag]
            }
            addCityToFilterIfNeeded()
            closeController()
            
            }
        
        }
    }
    
    func addCityToFilterIfNeeded() {
        if AppDelegate.changeFilterValue {
            var selectedCity: cityModel?
            selectedCity = DataSourceManagement.getSelectedStateAndCity()
            if let value = selectedCity {
                // find index of old selected city in the filter
                var index = -1
                if DataSourceManagement.filterList.count > 0 {
                    for i in 0..<DataSourceManagement.filterList.count {
                        if let value = DataSourceManagement.filterList[i] as? filterObject {
                            if value.key == "selectedCity" || value.key == "attachedCityId" {
                                index = i
                            }
                        }
                    }
                    // remove the old city data
                    if index != -1 {
                        DataSourceManagement.filterList.remove(at: index)
                    }
                }
                // create new object
                let tData = (value.city_name)! //+ " (" + (value.state_name)! + ")"
                let object = filterObject.init(name: tData, key: "selectedCity", value1: tData, value2: nil)
                // add new object to the filter
                DataSourceManagement.filterList.append(object)
            }
        }
    }
    
    //************** softkeyboard tool bar **********************//
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField
        {
            //        case personalID:
            //            personalID.resignFirstResponder()
            //            password.becomeFirstResponder()
            //            break
            //        case password:
            //            password.resignFirstResponder()
            //            break
        //
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
    func doneWithKeyboard(_ sender: UITextField){
        
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        keyboardToolbar.isTranslucent = true
        keyboardToolbar.barTintColor = UIColor.white
        
        let button = UIButton(type: .system)
        button.setTitle("بستن", for: .normal)
        button.sizeToFit()
        button.titleLabel?.font = UIFont(name: "IRANSansMobile", size: 12)
        button.tintColor = UIColor.black
        button.addTarget(self, action: #selector(closeKeyboard(_:)), for: .touchUpInside)
        

        
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        keyboardToolbar.items = [flexBarButton,UIBarButtonItem(customView: button)]
        //keyboardToolbar.items = [itemDown,itemUp,flexBarButton,UIBarButtonItem(customView: button)]
        
        sender.inputAccessoryView = keyboardToolbar
        
    }

    
    //************** softkeyboard tool bar **********************//
}

