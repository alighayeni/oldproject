//
//  PoolFiltersNewVersion.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/24/19.
//  Copyright © 2019 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import RangeSeekSlider

class PoolFiltersNew: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var listOfSection: [String] = []
    //var numberOfSectionsinTableView = 0
    var list : [areasModel] = []
    var list01 : [String] = []
    var jensiatList : JensiatExpandable = JensiatExpandable.init(isExpanded: false, jensiatObjects: [])
    var poolTypeList : PoolTypeExpandable = PoolTypeExpandable.init(isExpanded: false, poolTypeObject: [])
    var serviceTypeList : ServiceTypeExpandable = ServiceTypeExpandable .init(isExpanded: false, serviceTypeObject: [])
    var isSale = "true"
    var isSaleTicket = "false"
    var isSaleCourse = "false"
    var isSalePack = "false"
    var selectedCityId = ""
    static var filterConfigType = "pack"
    
    // result
    static var poolName = ""
    var selectedCity: cityModel?
    var nearMe = false
    var hasDiscount = false
    static var selectedGenderType = "2"
    static var selectedGenderTypePosition = 0
    var selectedMinValue: CGFloat?
    var selectedMaxValue: CGFloat?
    static var selectedPoolTypeId = ""
    static var selectedPoolTypeName = ""

    
//    static var filterList: [filterObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "CustomHeaderNearMe", bundle: nil), forHeaderFooterViewReuseIdentifier: "CustomHeaderNearMe")
        tableView.register(UINib(nibName: "FilterHeaderSection", bundle: nil), forHeaderFooterViewReuseIdentifier: "FilterHeaderSection")
        tableView.register(UINib.init(nibName: "CustomCellButtonInside", bundle: nil), forCellReuseIdentifier: "CustomCellButtonInside")
        tableView.register(UINib(nibName: "CustomHeaderDiscount", bundle: nil), forHeaderFooterViewReuseIdentifier: "CustomHeaderDiscount")
         tableView.register(UINib(nibName: "CustomSliderRangeHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "CustomSliderRangeHeader")

        tableView.delegate = self
        tableView.dataSource = self
    
        getPoolTypes()
        prepareTableViewConfig(PoolFiltersNew.filterConfigType)
        setUp()
        self.tableView.reloadData()
        self.title = "فیلتر"
      

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       // self.setSelectedCity()
    }
    
    func prepareTableViewConfig(_ type: String) {
        switch type {
        case "pack":
            self.listOfSection = []
            listOfSection.append("poolName")
            //listOfSection.append("cityFilter")
            listOfSection.append("nearMe")
            listOfSection.append("gender")
            listOfSection.append("hasDiscount")
            listOfSection.append("priceSlider")
        case "course":
            self.listOfSection = []
            listOfSection.append("poolName")
            //listOfSection.append("cityFilter")
            listOfSection.append("nearMe")
            listOfSection.append("gender")
            listOfSection.append("hasDiscount")
            listOfSection.append("priceSlider")
        case "seans":
            self.listOfSection = []
            listOfSection.append("poolName")
            //listOfSection.append("cityFilter")
            listOfSection.append("nearMe")
            listOfSection.append("gender")
            listOfSection.append("hasDiscount")
            listOfSection.append("priceSlider")
        case "allpools":
            self.listOfSection = []
            listOfSection.append("poolName")
            //listOfSection.append("cityFilter")
            listOfSection.append("nearMe")
            listOfSection.append("gender")
            listOfSection.append("hasDiscount")
            listOfSection.append("poolType")
            //listOfSection.append("priceSlider")
        default:
            break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField
        {
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
    func doneWithKeyboard(_ sender: UITextField){
        
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        keyboardToolbar.isTranslucent = true
        keyboardToolbar.barTintColor = UIColor.white
        
        let button = UIButton(type: .system)
        button.setTitle("بستن", for: .normal)
        button.sizeToFit()
        button.titleLabel?.font = UIFont(name: "IRANSansMobile", size: 12)
        button.tintColor = UIColor.black
        button.addTarget(self, action: #selector(closeKeyboardFilterSection(_:)), for: .touchUpInside)
        
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        keyboardToolbar.items = [flexBarButton,UIBarButtonItem(customView: button)]
        
        if let value = sender as? UITextField {
            value.inputAccessoryView = keyboardToolbar
        }else if let value = sender as? UITextView {
            value.inputAccessoryView = keyboardToolbar
        }
        
    }
    
    @objc func closeKeyboardFilterSection(_ sender:UITextField){
        
//        let ndx = IndexPath(row:0, section: 0)
//        if let cell = tableview.cellForRow(at:ndx) as? FilterCustomCellTextInput {
//            if let value = cell.poolName.text {
//                DataSourceManagement.addSearchPoolName(value)
//                PoolFilters.poolName = value
//            }
//        }
        self.dismissKeyboard()
    }
    
    func setSelectedCity() {
        let indexp = IndexPath.init(row: 0, section: 1)
        if let cell = self.tableView.dequeueReusableCell(withIdentifier: "CustomCellButtonInside", for: indexp) as? CustomCellButtonInside {
            selectedCity = DataSourceManagement.getSelectedStateAndCity()
            let tData = (selectedCity?.city_name)! + " (" + (selectedCity?.state_name)! + ")"
            cell.selectBtn.setTitle(tData, for: .normal)
            self.tableView.reloadRows(at: [indexp], with: UITableView.RowAnimation.fade)
        }
    }
    
    @objc
    func selectedCityAndStateAction() {
        DataSourceManagement.fromInbox = true
        openNewClass(classIdentifier: "ChooseStateAndCity")
    }
    
    @objc func nearMeValueChange() {
        self.nearMe = !nearMe
    }
    
    @objc func hasDiscountChange() {
        self.hasDiscount = !hasDiscount
    }

    @IBAction func setFilters()->Void {
   
        // at firs we need to clear old files, before that we should make sure about one thing (selected city and state)
        var reserveTheSelectedCity: filterObject?
        if DataSourceManagement.filterList.count > 0 {
            for i in 0..<DataSourceManagement.filterList.count {
                if let value = DataSourceManagement.filterList[i] as? filterObject {
                    if value.key == "selectedCity" {
                        reserveTheSelectedCity = value
                        break
                    }
                }
            }
            DataSourceManagement.filterList = []
            if let value = reserveTheSelectedCity {
                DataSourceManagement.filterList.append(value)
            }
        }
    
        // add all new filters property to the filter array
        var poolnamelenght = ""
        if let value = PoolFiltersNew.poolName as? String, value.count > 0 {
            let object = filterObject.init(name: "نام استخر: \(value)", key: "poolName", value1: value, value2: nil)
            poolnamelenght = "نام استخر: \(value)"
            DataSourceManagement.filterList.append(object)
        }
        
        if self.nearMe {
            let object = filterObject.init(name: "استخر های نزدیک من", key: "nearMe", value1: "true", value2: nil)
            DataSourceManagement.filterList.append(object)
            PoolsListVC.nearme = true
        } else {
            PoolsListVC.nearme = false
        }
        
        let object = filterObject.init(name: " جنسیت: \(jensiatList.jensiatObjects[PoolFiltersNew.selectedGenderTypePosition].name)", key: "gender", value1: PoolFiltersNew.selectedGenderType, value2: nil)
        if PoolFiltersNew.selectedGenderTypePosition != 0 {
            DataSourceManagement.filterList.append(object)
        }

        if self.hasDiscount {
            let object = filterObject.init(name: "دارای تخفیف", key: "has_discount", value1: "true", value2: nil)
            DataSourceManagement.filterList.append(object)
        }
        
        if let minValue = self.selectedMinValue, let maxValue = self.selectedMaxValue {
            let object = filterObject.init(name: "فیلتر قیمت", key: "priceSlider", value1: "\(Int(minValue))", value2: "\(Int(maxValue))")
            DataSourceManagement.filterList.append(object)
        }
        
        if PoolFiltersNew.selectedPoolTypeId != "" {
            let object = filterObject.init(name: "نوع مجموعه:" + "\(PoolFiltersNew.selectedPoolTypeName)", key: "poolType", value1: PoolFiltersNew.selectedPoolTypeId, value2: nil)
            DataSourceManagement.filterList.append(object)
        }

        AppDelegate.changeFilterValue = true
        
        _ = navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func removeFilters()->Void {
        
        DataSourceManagement.filterList = []
        AppDelegate.changeFilterValue = true
        PoolFiltersNew.poolName = ""
        nearMe = false
        hasDiscount = false
        PoolFiltersNew.selectedGenderType = "2"
        PoolFiltersNew.selectedGenderTypePosition = 0
        
        self.tableView.reloadData()
        shareData.shared.filterPoolListEnable = false
        shareData.shared.filterPoolListQuery = nil
        DataSourceManagement.searchPoolName = ""
        
        PoolFiltersNew.selectedPoolTypeId = ""
        PoolFiltersNew.selectedPoolTypeName = ""
        _ = navigationController?.popViewController(animated: true)
    }
    
    /*  get list of states */
    func getPoolTypes() -> Void {
        let client_id = Constants.clientId
        // let just_active_states = true
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.GET_POOL_TYPE,"client_id=\(client_id)&access_token=\(access_token)")
        index.start{ (indexData) in
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    if let item = json as? [String: AnyObject]{
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                if let items = item["types"] as? [[String: AnyObject]] {
                                    var tPoolType : [PoolType] = []
                                    tPoolType.append(PoolType.prepareDefault())
                                    for itemValue in items {
                                        if let value = PoolType.prepare(itemValue as AnyObject) as? PoolType{
                                            tPoolType.append(value)
                                        }
                                    }
                                    self.poolTypeList = PoolTypeExpandable.init(isExpanded: false, poolTypeObject: tPoolType)
                                }
                            }
                        }
                    }
                }
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
              
            })
        }
    }
}

extension PoolFiltersNew: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.listOfSection.count
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        switch self.listOfSection[section] {
        case "poolName":
            let infoView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "FilterHeaderSection") as! FilterHeaderSection
            infoView.itemBtn.setTitle("   نام مجموعه", for: .normal)
            infoView.itemBtn.backgroundColor = .white
            infoView.itemBtn.tag = 0
            infoView.itemIcon.image = nil
            infoView.rowIcon.image = nil
            return infoView
            
        case "nearMe":
            let infoView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CustomHeaderNearMe") as! CustomHeaderNearMe
            if nearMe {
                infoView.nearMeSwitch.isOn = true
            } else {
                infoView.nearMeSwitch.isOn = false
            }
            infoView.tag = section
            infoView.nearMeSwitch.addTarget( self, action: #selector(self.nearMeValueChange), for: .valueChanged)
            let tapOnHeaderGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapOnNearMeHeaderGestureRecognizer))
            infoView.addGestureRecognizer(tapOnHeaderGestureRecognizer)
            return infoView
            
        case "hasDiscount":
            let infoView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CustomHeaderDiscount") as! CustomHeaderDiscount
            if hasDiscount {
                 infoView.hasDiscount.isOn = true
            } else {
                 infoView.hasDiscount.isOn = false
            }
            infoView.hasDiscount.addTarget( self, action: #selector(self.hasDiscountChange), for: .valueChanged)
            infoView.tag = section
            let tapOnHeaderGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapOnDiscountHeaderGestureRecognizer))
            infoView.addGestureRecognizer(tapOnHeaderGestureRecognizer)
            return infoView
      
        case "priceSlider":
            let infoView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CustomSliderRangeHeader") as! CustomSliderRangeHeader
            infoView.sliderRange.delegate = self
//            infoView.priceLabel.text
            
            return infoView
            
        case "gender":
            
            let infoView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "FilterHeaderSection") as! FilterHeaderSection
            infoView.itemBtn.setTitle("      جنسیت   ", for: .normal)
            infoView.itemBtn.tag = 122
            infoView.rowIcon.image = UIImage.init(named: "ic_gender_gray")
            infoView.itemBtn.addTarget(self, action: #selector(handleExpandClose), for: .touchUpInside)
            if !self.jensiatList.isExpanded {
                infoView.itemIcon.image = UIImage(named: "ic_arrow_left_gray")
            }else{
                infoView.itemIcon.image = UIImage(named:"ic_arrow_down_blue")
            }
            infoView.clipsToBounds = true
            return infoView
            
        case "cityFilter":
            
            let infoView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "FilterHeaderSection") as! FilterHeaderSection
            infoView.itemBtn.setTitle("    انتخاب شهر", for: .normal)
            infoView.itemBtn.addTarget(self, action: #selector(handleExpandClose), for: .touchUpInside)            
            infoView.clipsToBounds = true
            infoView.rowIcon.image = nil
            return infoView
            
        case "poolType":
            let infoView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "FilterHeaderSection") as! FilterHeaderSection
            infoView.itemBtn.setTitle("      نوع مجموعه   ", for: .normal)
            infoView.itemBtn.tag = 133
            infoView.itemBtn.addTarget(self, action: #selector(handleExpandClose), for: .touchUpInside)
            if let isExpanded = self.poolTypeList.isExpanded as? Bool{
                if !isExpanded {
                    infoView.itemIcon.image = UIImage(named: "ic_arrow_left_gray")
                }else{
                    infoView.itemIcon.image = UIImage(named:"ic_arrow_down_blue")
                }
            }
            infoView.clipsToBounds = true
            return infoView
            
        default:
            print("")
        }
        
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let type = self.listOfSection[indexPath.section]
        
        switch type {
        case "poolName":
            let identifier = "FilterCustomCellTextInput"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! FilterCustomCellTextInput
            cell.poolName.delegate = self
            cell.poolName.text = PoolFiltersNew.poolName
            doneWithKeyboard(cell.poolName)
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        case "gender":
            let identifier = "FilterCustomCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! FilterCustomCell
            cell.name?.text = self.jensiatList.jensiatObjects[indexPath.row].name
            if !self.jensiatList.jensiatObjects[indexPath.row].status {
                cell.selectedItemImage.image = UIImage(named: "ic_radio_button_dark")
            }else{
                cell.selectedItemImage.image = UIImage(named: "ic_radio_button_green")
            }
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        case "cityFilter":
            let identifier = "CustomCellButtonInside"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! CustomCellButtonInside
            cell.selectBtn.addTarget(self, action: #selector(self.selectedCityAndStateAction), for: .touchUpInside)
            selectedCity = DataSourceManagement.getSelectedStateAndCity()
            let tData = (selectedCity?.city_name)! + " (" + (selectedCity?.state_name)! + ")"
            cell.selectBtn.setTitle(tData, for: .normal)
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        case "poolType":
            let identifier = "FilterCustomCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! FilterCustomCell
            cell.name?.text = self.poolTypeList.poolTypeObject[indexPath.row].name
            if (self.poolTypeList.poolTypeObject[indexPath.row].type_id)! != PoolFiltersNew.selectedPoolTypeId {
                cell.selectedItemImage.image = UIImage(named: "ic_radio_button_dark")
            }else{
                cell.selectedItemImage.image = UIImage(named: "ic_radio_button_green")
            }
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        default:
            break
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch self.listOfSection[section] {
        case "poolName":
            return 1
        case "cityFilter":
            return 1
        case "gender":
            if !self.jensiatList.isExpanded {
                return 0
            }
            return self.jensiatList.jensiatObjects.count
        case "nearMe":
            return 0
        case "hasDiscount":
           return 0
        case "priceSlider":
            return 0
        case "poolType":
            if !self.poolTypeList.isExpanded {
                return 0
            }
            return self.poolTypeList.poolTypeObject.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let type = self.listOfSection[section]
        switch type {
        case "gender":
            return 56
        case "priceSlider":
            return 135
        default:
           break
        }
        return 48
    }
    
    @objc func handleExpandClose(button: UIButton){
        
        let section = button.tag
        
        switch section {
        case 122:
            if let sectionIndex = listOfSection.firstIndex(of: "gender") {
                var indexPaths = [IndexPath]()
                for row in self.jensiatList.jensiatObjects.indices {
                    let indexPath = IndexPath(row: row, section : sectionIndex)
                    indexPaths.append(indexPath)
                }
                let isExpanded = self.jensiatList.isExpanded
                self.jensiatList.isExpanded = !isExpanded
                if !isExpanded {
                    tableView.insertRows(at: indexPaths, with: .fade)
                }else{
                    tableView.deleteRows(at: indexPaths, with: .fade)
                }
                
                if let header = self.tableView.headerView(forSection: sectionIndex) {
                    if let sectionHeader = header as? FilterHeaderSection {
                        if !isExpanded {
                            sectionHeader.itemIcon?.image = UIImage(named:"ic_arrow_down_blue")
                        }else{
                            sectionHeader.itemIcon?.image = UIImage(named:"ic_arrow_left_gray")
                            
                        }
                    }
                }
                
            }
            printLog(m: "")
        case 133:
             if let sectionIndex = listOfSection.firstIndex(of: "poolType") {
            var indexPaths = [IndexPath]()
            for row in self.poolTypeList.poolTypeObject.indices {
                let indexPath = IndexPath(row: row, section : sectionIndex)
                indexPaths.append(indexPath)
            }
            if let isExpanded = self.poolTypeList.isExpanded as? Bool{
                self.poolTypeList.isExpanded = !isExpanded
                if !isExpanded {
                    tableView.insertRows(at: indexPaths, with: .fade)
                }else{
                    tableView.deleteRows(at: indexPaths, with: .fade)
                }

                if let header = self.tableView.headerView(forSection: sectionIndex) {
                    if let sectionHeader = header as? FilterHeaderSection {
                        if !isExpanded {
                            sectionHeader.itemIcon?.image = UIImage(named:"ic_arrow_down_blue")
                            //                    infoView.theTitle.textColor = self.setBgColor( 34, 169, 228)
                            //                    infoView.arrow.image = UIImage(named:"ic_arrow_down_blue")
                        }else{
                            sectionHeader.itemIcon?.image = UIImage(named:"ic_arrow_left_gray")

                        }
                    }
                }
            }
             }
//
//            printLog(m: "")
//        case 4:
//            var indexPaths = [IndexPath]()
//            for row in self.serviceTypeList.serviceTypeObject.indices {
//                let indexPath = IndexPath(row: row, section : section)
//                indexPaths.append(indexPath)
//            }
//            if let isExpanded = self.serviceTypeList.isExpanded as? Bool{
//                self.serviceTypeList.isExpanded = !isExpanded
//                if !isExpanded {
//                    tableview.insertRows(at: indexPaths, with: .fade)
//                }else{
//                    tableview.deleteRows(at: indexPaths, with: .fade)
//                }
//                if let header = self.tableview.headerView(forSection: section) {
//                    if let sectionHeader = header as? FilterHeaderSection {
//                        if !isExpanded {
//                            sectionHeader.itemIcon?.image = UIImage(named:"ic_arrow_down_blue")
//                            //                    infoView.theTitle.textColor = self.setBgColor( 34, 169, 228)
//                            //                    infoView.arrow.image = UIImage(named:"ic_arrow_down_blue")
//                        }else{
//                            sectionHeader.itemIcon?.image = UIImage(named:"ic_arrow_left_gray")
//
//                        }
//                    }
//                }
//            }
            printLog(m: "")
        default:
            print("")
        }
    }
    
    func setUp() {
        
        var t1 = JensiatExpandable.init(isExpanded: false, jensiatObjects: [Jensiat.init(status: false, id: "2", name: "همه"), Jensiat.init(status: false, id: "0", name: "آقایان"), Jensiat.init(status: false, id: "1", name: "بانوان")])
        t1.jensiatObjects[PoolFilters.selectedGenderTypePosition].status = true
        jensiatList = t1
        
        let t2 = ServiceTypeExpandable.init(isExpanded: false, serviceTypeObject: [ServiceType.init("0", "همه"), ServiceType.init("1", "رزرو بلیت"), ServiceType.init("2", "کلاس شنا"), ServiceType.init("3", "پک")])
        serviceTypeSelected(PoolFilters.selectedServiceTypePosition)
        serviceTypeList = t2
        
        
        if DataSourceManagement.filterList.count > 0 {
            for i in 0..<DataSourceManagement.filterList.count {
                if let value = DataSourceManagement.filterList[i] as? filterObject {
                    switch value.key {
                    case "poolName":
                        PoolFiltersNew.poolName = value.value1 ?? ""
                        break
                    case "nearMe":
                        if value.value1 == "true" {
                            self.nearMe = true
                        }
                        break
                    case "gender":
                        PoolFiltersNew.selectedGenderType = value.value1 ?? "2"
                        t1.jensiatObjects[0].status = false
                        switch PoolFiltersNew.selectedGenderType {
                        case "0":
                            PoolFiltersNew.selectedGenderTypePosition = 1
                            t1.jensiatObjects[1].status = true
                            break
                        case "1":
                            PoolFiltersNew.selectedGenderTypePosition = 2
                            t1.jensiatObjects[2].status = true
                            break
                        case "2":
                            PoolFiltersNew.selectedGenderTypePosition = 0
                            t1.jensiatObjects[0].status = true
                            break
                        default:
                            break
                        }
                        jensiatList = t1
                        break
                    case "has_discount":
                        if value.value1 == "true" {
                            self.hasDiscount = true
                        }
                        break
                    case .none:
                        break
                    case .some(_):
                        break
                    }
                }
            }
            
        }
    }
    
    func serviceTypeSelected(_ row: Int){
        
        switch row{
        case 0:
            isSale = "true"
            isSaleTicket = "false"
            isSaleCourse = "false"
            isSalePack = "false"
            break
        case 1:
            isSaleTicket = "true"
            isSale = "false"
            isSaleCourse = "false"
            isSalePack = "false"
            break
        case 2:
            isSaleCourse = "true"
            isSale = "false"
            isSaleTicket = "false"
            isSalePack = "false"
            break
        case 3:
            isSalePack = "true"
            isSale = "false"
            isSaleTicket = "false"
            isSaleCourse = "false"
            break
        default:
            print("")
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch self.listOfSection[indexPath.section] {
        case "gender":
            switch indexPath.row{
            case 0:
                PoolFiltersNew.selectedGenderTypePosition = 0
                PoolFiltersNew.selectedGenderType = "2"
                self.jensiatList.jensiatObjects[0].status = true
                self.jensiatList.jensiatObjects[1].status = false
                self.jensiatList.jensiatObjects[2].status = false
                break
            case 1:
                PoolFiltersNew.selectedGenderTypePosition = 1
                PoolFiltersNew.selectedGenderType = "0"
                self.jensiatList.jensiatObjects[0].status = false
                self.jensiatList.jensiatObjects[1].status = true
                self.jensiatList.jensiatObjects[2].status = false
                break
            case 2:
                PoolFiltersNew.selectedGenderTypePosition = 2
                PoolFiltersNew.selectedGenderType = "1"
                self.jensiatList.jensiatObjects[0].status = false
                self.jensiatList.jensiatObjects[1].status = false
                self.jensiatList.jensiatObjects[2].status = true
                break
            default:
                print("")
            }
            tableView.reloadData()
            
        case "poolType":
            PoolFiltersNew.selectedPoolTypeId = self.poolTypeList.poolTypeObject[indexPath.row].type_id!
            PoolFiltersNew.selectedPoolTypeName = self.poolTypeList.poolTypeObject[indexPath.row].name!
            tableView.reloadData()
            
//        case 4:
//            PoolFilters.selectedServiceType = self.serviceTypeList.serviceTypeObject[indexPath.row].type_id!
//            serviceTypeSelected(indexPath.row)
//            PoolFilters.selectedServiceTypePosition = indexPath.row
//            tableView.reloadData()
//            break
        default:
            print("")
        }
        
    }
    
    @objc
    func tapOnNearMeHeaderGestureRecognizer(sender: UITapGestureRecognizer){
        guard let sectionIndex = sender.view!.tag as? Int else {
            return
        }
        var indexPath = IndexPath(row: 0, section : sectionIndex)
        nearMeValueChange()
        if let header = self.tableView.headerView(forSection: indexPath.section) {
            if let sectionHeader = header as? CustomHeaderNearMe {
                if !self.nearMe {
                    sectionHeader.nearMeSwitch.isOn = false
                } else {
                    sectionHeader.nearMeSwitch.isOn = true
                }
            }
        }
    }
    
    @objc
    func tapOnDiscountHeaderGestureRecognizer(sender: UITapGestureRecognizer){
        guard let sectionIndex = sender.view!.tag as? Int else {
            return
        }
        var indexPath = IndexPath(row: 0, section : sectionIndex)
        hasDiscountChange()
        if let header = self.tableView.headerView(forSection: indexPath.section) {
            if let sectionHeader = header as? CustomHeaderDiscount {
                if !self.hasDiscount {
                    sectionHeader.hasDiscount.isOn = false
                } else {
                    sectionHeader.hasDiscount.isOn = true
                }
            }
        }
    }
    
    
}

extension PoolFiltersNew: RangeSeekSliderDelegate {
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        self.selectedMinValue = slider.selectedMinValue
        self.selectedMaxValue = slider.selectedMaxValue
        let toman = " تومان "
        let to = " تا "
        let message = "\(Int(self.selectedMaxValue!)) \(toman) \(to) \(Int(self.selectedMinValue!)) \(toman)"
        //print("min: \(Int(selectedMinValue)) & max: \(Int(selevtedMaxValue)) ")
        if let header = self.tableView.headerView(forSection: 4) {
            if let sliderHEader = header as? CustomSliderRangeHeader {
                 sliderHEader.priceLabel.text = setSeprator(message)
            }
        }
    }
    
    func didStartTouches(in slider: RangeSeekSlider) {
        print("did start touches")
    }
    
    func didEndTouches(in slider: RangeSeekSlider) {
        print("did end touches")
    }

}

extension PoolFiltersNew: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let value = textField.text {
            PoolFiltersNew.poolName = value
        }
    }
   
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
            if let value = textField.text {
                PoolFiltersNew.poolName = value
            }
        return true 
    }
    
}

enum filterConfig {
    case poolName // نام استخر یا مجموعه
    case poolArea // منطقه
    case gender // جنسیت مجاز // 122
    case poolType // نوع مجموعه
    case serviceType // خدمات
    case priceSlider // رنچ قیمت
    case hasDiscount // دارای تخفیف
    case nearMe // نزدیک من
    case cityFilter //  انتخاب شهر
}

private func returnConfigForPack(_ config: filterConfig) -> Any {
    switch config {
    case .poolName:
        return true
    case .poolArea:
        return false
    case .gender:
        return true
    case .poolType:
        return false
    case .priceSlider:
        return true
    case .hasDiscount:
        return true
    case .nearMe:
        return true
    case .cityFilter:
        return true
    default:
        return false
    }
}

private func returnConfigForCourse(_ config: filterConfig) -> Any {
    switch config {
    case .poolName:
        return true
    case .poolArea:
        return false
    case .gender:
        return true
    case .poolType:
        return false
    case .priceSlider:
        return true
    case .hasDiscount:
        return true
    case .nearMe:
        return true
    case .cityFilter:
        return true
    default:
        return false
    }
}

struct filterObject {
    var name: String?
    var key: String?
    var value1: String?
    var value2: String?
}


extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.font): font]), context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.font): font]), context: nil)
        
        return ceil(boundingBox.width)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}
