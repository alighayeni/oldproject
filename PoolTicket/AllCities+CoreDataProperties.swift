//
//  AllCities+CoreDataProperties.swift
//  
//
//  Created by Ali Ghayeni on 5/16/18.
//
//

import Foundation
import CoreData


extension AllCities {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AllCities> {
        return NSFetchRequest<AllCities>(entityName: "AllCities")
    }

    @NSManaged public var city_id: String?
    @NSManaged public var city_name: String?
    @NSManaged public var state_id: String?
    @NSManaged public var state_name: String?

}
