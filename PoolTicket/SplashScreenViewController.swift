//
//  SplashScreenVC.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/11/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import SwiftyDrop
import SCLAlertView

class SplashScreenViewController: UIViewController {
    
    var dataController: DataController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataController = DataController.init()
        /*set the device screen size*/
        setDeviceType();
        
        
        if  let _ = KeychainService.loadDeviceId() {
            // do nothing
        } else {
            getNewDeviceId()
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.refreshToken()
        self.getAppSetting()
        self.getALLCitiesAndStateList()

     
    }

    func refreshToken() -> Void {
        let client_id = Constants.clientId
        let refresh_token = "refresh_token";
        let front = "front";
        var WSResponseStatus = false
        var deviceId = ""
        if let value = KeychainService.loadDeviceId(), value != nil {
            deviceId = String(value)
        }
        let index = wsPOST.init(Constants.TOKEN,"client_id=\(client_id)&grant_type=\(refresh_token)&scope=\(front)&access_token=\(SwiftUtil.getUserAccessToken())&refresh_token=\(self.getUserRefreshToken())&device_id=\(deviceId)")
      
        index.start{ (indexData) in
            
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    //print(json)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                //print("succes")
                                WSResponseStatus = true
                            }
                        }
                        
                        if let value = item["access_token"] as? String {
                            self.setUserAccessToken(value)
                        }
                        
                        if let value = item["refresh_token"] as? String {
                            self.setUserRefreshToken(value)
                        }
                    }
                    
                }
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                if WSResponseStatus {
                    // self.showDropMessage("token is refreshed")
                    // self.createMenuView()
                    self.openMainView()
                }else{
                    // self.showDropMessage("token is fail, need checking phone number")
                    self.setUserAccessToken("")
                    // self.createMenuView()
                    self.openMainView()
                    // self.openCheckPhoneNumberRegistration()
                }
            })
            
        }
        
        
    }
    
    func openMainView(){
        let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarMainVC") as! TabBarMainVC
        viewController.modalPresentationStyle = .fullScreen
        self.present(viewController, animated: true, completion: nil)
    }

    func openCheckPhoneNumberRegistration()->Void{
        openNewClass(classIdentifier: "CheckPhoneNumberRegistrationVC")
    }
    
//    fileprivate func createMenuView() {
//
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//
//        let mainViewController = storyboard.instantiateViewController(withIdentifier: "mainView") as! TabBarMainVC
//
//        let rightViewController = storyboard.instantiateViewController(withIdentifier: "rightView") as! RightDrawerVC
//
//        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
//        UINavigationBar.appearance().tintColor = UIColor.brown
//
//        let slideMenuController = SlideMenuController(mainViewController: mainViewController, rightMenuViewController: rightViewController)
//        slideMenuController.automaticallyAdjustsScrollViewInsets = true
//        slideMenuController.delegate = mainViewController
//
//        self.present(slideMenuController, animated: false, completion: nil)
//    }


    
    /*  get list of cities and states || active city true*/
    func getALLCitiesAndStateList() -> Void {
        let client_id = Constants.clientId
        let just_active_cities = 1
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.ALL_CITIES_AND_STATES,"client_id=\(client_id)&just_active_cities=\(just_active_cities)&access_token=\(access_token)")
        index.start{ (indexData) in
            
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                
                                if let items = item["cities"] as? [[String: AnyObject]] {
                                    
                                    let moc = self.dataController.managedObjectContext
                                    if items.count > 0 {
                                        self.emptyCityEntity(moc: moc)
                                    }
                                    
                                    for itemValue in items {
                                        if let value = cityModel.prepare(itemValue as AnyObject) as? cityModel{
                                            self.insertCityItem(value.city_id!, value.city_name!, value.state_id!,value.state_name!, moc: moc)
                                        }
                                    }
                                    
                                    if moc.hasChanges == true {
                                        moc.performAndWait {
                                            do{
                                                try moc.save()
                                            }catch{
                                                fatalError("Failure to save context: \(error)")
                                            }
                                        }
                                    }
                                    
                                }
                                
                            }
                        }
                        
                        
                    }
                }
            }catch{
                print("this is the error: \(error)")
            }
            
            
        }
    }
    
    /*  get list of cities and states || active city false*/
    func getALLCitiesAndStateListV2() -> Void {
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.ALL_CITIES_AND_STATES,"client_id=\(client_id)&access_token=\(access_token)")
        index.start{ (indexData) in
            
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    //print(json)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                
                                if let items = item["cities"] as? [[String: AnyObject]] {
                                    
                                    let moc1 = self.dataController.managedObjectContext
                                    if items.count > 0 {
                                        self.emptyCityEntityV2(moc: moc1)
                                    }
                                    
                                    for itemValue in items {
                                        if let value = cityModel.prepare(itemValue as AnyObject) as? cityModel{
                                            self.insertCityItemV2(value.city_id!, value.city_name!, value.state_id!,value.state_name!, moc: moc1)
                                        }
                                    }
                                    
                                    if moc1.hasChanges == true {
                                        moc1.performAndWait {
                                            do{
                                                try moc1.save()
                                            }catch{
                                                fatalError("Failure to save context: \(error)")
                                            }
                                        }
                                    }
                                  
                                    DispatchQueue.main.async {
//                                        if moc.hasChanges == true {
//                                            moc.performAndWait {
//                                                do{
//                                                    try moc.save()
//                                                }catch{
//                                                    fatalError("Failure to save context: \(error)")
//                                                }
//                                            }
//                                        }
                                        
                                        if let value = item["latest_all_cities_update"] as? String {
                                            self.setAllCitiesUpdate(value)
                                        }
                                        
                                    }
                                    
                                   
                                }
                                
                               
                                
                            }else{
                            }
                        }
                        
                        
                    }
                }
            }catch{
                print("this is the error: \(error)")
            }
            
        }
    }
    
    // MARK: CoreData Cities
    func emptyCityEntity(moc: NSManagedObjectContext){
        let fetch = NSFetchRequest<CitiesV2>(entityName: "CitiesV2")
        let request = NSBatchDeleteRequest(fetchRequest: fetch as! NSFetchRequest<NSFetchRequestResult> )
           moc.performAndWait {
                do{
                    try moc.execute(request)
                }catch{
                    fatalError("this is error for drop entity : \(error)")
                }
        }
    }
    
    func insertCityItem(_ city_id: String, _ city_name: String, _ state_id: String, _ state_name: String, moc: NSManagedObjectContext)->Void{
        let entity = NSEntityDescription.insertNewObject(forEntityName: "CitiesV2", into: moc) as! CitiesV2
        entity.order_id = Date()
        entity.state_name = state_name
        entity.state_id = state_id
        entity.city_id = city_id
        entity.city_name = city_name
    }
    
    // MARK: CoreData AllCities
    func emptyCityEntityV2(moc: NSManagedObjectContext){
        let fetch = NSFetchRequest<AllCities>(entityName: "AllCities")
        let request = NSBatchDeleteRequest(fetchRequest: fetch as! NSFetchRequest<NSFetchRequestResult> )
        moc.performAndWait {
            do{
                try moc.execute(request)
            }catch{
                fatalError("this is error for drop entity : \(error)")
            }
        }
    }
    
    
    func insertCityItemV2(_ city_id: String, _ city_name: String, _ state_id: String, _ state_name: String, moc: NSManagedObjectContext)->Void{
        let entity = NSEntityDescription.insertNewObject(forEntityName: "AllCities", into: moc) as! AllCities
        entity.state_name = state_name
        entity.state_id = state_id
        entity.city_id = city_id
        entity.city_name = city_name
    }
    
    func getNewDeviceId() -> Void {
        let client_id = Constants.clientId
        let typeOperator = "operator";
        var WSResponseStatus = false
        var uuid = NSUUID().uuidString.lowercased()
        uuid = uuid.replacingOccurrences(of: "-", with: "")
        let index = wsPOST.init(Constants.TOKEN,"client_id=\(client_id)&grant_type=check_device_id&scope=\(typeOperator)&device_id=\(uuid)")
        index.start{ (indexData) in
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    
                    if let item = json as? [String: AnyObject]{
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                if let device_id_exists = item["device_id_exists"] as? Bool {
                                    if !device_id_exists  {
                                        if let deviceId = uuid as? NSString {
                                            KeychainService.saveDeviceId(device_id: deviceId)
                                            WSResponseStatus = true
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                if WSResponseStatus {
                    // do nothing
                }
            })
            
        }
    }
    
   /* set the device type*/
    func setDeviceType()->Void{
        
        if DeviceType.IS_IPHONE_4_OR_LESS {
            //
        }else if DeviceType.IS_IPHONE_5 {
            //
        }else if DeviceType.IS_IPHONE_6_6S_7_8 {
            //
        }else if DeviceType.IS_IPHONE_6P_6SP_7P_8P {
            //
        }else if DeviceType.IS_IPAD {
            //
        }else if DeviceType.IS_IPAD_PRO {
            //
        }
        
    }
    
    /*  get the App Setting */
    func getAppSetting() -> Void {
        var latest_all_cities_update = ""
        var latest_app_download_link = ""
        var latest_build = ""
        var latest_build_support = ""
        var latest_version = ""
        var timestamp = ""
        let access_token = SwiftUtil.getUserAccessToken()
        let client_id = Constants.clientId
        var pushNotificationExtendQuery = ""
        if let value = UserDefaults.standard.value(forKey: UserDefaults.Keys.PushNotificationToken) as? String {
            pushNotificationExtendQuery = "&push_token=" + value
        }
        let scope = "front"
        let index = wsPOST.init(Constants.APP_SETTING,"client_id=\(client_id)&package_name=org.poolticket.customer.ios&access_token=\(access_token)&scope=\(scope)&appClassPosition=DrawerMenu\(pushNotificationExtendQuery)")
        index.start{ (indexData) in
            
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    //print(json)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                
                                if let app = item["app"] as? [String: AnyObject] {
                                    
                                    if let value = app["latest_all_cities_update"] as? String {
                                        latest_all_cities_update = value
                                    }
                                    if let value = app["latest_app_download_link"] as? String {
                                        latest_app_download_link = value
                                    }
                                    if let value = app["latest_build"] as? Int {
                                        latest_build = String(value)
                                    }
                                    if let value = app["latest_build_support"] as? String {
                                        latest_build_support = value
                                    }
                                    if let value = app["latest_version"] as? String {
                                        latest_version = value
                                    }
                                    if let tTimestamp = app["timestamp"] as? [String:AnyObject] {
                                        
                                        if let value = tTimestamp["date"] as? String {
                                            timestamp = value
                                        }
                                        
                                    }
                                    // storing the social network address for the PoolTicket
                                    if let socials = app["socials"] as? [String: AnyObject] {
                                        let userDefault = UserDefaults.standard
                                        if let facebook = socials["facebook"] as? String {
                                            userDefault.set(facebook, forKey: UserDefaults.Keys.facebook)
                                        }
                                        if let telegram = socials["telegram"] as? String {
                                            userDefault.set(telegram, forKey: UserDefaults.Keys.telegram)
                                        }
                                        if let instagram = socials["instagram"] as? String {
                                            userDefault.set(instagram, forKey: UserDefaults.Keys.instagram)
                                        }
                                        if let _ = socials["twitter"] as? String {
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                
                
                // check if the all cities was updated ...
                if self.getAllCityItemCount() > 0 {
                    if self.stringToDate(self.getAllCitiesUpdate())  < self.stringToDate(latest_all_cities_update) {
                        self.getALLCitiesAndStateListV2()
                    }else{
                        // does not need an action
                    }
                }else{
                    self.getALLCitiesAndStateListV2()
                }
                
                
                // check if the app need to update
                do {
                var currentBuildVersion = 0
                var latestBuildVersion = 0
                if let value = Int(self.getBuildVersion()) {
                    currentBuildVersion = value
                }
                if let value = Int(latest_build) {
                    latestBuildVersion = value
                }
                    if self.getCodeVersion() == latest_version || currentBuildVersion >= latestBuildVersion {
                        // does not need an update
                        //print("does not need an update")
                    }else if self.getCodeVersion() != latest_version || currentBuildVersion < latestBuildVersion {
                        if self.getNextTimeUpdate() != "" {
                            if self.stringToDate(self.getNextTimeUpdate()) < self.stringToDate(timestamp) {
                                self.showAlert("needUpdate", self.replaceString(latest_version), self.stringToDate(timestamp), latest_app_download_link)
                            }else{
                                //it has to wait for 48 hour
                            }
                        }else{
                                self.showAlert("needUpdate", self.replaceString(latest_version), self.stringToDate(timestamp), latest_app_download_link)
                        }
                    }
                
                    
                }catch{
                    fatalError("this error is happend while checking the version number , error : \(error)")
                }
                
            })
            
        }
    }
    
    func getAllCityItemCount()->Int{
        let moc = dataController.managedObjectContext
        let fetch = NSFetchRequest<AllCities>(entityName: "AllCities")
        do {
            let items =  try moc.fetch(fetch) as [AllCities]
            return items.count;
        }catch{
            fatalError("Failure to retrieve context: \(error)")
        }
        return 0
    }
    
    fileprivate func stringToDate(_ timString: String) -> Date{
        do {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZ"
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+3:30") //Current time zone
        let date = dateFormatter.date(from: timString) //according to date format your date string
        //print(date ?? "")
        return date ?? Date()
        }catch{
            fatalError("this is the error \(error)")
        }
        
        return Date()
    }
    
    func showAlert(_ type: String, _ version: String, _ currentTimeFromServer: Date, _ downloadLink: String) {
        
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "IRANSansMobile", size: 17)!,
            kTextFont: UIFont(name: "IRANSansMobile", size: 14)!,
            kButtonFont: UIFont(name: "IRANSansMobile", size: 14)!,
            showCloseButton: false,
            showCircularIcon: false
        )
       
        let alertView = SCLAlertView(appearance: appearance)
        
        switch type {
        case "needUpdate":
            let alertViewIcon = UIImage(named: "AppIcon")
            alertView.addButton("دریافت نسخه جدید") {
                guard let url = URL(string: downloadLink) else {
                    return //be safe
                }
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
            alertView.addButton("بعدا") {
                let tDate = currentTimeFromServer.addingTimeInterval(172800)
                self.setNextTimeUpdate(String(describing: tDate))
            }
            alertView.showInfo("انتشار نسخه: \(version)", subTitle: "نسخه جدید اپلیکیشن پول تیکت منتشر شد، پیشنهاد می کنیم نسخه جدید را نصب بفرمائید.")
            break
        default:
            print("")
        }
        
    }
   
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
