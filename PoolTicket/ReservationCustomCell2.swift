//
//  ReservationCustomCell2.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/9/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
class ReservationCustomCell2: UITableViewCell {
    
    @IBOutlet weak var priceWithDiscountTitle: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var priceWithDiscount: UILabel!
    @IBOutlet weak var number: UITextField!
    @IBOutlet weak var posetive: UIButton!
    @IBOutlet weak var negative: UIButton!
    
}
