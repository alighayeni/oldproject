//
//  pool_specification_customCellV2.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/21/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit

class pool_specification_customCellV2: UITableViewCell {
    
    @IBOutlet weak var rowKey: UILabel!
    @IBOutlet weak var rowValue: UILabel!
    
}
