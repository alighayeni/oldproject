//
//  ProfileVC.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/28/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit


class FavoriteVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var pools : [poolsListModel] = []
    var IS_LOGGED_IN : Bool?
    @IBOutlet weak var mainEmptyLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.navigationController?.navigationBar.barTintColor = self.setBgColor(43, green: 163, blue: 215)
        self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.font.rawValue: UIFont(name: "IRANSansMobile", size: 16),NSAttributedString.Key.foregroundColor.rawValue:UIColor.white])
        self.title = "علاقه مندی"
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if SwiftUtil.getUserAccessToken() == "" {
            self.IS_LOGGED_IN = false
            tableView.alpha = 0
            presentProperMessage()
            tableView.reloadData()
        }else{
            self.IS_LOGGED_IN = true
            tableView.alpha = 1
            getFavoriteList()
        }
    }
    
   //get the list
    func getFavoriteList() -> Void {
        LoadingOverlay.shared.showOverlay(view: self.view)
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.FAVORITE,"client_id=\(client_id)&access_token=\(access_token)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            //var page : String?
            //var limit :  String?
            
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    if let item = json as? [String: AnyObject]{
                    self.pools = []
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                if let pools = item["favorites"] as? [[String: AnyObject]] {
                                    DataSourceManagement.cleanFavoritePoolsList()
                                    
                                    for pool in pools {
                                        DataSourceManagement.addFavoritePoolsList(poolsListModel.prepare(pool as AnyObject))
                                    }
                                }
                                
                            }
                        }
//                       if let value = item["limit"] as? String {
//                          //limit = value
//                       }
//                       if let value = item["page"] as? String {
//                         // page = value
//                       }
                    }
                }
            }catch{
                print("this is the error: \(error)")
            }
            DispatchQueue.main.async(execute: {
                LoadingOverlay.shared.hideOverlayView()
                if WSResponseStatus {
                self.pools = DataSourceManagement.favoritePoolList
                    if self.pools.count > 0 {
                        self.tableView.alpha = 1
                        self.tableView.reloadData()
                    } else {
                        self.tableView.alpha = 0
                        self.presentProperMessage()
                    }
                }else{
                     self.presentProperMessage()
                    //try again
                    //Drop.down("username or password is wrong! please try again")
                }
            })
        }
    }
    
    func presentProperMessage() {
        if SwiftUtil.getUserAccessToken() == "" {
           self.mainEmptyLabel.text = "برای مشاهده لیست علاقه مندی ها  به حساب کاربری خود وارد شوید."
        } else {
           self.mainEmptyLabel.text = "شما تا به حال هیچ استخری را در علاقه مندی خود اضافه نکرده اند."
        }
    }
    
   //delete the item from this list
    func usetFavoritePool(_ pool_id: String) -> Void {
        LoadingOverlay.shared.showOverlay(view: self.view)
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.UNSET_FAVORITE,"client_id=\(client_id)&access_token=\(access_token)&pool_id=\(pool_id)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            do{
                
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                            }
                        }
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                LoadingOverlay.shared.hideOverlayView()
                if WSResponseStatus {
                    self.showDropMessage("استخر مورد نظر با موفقیت از لیست علاقه مندی های حذف گردید.")
                    self.pools = self.pools.filter({
                        $0.pool_id != pool_id
                    })
                    self.tableView.reloadData()
                    self.getFavoriteList()
                }else{
                    self.showDropMessage("درخواست حذف با خطا مواجه شده است، لطفا مجدد تلاش کنید.")
                }
            })
            
        }
        
        
    }
    
    
    // open drawer
    @IBAction func openDrawer(){
        if let _ = self.slideMenuController() {
            self.slideMenuController()?.openRight()
        }
    }
    
    func openCheckPhoneNumberRegistration()->Void{
        openNewClass(classIdentifier: "CheckPhoneNumberRegistrationVC")
    }
}

extension FavoriteVC: UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if !self.IS_LOGGED_IN! {
            let cellIdentifier =  "Cell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        }
        
        let cellIdentifier =  "favoriteCustomRow"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! favoriteCustomRow
        
        let data = pools[indexPath.row] as poolsListModel
        
        //clear objcet items
        cell.availableForMan.alpha = 0
        cell.availableForWomen.alpha = 0
        
        cell.favoriteTitle.text = data.name
        if let value = data.area_name {
            if value == "<null>" {
                cell.favoriteAddress.text = data.city_name
            }else{
                cell.favoriteAddress.text = value
            }
        }
        
        if let value = data.rating as? String {
            cell.poolRateBar.rating = setRatingStar(value)
        }
        switch data.gender {
        case "0"?:
            cell.availableForMan.alpha = 1
            break
        case "1"?:
            cell.availableForWomen.alpha = 1
            break
        case "2"?:
            cell.availableForWomen.alpha = 1
            cell.availableForMan.alpha = 1
            break
        default:
            printLog(m: "")
        }
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.IS_LOGGED_IN != nil {
            if !self.IS_LOGGED_IN! {
                return 0
            }else{
                return pools.count;
            }
        }
        return 0
    }
    
}

extension FavoriteVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "حذف") { (action, indexPath) in
            // delete item at indexPath
            let data = self.pools[indexPath.row] as poolsListModel
            self.usetFavoritePool(data.pool_id!)
        }
        return [delete]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.IS_LOGGED_IN! {
            let data = pools[indexPath.row] as poolsListModel
            DataSourceManagement.setSelectedPoolId(data.pool_id!)
            self.pushNewClass(classIdentifier: "PoolVC")
        }else{
            openCheckPhoneNumberRegistration()
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
