//
//  ShareData.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/11/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
final class shareData {
    static let shared = shareData() //lazy init, and it only runs once
    
    var mainSettingResponse : String?
    var RTUModel: refreshTokenUsersModel?
    var loginInputPhoneNumber: String?
    var purchase_id: String?
    var register_token: String?
    var selectedCityAndState: cityModel?
    var selectedPoolObject: poolsById?
    
    var selectedPoolName: String?
    var selectedChoosenServices: String?
    var selectedChoosenPoolsRulesAndDetails: String?

    var topHeaderLabelPoolList : String?
   
    var selectedSaensRow: saensModel?
    var selectedPackRow: packsModel?
    var selectedCourseRow: courseModel?
    
    var selectedAdultCount: String?
    var selectedChildCount: String?
    var startReservationURL: String?
    
    var ReservationFinalResponse: PRFR?
    var filterPoolListEnable: Bool?
    var filterPoolListQuery: String?
    
    var selectedIndexTextType: textModel?
    var selectedCommentPoolId: String?
    var selectedCommentWriterPoolId: String?

    var selectedBlogLink: String?
    var selectedPurchaseQRString: String?

    var selectedBirthModel: cityModel?
    
    var forgetPassApprovedToken: String?
    
    var reservationObject: reservationObject?
    
    var IncreaseCreditResult_Status: String?
    var IncreaseCreditResult_Credit: String?
    var IncreaseCreditResult_setColse: Bool?
    
    var Pool_list_map_Lat: String?
    var Pool_list_map_Lng: String?
    var Pool_list_map_zoom: String?
    
    var poolDescription: String?

}

// use full object for success final result
//Pack Reservation Final Response
struct PRFR {
    var status : String
    var voucher: String
    var reservation_status: String
    var reservation_id: String
}

// because we have to add try again action when something
// is wrong in payment forms
// we need to duplicate the data
struct reservationObject {
    var type: String
    var objectId : String
    var objectAdultCount: String
    var objectChildCount: String
    var payment_type: String
    var coupon: String
}


// pool ratings attrebute
struct ratingsObject {
    var name: String
    var value : String
    var rated_count: String
}

