//
//  CheckPhoneNumberRegistrationVC.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/12/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit

class CheckPhoneNumberRegistrationVC: UIViewController, UITextFieldDelegate {
    
    
    var connectionStatus = false

    @IBOutlet weak var scrollView:UIScrollView!
    var activeField: UITextField?

    @IBOutlet weak var phoneNumber: UITextField!;
    @IBOutlet weak var mainViewBg: UIView!;
    @IBOutlet weak var checkTheNumberBtn: UIButton!;
    @IBAction func checkTheNumber(_ Sender: Any){
        
        if var mNumber = phoneNumber.text  as? String {
            mNumber = removeWhiteSpace(mNumber)
            if !connectionStatus {
                checkIfPhoneNumberRegistered(mNumber)
            }
           
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let closeTopBarBtn = UIBarButtonItem(image: UIImage(named: "ic_arrow_left_white"),style: .plain ,target: self, action: #selector(self.back(img:)))
        self.navigationItem.leftBarButtonItem = closeTopBarBtn
        
        
        self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.font.rawValue: UIFont(name: "IRANSansMobile", size: 14),NSAttributedString.Key.foregroundColor.rawValue:UIColor.white])
        let customFont = UIFont(name: "IRANSansMobile", size: 13.0)!
        UIBarButtonItem.appearance().setTitleTextAttributes(convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.font): customFont]), for: .normal)
        self.title = "ورود/عضویت"
        
        phoneNumber.delegate = self
        doneWithKeyboard(phoneNumber)
        
        phoneNumber.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

        

    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if let textFieldStringData = textField.text {
            let range = NSRange(location: 0, length: textFieldStringData.utf16.count)
            let mobileNoRegex = "\\A09[0-9]{9}\\z"
            let regex = try! NSRegularExpression(pattern: mobileNoRegex)
            if regex.firstMatch(in: textFieldStringData, options: [], range: range) != nil {
                checkTheNumberBtn.isEnabled = true
                let lightBlue = UIColor(red: 32/255.0, green: 161/255.0, blue: 218/255.0, alpha: 1.0)
                checkTheNumberBtn.setBackgroundColor(color: lightBlue, forState: .normal) //(32 161 218) 20A1DA
            } else {
                checkTheNumberBtn.isEnabled = false
                let lightGray = UIColor.init(red: 200, green: 200, blue: 200, alpha: 1.0)
                checkTheNumberBtn.setBackgroundColor(color: lightGray, forState: .normal)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage() //remove pesky 1 pixel line
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        
        //UIColor(red: 254/255.0, green: 210/255.0, blue: 15.0/255.0, alpha: 1.0)
        //self.navigationController?.navigationBar.isTranslucent = false

        mainViewBg.dropShadow()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
     registerForKeyboardNotifications()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    deregisterFromKeyboardNotifications()
    }
    
    
    func checkIfPhoneNumberRegistered(_ mobileNo: String){
        LoadingOverlayV2.sharedV2.showOverlayV2(view: self.view, message: "در حال تائید کد اعتبار سنجی")
        connectionStatus = true
        let client_id = Constants.clientId
        let approve_send_type = "sms"
        let send_approve_code = true
        var registerResponse = false
        var WSResponse = false
        var forceOneTimePassword = false
        
        let index = wsPOST.init(Constants.CHECK_REGISTRATION,"mobile=\(mobileNo)&client_id=\(client_id)&approve_send_type=\(approve_send_type)&send_approve_code=\(send_approve_code)")
        index.start{ (indexData) in
            
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    
                    if let item = json as? [String: AnyObject]{
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponse = true
                            }
                        }
                        if let value = item["registered"] as? Int {
                            // self.printLog(m: String(value))
                            if value == 1 {
                                registerResponse = true
                            }
                        }
                        if let value = item["force_one-time_password"] as? Int {
                            if value == 1 {
                                forceOneTimePassword = true
                            }
                        }
                    }
                }
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                LoadingOverlayV2.sharedV2.hideOverlayView()
                self.connectionStatus = false
                if WSResponse {
                    shareData.shared.loginInputPhoneNumber = mobileNo
                    if forceOneTimePassword {
                        let viewController:UIViewController = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "SubmitActivationOneTimePassword") as UIViewController
                       // self.present(viewController, animated: true, completion: nil)
                        viewController.modalPresentationStyle = .fullScreen
                        self.navigationController?.pushViewController(viewController, animated: true)
                    } else {
                        if !registerResponse {
                            self.pushNewClass(classIdentifier: "SubmitActivationNumber")
                        } else {
                            self.pushNewClass(classIdentifier: "LoginVC")
                        }
                    }
                }
            
            })
            
        }
    }

    @objc func back(img: AnyObject)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    //************** softkeyboard tool bar **********************//
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField
        {
//        case personalID:
//            personalID.resignFirstResponder()
//            password.becomeFirstResponder()
//            break
//        case password:
//            password.resignFirstResponder()
//            break
//
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
    func doneWithKeyboard(_ sender: UITextField){
        
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        keyboardToolbar.isTranslucent = true
        keyboardToolbar.barTintColor = UIColor.white
        
        let button = UIButton(type: .system)
        button.setTitle("بستن", for: .normal)
        button.sizeToFit()
       
        button.titleLabel?.font = UIFont(name: "IRANSansMobile", size: 12)
        button.tintColor = UIColor.black
        button.addTarget(self, action: #selector(closeKeyboard(_:)), for: .touchUpInside)
        
        
//        let btnUp = UIButton(type: .custom)
//        btnUp.setImage(UIImage(named: "ic_keyboard_arrow_up"), for: .normal)
//        btnUp.frame = CGRect(x: 0, y: 0, width: 13, height: 13)
//        btnUp.tag = sender.tag
//        btnUp.addTarget(self, action: #selector(textFieldUp), for: .touchUpInside)
//        let itemUp = UIBarButtonItem(customView: btnUp)
//
//        let btnDown = UIButton(type: .custom)
//        btnDown.setImage(UIImage(named: "ic_keyboard_arrow_down"), for: .normal)
//        btnDown.frame = CGRect(x: 0, y: 0, width: 13, height: 13)
//        btnDown.tag = sender.tag
//        btnDown.addTarget(self, action: #selector(textFieldDown), for: .touchUpInside)
//        let itemDown = UIBarButtonItem(customView: btnDown)
        
        
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        keyboardToolbar.items = [flexBarButton,UIBarButtonItem(customView: button)]
        //keyboardToolbar.items = [itemDown,itemUp,flexBarButton,UIBarButtonItem(customView: button)]
        
        if let value = sender as? UITextField {
            value.inputAccessoryView = keyboardToolbar
        }else if let value = sender as? UITextView {
            value.inputAccessoryView = keyboardToolbar
        }
        
    }
    
    

    
   /*
    @objc func textFieldUp(_ sender: Any)-> Void {
        
        let tObject = (sender as AnyObject).tag
        if let value = tObject as? Int {
            switch value
            {
            case 1:
                personalID.resignFirstResponder()
                password.becomeFirstResponder()
                break
            case 2:
                password.resignFirstResponder()
                personalID.becomeFirstResponder()
                break
            default:
                print("")
            }
        }
    }
    
    @objc func textFieldDown(_ sender: Any)-> Void {
        
        let tObject = (sender as AnyObject).tag
        if let value = tObject as? Int {
            switch value
            {
            case 1:
                personalID.resignFirstResponder()
                password.becomeFirstResponder()
                break
            case 2:
                password.resignFirstResponder()
                personalID.becomeFirstResponder()
                break
            default:
                print("")
            }
        }
    }
     */
    
    //************** softkeyboard tool bar **********************//

    /*keyboard automatic scroll*/
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.isScrollEnabled = true
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: keyboardSize!.height, right: 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.activeField {
            if (!aRect.contains(activeField.frame.origin)){
                self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: -keyboardSize!.height, right: 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollView.isScrollEnabled = false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }

}





// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}
