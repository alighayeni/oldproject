//
//  comment_customCellV2.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/10/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit

class comment_customCellV1: UITableViewCell {
    
    
    @IBOutlet weak var commentName: UILabel!
    @IBOutlet weak var commentPublishDate: UILabel!
    @IBOutlet weak var commentDetail: UILabel!
    @IBOutlet weak var commentLikeCount: UILabel!
    @IBOutlet weak var commentDisLikeCount: UILabel!
    
    @IBOutlet weak var commentPosetivePoint: UILabel!
    @IBOutlet weak var commentNegativePoint: UILabel!
    
    @IBOutlet weak var responseBtn:UIButton!

    @IBOutlet weak var likeBtn:UIButton!
    @IBOutlet weak var dislikeBtn:UIButton!
}
