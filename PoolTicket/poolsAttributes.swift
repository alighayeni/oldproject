//
//  poolsAttributes.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/1/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation

class poolsAttributes: NSObject {
    
    var name:String?
    var icon:String?
    
    init(_ name:String,_ icon:String) {
        self.name = name
        self.icon = icon
    }
    
    static func prepare(_ item:AnyObject) -> poolsAttributes{
        let name = String(describing: item["name"] as AnyObject)
        let icon = String(describing: item["icon"] as AnyObject)
        let instance = poolsAttributes(name, icon)
        return instance
    }
}
