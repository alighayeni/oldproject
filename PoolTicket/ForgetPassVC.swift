//
//  ForgetPass.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/15/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyDrop

class ForgetPassVC: UIViewController, UITextFieldDelegate {
    
    var connectionStatus = false
    @IBOutlet weak var username:UITextField!
    @IBOutlet weak var scrollView:UIScrollView!
    var activeField: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.font.rawValue: UIFont(name: "IRANSansMobile", size: 14),NSAttributedString.Key.foregroundColor.rawValue:UIColor.white])
        
        self.title = "بازیابی کلمه عبور"

        
        username.delegate = self
        doneWithKeyboard(username)
        
        
        if let value = shareData.shared.loginInputPhoneNumber as? String {
            self.username.text = value
        }
        
    }
    
    @IBAction func submitForgotPassword(){
        if !connectionStatus {
            guard let username = username.text else {
                self.showDropMessage("missing username")
                return
            }
            self.forgottenPassword(username,"sms")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        registerForKeyboardNotifications()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        deregisterFromKeyboardNotifications()
    }
    
    func forgottenPassword(_ username: String,_ type: String) -> Void {
        self.connectionStatus = true
        LoadingOverlayV2.sharedV2.showOverlayV2(view: self.view, message: "در حال بررسی شماره همراه")
        let client_id = Constants.clientId
        let grant_type = "forgotten";
        let reset_send_type = type
        let scope = "front";

        let index = wsPOST.init(Constants.PASSWORD,"client_id=\(client_id)&grant_type=\(grant_type)&username=\(username)&reset_send_type=\(reset_send_type)&scope=\(scope)")

        var WSResponseStatus = false
        index.start{ (indexData) in

            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    if let item = json as? [String: AnyObject]{

                        if let value = item["status"] as? String {
                            if value == "1" {
                                WSResponseStatus = true
                            }
                        }
                    }

                }
            }catch{
                print("this is the error: \(error)")
            }

            DispatchQueue.main.async(execute: {
                self.connectionStatus = false
                 LoadingOverlayV2.sharedV2.hideOverlayView()
                if WSResponseStatus {
                    self.pushNewClass(classIdentifier: "SubmitActivationNumberForgetPass")
                }else{

                }
            })

        }


    }
    
    // if user does not get the token with sms
//    func forgottenPassword(_ mobileNo: String, _ type: String){
//        LoadingOverlayV2.sharedV2.showOverlayV2(view: self.view, message: "درخواست مجدد کد")
//        connectionStatus = true
//        let client_id = Constants.clientId
//        let approve_send_type = type
//        var WSResponseStatus = false
//        let index = wsPOST.init(Constants.RESEND_TOKEN,"mobile=\(mobileNo)&client_id=\(client_id)&approve_send_type=\(approve_send_type)")
//        index.start{ (indexData) in
//            var requestTime = 0
//            do{
//                if let indexValue = indexData {
//                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
//
//                    if let item = json as? [String: AnyObject]{
//                        if let value = item["status"] as? Int {
//                            if value == 1 {
//                                WSResponseStatus = true
//                            }
//                        }
//                        if let value = item["available_in"] as? Int {
//                            requestTime = value
//                        }
//                    }
//                }
//            }catch{
//                print("this is the error: \(error)")
//            }
//
//            DispatchQueue.main.async(execute: {
//                self.connectionStatus = false
//                LoadingOverlayV2.sharedV2.hideOverlayView()
//                if WSResponseStatus {
//                    self.pushNewClass(classIdentifier: "SubmitActivationNumberForgetPass")
//                }
//            })
//
//        }
//    }
    //************** softkeyboard tool bar **********************//
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField
        {
            //        case personalID:
            //            personalID.resignFirstResponder()
            //            password.becomeFirstResponder()
            //            break
            //        case password:
            //            password.resignFirstResponder()
            //            break
        //
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
    func doneWithKeyboard(_ sender: UITextField){
        
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        keyboardToolbar.isTranslucent = true
        keyboardToolbar.barTintColor = UIColor.white
        
        let button = UIButton(type: .system)
        button.setTitle("بستن", for: .normal)
        button.sizeToFit()
        button.titleLabel?.font = UIFont(name: "IRANSansMobile", size: 12)
        button.tintColor = UIColor.black
        button.addTarget(self, action: #selector(closeKeyboard(_:)), for: .touchUpInside)
        
        
        //        let btnUp = UIButton(type: .custom)
        //        btnUp.setImage(UIImage(named: "ic_keyboard_arrow_up"), for: .normal)
        //        btnUp.frame = CGRect(x: 0, y: 0, width: 13, height: 13)
        //        btnUp.tag = sender.tag
        //        btnUp.addTarget(self, action: #selector(textFieldUp), for: .touchUpInside)
        //        let itemUp = UIBarButtonItem(customView: btnUp)
        //
        //        let btnDown = UIButton(type: .custom)
        //        btnDown.setImage(UIImage(named: "ic_keyboard_arrow_down"), for: .normal)
        //        btnDown.frame = CGRect(x: 0, y: 0, width: 13, height: 13)
        //        btnDown.tag = sender.tag
        //        btnDown.addTarget(self, action: #selector(textFieldDown), for: .touchUpInside)
        //        let itemDown = UIBarButtonItem(customView: btnDown)
        
        
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        keyboardToolbar.items = [flexBarButton,UIBarButtonItem(customView: button)]
        //keyboardToolbar.items = [itemDown,itemUp,flexBarButton,UIBarButtonItem(customView: button)]
        
        if let value = sender as? UITextField {
            value.inputAccessoryView = keyboardToolbar
        }else if let value = sender as? UITextView {
            value.inputAccessoryView = keyboardToolbar
        }
        
    }
    
    
    
    
    /*
     @objc func textFieldUp(_ sender: Any)-> Void {
     
     let tObject = (sender as AnyObject).tag
     if let value = tObject as? Int {
     switch value
     {
     case 1:
     personalID.resignFirstResponder()
     password.becomeFirstResponder()
     break
     case 2:
     password.resignFirstResponder()
     personalID.becomeFirstResponder()
     break
     default:
     print("")
     }
     }
     }
     
     @objc func textFieldDown(_ sender: Any)-> Void {
     
     let tObject = (sender as AnyObject).tag
     if let value = tObject as? Int {
     switch value
     {
     case 1:
     personalID.resignFirstResponder()
     password.becomeFirstResponder()
     break
     case 2:
     password.resignFirstResponder()
     personalID.becomeFirstResponder()
     break
     default:
     print("")
     }
     }
     }
     */
    
    //************** softkeyboard tool bar **********************//

    /*keyboard automatic scroll*/
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.isScrollEnabled = true
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: keyboardSize!.height, right: 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.activeField {
            if (!aRect.contains(activeField.frame.origin)){
                self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: -keyboardSize!.height, right: 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollView.isScrollEnabled = false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
