//
//  Components.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/11/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyDrop
import AudioToolbox


extension UIViewController {
    
    func menuAction(_ sender: Any){
        self.slideMenuController()?.openRight()
    }
    
    func menuActionClose(){
        self.slideMenuController()?.closeRight()
    }
    
    func openNewClass(classIdentifier: String){
        
        let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: classIdentifier) as UIViewController
        viewController.modalPresentationStyle = .fullScreen
        self.present(viewController, animated: true, completion: nil)
    }
    
    func pushNewClass(classIdentifier: String){
        let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: classIdentifier) as UIViewController
        viewController.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func searchAction(img: AnyObject)
    {
        self.pushNewClass(classIdentifier:"SearchProductViewController")
    }
    
    // get the UIButton bg color
    func getObjectColor(_ type: String)-> UIColor {
        
        switch type {
        case "greenBtn":
            return UIColor(red: 20/255, green: 199/255, blue: 87/255, alpha: 1.0)
        default:
            return UIColor(red: 20/255, green: 199/255, blue: 87/255, alpha: 1.0)
        }
        
    }
    
    func setPageTitle(_ pageName: String){
        
        self.navigationItem.title = pageName
    }
    
    func tabBarSetHidden(_ status: Bool) {
        self.tabBarController?.tabBar.isHidden = status
        
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func printLog(m: String)->Void{
        print(m)
    }
    
    
    
    /* the all type of Screen Size
     
     sample code
     
     if DeviceType.IS_IPHONE_6 {
     
     }else if DeviceType.IS_IPHONE_6P{
     
     }
     */
    enum UIUserInterfaceIdiom : Int
    {
        case Unspecified
        case Phone
        case Pad
    }
    
    struct ScreenSize
    {
        static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    struct DeviceType
    {
        static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6_6S_7_8    = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P_6SP_7P_8P   = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
        static let IS_IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
    }
    
    
    func setBgColor(_ red:CGFloat, green:CGFloat, blue:CGFloat) ->UIColor {
        return UIColor(red: red/256, green: green/256, blue: blue/256, alpha: 1)
    }
    
    
    // persian replace character
    func replaceString(_ stringNumber: String)->String{
        var token = stringNumber
        token =  token.replacingOccurrences(of: "0", with:"۰")
        token =  token.replacingOccurrences(of: "1", with:"۱")
        token =  token.replacingOccurrences(of: "2", with:"۲")
        token =  token.replacingOccurrences(of: "3", with:"۳")
        token =  token.replacingOccurrences(of: "4", with:"۴")
        token =  token.replacingOccurrences(of: "5", with:"۵")
        token =  token.replacingOccurrences(of: "6", with:"۶")
        token =  token.replacingOccurrences(of: "7", with:"۷")
        token =  token.replacingOccurrences(of: "8", with:"۸")
        token =  token.replacingOccurrences(of: "9", with:"۹")
        return token
    }
    
    
    // persian rate value (RTL)
    func setRatingStar(_ rate:String)->Double{
        
        var roundRate = ""
        
        do{
            let tRate = Double(rate)
            return tRate!
//            roundRate = String(round(tRate!))
//            switch roundRate {
//            case "0.0":
//                return 5.0
//            case "0.5":
//                return 4.5
//            case "1.0":
//                return 4.0
//            case "1.5":
//                return 3.5
//            case "2.0":
//                return 3.0
//            case "2.5":
//                return 2.5
//            case "3.0":
//                return 2.0
//            case "3.5":
//                return 1.5
//            case "4.0":
//                return 1.0
//            case "4.5":
//                return 0.5
//            case "5.0":
//                return 0.0
//            default:
//                return 5.0
//            }
        }catch{
            fatalError("this is the error when parsing rate star \(error)")
            return 0.0
        }
        
    }
    
    func returnJalaliDate(_ tDate: String)->String{
        do{
            let temp = tDate.replacingOccurrences(of: "T", with: " ")
            var fullNameArr = temp.components(separatedBy: " ")
            var tDateFinal: String = fullNameArr[0]
            tDateFinal.replacingOccurrences(of: "-", with: "/")
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy/MM/dd"
            formatter.calendar = Calendar(identifier: .persian)
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy/MM/dd"
            
            let date03 = dateFormatter.date(from: tDateFinal)
            if let value = date03 as? Date {
                return formatter.string(from: value)
            }
        }catch{
            fatalError("item by id; change date ; error \(error)")
        }
        return "نا مشخص"
    }
    
    
    func getJalaliMonthName(_ index: Int)->String {
        do{
            let pickerDataMonth = ["فروردین","اردیبهشت","خرداد","تیر","مرداد","شهریور","مهر","آبان","آذر","دی ","بهمن","اسفند"]
            
            return pickerDataMonth[index]
        }catch{
            fatalError("this is the error in jalali get month name \(error)")
        }
        return ""
    }
    
    func removeWhiteSpace(_ mString: String)->String {
        return mString.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    func getCgColor(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat) ->UIColor {
        return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: 1.0)
    }
    
//    func showDropMessage(_ text: String){
//        AudioServicesPlaySystemSound(1521)
//        Drop.down(text, state: Custom.BlackYellow, duration: 3.0, action: nil)
//    }
   
    func dismissCurrentView() -> Void {
        self.dismiss(animated: true, completion: nil)
    }
 
    @objc
    func closeKeyboard(_ sender:UITextField){
        self.dismissKeyboard()
    }
    
    
    // custom dropDown
    func showDropMessage(_ text: String){
        AudioServicesPlaySystemSound(1521)
        Drop.down(text, state: Custom.BlueWhite, duration: 4.0, action: nil)
    }

    //remove html characters
    func removeHtmlCharacters(_ pText: String) -> String{
        var cleanText = pText
        cleanText =  cleanText.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
        return cleanText
    }
    
    // set price seprator
    func setSeprator(_ number: String)->String{
        var mCreditNSNumber : NSNumber?
        let someString = number
        if let myInteger = Int(someString) as? Int{
            mCreditNSNumber = NSNumber(value:myInteger)
        }
        let formatter = NumberFormatter()
        formatter.locale = Locale.current // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
        formatter.numberStyle = .currency
        if let value = mCreditNSNumber as? NSNumber {
            if let formattedTipAmount = formatter.string(from: value as NSNumber) {
                //tipAmountLabel.text = "Tip Amount: \(formattedTipAmount)"
                var tString = formattedTipAmount.replacingOccurrences(of: ".00", with:"")
                tString = tString.replacingOccurrences(of: "$", with:"")
                tString = tString.replacingOccurrences(of: "IRR", with:"")
                tString = tString + " تومان "
                return self.replaceString(tString)
            }
        }
        return self.replaceString(number)
    }
    
    
    // get build version
    func getBuildVersion()->String{
        if let version = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            return version
        }
        return "-1"
    }
    
    // get code version
    func getCodeVersion()->String{
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            return version
        }
        return "00"
    }
    
    // get UIID from app
    func getUIID()->String{
        return  UIDevice.current.identifierForVendor!.uuidString
    }
    
    // get os version
    func getOSVersion()->String{
        return UIDevice.current.systemVersion;
    }
    
    // json to string 
    func jsonToString(json: AnyObject)-> String{
        do {
            let data1 =  try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
            let convertedString = String(data: data1, encoding: String.Encoding.utf8) // the data will be converted to the string
            //     print(convertedString)
            return convertedString! // <-- here is ur string
            
        } catch let myJSONError {
            print(myJSONError)
        }
        return ""
    }
    
}

enum Custom: DropStatable {
    case BlackYellow
    case BlueWhite

    var backgroundColor: UIColor? {
        switch self {
        case .BlackYellow: return .black
        case .BlueWhite: return .white

        }
    }
    var font: UIFont? {
        switch self {
        case .BlackYellow: return UIFont(name: "IRANSansMobile", size: 14.0)
        case .BlueWhite: return UIFont(name: "IRANSansMobile", size: 14.0)

        }
    }
    var textColor: UIColor? {
        switch self {
        case .BlackYellow: return UIColor(red: 255/255.0, green: 202/255.0, blue: 17/255.0, alpha: 1.0)
        case .BlueWhite: return UIColor(red: 35/255.0, green: 139/255.0, blue: 202/255.0, alpha: 1.0)
        }
    }
    var blurEffect: UIBlurEffect? {
        switch self {
        case .BlackYellow: return nil
        case .BlueWhite: return nil

        }
    }
}



extension UIView {
    
    func addShadowView(width:CGFloat=0.0, height:CGFloat=5.0, Opacidade:Float=0.9, maskToBounds:Bool=false, radius:CGFloat=3.0){
        self.layer.shadowColor =  UIColor(red: 0, green: 0, blue: 0, alpha: 0.35).cgColor
        self.layer.shadowOffset = CGSize(width: width, height: height)
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = Opacidade
        self.layer.masksToBounds = maskToBounds
    }
    
    func dropShadowVSearchBtn(scale: Bool = true) {
            self.layer.masksToBounds = false
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOpacity = 0.2
            self.layer.shadowOffset = CGSize(width: -0.2, height: 0)
            self.layer.shadowRadius = 10
            
            self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
            self.layer.shouldRasterize = true
            self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
        
    func dropShadow(scale: Bool = true) {
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOpacity = 0.3
            self.layer.shadowOffset = CGSize(width: 0, height: 0)
            self.layer.shadowRadius = 4
    }
        
    
    
}



extension String {
    // persian replace character
    func replaceString(_ stringNumber: String)->String{
        var token = stringNumber
        token =  token.replacingOccurrences(of: "0", with:"۰")
        token =  token.replacingOccurrences(of: "1", with:"۱")
        token =  token.replacingOccurrences(of: "2", with:"۲")
        token =  token.replacingOccurrences(of: "3", with:"۳")
        token =  token.replacingOccurrences(of: "4", with:"۴")
        token =  token.replacingOccurrences(of: "5", with:"۵")
        token =  token.replacingOccurrences(of: "6", with:"۶")
        token =  token.replacingOccurrences(of: "7", with:"۷")
        token =  token.replacingOccurrences(of: "8", with:"۸")
        token =  token.replacingOccurrences(of: "9", with:"۹")
        return token
    }
}
