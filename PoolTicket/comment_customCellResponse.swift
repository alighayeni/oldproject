//
//  comment_customCellResponse.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/10/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit

class comment_customCellResponse: UITableViewCell {
    
    
    @IBOutlet weak var commentName: UILabel!
    @IBOutlet weak var commentPublishDate: UILabel!
    @IBOutlet weak var commentDetail: UILabel!
    @IBOutlet weak var commentLikeCount: UILabel!
    @IBOutlet weak var commentDisLikeCount: UILabel!
    
    @IBOutlet weak var likeBtn:UIButton!
    @IBOutlet weak var dislikeBtn:UIButton!
    @IBOutlet weak var responseBtn:UIButton!

}
