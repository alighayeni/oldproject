//
//  textCustomRow.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/26/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit

class textCustomRow: UICollectionViewCell {

    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var content: UILabel!
    @IBOutlet weak var image: UIImageView!


}
