//
//  service_course_model.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/18/19.
//  Copyright © 2019 OneSignal. All rights reserved.
//

import Foundation
class service_course_model: NSObject {
    
    var course_id:String?
    var course_name:String?
    var pool_name:String?
    var gender:String?
    var remain_capacity:String?
    var session_num:String?
    var lat:String?
    var lng:String?
    var rating:String?
    var address:String?
    var start_datetime:String?
    var finish_datetime:String?
    var price_market:String?
    var price_sale:String?
    var pattern_days:String?
    var discount_percent:String?
    var pool_id:String?
    
    init(course_id: String, course_name: String, pool_name: String, gender: String, remain_capacity: String, session_num: String, lat: String, lng: String, rating: String, address: String, start_datetime: String, finish_datetime: String, price_market: String, price_sale: String, pattern_days: String, discount_percent: String, pool_id: String) {
        self.course_id = course_id
        self.course_name = course_name
        self.pool_name = pool_name
        self.gender = gender
        self.remain_capacity = remain_capacity
        self.session_num = session_num
        self.lat = lat
        self.lng = lng
        self.rating = rating
        self.address = address
        self.start_datetime = start_datetime
        self.finish_datetime = finish_datetime
        self.price_market = price_market
        self.price_sale = price_sale
        self.pattern_days = pattern_days
        self.discount_percent = discount_percent
        self.pool_id = pool_id
    }
    
    
    static func prepare(_ item:AnyObject) -> service_course_model{
        
        let course_id = String(describing: item["course_id"] as AnyObject)
        let course_name = String(describing: item["course_name"] as AnyObject)
        let pool_name = String(describing: item["pool_name"] as AnyObject)
        let gender = String(describing: item["gender"] as AnyObject)
        let remain_capacity = String(describing: item["remain_capacity"] as AnyObject)
        let session_num = String(describing: item["session_num"] as AnyObject)
        let lat = String(describing: item["lat"] as AnyObject)
        let lng = String(describing: item["lng"] as AnyObject)
        let rating = String(describing: item["rating"] as AnyObject)
        let address = String(describing: item["address"] as AnyObject)
        let start_datetime = String(describing: item["start_datetime"] as AnyObject)
        let finish_datetime = String(describing: item["finish_datetime"] as AnyObject)
        let price_market = String(describing: item["price_market"] as AnyObject)
        let price_sale = String(describing: item["price_sale"] as AnyObject)
        let pattern_days = String(describing: item["pattern_days"] as AnyObject)
        let discount_percent = String(describing: item["discount_percent"] as AnyObject)
        let pool_id = String(describing: item["discount_percent"] as AnyObject)
        
        
        let instance = service_course_model(course_id: course_id, course_name: course_name, pool_name: pool_name, gender: gender, remain_capacity: remain_capacity, session_num: session_num, lat: lat, lng: lng, rating: rating, address: address, start_datetime: start_datetime, finish_datetime: finish_datetime, price_market: price_market, price_sale: price_sale, pattern_days: pattern_days, discount_percent: discount_percent, pool_id:pool_id)
        return instance
    }
    
}
