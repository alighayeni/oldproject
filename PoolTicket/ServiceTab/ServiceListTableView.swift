//
//  ServiceListTableView.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/18/19.
//  Copyright © 2019 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyDrop

class ServiceListTableView: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pgTitle: UILabel!
    @IBOutlet weak var SortBtn: UIButton!
    @IBOutlet weak var filterViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var pageTitleBtn: UIButton!
    var refreshAllDataVariable = false
    var list: [AnyHashable]?
    var type = ""
    var pTitle = ""
    var selectedCity: cityModel?
    var extraQuery = ""
    var sortType = ""
    var sortTitle = [String]()
    var sortValue = [String]()
    var lat = ""
    var lng = ""
    var pageIndex = 1
    var lastPageLoaded = 0
    let count = 10


    override func viewDidLoad() {
        super.viewDidLoad()
        DataSourceManagement.filterList = []
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        // self.prepareDefaultFilter()
        
        // clear the filter
        PoolFiltersNew.poolName = ""
        PoolFiltersNew.selectedPoolTypeName = ""
        PoolFiltersNew.selectedPoolTypeId = ""
        PoolFiltersNew.selectedGenderTypePosition = 0
        
        self.tableView.register(UINib.init(nibName: "serviceCustomCellType1", bundle: nil), forCellReuseIdentifier: "serviceCustomCellType1")
        self.tableView.register(UINib.init(nibName: "serviceCustomCell", bundle: nil), forCellReuseIdentifier: "serviceCustomCell")
        self.tableView.register(UINib.init(nibName: "serviceCustomSaensCell", bundle: nil), forCellReuseIdentifier: "serviceCustomSaensCell")
        self.tableView.register(UINib.init(nibName: "serviceCustomSaensCell1", bundle: nil), forCellReuseIdentifier: "serviceCustomSaensCell1")
        
        self.navigationController?.navigationBar.barTintColor = self.setBgColor(43, green: 163, blue: 215)
        self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.font.rawValue: UIFont(name: "IRANSansMobile", size: 16),NSAttributedString.Key.foregroundColor.rawValue:UIColor.white])
        
        navigationController?.navigationBar.tintColor = UIColor.white
        self.title = ""
        
        let closeTopBarBtn = UIBarButtonItem(image: UIImage(named: "ic_filters"),style: .plain ,target: self, action: #selector(self.openFilter))
        self.navigationItem.rightBarButtonItem = closeTopBarBtn
        
        self.pgTitle.text = pTitle
        pageIndex = 1
        lastPageLoaded = 0
        getDefaultData()
        
        
        
    }
    
    func getDefaultData() {
        
        if !AppDelegate.changeFilterValue {
            if DataSourceManagement.filterList.count > 0 {
                self.collectionView.reloadData()
                self.filterViewHeightConstraint.constant = 50
            } else {
                self.filterViewHeightConstraint.constant = 0
            }
            // clear the list
            self.list = []
            self.tableView.reloadData()
            self.prepareQueryFilter()
            getDataFromServer()
        }
        
    }
    
    func getDataFromServer() {
        var access_token = SwiftUtil.getUserAccessToken()

        // load data from server
        switch type {
        case "pack":
            getPoolPackList( client_id: Constants.clientId, access_token: access_token, page: String(self.pageIndex))
        case "course":
            getPoolCourseList( client_id: Constants.clientId, access_token: access_token, page: String(self.pageIndex))
        case "saens":
            getPoolSaensList( client_id: Constants.clientId, access_token: access_token, page: String(self.pageIndex))
        default:
            break
        }
    }
    
    @objc
    func openFilter() {
         PoolFiltersNew.filterConfigType = "seans"
         self.pushNewClass(classIdentifier: "PoolFiltersNew")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
     
        self.lat = DataSourceManagement.lat
        self.lng = DataSourceManagement.lng
        
        if PoolsListVC.nearme {
            pageTitleBtn.setTitle("", for: UIControl.State.normal)
        } else {
            pageTitleBtn.setTitle("تغییر شهر", for: UIControl.State.normal)
        }
        
        if AppDelegate.changeFilterValue {
            
            if DataSourceManagement.filterList.count > 0 {
                self.collectionView.reloadData()
                self.filterViewHeightConstraint.constant = 50
            } else {
                self.filterViewHeightConstraint.constant = 0
            }
            // clear the list
            self.list = []
            self.tableView.reloadData()
            // load data from server
            pageIndex = 1
            lastPageLoaded = 0
            self.prepareQueryFilter()
            getDataFromServer()
            AppDelegate.changeFilterValue = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // make sure this filter is set to default
        PoolsListVC.nearme = false
    }
    
    func getPoolSaensList( client_id: String, access_token: String, page: String) -> Void {
        refreshAllDataVariable = true
        var tList = [service_saens_list]()
        var sortQuery = ""
        if sortType.count > 0 {
            sortQuery = "&sort_by=\(sortType)"
        }
        let Query = "access_token=\(access_token)&client_id=\(client_id)\(extraQuery)\(sortQuery)"
        let index = wsPOST.init(Constants.SAENS_LIST + "\(page)/\(count)" ,Query)
        var WSResponseStatus = false
        var sorted_by = ""
        index.start{ (indexData) in
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    if let value = Int(page) {
                        self.lastPageLoaded = value
                    }
                    if let item = json as? [String: AnyObject]{
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                if let saens = item["saens"] as? [[String:AnyObject]] {
                                    for saen in saens {
                                        tList.append(service_saens_list.prepare(saen as AnyObject))
                                    }
                                    if saens.count == self.count {
                                        self.pageIndex += 1
                                    }
                                }
                                self.sortTitle = []
                                self.sortValue = []
                                if let sort_list = item["sort_list"] as? [[String:AnyObject]] {
                                    for sortItem in sort_list {
                                        if let code = sortItem["code"] as? String, let title = sortItem["title"] as? String {
                                            self.sortTitle.append(title)
                                            self.sortValue.append(code)
                                        }
                                    }
                                }
                                if let value = item["sorted_by"] as? String {
                                    sorted_by = value
                                }
                            }
                        }
                    }
                }
            }catch{
                print("this is the error: \(error)")
            }
            DispatchQueue.main.async(execute: {
                if WSResponseStatus {
                    self.list?.append(contentsOf: tList)
                    self.tableView.reloadData()
                    let indexof = self.sortValue.firstIndex(of: sorted_by)
                    if let value = indexof {
                        self.SortBtn.setTitle(self.sortTitle[value], for: .normal)
                    }
                }
                self.refreshAllDataVariable = false
            })
        }
    }
    
    func getPoolPackList( client_id: String, access_token: String, page: String) -> Void {
        refreshAllDataVariable = true
        var tList = [service_pack_model]()
        var sortQuery = ""
        if sortType.count > 0 {
            sortQuery = "&sort_by=\(sortType)"
        }
        let Query = "access_token=\(access_token)&client_id=\(client_id)\(extraQuery)\(sortQuery)"
        let index = wsPOST.init(Constants.PACK_LIST + "\(page)/\(count)" ,Query)
        var WSResponseStatus = false
        var sorted_by = ""
        index.start{ (indexData) in
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    if let value = Int(page) {
                        self.lastPageLoaded = value
                    }
                    if let item = json as? [String: AnyObject]{
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                if let packs = item["packs"] as? [[String:AnyObject]] {
                                    for pack in packs {
                                        tList.append(service_pack_model.prepare(pack as AnyObject))
                                    }
                                    if packs.count == self.count {
                                        self.pageIndex += 1
                                    }
                                }
                                self.sortTitle = []
                                self.sortValue = []
                                if let sort_list = item["sort_list"] as? [[String:AnyObject]] {
                                    for sortItem in sort_list {
                                        if let code = sortItem["code"] as? String, let title = sortItem["title"] as? String {
                                            self.sortTitle.append(title)
                                            self.sortValue.append(code)
                                        }
                                    }
                                }
                                if let value = item["sorted_by"] as? String {
                                    sorted_by = value
                                }
                            }
                        }
                    }
                }
            }catch{
                print("this is the error: \(error)")
            }
            DispatchQueue.main.async(execute: {
                if WSResponseStatus {
                    self.list?.append(contentsOf: tList)
                    self.tableView.reloadData()
                    let indexof = self.sortValue.firstIndex(of: sorted_by)
                    if let value = indexof {
                        self.SortBtn.setTitle(self.sortTitle[value], for: .normal)
                    }
                }
                self.refreshAllDataVariable = false
            })
        }
    }
    
    func getPoolCourseList(client_id: String, access_token: String, page: String) -> Void {
        refreshAllDataVariable = true
        var sortQuery = ""
        if sortType.count > 0 {
            sortQuery = "&sort_by=\(sortType)"
        }
        let Query = "access_token=\(access_token)&client_id=\(client_id)\(extraQuery)\(sortQuery)"
        let index = wsPOST.init(Constants.COURSE_LIST + "\(page)/\(count)",Query)
        var WSResponseStatus = false
        var sorted_by = ""
        var tList = [service_course_model]()
        index.start{ (indexData) in
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    if let value = Int(page) {
                        self.lastPageLoaded = value
                    }
                    if let item = json as? [String: AnyObject]{
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                if let courses = item["courses"] as? [[String:AnyObject]] {
                                    for course in courses {
                                        tList.append(service_course_model.prepare(course as AnyObject))
                                    }
                                    if courses.count == self.count {
                                        self.pageIndex += 1
                                    }
                                }
                                self.sortTitle = []
                                self.sortValue = []
                                if let sort_list = item["sort_list"] as? [[String:AnyObject]] {
                                    for sortItem in sort_list {
                                        if let code = sortItem["code"] as? String, let title = sortItem["title"] as? String {
                                            self.sortTitle.append(title)
                                            self.sortValue.append(code)
                                        }
                                    }
                                }
                                if let value = item["sorted_by"] as? String {
                                    sorted_by = value
                                }
                            }
                        }
                    }
                }
            }catch{
                print("this is the error: \(error)")
            }
            DispatchQueue.main.async(execute: {
                if WSResponseStatus {
                    self.list?.append(contentsOf: tList)
                    self.tableView.reloadData()
                    let indexof = self.sortValue.firstIndex(of: sorted_by)
                    if let value = indexof {
                        self.SortBtn.setTitle(self.sortTitle[value], for: .normal)
                    }
                }
                self.refreshAllDataVariable = false
            })
        }
    }
    
    func prepareQueryFilter() -> Void {
        let filterList = DataSourceManagement.filterList
        extraQuery = ""
        for item in filterList {
          
            switch item.key {
            case "has_discount":
                extraQuery += "&\(item.key!)=\(item.value1!)"
            case "gender":
                if item.value1 != "2" {
                    extraQuery += "&\(item.key!)=\(item.value1!)"
                }
            case "selectedCity":
                selectedCity = DataSourceManagement.getSelectedStateAndCity()
                if let value = selectedCity, let cityId = value.city_id {
                    extraQuery += "&city_id=\(String(cityId))"
                }
            case "priceSlider":
                if let minPrice = item.value1, let maxPrice = item.value2 {
                    extraQuery += "&min_price=\(minPrice)&max_price=\(maxPrice)"
                }
            case "poolName":
                if let poolName = item.value1, poolName.count > 0 {
                     extraQuery += "&name=\(poolName)"
                }
            case "nearMe":
                // find index of old selected city in the filter
                var index = -1
                if DataSourceManagement.filterList.count > 0 {
                    for i in 0..<DataSourceManagement.filterList.count {
                        if let value = DataSourceManagement.filterList[i] as? filterObject {
                            if value.key == "selectedCity" {
                                index = i
                            }
                        }
                    }
                    // remove the old city data
                    if index != -1 {
                        DataSourceManagement.filterList.remove(at: index)
                        prepareQueryFilter()
                        continue
                    }
                    // add lat lang to extra query
                    extraQuery += "&lng=\(lng)&lat=\(lat)"
                }
                break
            case .none:
                break
            case .some(_):
                break
            }
        }
        
        
    }


    @objc
    func removeThefilter(_ button: UIButton) -> Void {
        if let index = button.tag as? Int{
            if DataSourceManagement.filterList.count > index {
                switch DataSourceManagement.filterList[index].key {
                case "poolName":
                    PoolFiltersNew.poolName = ""
                case "gender":
                    PoolFiltersNew.selectedGenderTypePosition = 0
                case "nearMe":
                    PoolsListVC.nearme = false
                    pageTitleBtn.setTitle("تغییر شهر", for: UIControl.State.normal)
                default:
                    break
                }
                DataSourceManagement.filterList.remove(at: index)
                collectionView.reloadData()
                prepareQueryFilter()
                getDefaultData()
            }
        }
    }
    
    @IBAction func opennewFilter() {
        //self.pushNewClass(classIdentifier: "PoolFiltersNew")
        DataSourceManagement.fromInbox = true
        AppDelegate.changeFilterValue = true
        openNewClass(classIdentifier: "ChooseStateAndCity")
    }
    
    @IBAction func changeSort(_ button: Any) {
        
        if self.sortTitle.count > 0 {
            self.showDurationAlert( button, self.sortTitle, self.SortBtn)
        }
        
    }

    func showDurationAlert(_ sender: Any, _ strArray: [String], _ btn: UIButton) {
        // var durations = ["bank", "doctor", "restaurant"]
        let actions = strArray.map { seconds in
            return UIAlertAction(title: "\(seconds)", style: .default) { _ in
                
                btn.setTitle( seconds, for: .normal)
                if let value = self.sortTitle.firstIndex(of: seconds){
                    self.sortType = self.sortValue[value]
                    self.getDefaultData()
                }                
                
            }
        }
        
        
        let cancelAction = UIAlertAction(title: "بستن", style: .cancel, handler: nil)
        
        let controller = UIAlertController(title: "مرتب سازی بر اساس", message: nil, preferredStyle: .actionSheet)
        for action in [cancelAction] + actions {
            controller.addAction(action)
            self.sortType = ""
        }
        showAlert(controller, sourceView: sender as? UIView)
    }
    
    func showAlert(_ controller: UIAlertController, sourceView: UIView? = nil) {
        
        if UIDevice.current.userInterfaceIdiom == .pad  {
            if let sourceView = sourceView {
                let rect = sourceView.convert(sourceView.bounds, to: view)
                controller.popoverPresentationController?.sourceView = view
                controller.popoverPresentationController?.sourceRect = rect
            }
        }
        
        present(controller, animated: true, completion: nil)
    }
    
    func upAllDrops(_ sender: AnyObject) {
        if let hidden = navigationController?.isNavigationBarHidden {
            navigationController?.setNavigationBarHidden(!hidden, animated: true)
        }
        
        Drop.upAll()
    }

}

extension ServiceListTableView: UITableViewDelegate, UITableViewDataSource {
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.list?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cellData = self.list?[indexPath.row]
        var cellDataTypePack: service_pack_model?
        var cellDataTypeCourse: service_course_model?
        var cellDataTypeSaens: service_saens_list?
        
        if let cellData = cellData as? service_pack_model {
            cellDataTypePack = cellData
        } else if let cellData = cellData as? service_course_model {
            cellDataTypeCourse = cellData
        } else if let cellData = cellData as? service_saens_list {
            cellDataTypeSaens = cellData
        }
      
        
        if let data = cellDataTypePack {
            var cell = tableView.dequeueReusableCell(withIdentifier: "serviceCustomCell", for: indexPath) as! serviceCustomCell
            if data.discount_percent != "null", data.discount_percent != "0" {
                if data.discount_percent != nil , data.discount_percent!.count > 0 {
                    cell = tableView.dequeueReusableCell(withIdentifier: "serviceCustomCell", for: indexPath) as! serviceCustomCell
                    // discount is available
                    cell.discountBg?.alpha = 1
                    cell.poolDiscount.text = "%\(replaceString(data.discount_percent!)) تخفیف"
                    // if discount is not zero, the red line show be shown on the price
                    let lineView = UIView(
                        frame: CGRect(x: 0,
                                      y: cell.poolPriceInGishe.bounds.size.height / 2,
                                      width: cell.poolPriceInGishe.bounds.size.width,
                                      height: 1
                        )
                    )
                    lineView.backgroundColor = UIColor.red;
                    cell.poolPriceInGishe.addSubview(lineView)
                    if let price = data.price_market {
                        cell.poolPriceInGishe.text = setSeprator(price)//self.replaceString(price) + " " + "تومان"
                    } else {
                         cell.poolPriceInGishe.text = ""
                    }
                    if let salePrice = data.price_sale {
                        cell.poolPrice.text = setSeprator(salePrice)//self.replaceString(salePrice) + " " + "تومان"
                    } else {
                         cell.poolPrice.text = ""
                    }
                } else {
                    // discount is not available
                    cell.discountBg?.alpha = 0
                    cell.poolDiscount.text = ""
                    if let salePrice = data.price_market {
                        cell.poolPrice.text = setSeprator(salePrice)//self.replaceString(salePrice) + " " + "تومان"
                    } else {
                         cell.poolPrice.text = ""
                    }
                }
            } else {
                if let salePrice = data.price_market {
                    cell.poolPrice.text = setSeprator(salePrice)//self.replaceString(salePrice) + " " + "تومان"
                } else {
                    cell.poolPrice.text = ""
                }
                cell.poolPriceInGishe.text = ""
                cell.poolPriceInGishe.alpha = 0
                cell.discountBg?.alpha = 0
                cell.poolDiscount.text = ""
            }
            cell.poolName.text = data.pool_name
            cell.availableForWomen.alpha = 0
            cell.availableForMan.alpha = 0

            // set title
            cell.cellTitle.text = data.pack_name
            // set address
            if "" != data.address, "<null>" != data.address {
                cell.poolAddress.text = data.address
            }else{
                cell.poolAddress.text = ""
            }
            // set gender
            switch data.gender! {
            case "0":
                cell.availableForMan.alpha = 1
                cell.availableFor_title.text = "آقایان"
                break
            case "1":
                cell.availableForWomen.alpha = 1
                cell.availableFor_title.text = "بانوان"
                break
            case "2":
                cell.availableForWomen.alpha = 1
                cell.availableForMan.alpha = 1
                cell.availableFor_title.text = "همه"
                break
            default:
                cell.availableForWomen.alpha = 0
                cell.availableForMan.alpha = 0
                cell.availableFor_title.text = ""
            }
            // start time
            if let value = data.session_expire_start {
                cell.startDate.text = "تاریخ شروع: \(self.replaceString(value))"
            } else {
                cell.startDate.text = "تاریخ شروع: "
            }
            // end time
            if let value = data.session_expire_finish {
                cell.endDate.text = "تاریخ پایان: \(self.replaceString(value))"
            } else {
                cell.endDate.text = "تاریخ پایان: "
            }
            // session count
            if let value = data.session_num, value != "<null>" {
                cell.remainingClassCount.text = "تعداد جلسه: \(self.replaceString(value))"
            } else {
                cell.remainingClassCount.text = "تعداد جلسه: "
            }
            // remain capacity
            if let value = data.remain_capacity {
                cell.remainingPeopleCount.text = "ظرفیت باقی مانده: \(self.replaceString(value))"
            } else {
                cell.remainingPeopleCount.text = "ظرفیت باقی مانده"
            }
            // set service rating
            if let value = data.rating {
                cell.poolRateBar.rating = setRatingStar(value)
            } else {
                cell.poolRateBar.rating = setRatingStar("0.0")
            }
            
            return cell
        } else if let data = cellDataTypeCourse {
            var cell = tableView.dequeueReusableCell(withIdentifier: "serviceCustomCell", for: indexPath) as! serviceCustomCell
            if data.discount_percent != "null", data.discount_percent != "0" {
                if data.discount_percent != nil , data.discount_percent!.count > 0 {
                    cell = tableView.dequeueReusableCell(withIdentifier: "serviceCustomCell", for: indexPath) as! serviceCustomCell
                    // discount is available
                    cell.discountBg?.alpha = 1
                    cell.poolDiscount.text = "%\(replaceString(data.discount_percent!)) تخفیف"
                    // if discount is not zero, the red line show be shown on the price
                    let lineView = UIView(
                        frame: CGRect(x: 0,
                                      y: cell.poolPriceInGishe.bounds.size.height / 2,
                                      width: cell.poolPriceInGishe.bounds.size.width,
                                      height: 1
                        )
                    )
                    lineView.backgroundColor = UIColor.red;
                    cell.poolPriceInGishe.addSubview(lineView)
                    if let price = data.price_market {
                        cell.poolPriceInGishe.text = setSeprator(price)//self.replaceString(price) + " " + "تومان"
                    } else {
                        cell.poolPriceInGishe.text = ""
                    }
                    if let salePrice = data.price_sale {
                        cell.poolPrice.text = setSeprator(salePrice)//self.replaceString(salePrice) + " " + "تومان"
                    } else {
                        cell.poolPriceInGishe.text = ""
                    }
                } else {
                    cell.discountBg?.alpha = 1
                    cell.poolDiscount.text = ""
                    if let salePrice = data.price_market {
                        cell.poolPrice.text = setSeprator(salePrice)//self.replaceString(salePrice) + " " + "تومان"
                    } else {
                        cell.poolPrice.text = ""
                    }
                }
            } else {
                if let salePrice = data.price_market {
                    cell.poolPrice.text = setSeprator(salePrice)//self.replaceString(salePrice) + " " + "تومان"
                } else {
                     cell.poolPrice.text = ""
                }
                cell.poolPriceInGishe.text = ""
                cell.poolPriceInGishe.alpha = 0
                cell.discountBg?.alpha = 0
                cell.poolDiscount.text = ""
            }
            cell.poolName.text = data.pool_name
            cell.availableForWomen.alpha = 0
            cell.availableForMan.alpha = 0
            
            // set title
            cell.cellTitle.text = data.course_name
            // set address
            if "" != data.address, "<null>" != data.address {
                cell.poolAddress.text = data.address
            }else{
                cell.poolAddress.text = ""
            }
            // set gender
            switch data.gender! {
            case "0":
                cell.availableForMan.alpha = 1
                cell.availableFor_title.text = "آقایان"
                break
            case "1":
                cell.availableForWomen.alpha = 1
                cell.availableFor_title.text = "بانوان"
                break
            case "2":
                cell.availableForWomen.alpha = 1
                cell.availableForMan.alpha = 1
                cell.availableFor_title.text = "همه"
                break
            default:
                cell.availableForWomen.alpha = 0
                cell.availableForMan.alpha = 0
                cell.availableFor_title.text = ""
            }
            // start time
            if let value = data.start_datetime {
                cell.startDate.text = "تاریخ شروع: \(self.replaceString(value))"
            } else {
                cell.startDate.text = "تاریخ شروع: "
            }
            // end time
            if let value = data.finish_datetime {
                cell.endDate.text = "تاریخ پایان: \(self.replaceString(value))"
            } else {
                cell.endDate.text = "تایریخ پایان: "
            }
            // session count
            if let value = data.session_num, value != "<null>" {
                cell.remainingClassCount.text = "تعداد جلسه: \(self.replaceString(value))"
            } else {
                cell.remainingClassCount.text = "تعداد جلسه: "
            }
            // remain capacity
            if let value = data.remain_capacity {
                cell.remainingPeopleCount.text = "ظرفیت باقی مانده: \(self.replaceString(value))"
            } else {
                cell.remainingPeopleCount.text = "ظرفیت باقی مانده: "
            }
            // set service rating
            if let value = data.rating {
                cell.poolRateBar.rating = setRatingStar(value)
            } else {
                cell.poolRateBar.rating = setRatingStar("0.0")
            }
            return cell
        } else if let data = cellDataTypeSaens {
            var cell = tableView.dequeueReusableCell(withIdentifier: "serviceCustomSaensCell", for: indexPath) as! serviceCustomSaensCell
            if data.discount_percent != "null", data.discount_percent != "0" {
                if data.discount_percent != nil , data.discount_percent!.count > 0 {
                    cell = tableView.dequeueReusableCell(withIdentifier: "serviceCustomSaensCell", for: indexPath) as! serviceCustomSaensCell
                    // discount is available
                    cell.discountBg?.alpha = 1
                    cell.poolDiscount.text = "%\(replaceString(data.discount_percent!)) تخفیف"
                    if let price = data.price_market {
                        // if discount is not zero, the red line show be shown on the price
                        let lineView = UIView(
                            frame: CGRect(x: 0,
                                          y: cell.poolPriceInGishe.bounds.size.height / 2,
                                          width: cell.poolPriceInGishe.bounds.size.width,
                                          height: 1
                            )
                        )
                        lineView.backgroundColor = UIColor.red;
                        cell.poolPriceInGishe.addSubview(lineView)
                        cell.poolPriceInGishe.text = setSeprator(price)//self.replaceString(price) + " " + "تومان"
                    }
                    if let salePrice = data.price_sale {
                        cell.poolPrice.text = setSeprator(salePrice)//self.replaceString(salePrice) + " " + "تومان"
                    } else {
                        cell.poolPrice.text = ""
                    }
                } else {
                    cell.discountBg?.alpha = 1
                    cell.poolDiscount.text = "%\(replaceString(data.discount_percent!)) تخفیف"
                    if let salePrice = data.price_market {
                        cell.poolPrice.text = setSeprator(salePrice)//self.replaceString(salePrice) + " " + "تومان"
                    } else {
                        cell.poolPrice.text = ""
                    }
                }
            } else {
                if let salePrice = data.price_market {
                    cell.poolPrice.text = setSeprator(salePrice)//self.replaceString(salePrice) + " " + "تومان"
                } else {
                    cell.poolPrice.text = ""
                }
                cell.poolPriceInGishe.text = ""
                cell.poolPriceInGishe.alpha = 0
                cell.discountBg?.alpha = 0
                cell.poolDiscount.text = ""
            }
            cell.poolName.text = data.pool_name
            cell.availableForWomen.alpha = 0
            cell.availableForMan.alpha = 0
            // set title
            cell.cellTitle.text = data.saens_name
            // set address
            if "" != data.address, "<null>" != data.address {
                cell.poolAddress.text = data.address
            }else{
                cell.poolAddress.text = ""
            }
            // set gender
            switch data.gender! {
            case "0":
                cell.availableForMan.alpha = 1
                cell.availableFor_title.text = "آقایان"
                break
            case "1":
                cell.availableForWomen.alpha = 1
                cell.availableFor_title.text = "بانوان"
                break
            case "2":
                cell.availableForWomen.alpha = 1
                cell.availableForMan.alpha = 1
                cell.availableFor_title.text = "آقایان و بانوان"
                break
            default:
                cell.availableForWomen.alpha = 0
                cell.availableForMan.alpha = 0
                cell.availableFor_title.text = ""
            }
            // set service rating
            if let value = data.rating {
                cell.poolRateBar.rating = setRatingStar(value)
            } else {
                cell.poolRateBar.rating = setRatingStar("0.0")
            }
            
            return cell
        }
        
      
        
//        let cellData = data as? service_pack_model {
//
//        } else
//
//        if cellData.discount_percent != "null", cellData.discount_percent != "0" {
//            if cellData.discount_percent != nil , cellData.discount_percent!.count > 0 {
//
//            }
//        }
//
//        var  cell = tableView.dequeueReusableCell(withIdentifier: "serviceCustomCellType1", for: indexPath) as! serviceCustomCellType1
//
//        if let cellData = data as? service_pack_model {
//
//
//
//
//            cell.cellTitle.text = cellData.pack_name
//
//            if "" != cellData.address, "<null>" != cellData.address {
//                cell.poolAddress.text = cellData.address
//            }else{
//                cell.poolAddress.text = ""
//            }
//
//            switch cellData.gender! {
//            case "0":
//                cell.availableForMan.alpha = 1
//                cell.availableFor_title.text = "آقایان"
//                break
//            case "1":
//                cell.availableForWomen.alpha = 1
//                cell.availableFor_title.text = "بانوان"
//                break
//            case "2":
//                cell.availableForWomen.alpha = 1
//                cell.availableForMan.alpha = 1
//                cell.availableFor_title.text = "آقایان و بانوان"
//                break
//            default:
//                cell.availableForWomen.alpha = 0
//                cell.availableForMan.alpha = 0
//                cell.availableFor_title.text = ""
//            }
//
//            if cellData.discount_percent != "null", cellData.discount_percent != "0" {
//                if cellData.discount_percent != nil , cellData.discount_percent!.count > 0 {
//                    cell.poolDiscount.text = "%"+replaceString(data.discount_percent!)
//                }else{
//                    cell.poolDiscount.text = ""
//                }
//            }else{
//                cell.poolDiscount.text = ""
//            }
//
//
//        } else if let cellData = data as? service_course_model {
//            cell.cellTitle.text = cellData.course_name
//        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cellData = self.list?[indexPath.row]
        var cellDataTypePack: service_pack_model?
        var cellDataTypeCourse: service_course_model?
        var cellDataTypeSaens: service_saens_list?
        
        if let cellData = cellData as? service_pack_model {
            cellDataTypePack = cellData
        } else if let cellData = cellData as? service_course_model {
            cellDataTypeCourse = cellData
        } else if let cellData = cellData as? service_saens_list {
            cellDataTypeSaens = cellData
        }
        
        if let data = cellDataTypePack, let id = data.pool_id {
            DataSourceManagement.selectedPoolID = id
            let viewController:Pools_Pack_list = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Pools_Pack_list") as! Pools_Pack_list
            viewController.select_pack_id = data.pack_id!
            viewController.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(viewController, animated: true)
        } else if let data = cellDataTypeCourse, let id = data.pool_id {
            DataSourceManagement.selectedPoolID = id
            let viewController:Pools_Course_list = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Pool_Course_list") as! Pools_Course_list
            viewController.select_course_id = data.course_id!
            viewController.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(viewController, animated: true)
        } else if let data = cellDataTypeSaens, let id = data.pool_id {
            DataSourceManagement.selectedPoolID = id
            let viewController:Pools_Saens_list = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Pools_Saens_list") as! Pools_Saens_list
            viewController.select_saens_id = data.saens_id!
            viewController.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(viewController, animated: true)
        }
//                    self.pushNewClass(classIdentifier: "Pools_Pack_list")
//
//                    self.pushNewClass(classIdentifier: "Pools_Saens_list")
//
//                    self.pushNewClass(classIdentifier: "Pool_Course_list")
        
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        if let list = self.list {
            let lastElement = list.count - 1
            if indexPath.row == lastElement, !refreshAllDataVariable {
                print(">>>>", "Next Page")
                if lastPageLoaded < pageIndex {
                    getDataFromServer()
                }
            }
        }
        
    }
}

extension ServiceListTableView: UICollectionViewDelegate, UICollectionViewDataSource {
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DataSourceManagement.filterList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "filterCollectionViewCell", for: indexPath) as! filterCollectionViewCell
        let data = DataSourceManagement.filterList[indexPath.row]
        cell.labelName.text = data.name
        cell.removeItemFromFilter.tag = indexPath.row
        cell.removeItemFromFilter.addTarget(self, action: #selector(self.removeThefilter(_:)), for: .touchUpInside)
        return cell
    }
    
}

extension ServiceListTableView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let data = DataSourceManagement.filterList[indexPath.row].name
        let widthlenght = data?.width(withConstrainedHeight: 50.0, font: UIFont.init(name: "IRANSansMobile", size: 17.5)!) ?? 0.0
        return CGSize.init(width: (widthlenght + 70), height: 50)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
