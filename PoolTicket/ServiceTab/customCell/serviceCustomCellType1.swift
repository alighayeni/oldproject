//
//  serviceCustomCellType1.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/20/19.
//  Copyright © 2019 OneSignal. All rights reserved.
//

import UIKit
import Cosmos

class serviceCustomCellType1: UITableViewCell {

    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet var poolRateBar: CosmosView!
    
    @IBOutlet weak var availableForWomen: UIImageView!
    @IBOutlet weak var availableForMan: UIImageView!
    @IBOutlet weak var availableFor_title: UILabel!
    @IBOutlet weak var poolAddress: UILabel!
    @IBOutlet weak var poolName: UILabel!
    
    @IBOutlet weak var poolPrice: UILabel!
    @IBOutlet weak var remainingPeopleCount: UILabel!
    @IBOutlet weak var remainingClassCount: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var endDate: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
