//
//  File.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 5/4/19.
//  Copyright © 2019 OneSignal. All rights reserved.
//

import Foundation
import UIKit

class filterCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var removeItemFromFilter: UIButton!
    
}
