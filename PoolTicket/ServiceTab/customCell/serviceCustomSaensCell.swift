//
//  serviceCustomSaensCell.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 5/13/19.
//  Copyright © 2019 OneSignal. All rights reserved.
//

import UIKit
import Cosmos

class serviceCustomSaensCell: UITableViewCell {

    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet var poolRateBar: CosmosView!
    @IBOutlet weak var poolDiscount: UILabel!
    @IBOutlet weak var availableForWomen: UIImageView!
    @IBOutlet weak var availableForMan: UIImageView!
    @IBOutlet weak var availableFor_title: UILabel!
    @IBOutlet weak var poolAddress: UILabel!
    @IBOutlet weak var poolName: UILabel!
    @IBOutlet weak var poolPrice: UILabel!
    @IBOutlet weak var poolPriceInGishe: UILabel!
    @IBOutlet weak var discountBg: UIView!


    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
