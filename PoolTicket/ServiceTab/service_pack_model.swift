//
//  service_pack_model.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/18/19.
//  Copyright © 2019 OneSignal. All rights reserved.
//

import Foundation
class service_pack_model: NSObject {
    
    var pack_id:String?
    var pack_name:String?
    var pool_name:String?
    var gender:String?
    var remain_capacity:String?
    var session_num:String?
    var lat:String?
    var lng:String?
    var address:String?
    var price_market:String?
    var price_sale:String?
    var pattern_days:String?
    var discount_percent:String?
    var session_expire_start:String?
    var session_expire_finish:String?
    var pool_id:String?
    var rating:String?

    
    init(pack_id: String, pack_name: String, pool_name: String, gender: String, remain_capacity: String, session_num: String, lat: String, lng: String, address: String, price_market: String, price_sale: String, pattern_days: String, discount_percent: String, session_expire_start: String, session_expire_finish: String, pool_id: String, rating: String) {
        self.pack_id = pack_id
        self.pack_name = pack_name
        self.pool_name = pool_name
        self.gender = gender
        self.remain_capacity = remain_capacity
        self.session_num = session_num
        self.lat = lat
        self.lng = lng
        self.address = address
        self.price_market = price_market
        self.price_sale = price_sale
        self.pattern_days = pattern_days
        self.discount_percent = discount_percent
        self.session_expire_start = session_expire_start
        self.session_expire_finish = session_expire_finish
        self.pool_id = pool_id
        self.rating = rating
    }
    
    
    static func prepare(_ item:AnyObject) -> service_pack_model{
        
        let pack_id = String(describing: item["pack_id"] as AnyObject)
        let pack_name = String(describing: item["pack_name"] as AnyObject)
        let pool_name = String(describing: item["pool_name"] as AnyObject)
        let gender = String(describing: item["gender"] as AnyObject)
        let remain_capacity = String(describing: item["remain_capacity"] as AnyObject)
        let session_num = String(describing: item["session_num"] as AnyObject)
        let lat = String(describing: item["lat"] as AnyObject)
        let lng = String(describing: item["lng"] as AnyObject)
        let address = String(describing: item["address"] as AnyObject)
        let price_market = String(describing: item["price_market"] as AnyObject)
        let price_sale = String(describing: item["price_sale"] as AnyObject)
        let pattern_days = String(describing: item["pattern_days"] as AnyObject)
        let discount_percent = String(describing: item["discount_percent"] as AnyObject)
        let session_expire_start = String(describing: item["session_expire_start"] as AnyObject)
        let session_expire_finish = String(describing: item["session_expire_finish"] as AnyObject)
        let pool_id = String(describing: item["pool_id"] as AnyObject)
        let rating = String(describing: item["rating"] as AnyObject)

        
        let instance = service_pack_model(pack_id: pack_id, pack_name: pack_name, pool_name: pool_name, gender: gender, remain_capacity: remain_capacity, session_num: session_num, lat: lat, lng: lng, address: address, price_market: price_market, price_sale: price_sale, pattern_days: pattern_days, discount_percent: discount_percent, session_expire_start:session_expire_start, session_expire_finish:session_expire_finish, pool_id:pool_id, rating: rating)
        return instance
    }
    
}
