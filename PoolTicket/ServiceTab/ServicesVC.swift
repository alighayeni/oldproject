//
//  ServicesVC.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/13/19.
//  Copyright © 2019 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class ServicesVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.navigationController?.navigationBar.barTintColor = self.setBgColor(43, green: 163, blue: 215)
        self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.font.rawValue: UIFont(name: "IRANSansMobile", size: 16),NSAttributedString.Key.foregroundColor.rawValue:UIColor.white])
        self.title = "خدمات"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
}

extension ServicesVC: UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//      pushNewClass(classIdentifier: "ServiceListTableView")
        var position = ""
        var pageTitle = ""

        switch indexPath.row {
        case 0:
            position = "saens"
            pageTitle = "رزرو بلیت"
            Analytics.logEvent("open_reservation_service", parameters: nil)
            AppDelegate()._manager?.track("open_reservation_service", data: [:])
        case 1:
            position = "course"
            pageTitle = "کلاس شنا"
            AppDelegate()._manager?.track("open_course_service_list", data: [:])
            Analytics.logEvent("open_course_service_list", parameters: nil)
        case 2:
            position = "pack"
            pageTitle = "پک"
            AppDelegate()._manager?.track("open_pack_service_list", data: [:])
            Analytics.logEvent("open_pack_service_list", parameters: nil)
        case 3:
            pageTitle = "ماساژ"
        default:
            break
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ServiceListTableView") as! ServiceListTableView
            viewController.type = position
            viewController.pTitle = pageTitle
            viewController.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(viewController, animated: true)

        
    }
    
    @objc
    func tapOnMoreInfo(_ sender: Any) {
        
        let tObject = (sender as AnyObject).tag
        if let value = tObject as? Int {
        switch value {
        case 0:
            ServiceDescriptionController.serviceType = "ticket"
        case 1:
            ServiceDescriptionController.serviceType = "course"
        case 2:
            ServiceDescriptionController.serviceType = "pack"
        case 3:
            break
        default:
            break
        }
        pushNewClass(classIdentifier: "moreInfo")
        }
        
    }
    
}

extension ServicesVC: UITableViewDataSource {
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath) as! ServiceCellMain
            cell.infoBtn.tag = indexPath.row
            cell.infoBtn.addTarget(self, action: #selector(self.tapOnMoreInfo), for: UIControl.Event.touchUpInside)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell2", for: indexPath) as! ServiceCellMain
            cell.infoBtn.tag = indexPath.row
            cell.infoBtn.addTarget(self, action: #selector(self.tapOnMoreInfo), for: UIControl.Event.touchUpInside)
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell3", for: indexPath) as! ServiceCellMain
            cell.infoBtn.tag = indexPath.row
            cell.infoBtn.addTarget(self, action: #selector(self.tapOnMoreInfo), for: UIControl.Event.touchUpInside)
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell4", for: indexPath) as! ServiceCellMain
            cell.infoBtn.tag = indexPath.row
            cell.infoBtn.addTarget(self, action: #selector(self.tapOnMoreInfo), for: UIControl.Event.touchUpInside)
            return cell
        default:
            break
        }
        return UITableViewCell()
    }
    
    
}



// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
