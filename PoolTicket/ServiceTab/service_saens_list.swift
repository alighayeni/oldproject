//
//  service_saens_list.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 5/12/19.
//  Copyright © 2019 OneSignal. All rights reserved.
//

import Foundation

class service_saens_list: NSObject {
    
    var address:String?
    var discount_percent:String?
    var gender:String?
    var lat:String?
    var lng:String?
    var pool_name:String?
    var price_market:String?
    var price_sale:String?
    var rating:String?
    var saens_id:String?
    var saens_name:String?
    var pool_id: String?
  
    init(saens_id: String, pool_name: String, gender: String,  lat: String, lng: String, address: String, price_market: String, price_sale: String, discount_percent: String, rating: String, saens_name: String, pool_id: String) {
        self.saens_id = saens_id
        self.pool_name = pool_name
        self.gender = gender
        self.lat = lat
        self.lng = lng
        self.address = address
        self.price_market = price_market
        self.price_sale = price_sale
        self.discount_percent = discount_percent
        self.rating = rating
        self.saens_name = saens_name
        self.pool_id = pool_id
    }
    
    static func prepare(_ item:AnyObject) -> service_saens_list{
        
        let saens_id = String(describing: item["saens_id"] as AnyObject)
        let pool_name = String(describing: item["pool_name"] as AnyObject)
        let gender = String(describing: item["gender"] as AnyObject)
        let lat = String(describing: item["lat"] as AnyObject)
        let lng = String(describing: item["lng"] as AnyObject)
        let address = String(describing: item["address"] as AnyObject)
        let price_market = String(describing: item["price_market"] as AnyObject)
        let price_sale = String(describing: item["price_sale"] as AnyObject)
        let discount_percent = String(describing: item["discount_percent"] as AnyObject)
        let rating = String(describing: item["rating"] as AnyObject)
        let saens_name = String(describing: item["saens_name"] as AnyObject)
        let pool_id = String(describing: item["pool_id"] as AnyObject)

        
        let instance = service_saens_list(saens_id: saens_id, pool_name: pool_name, gender: gender, lat: lat, lng: lng, address: address, price_market: price_market, price_sale: price_sale, discount_percent: discount_percent, rating: rating, saens_name: saens_name, pool_id:pool_id)
        return instance
    }

}
