//
//  ResponseComments04CustomRow.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/18/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import Cosmos
import UIKit


class ResponseComments04CustomRow: UITableViewCell {
    
    
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var starRate: CosmosView!

    override func awakeFromNib() {
        starRate.settings.fillMode = .half
    }
    
}
