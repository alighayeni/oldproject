//
//  pool_attribute_cell.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 3/26/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import Cosmos

class pool_attribute_cell: UITableViewCell {
    

    @IBOutlet var attributeRateBar: CosmosView!
    @IBOutlet weak var attributeName: UILabel!
    @IBOutlet weak var attributeValue: UILabel!
    
    override func awakeFromNib() {
        attributeRateBar.settings.fillMode = .half
    }
    
}
