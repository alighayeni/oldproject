//
//  favoriteCustomRow.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/28/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import Cosmos

class favoriteCustomRow: UITableViewCell {
    
    @IBOutlet var poolRateBar: CosmosView!
    @IBOutlet weak var favoriteTitle: UILabel!
    @IBOutlet weak var availableForWomen: UIImageView!
    @IBOutlet weak var availableForMan: UIImageView!
    @IBOutlet weak var favoriteAddress: UILabel!

    override func awakeFromNib() {
        poolRateBar.settings.fillMode = .half
    }
    
}
