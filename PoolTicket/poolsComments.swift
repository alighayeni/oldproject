//
//  poolsComments.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 3/31/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation

class poolsComments: NSObject {
    
    var pool_comment_id:String?
    var content:String?
    var created_at:String?
    var parent:String?
    var user_name:String?
    var positive_points:AnyObject?
    var negative_points:AnyObject?
    var count_like:String?
    var count_dislike:String?
    var is_liked:String?
    var rating:AnyObject?
    var childs:AnyObject?
    var type:String?
    var inResponseWith:String?




    init(_ type:String, _ pool_comment_id:String,_ content:String,_ created_at:String,_ parent:String,_ user_name:String,_ positive_points:AnyObject,_ negative_points:AnyObject, _ count_like:String, _ count_dislike:String, _ is_liked:String, _ rating: AnyObject, _ childs: AnyObject, _ inResponseWith: String) {
        self.pool_comment_id = pool_comment_id
        self.content = content
        self.created_at = created_at
        self.parent = parent
        self.user_name = user_name
        self.positive_points = positive_points
        self.negative_points = negative_points
        self.count_like = count_like
        self.count_dislike = count_dislike
        self.is_liked = is_liked
        self.rating =  rating
        self.childs = childs
        self.type = type
        self.inResponseWith = inResponseWith
    }
    
    static func prepare(_ item:AnyObject,_ type: String) -> poolsComments{
        let pool_comment_id = String(describing: item["pool_comment_id"] as AnyObject)
        let content = String(describing: item["content"] as AnyObject)
        let created_at = String(describing: item["created_at"] as AnyObject)
        let parent = String(describing: item["parent"] as AnyObject)
        let user_name = String(describing: item["user_name"] as AnyObject)
        let positive_points = item["positive_points"] as AnyObject//String(describing: item["content"] as AnyObject)
        let negative_points = item["negative_points"] as AnyObject
        let count_like = String(describing: item["count_like"] as AnyObject)
        let count_dislike = String(describing: item["count_dislike"] as AnyObject)
        let is_liked = String(describing: item["is_liked"] as AnyObject)
        let rating = item["rating"] as AnyObject
        let childs = item["childs"] as AnyObject
        let type = type
        let inResponseWith = "-1"
        let instance = poolsComments(type, pool_comment_id, content, created_at, parent, user_name, positive_points, negative_points, count_like, count_dislike, is_liked, rating, childs, inResponseWith)
        return instance
    }
    
    static func prepareChild(_ item:AnyObject,_ type: String, _ inResponseWith: String) -> poolsComments{
        let pool_comment_id = String(describing: item["pool_comment_id"] as AnyObject)
        var content = String(describing: item["content"] as AnyObject)
        content = content.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
        
        let created_at = String(describing: item["created_at"] as AnyObject)
        let parent = String(describing: item["parent"] as AnyObject)
        let user_name = String(describing: item["user_name"] as AnyObject)
        let positive_points = "-1" as AnyObject
        let negative_points = "-1" as AnyObject
        let count_like = String(describing: item["count_like"] as AnyObject)
        let count_dislike = String(describing: item["count_dislike"] as AnyObject)
        let is_liked = String(describing: item["is_liked"] as AnyObject)
        let rating = "-1" as AnyObject
        let childs = item["childs"] as AnyObject
        let inResponseWith = inResponseWith
        let type = type
        let instance = poolsComments(type, pool_comment_id, content, created_at, parent, user_name, positive_points, negative_points, count_like, count_dislike, is_liked, rating, childs, inResponseWith)
        return instance
    }
    
  
}
