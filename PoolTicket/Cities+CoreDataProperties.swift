//
//  Cities+CoreDataProperties.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/18/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//
//

import Foundation
import CoreData


extension Cities {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Cities> {
        return NSFetchRequest<Cities>(entityName: "Cities")
    }

    @NSManaged public var city_id: String?
    @NSManaged public var city_name: String?
    @NSManaged public var state_id: String?
    @NSManaged public var state_name: String?

}
