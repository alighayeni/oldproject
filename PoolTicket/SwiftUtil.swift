//
//  SwiftUtil.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 11/23/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit

open class SwiftUtil: NSObject {
    
    public static let shared = SwiftUtil()
    
    public static func openUrlInPhoneBrowser(_ urlString: String) {
        if let customURL = URL(string: urlString) {
            if UIApplication.shared.canOpenURL(customURL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(customURL)
                } else {
                    UIApplication.shared.openURL(customURL)
                }
            }
        }
    }
    
    public static func getUserAccessToken()->String{
        let defaults = UserDefaults.standard
        if let value = defaults.string(forKey: UserDefaults.Keys.AccessTokenTitle) as? String {
            if value != "" {
                Constants.Access_Token = value
                return value
            }
        }
        return ""
    }
    
}
//
//extension UIView {
//    
//    @IBInspectable var cornerRadius: CGFloat {
//        get {
//            return layer.cornerRadius
//        }
//        set {
//            layer.cornerRadius = newValue
//            layer.masksToBounds = newValue > 0
//        }
//    }
//    
//    @IBInspectable var borderWidth: CGFloat {
//        get {
//            return layer.borderWidth
//        }
//        set {
//            layer.borderWidth = newValue
//        }
//    }
//    
//    @IBInspectable var borderColor: UIColor? {
//        get {
//            return UIColor(cgColor: layer.borderColor!)
//        }
//        set {
//            layer.borderColor = newValue?.cgColor
//        }
//    }
//}
