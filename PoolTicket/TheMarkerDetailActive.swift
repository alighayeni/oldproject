//
//        class func instanceFromNib() -> TheMarkerDetailInActive {         return UINib(nibName: "TheMarkerDetailInActive", bundle: nil).swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 3/7/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import UIKit

class TheMarkerDetailActive: UIView {
    
    @IBOutlet weak var pTitle: UILabel!
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    class func instanceFromNib() -> TheMarkerDetailActive {
        return UINib(nibName: "TheMarkerDetailActive", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! TheMarkerDetailActive
    }
}
