//
//  reservePackEndCustomRow.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/5/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
class reservePackEndCustomRow: UITableViewCell {
    
    
    @IBOutlet weak var packPatternPrice: UILabel!
    @IBOutlet weak var packPatternTitle: UILabel!
    @IBOutlet weak var packPatternPriceWithDiscount: UILabel!
    @IBOutlet weak var packPatternDiscountRate: UILabel!
    @IBOutlet weak var packPatternReservePack: UIButton!
    @IBOutlet weak var mainView: UIView!

    override func awakeFromNib() {
        
        mainView.layer.borderWidth = 1.0
        mainView.layer.borderColor = UIColor.lightGray.cgColor
        mainView.clipsToBounds = true
      
    }

}
