//
//  States+CoreDataProperties.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/13/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//
//

import Foundation
import CoreData


extension States {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<States> {
        return NSFetchRequest<States>(entityName: "States")
    }


    @NSManaged public var state_id: String?
    @NSManaged public var state_name: String?

   
    
}
