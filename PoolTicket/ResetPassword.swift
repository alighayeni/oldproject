//
//  ResetPassword.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/18/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyDrop
import SCLAlertView


class ResetPassword: UIViewController, UITextFieldDelegate {
    
    
    var seconds = 0 //This variable will hold a starting value of seconds. It could be any amount above 0.
    var timer = Timer()
    var isTimerRunning = false //This will be used to make sure only one timer is created at a time.
    var tryToGetCode = 1

    var connectionStatus = false
    @IBOutlet weak var username:UITextField!
    @IBOutlet weak var newPassword1:UITextField!
    @IBOutlet weak var newPasswprd2:UITextField!
//    @IBOutlet weak var getAnotherCodeBtn:UIButton!
//    @IBOutlet weak var remainingTime:UILabel!
    @IBOutlet weak var showAndHidePassword1:UIButton!
    @IBOutlet weak var showAndHidePassword2:UIButton!

//
//    func runTimer() {
//        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
//    }
//
//    func updateTimer() {
//        seconds -= 1     //This will decrement(count down)the seconds.
//
//        if seconds == 0 {
//            timer.invalidate()
//            self.remainingTime.text = ""
//        }else{
//
//            self.remainingTime.text = "(\(seconds))"
//        }
//    }
//
    
    @IBAction func showAndHidePassword1Action(_ Sender: Any){

        if !newPassword1.isSecureTextEntry  {
            newPassword1.isSecureTextEntry = true
            self.showAndHidePassword1.setImage(UIImage(named: "ic_pass_show")!, for: UIControl.State.normal)
        }else{
            newPassword1.isSecureTextEntry = false
            self.showAndHidePassword1.setImage(UIImage(named: "ic_pass_hide")!, for: UIControl.State.normal)

        }

    }
    
    @IBAction func showAndHidePassword2Action(_ Sender: Any){
        
        if !newPasswprd2.isSecureTextEntry  {
            newPasswprd2.isSecureTextEntry = true
            self.showAndHidePassword2.setImage(UIImage(named: "ic_pass_show")!, for: UIControl.State.normal)
        }else{
            newPasswprd2.isSecureTextEntry = false
            self.showAndHidePassword2.setImage(UIImage(named: "ic_pass_hide")!, for: UIControl.State.normal)
            
        }
        
    }
    
    @IBAction func changePassword(){
        
        guard let username = username.text else {
            self.showDropMessage("missing username")
            return
        }
        guard let newPassword1 = newPassword1.text else {
            self.showDropMessage("missing password1")
            return
        }
        if newPassword1.count <= 5 {
            self.showDropMessage("تعداد کاراکتر های وارد شده کلمه عبور اشتباه می باشد.")
            return
        }
        guard let newPasswprd2 = newPasswprd2.text else {
            self.showDropMessage("missing password 2")
            return
        }
        if newPasswprd2.count <= 5 {
            self.showDropMessage("تعداد کاراکتر های وارد شده تکرار کلمه عبور اشتباه می باشد.")
            return
        }
        if newPassword1 != newPasswprd2 {
            self.showDropMessage("تکرار رمز عبور صحیح نمی باشد.")
            return
        }
        guard let token = shareData.shared.forgetPassApprovedToken else {
            print("not set")
            return
        }
        if !connectionStatus {
            self.resetPassword(username, newPassword1, token)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        username.delegate = self
        username.tag = 1
        doneWithKeyboard(username)
        
        newPassword1.delegate = self
        newPassword1.tag = 2
        doneWithKeyboard(newPassword1)
        
        newPasswprd2.delegate = self
        newPasswprd2.tag = 3
        doneWithKeyboard(newPasswprd2)
        
       
        
        if let value = shareData.shared.loginInputPhoneNumber as? String {
            self.username.text = value
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        shareData.shared.forgetPassApprovedToken = nil
    }
    
//    @IBAction func getAnotherCode(){
//
//        if self.tryToGetCode < 3 {
//            if !connectionStatus, seconds <= 0 {
//                if let value = shareData.shared.loginInputPhoneNumber as? String {
//                   forgottenPassword(value)
//                }
//            }else{
//                showDropMessage("لطفا شکیبا باشید.")
//            }
//        }else{
//            showDropMessage("شما مجاز به ثبت درخواست مجدد نیستید.")
//        }
//
//    }
    
//    func checkToken(_ username: String,_ password: String,  _ token: String ) -> Void {
//        let client_id = Constants.clientId
//        let grant_type = "check";
//        let scope = "front";
//        LoadingOverlayV2.sharedV2.showOverlayV2(view: self.view, message: "در حال تغییر رمز عبور")
//
//        let index = wsPOST.init(Constants.PASSWORD,"client_id=\(client_id)&grant_type=\(grant_type)&username=\(username)&scope=\(scope)&token=\(token)")
//        var WSResponseStatus = false
//        index.start{ (indexData) in
//
//            do{
//                if let indexValue = indexData {
//                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
//                    if let item = json as? [String: AnyObject]{
//                        if let value = item["status"] as? Int {
//                            if value == 1 {
//                                WSResponseStatus = true
//                            }
//                        }
//                    }
//
//                }
//            }catch{
//                print("this is the error: \(error)")
//            }
//
//            DispatchQueue.main.async(execute: {
//                 LoadingOverlayV2.sharedV2.hideOverlayView()
//                                if WSResponseStatus {
//                                    self.resetPassword(username, password, token)
//                                }else{
//                                    self.showDropMessage("لطفا فیلد های لازم را بررسی کرده و مجددا درخواست دهید.")
//                                }
//            })
//
//        }
//    }
    
//    func forgottenPassword(_ username: String) -> Void {
//        self.connectionStatus = true
//        LoadingOverlayV2.sharedV2.showOverlayV2(view: self.view, message: "درخواست مجدد کد")
//        let client_id = Constants.clientId
//        let grant_type = "forgotten";
//        let scope = "front";
//
//        let index = wsPOST.init(Constants.PASSWORD,"client_id=\(client_id)&grant_type=\(grant_type)&username=\(username)&scope=\(scope)")
//
//        var WSResponseStatus = false
//        index.start{ (indexData) in
//
//            var requestTime = 0
//            do{
//                if let indexValue = indexData {
//                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
//                    
//                    if let item = json as? [String: AnyObject]{
//
//                        if let value = item["status"] as? String {
//                            if value == "1" {
//                                WSResponseStatus = true
//                            }
//                        }
//
//                        if let value = item["available_in"] as? Int {
//                          requestTime = value
//                        }
//                    }
//
//                }
//            }catch{
//                print("this is the error: \(error)")
//            }
//
//            DispatchQueue.main.async(execute: {
//                self.connectionStatus = false
//                 LoadingOverlayV2.sharedV2.hideOverlayView()
//                if WSResponseStatus {
//                    if requestTime == 60 {
//                        self.showDropMessage("درخواست شما با موفقیت ثبت شد.")
//                        self.tryToGetCode += 1
//
//                        if self.tryToGetCode == 3 {
//                            self.getAnotherCodeBtn.setTitle("تعداد مجاز درخواست کد تکمیل شد.", for: .normal)
//                        }
//                    }else{
//                        if let value = requestTime as? Int {
//                            self.timer.invalidate()
//                            self.seconds = value
//                            self.runTimer()
//                        }
//                    }
//                }else{
//                    self.showDropMessage("خطا در ثبت درخواست کد")
//                }
//            })
//
//        }
//
//
//    }
    
    func resetPassword(_ username: String, _ password: String, _ token: String ) -> Void {
        let client_id = Constants.clientId
        let grant_type = "reset";
        let front = "front";
        self.connectionStatus = true
        LoadingOverlayV2.sharedV2.showOverlayV2(view: self.view, message: "در حال تغییر رمز عبور")
        let index = wsPOST.init(Constants.PASSWORD,"client_id=\(client_id)&grant_type=\(grant_type)&username=\(username)&password=\(password)&scope=\(front)&token=\(token)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            var error = ""
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = String(describing: item["status"] as AnyObject) as? String {
                            if value == "1" {
                                WSResponseStatus = true
                            }
                        }
                        
                        if let value = item["error"] as? String {
                           error = value
                        }
                        
                    }
                    
                }
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                self.connectionStatus = false
                LoadingOverlayV2.sharedV2.hideOverlayView()
                if WSResponseStatus {
                  self.getLogin(username,password)
                }else{
                    if error == "invalid_token" {
                    self.showDropMessage("کد وارد شد صحیح نمی باشد.")
                    }
                }
            })
            
        }
    }
    
    func getLogin(_ username: String, _ password: String) -> Void {
        
        self.connectionStatus = true
        LoadingOverlayV2.sharedV2.showOverlayV2(view: self.view, message: "بررسی تلفن همراه و رمز عبور")
        
        var deviceId = ""
        if let value = KeychainService.loadDeviceId(), value != nil {
            deviceId = String(value)
        }
        
        
        let params : [String : String]  = ["platform_type" : "mobile_application", "os_type" : "ios", "os_version": getOSVersion(), "code_version": self.getBuildVersion(), "build_version": self.getCodeVersion(), "device_name": UIDevice().type.rawValue, "imei": deviceId]
        
     
        let client_id = Constants.clientId
        let front = "front";
        let grant_type = "password";
        let index = wsPOST.init(Constants.TOKEN,"client_id=\(client_id)&grant_type=\(grant_type)&username=\(username)&password=\(password)&scope=\(front)&metadata=\(jsonToString(json: (params as? AnyObject)!))&device_id=\(deviceId)")
        
        
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                // set the username for Onsignal and Analytics Tag
                                self.setUserName(username)
                                
                                if let value = item["access_token"] as? String {
                                    self.setUserAccessToken(value)
                                }
                                
                                if let value = item["refresh_token"] as? String {
                                    self.setUserRefreshToken(value)
                                }
                                
                                if let value = item["id"] as? Int {
                                    self.setUserId(String(value))
                                }
                                
                                do {
                                    shareData.shared.RTUModel  = refreshTokenUsersModel.prepare(item["user"] as AnyObject)
                                }catch{
                                    fatalError("this is error \(error)")
                                }
                            }
                        }
                    }
                    
                }
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                self.connectionStatus = false
                LoadingOverlayV2.sharedV2.hideOverlayView()
                
                if WSResponseStatus {
                    self.showLoginAlert("success")
                    self.dismiss(animated: true, completion: nil)
                }else{
                    self.showLoginAlert("fail")
                }
            })
            
        }
        
        
    }
    
    
    
    
    
    //************** softkeyboard tool bar **********************//
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField
        {
        case username:
            username.resignFirstResponder()
            newPassword1.becomeFirstResponder()
            break
       
        case newPassword1:
            newPassword1.resignFirstResponder()
            newPasswprd2.becomeFirstResponder()
            break
            
        case newPasswprd2:
            newPasswprd2.resignFirstResponder()
            username.becomeFirstResponder()
            break
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
    func doneWithKeyboard(_ sender: UITextField){
        
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        keyboardToolbar.isTranslucent = true
        keyboardToolbar.barTintColor = UIColor.white
        
        let button = UIButton(type: .system)
        button.setTitle("بستن", for: .normal)
        button.sizeToFit()
        button.titleLabel?.font = UIFont(name: "IRANSansMobile", size: 13)
        button.tintColor = UIColor.black
        button.addTarget(self, action: #selector(closeKeyboard(_:)), for: .touchUpInside)
        
        
        let btnUp = UIButton(type: .custom)
        btnUp.setImage(UIImage(named: "ic_keyboard_arrow_up"), for: .normal)
        btnUp.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        btnUp.tag = sender.tag
        btnUp.addTarget(self, action: #selector(textFieldUp), for: .touchUpInside)
        let itemUp = UIBarButtonItem(customView: btnUp)
        
        let btnDown = UIButton(type: .custom)
        btnDown.setImage(UIImage(named: "ic_keyboard_arrow_down"), for: .normal)
        btnDown.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        btnDown.tag = sender.tag
        btnDown.addTarget(self, action: #selector(textFieldDown), for: .touchUpInside)
        let itemDown = UIBarButtonItem(customView: btnDown)
        
        
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        //keyboardToolbar.items = [flexBarButton,UIBarButtonItem(customView: button)]
        keyboardToolbar.items = [itemDown,itemUp,flexBarButton,UIBarButtonItem(customView: button)]
        
        if let value = sender as? UITextField {
            value.inputAccessoryView = keyboardToolbar
        }else if let value = sender as? UITextView {
            value.inputAccessoryView = keyboardToolbar
        }
        
    }
    
    @objc func textFieldUp(_ sender: Any)-> Void {
        
        let tObject = (sender as AnyObject).tag
        if let value = tObject as? Int {
            switch value
            {
            case 1:
                username.resignFirstResponder()
                newPasswprd2.becomeFirstResponder()
                break
            case 2:
                newPasswprd2.resignFirstResponder()
                newPassword1.becomeFirstResponder()
                break
            case 3:
                newPassword1.resignFirstResponder()
                username.becomeFirstResponder()
                break
            default:
                print("")
            }
        }
    }
    
    @objc func textFieldDown(_ sender: Any)-> Void {
        
        let tObject = (sender as AnyObject).tag
        if let value = tObject as? Int {
            switch value
            {
            case 1:
                username.resignFirstResponder()
                newPassword1.becomeFirstResponder()
                break
            case 2:
                newPassword1.resignFirstResponder()
                newPasswprd2.becomeFirstResponder()
                break
            case 3:
                newPasswprd2.resignFirstResponder()
                username.becomeFirstResponder()
                break
          
            default:
                print("")
            }
        }
    }
    
    
    //************** softkeyboard tool bar **********************//
    
    func showLoginAlert(_ type: String) {
        
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "IRANSansMobile", size: 17)!,
            kTextFont: UIFont(name: "IRANSansMobile", size: 14)!,
            kButtonFont: UIFont(name: "IRANSansMobile", size: 14)!,
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        
        switch type {
        case "success":
            alertView.addButton("بستن") {
            }
            alertView.showInfo("پیام:", subTitle: "شما با موفقیت وارد شده اید.")
            break
        default:
            print("")
        }
        
    }
    
    }
