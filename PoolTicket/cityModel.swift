//
//  cityModel.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/18/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation


class cityModel: NSObject, NSCoding {
   
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(city_id, forKey: "city_id")
        aCoder.encode(city_name, forKey: "city_name")
        aCoder.encode(state_id, forKey: "state_id")
        aCoder.encode(state_name, forKey: "state_name")

    }
    
    required init?(coder aDecoder: NSCoder) {
        
        self.city_id = aDecoder.decodeObject(forKey: "city_id") as? String ?? ""
        self.city_name = aDecoder.decodeObject(forKey: "city_name") as? String ?? ""
        self.state_id = aDecoder.decodeObject(forKey: "state_id") as? String ?? ""
        self.state_name = aDecoder.decodeObject(forKey: "state_name") as? String ?? ""

    }
    
    
    var city_id:String?
    var city_name:String?
    var state_id:String?
    var state_name:String?
    
    required init(_ city_id: String, _ city_name: String, _ state_id:String,_ state_name:String) {
        self.city_id = city_id
        self.city_name = city_name
        self.state_id = state_id
        self.state_name = state_name
    }
    
    static func prepare(_ item:AnyObject) -> cityModel{
        
        let state_id = String(describing: item["state_id"] as AnyObject)
        let state_name = String(describing: item["state_name"] as AnyObject)
        let city_id = String(describing: item["city_id"] as AnyObject)
        let city_name = String(describing: item["city_name"] as AnyObject)

        let instance = cityModel(city_id, city_name, state_id, state_name)
        return instance
    }
    
    static func prepareDefault() -> cityModel{
        
        let state_id = "8"//String(describing: item["state_id"] as AnyObject)
        let state_name = "تهران"//String(describing: item["state_name"] as AnyObject)
        let city_id = "117"//String(describing: item["city_id"] as AnyObject)
        let city_name = "تهران"//String(describing: item["city_name"] as AnyObject)
        
//        let state_id = "-2"//String(describing: item["state_id"] as AnyObject)
//        let state_name = "تمام شهرها"//String(describing: item["state_name"] as AnyObject)
//        let city_id = ""//String(describing: item["city_id"] as AnyObject)
//        let city_name = "نمایش همه"//String(describing: item["city_name"] as AnyObject)
        
        let instance = cityModel(city_id, city_name, state_id, state_name)
        return instance
    }
    
    
    
}
