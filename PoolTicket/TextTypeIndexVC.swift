//
//  TextTypeIndex.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/18/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit

class TextTypeIndexVC: UIViewController {
    
    @IBOutlet weak var topImage: UIImageView!
    @IBOutlet weak var textTitle: UILabel!
    @IBOutlet weak var content: UILabel!
    @IBOutlet weak var topImagesHeight: NSLayoutConstraint!
    
    var tData: textModel?
    
    @IBAction func openTextInWebview(){
        
        if let value = tData as? textModel {
        print(value.url!)
        openNewClass(classIdentifier: "TextTypeIndexReadMoreVC")
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
         tData = shareData.shared.selectedIndexTextType
        if let value = tData as? textModel {
          
            self.content.text = value.content
            self.textTitle.text = value.title
            
            if let stringURL = URL.init(string: value.image!) as? URL {
                let imageView = self.topImage
                let URL = stringURL//Foundation.URL(string: data["thumbnail"]!)!
                let placeholderImage = UIImage(named: "main_top_bg.png")!
                imageView?.af_setImage(withURL: URL, placeholderImage: placeholderImage)
            }else{
                self.topImage.image = UIImage(named: "main_top_bg.png")
            }
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // make ui perfect for iphone plus
        if  DeviceType.IS_IPHONE_6P_6SP_7P_8P{
            if let value = self.topImagesHeight as? NSLayoutConstraint {
                value.constant = 170
            }
        }
        
    }
    
    @IBAction func close(){
        self.dismiss(animated: true, completion: nil)
    }
}
