//
//  saens.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/6/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
class saensModel: NSObject {
    
    var saens_name:String?
    var saens_id:String?
    var gender:String?
    var rows:AnyObject?
    var saens_row_id:String?
    var price_market_adult:String?
    var price_market_child:String?
    var price_sale_adult:String?
    var price_sale_child:String?
    var discount_percent_adult:String?
    var discount_percent_child:String?
    var status_sell:String?
    var pDescription:String?
    var adult_sale_status:String?
    var child_sale_status:String?
    var start_day:String?
    var start_date:String?
    var time:String?
    var type: String?
    var terms_of_use: String?

    init(_ type:String, _ saens_name:String,_ saens_id:String,_ gender:String,_ rows:AnyObject,_ saens_row_id:String,_ price_market_adult:String,_ price_market_child:String,_ price_sale_adult:String,_ price_sale_child:String,_ discount_percent_adult:String,_ discount_percent_child:String,_ status_sell:String,_ description:String,_ adult_sale_status:String,_ child_sale_status:String,_ start_day:String,_ start_date:String,_ time:String, _ terms_of_use: String) {
        
        self.saens_name = saens_name
        self.saens_id = saens_id
        self.gender = gender
        self.rows = rows
        self.saens_row_id = saens_row_id
        self.price_market_adult = price_market_adult
        self.price_market_child = price_market_child
        self.price_sale_adult = price_sale_adult
        self.price_sale_child = price_sale_child
        self.discount_percent_adult = discount_percent_adult
        self.discount_percent_child = discount_percent_child
        self.status_sell = status_sell
        self.pDescription = description
        self.child_sale_status = child_sale_status
        self.start_day = start_day
        self.start_date = start_date
        self.adult_sale_status = adult_sale_status
        self.time = time
        self.type = type
        self.terms_of_use = terms_of_use
    }
    
    static func prepare(_ item:AnyObject, _ type: String) -> saensModel{
        let saens_name = String(describing: item["saens_name"] as AnyObject)
        let saens_id = String(describing: item["saens_id"] as AnyObject)
        let gender = String(describing: item["gender"] as AnyObject)
        let terms_of_use = String(describing: item["terms_of_use"] as AnyObject)
        let rows = item["rows"] as AnyObject
        let saens_row_id = "-1"//String(describing: item["saens_row_id"] as AnyObject)
        let price_market_adult = "-1"//String(describing: item["price_market_adult"] as AnyObject)
        let price_market_child = "-1"//String(describing: item["price_market_child"] as AnyObject)
        let price_sale_adult = "-1"//String(describing: item["price_sale_adult"] as AnyObject)
        let price_sale_child = "-1"//String(describing: item["price_sale_child"] as AnyObject)
        let discount_percent_adult = "-1"//String(describing: item["discount_percent_adult"] as AnyObject)
        let discount_percent_child = "-1"//String(describing: item["discount_percent_child"] as AnyObject)
        let status_sell = "-1"//String(describing: item["status_sell"] as AnyObject)
        let description = "-1"//String(describing: item["description"] as AnyObject)
        let adult_sale_status = "-1"//String(describing: item["adult_sale_status"] as AnyObject)
        let child_sale_status = "-1"//String(describing: item["child_sale_status"] as AnyObject)
        let start_day = "-1"//String(describing: item["start_day"] as AnyObject)
        let start_date = "-1"//String(describing: item["start_date"] as AnyObject)
        let time = "-1"//String(describing: item["time"] as AnyObject)
        let type = type

        let instance = saensModel(type, saens_name, saens_id, gender, rows, saens_row_id, price_market_adult, price_market_child,price_sale_adult, price_sale_child, discount_percent_adult, discount_percent_child, status_sell, description, adult_sale_status, child_sale_status, start_day, start_date, time, terms_of_use)
        return instance
    }
    
    static func prepareRows(_ item:AnyObject, _ type: String) -> saensModel{
        let saens_name = "-1"//String(describing: item["saens_name"] as AnyObject)
        let saens_id = "-1"//String(describing: item["saens_id"] as AnyObject)
        let gender = "-1"//String(describing: item["gender"] as AnyObject)
        let rows = "" as AnyObject//item["rows"] as AnyObject
        let saens_row_id = String(describing: item["saens_row_id"] as AnyObject)
        let terms_of_use = String(describing: item["terms_of_use"] as AnyObject)
        let price_market_adult = String(describing: item["price_market_adult"] as AnyObject)
        let price_market_child = String(describing: item["price_market_child"] as AnyObject)
        let price_sale_adult = String(describing: item["price_sale_adult"] as AnyObject)
        let price_sale_child = String(describing: item["price_sale_child"] as AnyObject)
        let discount_percent_adult = String(describing: item["discount_percent_adult"] as AnyObject)
        let discount_percent_child = String(describing: item["discount_percent_child"] as AnyObject)
        let status_sell = String(describing: item["status_sell"] as AnyObject)
        let description = String(describing: item["description"] as AnyObject)
        let adult_sale_status = String(describing: item["adult_sale_status"] as AnyObject)
        let child_sale_status = String(describing: item["child_sale_status"] as AnyObject)
        let start_day = String(describing: item["start_day"] as AnyObject)
        let start_date = String(describing: item["start_date"] as AnyObject)
        let time = String(describing: item["time"] as AnyObject)
        let type = type
        
        let instance = saensModel(type, saens_name, saens_id, gender, rows, saens_row_id, price_market_adult, price_market_child,price_sale_adult, price_sale_child, discount_percent_adult, discount_percent_child, status_sell, description, adult_sale_status, child_sale_status, start_day, start_date, time, terms_of_use)
        return instance
    }
}
