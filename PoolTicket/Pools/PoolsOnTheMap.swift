//
//  PoolsOnTheMap.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 3/6/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps


class PoolsOnTheMap: UIViewController, GMSMapViewDelegate {
    
    @IBOutlet weak var poolsOnTheMap:GMSMapView!
//    @IBOutlet weak var segmentSection: UISegmentedControl!
//
//    @IBAction func segmentSectionAction(_ sender: Any) {
//
//        switch segmentSection.selectedSegmentIndex {
//        case 1:
//            self.dismiss(animated: true, completion: nil)
//            break
//
//        default:
//            print("")
//        }
//
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        poolsOnTheMap.delegate = self
        poolsOnTheMap.accessibilityElementsHidden = false
        self.showPoolsOnMap(DataSourceManagement.searchPoolList)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.title = "نمایش مجموعه ها بر روی نقشه"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = "نقشه"
    }
    
    private func showPoolsOnMap(_ allItems: [poolsListModel])-> Void{
        
        
        for item in allItems {
            
            var lat = 0.0
            var lng = 0.0
            var status_sell = "0"
            
            
            // the 1 means ready for sale in pool ticket, the 0 is not available
            if let value = item.status_sell as? String {
                status_sell = value
            }
            
            // lat and lng of the pool in the map area
            if let value = Double(item.lat!) {
                lat = value
            }
            if let value = Double(item.lng!){
                lng = value
            }
            
            
            if lat != 0.0 && lng != 0.0 {
                
                let marker = GMSMarker()
                
                // set the icon marker
                var markerImage = UIImage(named: "itemMarkerBlue")!.withRenderingMode(.alwaysOriginal)
                if status_sell == "1" {
                    markerImage = UIImage(named: "itemMarkerBlue")!.withRenderingMode(.alwaysOriginal)
                }else{
                    markerImage = UIImage(named: "itemMarkerRed")!.withRenderingMode(.alwaysOriginal)
                }
                let markerView = UIImageView(image: markerImage)//markerImage)
                marker.iconView = markerView
                
                // marker stand degree
                var degrees = 0.0
                marker.rotation = degrees
                
                marker.position = CLLocationCoordinate2D(latitude:  lat, longitude: lng)
              
                let data = markerCustomDetaile(item: item)
                marker.userData = data
                marker.map = poolsOnTheMap
             }
        }
        
        
        if allItems.count > 0 {
            var lat = 0.0
            var lng = 0.0
            var zoom: Float = 8.5

            if let value =  shareData.shared.Pool_list_map_Lat as? String {
                if let value01 = Double(value) {
                    lat = value01
                }
            }else if let value = Double(allItems[0].lat!) {
                lat = value
            }
            
            if let value = shareData.shared.Pool_list_map_Lng as? String {
                if let value01 = Double(value) {
                    lng = value01
                }
            }else if let value = Double(allItems[0].lng!){
                lng = value
            }
            
            if let value = shareData.shared.Pool_list_map_zoom as? String {
                if let value01 = Float(value) as? Float {
                    zoom = value01
                }
            }
            
            if lat != 0.0 && lng != 0.0 {
                
                let camera = GMSCameraPosition.camera(withLatitude:  lat, longitude: lng, zoom: zoom)
                self.poolsOnTheMap.camera = camera

            }
        }
        
        
    }
    
    
    public func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        //print("didtapat")

    }
    
    public func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        
        //print("didlongpress")
        
    }
    
    
    public func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker){
        
        //print("didtapinfo")
     
        if let object = marker.userData as? markerCustomDetaile {
            if let item = object.item  {
                printLog(m: item.name!+" "+item.pool_id!)
                
                DataSourceManagement.setSelectedPoolId(item.pool_id!)
                pushNewClass(classIdentifier: "PoolVC")
                
            }
        }
//        self.showDropMessage("قابلیت نمایش استخر در نسخه آینده فعال خواهد بود.")
//        shareData.shared.lastCarName = (marker.userData as! markerCustomDetaile).item?.Name
//        if let value = (marker.userData as! markerCustomDetaile).item {
//            shareData.shared.lastCarsModel = value
//            shareData.shared.ApllicationIconId = value.Application
//
//        }
//
//        shareData.shared.lastCarTime = ""
//        openNewClass(classIdentifier: "carTabView")
        
        
   
        
    }
    
    
    
    public func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        
        if let object = marker.userData as? markerCustomDetaile {
            if let item = object.item  {
                var poolTitle = ""
                var status_sell = ""

                if let value = item.name {
                    poolTitle = value
                }
                
                if let value = item.status_sell {
                    status_sell = value
                }
                if status_sell == "0" {
                    if let infoView = TheMarkerDetailInActive.instanceFromNib() as? TheMarkerDetailInActive {
                        infoView.pTitle?.text = poolTitle
                        return infoView
                    } else{
                        return nil
                    }
                }else if status_sell == "1" {
                    if let infoView01 = TheMarkerDetailActive.instanceFromNib() as? TheMarkerDetailActive {
                        infoView01.pTitle?.text = poolTitle
                        return infoView01
                    } else{
                        return nil
                    }
                }
            }
        }
        return nil
    }

    @IBAction func close(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

struct markerCustomDetaile {
    var item:poolsListModel?
}
