//
//  ReservationVC.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/8/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import SCLAlertView



class ReservationVC: UIViewController , UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
   
    var refreshAllDataVariable = false
    var list: [AnyObject] = []
    var tableTypeList: [String] = [];
    var saensCartModelObject: saensCartModel?
    var packCartModelObject: packCartModel?
    var courseCartModelObject: courseCartModel?
    var type = ""
    var adultCount = 0
    var childCount = 0
    var childStatus = false
    var adultStatus = false

    var pool_capacity_overflow = false
    var not_available_ticket_serial = false
    

    @IBAction func sodorfactorAndPardakht(_ sender: Any){
        
        if SwiftUtil.getUserAccessToken() == ""{
            openNewClass(classIdentifier: "CheckPhoneNumberRegistrationVC")
            return
        }
        if adultCount > 0 {
        
            if self.pool_capacity_overflow {
                showAlert("pool_capacity_overflow")
            }else if self.not_available_ticket_serial {
                showAlert("not_available_ticket_serial")
            }else {
                shareData.shared.selectedAdultCount = String(adultCount)
                shareData.shared.selectedChildCount = String(childCount)
                pushNewClass(classIdentifier: "ReservationVCPart2")
            }
            
        }else if childCount > 0 {
        
            if self.pool_capacity_overflow {
                showAlert("pool_capacity_overflow")
            }else if self.not_available_ticket_serial {
                showAlert("not_available_ticket_serial")
            }else {
                shareData.shared.selectedAdultCount = String(adultCount)
                shareData.shared.selectedChildCount = String(childCount)
                pushNewClass(classIdentifier: "ReservationVCPart2")
            }
        }else{
           
            // if user set the adult count to zero
            // system will set it to one
            // let ndx = IndexPath(row:1, section: 0)
            // if let cell = tableView.cellForRow(at:ndx) as? ReservationCustomCell2 {
            //       adultCount = 1
            //       cell.number.text = "1"
            // }
            showAlert("failZeroCount")

        }
    }
    
    @IBAction func submitCopounCode(_ sender: Any){
        
        let ndx = IndexPath(row:1, section: 0)
        let cell = tableView.cellForRow(at:ndx) as! ReservationCustomCell3
        let txt = cell.copounCode.text
        cell.copounCode.resignFirstResponder()
        
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       return self.tableTypeList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        switch tableTypeList[indexPath.row] {
        case "ReservationCustomCell1":
           
            let data = self.list[indexPath.row]
                if let value = data as? saensModel {
                    let identifier = "ReservationCustomCell1"
                    var cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ReservationCustomCell1
                    cell.pool_name?.text = shareData.shared.selectedPoolName
                    cell.fuldate?.text = self.replaceString(value.start_day! + " - " + value.start_date!)
                    cell.time?.text = self.replaceString(value.time!)
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    return cell;
                }else if let value = data as? courseModel {
                    let identifier = "ReservationCustomCell1V2"
                    var cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ReservationCustomCell1
                    cell.pool_name?.text = shareData.shared.selectedPoolName
                    cell.startDate?.text = "شروع: " + self.replaceString(value.start_datetime!)
                    cell.endDate?.text = "پایان: " + self.replaceString(value.finish_datetime!)
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    return cell;
                }else if let value = data as? packsModel {
                    let identifier = "ReservationCustomCell1V2"
                    var cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ReservationCustomCell1
                    cell.pool_name?.text = shareData.shared.selectedPoolName
                    cell.fuldate?.text = self.replaceString(value.session_expire_start! + " الی " + value.session_expire_finish!)
                    cell.startDate?.text = "شروع: " + self.replaceString(value.session_expire_start!)
                    cell.endDate?.text = "پایان: " + self.replaceString(value.session_expire_finish!)
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    return cell;
                }
            return UITableViewCell();
            
        case "ReservationCustomCell2":
            let identifier = "ReservationCustomCell2"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ReservationCustomCell2
            
            switch self.type {
            case "saens":
                let data = self.list[0]
                if let model = data as? saensModel {
                    if let value = model.price_sale_adult {
                        cell.price?.text = self.setSeprator(value)// + " تومان"
                    }
                    if let value = model.price_market_adult {
                        cell.priceWithDiscount?.text = self.setSeprator(value)// + " تومان"
                    }
                    cell.number.text = String(adultCount)
                    cell.posetive.addTarget(self, action: #selector(posetiveAdultTicketCount), for: .touchUpInside)
                    cell.negative.addTarget(self, action: #selector(negativeAdultTicketCount), for: .touchUpInside)
                    cell.number.delegate = self
                    cell.number.tag = indexPath.row
                    self.doneWithKeyboard(cell.number)
                }
                break;
            case "pack":
                let data = self.list[0]
                if let model = data as? packsModel {
                    if let value = model.price_sale {
                        cell.price?.text = self.setSeprator(value)// + " تومان"
                    }
                    if let value = model.price_market {
                        cell.priceWithDiscount?.text = self.setSeprator(value)// + " تومان"
                    }
                    cell.posetive.isHidden = true
                    cell.negative.isHidden = true
                    cell.number.text = "1"
                    cell.number.isEnabled = false
                }
                
                
                break;
            case "course":
                
                let data = self.list[0]
                if let model = data as? courseModel {
                    if let value = model.price_sale {
                        cell.price?.text = self.setSeprator(value)// + " تومان"
                    }
                    if let value = model.price_market {
                        cell.priceWithDiscount?.text = self.setSeprator(value)// + " تومان"
                    }
                    cell.posetive.addTarget(self, action: #selector(posetiveAdultTicketCount), for: .touchUpInside)
                    cell.negative.addTarget(self, action: #selector(negativeAdultTicketCount), for: .touchUpInside)
                    cell.number.delegate = self
                    cell.number.tag = indexPath.row
                    self.doneWithKeyboard(cell.number)
                    
                }
                
                break;
            default:
                print("")
            }
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell;
      
        case "ReservationCustomCell2Child":
            var identifier = "ReservationCustomCell2Child"
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ReservationCustomCell2Child
            switch self.type {
            case "saens":
                  let data = self.list[0]
                  if let model = data as? saensModel {
                    if let value = model.price_sale_child as? String {
                        cell.price?.text = self.setSeprator(value)
                    }
                    if let value = model.price_sale_child as? String {
                        cell.priceWithDiscount?.text = self.setSeprator(value)
                    }
                    cell.number.text = String(childCount)
                    cell.number.delegate = self
                    cell.number.tag = indexPath.row
                    self.doneWithKeyboard(cell.number)
                  }
                break;
            default:
                print("")
            }
            cell.posetive.addTarget(self, action: #selector(posetiveChildTicketCount), for: .touchUpInside)
            cell.negative.addTarget(self, action: #selector(negativeChildTicketCount), for: .touchUpInside)
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell;
        
       
        case "ReservationCustomCell4":
            var identifier = "ReservationCustomCell4"
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ReservationCustomCell4
            
            switch self.type {
            case "saens":
                if let value = self.saensCartModelObject as? saensCartModel {
                    cell.price?.text = self.setSeprator(value.total_sale!)
                    cell.discount?.text = " تخفیف: " + self.setSeprator(self.calculateDiscountPrice(value.total_market!, value.total_sale!))
                    if let value1 = value.coupon_message as? String {
                        if value1.count > 0 {
                            cell.discountMessage?.text = "( " + value1 + " )"
                        }else{
                            cell.discountMessage?.text = ""
                        }
                    }
                    if let value1 = value.coupon_credit as? String {
                        if value1.count > 0 {
                            cell.sodeShoma?.text = " سود شما (با استفاده از کوپن تخفیف)" + self.setSeprator(value1)
                        }else{
                            cell.sodeShoma?.text = "فاقد کوپن تخفیف"
                        }
                        
                    }
                }
                break;
            case "pack":
                  if let value = self.packCartModelObject as? packCartModel {
                     cell.price?.text = self.setSeprator(value.total_sale!)
                     cell.discount?.text = " تخفیف: " + self.setSeprator(self.calculateDiscountPrice(value.total_market!, value.total_sale!))
                    if let value1 = value.coupon_message as? String {
                        if value1.count > 0 {
                            cell.discountMessage?.text = "( " + value1 + " )"
                        }else{
                            cell.discountMessage?.text = ""
                        }
                    }
                    if let value1 = value.coupon_credit as? String {
                        if value1.count > 0 {
                            cell.sodeShoma?.text = " سود شما (با استفاده از کوپن تخفیف)" + self.setSeprator(value1)
                        }else{
                            cell.sodeShoma?.text = "فاقد کوپن تخفیف"
                        }
                        
                    }
                  }
                
                break;
            case "course":
                 if let value = self.courseCartModelObject as? courseCartModel {
                    cell.price?.text = self.setSeprator(value.total_sale!)
                    cell.discount?.text = " تخفیف: " + self.setSeprator(self.calculateDiscountPrice(value.total_market!, value.total_sale!))
                    if let value1 = value.coupon_message as? String {
                        if value1.count > 0 {
                            cell.discountMessage?.text = "( " + value1 + " )"
                        }else{
                            cell.discountMessage?.text = ""
                        }
                    }
                    if let value1 = value.coupon_credit as? String {
                        if value1.count > 0 {
                            cell.sodeShoma?.text = " سود شما (با استفاده از کوپن تخفیف)" + self.setSeprator(value1) 
                        }else{
                            cell.sodeShoma?.text = "فاقد کوپن تخفیف"
                        }
                        
                    }
                 }
                break;
            default:
                print("")
            }
            
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell;
        case "ReservationCustomCell5":
            let identifier = "ReservationCustomCell5"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ReservationCustomCell5
            cell.showRules.addTarget(self, action: #selector(openRulesOfUseingPools), for: .touchUpInside)
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell;
       
        case "ReservationCustomCell8":
            let identifier = "ReservationCustomCell8"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ReservationCustomCell8
            cell.rowTitle.text = "مبلغ کل:"
            if let value = self.saensCartModelObject  {
                cell.rowValue.text = self.setSeprator(value.total_market!)
            }else if let value = self.packCartModelObject {
                cell.rowValue.text = self.setSeprator(value.total_market!)
            }else if let value = self.courseCartModelObject  {
                cell.rowValue.text = self.setSeprator(value.total_market!)
            }
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell;
       
        case "ReservationCustomCell9":
            let identifier = "ReservationCustomCell9"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ReservationCustomCell8
            cell.rowTitle.text = "تخفیف هدیه:"
            if let value = self.saensCartModelObject  {
                cell.rowValue.text = self.setSeprator(self.calculateDiscountPrice(value.total_market!, value.total_sale!))
            }else if let value = self.packCartModelObject  {
                cell.rowValue.text = self.setSeprator(self.calculateDiscountPrice(value.total_market!, value.total_sale!))
            }else if let value = self.courseCartModelObject {
                cell.rowValue.text = self.setSeprator(self.calculateDiscountPrice(value.total_market!, value.total_sale!))
            }
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell;
       
        case "ReservationCustomCell10":
            let identifier = "ReservationCustomCell10"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ReservationCustomCell8
            cell.rowTitle.text = "مبلغ پرداختی:"
            if let value = self.saensCartModelObject  {
                cell.rowValue.text = self.setSeprator(value.total_sale!)
            }else if let value = self.packCartModelObject  {
                cell.rowValue.text = self.setSeprator(value.total_sale!)
            }else if let value = self.courseCartModelObject  {
                cell.rowValue.text = self.setSeprator(value.total_sale!)
            }
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell;
        default:
            
            print("")
        }
        
        
        return UITableViewCell()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
      
        if shareData.shared.selectedSaensRow != nil {
            self.type = "saens"
            self.list.append(shareData.shared.selectedSaensRow!)
            if let value = shareData.shared.selectedSaensRow?.saens_row_id as? String {
                
                if let model = shareData.shared.selectedSaensRow as? saensModel {
                    if let result = model.adult_sale_status as? String {
                        if result == "1" {
                            self.adultStatus = true
                            self.adultCount = 1
                        }
                    }
                    if let result = model.child_sale_status as? String {
                        if result == "1" {
                            self.childStatus = true
                            if self.adultCount == 0 {
                                self.childCount = 1
                            }
                        }
                    }
                }
                
            self.CalculateReservation(value,String(self.adultCount),String(childCount),"");
                
            }
        }else if shareData.shared.selectedPackRow != nil{
            self.type = "pack"
            self.list.append(shareData.shared.selectedPackRow!)
            if let value = shareData.shared.selectedPackRow?.pack_id as? String {
                self.CalculatePackReservation(value,"");
            }
        }else if shareData.shared.selectedCourseRow != nil{
            self.type = "course"
            self.list.append(shareData.shared.selectedCourseRow!)
            if let value = shareData.shared.selectedCourseRow?.course_id as? String {
                self.adultCount = 1
                self.CalculateCourseReservation(value,String(self.adultCount),"");
            }
        }
        
       

    }
    
    func prepareDataForTheView()->Void{
        
        self.tableTypeList = []
        
        self.tableTypeList.append("ReservationCustomCell1")
        
        if shareData.shared.selectedSaensRow != nil {
            self.list.append(shareData.shared.selectedSaensRow!)
            if let value = shareData.shared.selectedSaensRow as? saensModel {
                if let result = value.adult_sale_status as? String {
                    if result == "1" {
                       self.tableTypeList.append("ReservationCustomCell2")
                       self.adultStatus = true
//                       if self.adultCount == 0 {
//                        self.adultCount = 1
//                       }
                    }
                }
                if let result = value.child_sale_status as? String {
                    if result == "1" {
                        self.tableTypeList.append("ReservationCustomCell2Child")
                        self.childStatus = true
//                        if self.adultCount == 0, self.childCount == 0 {
//                            self.childCount = 1
//                        }
                    }
                }
            }
        }else if shareData.shared.selectedPackRow != nil{
            self.list.append(shareData.shared.selectedPackRow!)
        }else if shareData.shared.selectedCourseRow != nil{
            self.list.append(shareData.shared.selectedCourseRow!)
            self.tableTypeList.append("ReservationCustomCell2")
        }

        if self.childCount > 0 || self.adultCount > 0 {
        //self.tableTypeList.append("ReservationCustomCell4")
        self.tableTypeList.append("ReservationCustomCell8")
        self.tableTypeList.append("ReservationCustomCell9")
        self.tableTypeList.append("ReservationCustomCell10")
        }
        
        if let value = shareData.shared.selectedChoosenPoolsRulesAndDetails as? String {
            if value != "", value != "<null>" {
                self.tableTypeList.append("ReservationCustomCell5")
            }
        }
        
     
       
        
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.font.rawValue: UIFont(name: "IRANSansMobile", size: 14),NSAttributedString.Key.foregroundColor.rawValue:UIColor.white])
        
        if shareData.shared.selectedSaensRow != nil {
            //self.time_ic.isHidden = false
            self.title = shareData.shared.selectedChoosenServices ?? "نام سانس انتخاب شده"
        }else if shareData.shared.selectedPackRow != nil{
            //self.time_ic.isHidden = true
            self.title = shareData.shared.selectedChoosenServices ?? "نام پک انتخاب شده"
        }else if shareData.shared.selectedCourseRow != nil{
            //self.time_ic.isHidden = true
            self.title = shareData.shared.selectedChoosenServices ?? "نام کلاس شنا انتخاب شده"
        }
        
        // add observer
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if shareData.shared.selectedSaensRow != nil {
            //self.time_ic.isHidden = false
            self.title = "سانس"
        }else if shareData.shared.selectedPackRow != nil{
            //self.time_ic.isHidden = true
            self.title = "پک"
        }else if shareData.shared.selectedCourseRow != nil{
            //self.time_ic.isHidden = true
            self.title = "کلاس"
        }
        
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func CalculateReservation(_ saens_row_id:String, _ count_adult: String, _ count_child:String, _ coupon: String) -> Void {
        LoadingOverlayV2.sharedV2.showOverlayV2(view: self.view, message: "در حال محاسبه فاکتور")
        self.refreshAllDataVariable = true
       
        self.pool_capacity_overflow = false
        self.not_available_ticket_serial = false
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken();
        let index = wsPOST.init(Constants.CALCULATE_RESERVATION,"client_id=\(client_id)&access_token=\(access_token)&saens_row_id=\(saens_row_id)&count_adult=\(count_adult)&count_child=\(count_child)&coupon=\(coupon)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            do{
                
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    //print(json)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                if let cart =  item["cart"] as? [String: AnyObject]{
                                   self.saensCartModelObject =  saensCartModel.prepare(cart as AnyObject)
                                }
                                

                            }else if value == 0 {
                                if let errorValue =  item["error"] as? String{
                                    if errorValue == "pool_capacity_overflow" {
                                        self.pool_capacity_overflow = true
                                    }else if errorValue == "not_available_ticket_serial" {
                                        self.not_available_ticket_serial = true
                                    }
                                }
                            }
                        }
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                LoadingOverlayV2.sharedV2.hideOverlayView()
                self.refreshAllDataVariable = false
                if WSResponseStatus {
                    self.pool_capacity_overflow = false
                    self.not_available_ticket_serial = false
                    self.prepareDataForTheView();
                }else if self.pool_capacity_overflow == true {
                    self.showAlert("pool_capacity_overflow")
                }else if self.not_available_ticket_serial == true {
                    self.showAlert("not_available_ticket_serial")
                }
            })
            
        }
        
        
    }
    
    func CalculatePackReservation(_ pack_id:String, _ coupon: String) -> Void {
        LoadingOverlayV2.sharedV2.showOverlayV2(view: self.view, message: "در حال محاسبه فاکتور")
        self.refreshAllDataVariable = true
        
        self.pool_capacity_overflow = false
        self.not_available_ticket_serial = false
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.CALCULATE_PACK,"client_id=\(client_id)&access_token=\(access_token)&pack_id=\(pack_id)&coupon=\(coupon)")
        var WSResponseStatus = false
        index.start{ (indexData) in

            do{

                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    //print(json)
                    if let item = json as? [String: AnyObject]{

                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true

                                if let cart =  item["cart"] as? [String: AnyObject]{
                                    self.packCartModelObject =  packCartModel.prepare(cart as AnyObject)
                                }
                            }else if value == 0 {
                                if let errorValue =  item["error"] as? String{
                                    if errorValue == "pool_capacity_overflow" {
                                        self.pool_capacity_overflow = true
                                    }else if errorValue == "not_available_ticket_serial" {
                                        self.not_available_ticket_serial = true
                                    }
                                }
                            }
                        }
                    }
                }

            }catch{
                print("this is the error: \(error)")
            }

            DispatchQueue.main.async(execute: {
                LoadingOverlayV2.sharedV2.hideOverlayView()
                self.refreshAllDataVariable = false
                if WSResponseStatus {
                    self.pool_capacity_overflow = false
                    self.not_available_ticket_serial = false
                    self.adultCount = 1
                    self.prepareDataForTheView();

                }else if self.pool_capacity_overflow == true {
                    self.showAlert("pool_capacity_overflow")
                }else if self.not_available_ticket_serial == true {
                    self.showAlert("not_available_ticket_serial")
                }
            })

        }


    }
    
    func CalculateCourseReservation(_ course_id:String, _ count_adult: String, _ coupon: String) -> Void {
        LoadingOverlayV2.sharedV2.showOverlayV2(view: self.view, message: "در حال محاسبه فاکتور")
        self.refreshAllDataVariable = true
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.CALCULATE_COURSE_RESERVATION,"client_id=\(client_id)&access_token=\(access_token)&course_id=\(course_id)&count=\(count_adult)&coupon=\(coupon)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            var errorMessage = ""
            do{
                
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    //print(json)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                if let cart =  item["cart"] as? [String: AnyObject]{
                                    self.courseCartModelObject =  courseCartModel.prepare(cart as AnyObject)
                                }
                                
                                
                            }else if value == 0 {
                                
                                 if let validation =  item["validation"] as? [String: AnyObject]{
                                    if let count = validation["count"] as? [String] {
                                        for item in count {
                                        errorMessage = item
                                        }
                                    }
                                    
                                }
                                
                            }
                        }
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                 LoadingOverlayV2.sharedV2.hideOverlayView()
                self.refreshAllDataVariable = false
                if WSResponseStatus {
                    self.prepareDataForTheView();
                    // set the response ticket Count
                    let ndx = IndexPath(row:1, section: 0)
                    if let cell = self.tableView.cellForRow(at:ndx) as? ReservationCustomCell2 {
                        if self.courseCartModelObject != nil {
                            cell.number.text = self.courseCartModelObject?.count
                        }
                    }
                }else{
                    self.showDropMessage(errorMessage)
                }
            })
            
        }
        
        
    }
    
    func calculateDiscountPrice(_ totalPrice: String, _ discountPrice: String) -> String {
        var total : Int64 = 0
        var discount : Int64 = 0
        
        do {
           
            if let value = Int64(totalPrice) {
              total = value
            }
            if let value = Int64(discountPrice) {
              discount = value
            }
            total = total - discount
            return String(total)
            
        }catch{
            fatalError("this is error; \(total)")
        }
        
        return "0"
    }
    
    @objc func posetiveAdultTicketCount(button: UIButton){
        
        //var number = 0
        let ndx = IndexPath(row:1, section: 0)
        if let cell = tableView.cellForRow(at:ndx) as? ReservationCustomCell2 {
        guard let count = cell.number.text as? String else {
            print("")
            return
        }
        if let value = Int(count) as? Int {
            self.adultCount = value + 1
            cell.number.text = String(self.adultCount)
            self.callCalculateFunction()
        }
        }
        
        
    }
    
    @objc func negativeAdultTicketCount(button: UIButton){
        
        var number = 0
        let ndx = IndexPath(row:1, section: 0)
        if let cell = tableView.cellForRow(at:ndx) as? ReservationCustomCell2 {
        guard let count = cell.number.text as? String else {
            print("")
            return
        }
        if let value = Int(count) as? Int {
            if value > 0 {
            self.adultCount = value - 1
            cell.number.text = String(self.adultCount)
                if self.adultCount > 0 {
                    self.callCalculateFunction()
                }else if self.adultCount == 0, self.childStatus, self.childCount > 0 {
                    self.callCalculateFunction()
                }else{
                    prepareDataForTheView()
                    showDropMessage("مقدار انتخاب شده مجاز نمی باشد.")
                }
            }else{
                prepareDataForTheView()
                showDropMessage("مقدار انتخاب شده مجاز نمی باشد.")
            }
        }
        }
        
        
    }
    
    @objc func posetiveChildTicketCount(button: UIButton){
        
        var number = 0
        let ndx = IndexPath(row:2, section: 0)
        if let cell = tableView.cellForRow(at:ndx) as? ReservationCustomCell2Child {
            guard let count = cell.number.text as? String else {
                print("")
                return
            }
            if let value = Int(count) as? Int {
                self.childCount = value + 1
                cell.number.text = String(self.childCount)
                self.callCalculateFunction()
            }
        }else{
            // when the adult part is not available
            let ndx = IndexPath(row:1, section: 0)
            if let cell = tableView.cellForRow(at:ndx) as? ReservationCustomCell2Child {
                guard let count = cell.number.text as? String else {
                    print("")
                    return
                }
                if let value = Int(count) as? Int {
                    self.childCount = value + 1
                    cell.number.text = String(self.childCount)
                    self.callCalculateFunction()
                }
            }
            
        }
        
    }
    
    @objc func negativeChildTicketCount(button: UIButton){
        
        
        var number = 0
        let ndx = IndexPath(row:2, section: 0)
        let cell = tableView.cellForRow(at:ndx) as! ReservationCustomCell2Child
        guard let count = cell.number.text as? String else {
            print("")
            return
        }
        if let value = Int(count) as? Int {
            if (value - 1) >= 0 {
                self.childCount = value - 1
                cell.number.text = String(self.childCount)
                if self.childCount > 0{
                    self.callCalculateFunction()
                }else if self.childCount == 0 , self.adultStatus, self.adultCount > 0 {
                    self.callCalculateFunction()
                }else{
                    prepareDataForTheView()
                    showDropMessage("مقدار انتخاب شده مجاز نمی باشد.")
                }
            }else{
                prepareDataForTheView()
                showDropMessage("مقدار انتخاب شده مجاز نمی باشد.")
            }
        }
    }
    
    @objc func openRulesOfUseingPools(button: UIButton) {
        
        openNewClass(classIdentifier: "ClearContentVC")
        
    }
    
    func callCalculateFunction()->Void {
        
        switch self.type {
        case "saens":
            if let value = shareData.shared.selectedSaensRow?.saens_row_id  {
                self.CalculateReservation(value,String(self.adultCount),String(childCount),"");
            }
            break;
        case "pack":
            
            break;
        case "course":
            if let value = shareData.shared.selectedCourseRow?.course_id as? String {
                self.CalculateCourseReservation(value,String(self.adultCount),"");
            }
            break;
        default:
            print("")
        }
        
    }
    
    // get data from profile
    func getProfile() -> Void {
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.PROFILE,"client_id=\(client_id)&access_token=\(access_token)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            var mCredit = ""
//            var mTelephone = ""
//            var mName = ""
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    
                    
                    
                    
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                if let profile = item["profile"] as? [String: AnyObject] {
//                                    if let value = profile["name"] as? String {
//                                        mName = value
//                                    }
//                                    if let value = profile["mobile"] as? String {
//                                        mTelephone = value
//                                    }
                                    if let value = profile["credit"] as? Int {
                                        mCredit = String(value) + " تومان"
                                    }
                                }
                            }
                        }
                    }
                    
                    
                }
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                
                if WSResponseStatus {
                    let ndx = IndexPath(row:6, section: 0)
                    let cell = self.tableView.cellForRow(at:ndx) as! ReservationCustomCell7
                    cell.cellTitle.setTitle("کسر از اعتبار ("+self.replaceString(mCredit) + ")", for: .normal)
                }
            })
            
        }
    }
    
    //************** softkeyboard tool bar **********************//
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField
        {
            //        case personalID:
            //            personalID.resignFirstResponder()
            //            password.becomeFirstResponder()
            //            break
            //        case password:
            //            password.resignFirstResponder()
            //            break
        //
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
    func doneWithKeyboard(_ sender: UITextField){
        
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        keyboardToolbar.isTranslucent = true
        keyboardToolbar.barTintColor = UIColor.white
        
       
        let buttonConfirm = UIButton(type: .system)
        buttonConfirm.setTitle("بستن", for: .normal)
        buttonConfirm.sizeToFit()
        buttonConfirm.titleLabel?.font = UIFont(name: "IRANSansMobile", size: 12)
        buttonConfirm.tintColor = UIColor.black
        buttonConfirm.tag = sender.tag
        buttonConfirm.addTarget(self, action: #selector(closeKeyboardAndConfirm(_:)), for: .touchUpInside)
        
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        keyboardToolbar.items = [flexBarButton,UIBarButtonItem(customView: buttonConfirm)]
        
        if let value = sender as? UITextField {
            value.inputAccessoryView = keyboardToolbar
        }else if let value = sender as? UITextView {
            value.inputAccessoryView = keyboardToolbar
        }
        
    }
    
    @objc func closeKeyboardAndConfirm(_ sender: UIButton){
        sender.resignFirstResponder()
        
        guard let position = sender.tag as? Int else {
            print("")
            return
        }
        //var number = 0
        if position == 0 {
            return
        }
        
        let ndx = IndexPath(row:position, section: 0)
        
        if let cell = tableView.cellForRow(at:ndx) as? ReservationCustomCell2 {
            
            guard let count = cell.number.text as? String else {
                return
            }
            if let value = Int(count) as? Int {
                self.adultCount = value
                cell.number.text = String(self.adultCount)
                if value > 0 {
                self.callCalculateFunction()
                }else{
                prepareDataForTheView()
                showAlert("failZeroCount")
                }
            }
        }else if let cell = tableView.cellForRow(at:ndx) as? ReservationCustomCell2Child {
            
            guard let count = cell.number.text as? String else {
                return
            }
            if let value = Int(count) as? Int {
                self.childCount = value
                cell.number.text = String(self.childCount)
                if value > 0 {
                    self.callCalculateFunction()
                }else{
                    prepareDataForTheView()
                    showAlert("failZeroCount")
                }
            }
            
        }
        
    }
    
    func showAlert(_ type: String) {
        
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "IRANSansMobile", size: 17)!,
            kTextFont: UIFont(name: "IRANSansMobile", size: 14)!,
            kButtonFont: UIFont(name: "IRANSansMobile", size: 14)!,
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        
        switch type {
        case "failZeroCount":
            alertView.addButton("بستن") {
            }
            alertView.showError("اخطار:", subTitle: "مقدار فیلد های وارد شده اشتباه می باشد. لطفا فیلد های را بررسی کنید.")
            break
        case "pool_capacity_overflow":
            alertView.showError("اخطار:", subTitle: "ظرفیت مجموعه تکمیل شده است.")
            alertView.addButton("بستن") {
                
            }
            break
        case "not_available_ticket_serial":
            alertView.addButton("بستن") {
            }
            alertView.showError("اخطار:", subTitle: "سریال به تعداد کافی موجود نیست.")

            break
        default:
            print("")
        }
        
    }

    @objc func keyboardWillShow(_ notification:Notification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            tableView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
        }
    }
    @objc func keyboardWillHide(_ notification:Notification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            tableView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
   
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
