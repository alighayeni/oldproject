//
//  StartReservation.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/13/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import WebKit
import Firebase

class StartReservation: UIViewController, WKNavigationDelegate {
    
    @IBOutlet weak var contentView: UIView!
    var webView: WKWebView!
    var urlObservation: NSKeyValueObservation?
    var pReservationResponse: PRFR?
    var purchase_history_id: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.font.rawValue: UIFont(name: "IRANSansMobile", size: 14),NSAttributedString.Key.foregroundColor.rawValue:UIColor.white])
        self.title = "پرداخت آنلاین"
        
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        let configuration = WKWebViewConfiguration()
        configuration.preferences = preferences
        
        self.webView = WKWebView(frame: self.contentView.bounds, configuration: configuration)
        self.webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.webView.navigationDelegate = self
        
        self.contentView.addSubview(self.webView)
        
        
        if let value = shareData.shared.startReservationURL as? String {
            let url = URL(string: value)!
            webView.load(URLRequest(url: url))
            
            webView.allowsBackForwardNavigationGestures = true
            // Add observation.
            urlObservation = webView.observe(\.url, changeHandler: { (webView, change) in
                
                //print("Web view URL changed to \(webView.url?.absoluteString ?? "Empty")")
                
                var status = ""
                var voucher = ""
                var reservation_status = ""
                var reservation_id = ""
                
                var tData = webView.url?.absoluteString ?? "Empty"
                if tData.contains("org.poolticket://pack-reservation?") {
                    tData = tData.replacingOccurrences(of: "org.poolticket://pack-reservation?", with: "")
                    var tData2 = tData.split(separator: "&")
                    for item in tData2 {
                        var tData3 = item.split(separator: "=")
                        switch tData3[0] {
                        case "status":
                            status = String(tData3[1])
                            
//                            if status == "1" {
//                                // track Pack Payment success event in ios
////                                let event = ADJEvent.init(eventToken: "xpfjrr")
////                                Adjust.trackEvent(event)
//                            }
                            
                            break;
                        case "voucher":
                            voucher = String(tData3[1])
                            break;
                        case "reservation_status":
                            reservation_status = String(tData3[1])
                            break;
                        case "reservation_id":
                            reservation_id = String(tData3[1])
                            break;
                        case "purchase_history_id":
                            self.purchase_history_id = String(tData3[1])
                            
                            if self.purchase_history_id != nil, (self.purchase_history_id?.count)! > 0 {
                                shareData.shared.purchase_id = self.purchase_history_id
                            }else{
                                shareData.shared.purchase_id = nil
                            }
                            break
                        default:
                            print("")
                        }
                        
                    }
                    let item01 : PRFR = PRFR.init(status: status, voucher: voucher, reservation_status: reservation_status, reservation_id: reservation_id)
                    shareData.shared.ReservationFinalResponse = item01
                    if status == "1" {
                        self.openNewClass(classIdentifier: "ResponseConfirmOrder")
                        self.navigationController?.popToRootViewController(animated: true)
                    }else if status == "0" {
                        //self.openNewClass(classIdentifier: "ResponseConfirmOrderVFail")
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                    
                }else if tData.contains("org.poolticket://course-reservation?"){
                    tData = tData.replacingOccurrences(of: "org.poolticket://course-reservation?", with: "")
                    var tData2 = tData.split(separator: "&")
                    for item in tData2 {
                        var tData3 = item.split(separator: "=")
                        switch tData3[0] {
                        case "status":
                            status = String(tData3[1])
                            
//                            if status == "1" {
//                                // track Course payment success event in ios
////                                let event = ADJEvent.init(eventToken: "8b89yi")
////                                Adjust.trackEvent(event)
//                            }
                            
                            break;
                        case "voucher":
                            voucher = String(tData3[1])
                            break;
                        case "reservation_status":
                            reservation_status = String(tData3[1])
                            break;
                        case "reservation_id":
                            reservation_id = String(tData3[1])
                            break;
                            
                        case "purchase_history_id":
                            self.purchase_history_id = String(tData3[1])
                            
                            if self.purchase_history_id != nil, (self.purchase_history_id?.count)! > 0 {
                                shareData.shared.purchase_id = self.purchase_history_id
                            }else{
                                shareData.shared.purchase_id = nil
                            }
                            break
                        default:
                            print("")
                        }
                    }
                    let item01 : PRFR = PRFR.init(status: status, voucher: voucher, reservation_status: reservation_status, reservation_id: reservation_id)
                    shareData.shared.ReservationFinalResponse = item01
                    if status == "1" {
                        AppDelegate()._manager?.track("course_payment_success", data: [:])
                        Analytics.logEvent("course_payment_success", parameters: nil)
                        self.openNewClass(classIdentifier: "ResponseConfirmOrder")
                        self.navigationController?.popToRootViewController(animated: true)
                    }else if status == "0" {
                       // self.openNewClass(classIdentifier: "ResponseConfirmOrderVFail")
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                } else if tData.contains("org.poolticket://reservation?"){
                    tData = tData.replacingOccurrences(of: "org.poolticket://reservation?", with: "")
                    var tData2 = tData.split(separator: "&")
                    for item in tData2 {
                        var tData3 = item.split(separator: "=")
                        switch tData3[0] {
                        case "status":
                            status = String(tData3[1])
                            
//                            if status == "1" {
//                                // track Reservation Payment success event in ios
//                                let event = ADJEvent.init(eventToken: "8blg24")
//                                Adjust.trackEvent(event)
//                            }
                            
                            break;
                        case "voucher":
                            voucher = String(tData3[1])
                            break;
                        case "reservation_status":
                            reservation_status = String(tData3[1])
                            break;
                        case "reservation_id":
                            reservation_id = String(tData3[1])
                            break;
                        case "purchase_history_id":
                            self.purchase_history_id = String(tData3[1])
                            
                            if self.purchase_history_id != nil, (self.purchase_history_id?.count)! > 0 {
                                shareData.shared.purchase_id = self.purchase_history_id
                            }else{
                                shareData.shared.purchase_id = nil
                            }
                            break
                        default:
                            print("")
                        }
                    }
                    let item01 : PRFR = PRFR.init(status: status, voucher: voucher, reservation_status: reservation_status, reservation_id: reservation_id)
                    shareData.shared.ReservationFinalResponse = item01
                    if status == "1" {
                        AppDelegate()._manager?.track("reservation_payment_success", data: [:])
                        Analytics.logEvent("reservation_payment_success", parameters: nil)
                        self.openNewClass(classIdentifier: "ResponseConfirmOrder")
                        self.navigationController?.popToRootViewController(animated: true)
                    }else if status == "0" {
//                        self.openNewClass(classIdentifier: "ResponseConfirmOrderVFail")
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                }
                
                
                
            })
            webView.addObserver(self, forKeyPath: #keyPath(WKWebView.navigationDelegate), options: .new, context: nil)
            //            print(webView?.url)
        }
        
    }
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        //        print(webView.url)
        
    }
    
    func webView(webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        //        print(webView.url!.absoluteString)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.layer.zPosition = 0
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if self.webView != nil {
            self.webView.removeObserver(self, forKeyPath: #keyPath(WKWebView.navigationDelegate))
            self.webView = nil
        }
        
    }
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Swift.Void)
    {
        print(webView.url?.absoluteURL ?? "Empty")
        switch navigationAction.navigationType {
        case .formSubmitted:
            print(navigationAction.request.description)
        case  .linkActivated:
            print(navigationAction.request.description)
            if let value = navigationAction.request.description as? String {
                if value.contains("jibit://pay.jibit.mobi") {
                    if self.openCustomURLScheme(customURLScheme: value) {
                        // app was opened successfully
                        print("success")
                        self.tabBarController?.tabBar.isHidden = false
                        self.tabBarController?.tabBar.layer.zPosition = 0
                        self.navigationController?.popToRootViewController(animated: true)
                    } else {
                        // handle unable to open the app, perhaps redirect to the App Store
                        print("fail")
                    }
                }
            }
        default:
            if let value = navigationAction.request.description as? String {
                if value.contains("jibit://pay.jibit.mobi") {
                    if self.openCustomURLScheme(customURLScheme: value) {
                        // app was opened successfully
                        print("success")
                        self.tabBarController?.tabBar.isHidden = false
                        self.tabBarController?.tabBar.layer.zPosition = 0
                        self.navigationController?.popToRootViewController(animated: true)
                    } else {
                        // handle unable to open the app, perhaps redirect to the App Store
                        print("fail")
                    }
                }
            }
        }
        
        
        decisionHandler(.allow)
    }
    
    func closeVC() -> Void{
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
}




// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
