//
//  ResponseConfirmOrder.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/16/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import SCLAlertView

class ResponseConfirmOrder: UIViewController, UITableViewDelegate, UITableViewDataSource {
  
    @IBOutlet weak var tableview: UITableView!
    var resultValue: PRFR?
    var refreshAllDataVariable = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.delegate = self
        tableview.dataSource = self
       
        
        self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.font.rawValue: UIFont(name: "IRANSansMobile", size: 14),NSAttributedString.Key.foregroundColor.rawValue:UIColor.white])
        self.title = "جزئیات خرید"
        
    }
    
   
    override func viewWillAppear(_ animated: Bool) {
        
        
        resultValue = shareData.shared.ReservationFinalResponse as? PRFR
        if resultValue == nil {
            closeFunction();
        }
        
        super.viewWillAppear(animated)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if resultValue != nil {
            tableview.reloadData()
        }else{
            closeFunction();
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if resultValue != nil {
            if resultValue?.status == "1"{
                return 2
            }else if resultValue?.status == "0"{
                return 1
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let identifier = "ResponseCustomCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ResponseCustomCell
            //if let value = shareData.shared.ReservationFinalResponse as? PRFR {
            if resultValue?.status == "1"{
                cell.showDetail.setTitle("کد رزرو: " + self.replaceString((resultValue?.voucher)!), for: .normal)
                cell.showDetail.addTarget(self, action: #selector(showDetail), for: .touchUpInside)
            }else if resultValue?.status == "0" {
                cell.showDetail.setTitle("تلاش مجدد", for: .normal)
                cell.showDetail.addTarget(self, action: #selector(tryAgain), for: .touchUpInside)
            }
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        
        case 1:
            let identifier = "ReservationCustomCell1"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ReservationCustomCell1
            
            if let data = shareData.shared.selectedSaensRow as? saensModel {
                cell.pool_name?.text = shareData.shared.selectedPoolName
                cell.fuldate?.text = self.replaceString(data.start_day! + " " + data.start_date!)
                cell.time?.text = self.replaceString(data.time!)
                
            }else if let data = shareData.shared.selectedPackRow as? packsModel{
                cell.pool_name?.text = shareData.shared.selectedPoolName
                cell.fuldate?.text = self.replaceString(data.session_expire_start! + " الی " + data.session_expire_finish!)
                cell.time?.text = ""//self.replaceString(value.from! + " الی " + value.to!)
                
            }else if let data = shareData.shared.selectedCourseRow as? courseModel{
                cell.pool_name?.text = shareData.shared.selectedPoolName
                cell.fuldate?.text = self.replaceString(data.start_datetime!)
                cell.time?.text = ""//self.replaceString(value.from! + " الی " + value.to!)
                
            }
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        default:
            print()
        }
       
        return UITableViewCell()
    }
    
    @objc func showDetail( button: UIButton) {
        
        
    }
    
    @objc func tryAgain( button: UIButton) {
        
        if let value = shareData.shared.reservationObject as? reservationObject {
            
            switch value.type {
            case "seans":
                self.StartReservation(value.objectId, value.objectAdultCount, value.objectChildCount, value.coupon, value.payment_type);
                break;
            case "pack":
                 self.StartPackReservation(value.objectId, value.coupon, value.payment_type);
                break;
            case "course":
                self.StartCourseReservation(value.objectId, value.objectAdultCount, value.coupon, value.payment_type);
                break;
            default:
                print("")
            }
            
        }
        
    }
    
    @IBAction func showTransactionHistory(){
        
        if shareData.shared.purchase_id != nil {
            openNewClass(classIdentifier: "ShowTransactionSpecificationVC")
        }
        
    }
    
    @IBAction func confirmAndClose(){
        
       closeFunction()
        
    }
    
    func closeFunction(){
        shareData.shared.selectedSaensRow = nil
        shareData.shared.selectedPackRow = nil
        shareData.shared.selectedCourseRow = nil
        shareData.shared.ReservationFinalResponse = nil
        shareData.shared.selectedAdultCount = nil
        shareData.shared.selectedChildCount = nil
        shareData.shared.startReservationURL = nil
        shareData.shared.selectedChoosenPoolsRulesAndDetails = nil
        self.navigationController?.popToRootViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func StartReservation(_ saens_row_id:String, _ count_adult: String, _ count_child:String, _ coupon: String,_ payment_type:String) -> Void {
        LoadingOverlayV2.sharedV2.showOverlayV2(view: self.view, message: "در حال ثبت درخواست...")
        self.refreshAllDataVariable = true
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.START_SAENS_RESERVATION,"client_id=\(client_id)&access_token=\(access_token)&saens_row_id=\(saens_row_id)&count_adult=\(count_adult)&count_child=\(count_child)&coupon=\(coupon)&payment_type=\(payment_type)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            
            var reserve_status = ""
            
            do{
                
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    if let item = json as? [String: AnyObject]{
                        //print(json)
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                if let reservation_status = item["reservation_status"] as? String{
                                    
                                    if reservation_status == "redirect_to_port" {
                                        reserve_status = reservation_status
                                        if let url = item["url"] as? String {
                                            shareData.shared.startReservationURL = url
                                        }
                                        
                                    }else if reservation_status == "reserved"  {
                                        reserve_status = reservation_status
                                        if let reservation = item["reservation"] as? [String: AnyObject]{
                                            
                                            let status = "1"
                                            let voucher = String(describing: reservation["voucher"] as AnyObject)
                                            
                                            let purchase_history_id = String(describing: reservation["purchase_history_id"] as AnyObject)
                                            if purchase_history_id != nil, purchase_history_id.count > 0 {
                                                shareData.shared.purchase_id = purchase_history_id
                                            }
                                            let reservation_status = "reserved"
                                            let reservation_id =  String(describing: reservation["reservation_id"])
                                            let item01 : PRFR = PRFR.init(status: status, voucher: voucher, reservation_status: reservation_status, reservation_id: reservation_id)
                                            shareData.shared.ReservationFinalResponse = item01
                                            
                                            
                                            
                                            
                                        }
                                    }
                                    
                                }
                                
                            }
                        }
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                LoadingOverlayV2.sharedV2.hideOverlayView()
                self.refreshAllDataVariable = false
                if WSResponseStatus {
                    if reserve_status == "redirect_to_port"  {
                        self.pushNewClass(classIdentifier: "StartReservation")

                    }else if reserve_status == "reserved" {
                        self.openNewClass(classIdentifier: "ResponseConfirmOrder")
                    }
                }else{
                    self.showAlert("fail")
                }
            })
            
        }
        
        
    }
    
    func StartPackReservation(_ pack_id:String, _ coupon: String, _ payment_type:String) -> Void {
        LoadingOverlay.shared.showOverlay(view: self.view)
        self.refreshAllDataVariable = true
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.START_PACK_RESERVATION,"client_id=\(client_id)&access_token=\(access_token)&pack_id=\(pack_id)&coupon=\(coupon)&payment_type=\(payment_type)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            var reserve_status = ""
            
            do{
                
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    //print(json)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                if let reservation_status = item["reservation_status"] as? String{
                                    
                                    if reservation_status == "redirect_to_port" {
                                        reserve_status = reservation_status
                                        if let url = item["url"] as? String {
                                            shareData.shared.startReservationURL = url
                                        }
                                        
                                    }else if reservation_status == "reserved"  {
                                        reserve_status = reservation_status
                                        if let reservation = item["reservation"] as? [String: AnyObject]{
                                            
                                            let status = "1"
                                            let voucher = String(describing: reservation["voucher"]!)
                                            let purchase_history_id = String(describing: reservation["purchase_history_id"] as AnyObject)
                                            if purchase_history_id != nil, purchase_history_id.count > 0 {
                                                shareData.shared.purchase_id = purchase_history_id
                                            }
                                            let reservation_status = "reserved"
                                            let reservation_id =  String(describing: reservation["reservation_id"])
                                            let item01 : PRFR = PRFR.init(status: status, voucher: voucher, reservation_status: reservation_status, reservation_id: reservation_id)
                                            shareData.shared.ReservationFinalResponse = item01
                                            
                                        }
                                    }
                                    
                                }
                                
                                
                            }
                        }
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                LoadingOverlay.shared.hideOverlayView()
                self.refreshAllDataVariable = false
                if WSResponseStatus {
                    if reserve_status == "redirect_to_port"  {
                        self.pushNewClass(classIdentifier: "StartReservation")

                    }else if reserve_status == "reserved" {
                        self.openNewClass(classIdentifier: "ResponseConfirmOrder")
                    }
                }else{
                    self.showAlert("fail")
                }
            })
            
        }
        
        
    }
    
    func StartCourseReservation(_ course_id:String, _ count_adult: String, _ coupon: String, _ payment_type:String) -> Void {
        LoadingOverlay.shared.showOverlay(view: self.view)
        self.refreshAllDataVariable = true
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.START_COURSE_RESERVATION,"client_id=\(client_id)&access_token=\(access_token)&course_id=\(course_id)&count=\(count_adult)&coupon=\(coupon)&payment_type=\(payment_type)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            var reserve_status = ""
            var errorMessage = ""
            do{
                
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    //print(json)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                if let reservation_status = item["reservation_status"] as? String{
                                    
                                    if reservation_status == "redirect_to_port" {
                                        reserve_status = reservation_status
                                        if let url = item["url"] as? String {
                                            shareData.shared.startReservationURL = url
                                        }
                                        
                                    }else if reservation_status == "reserved"  {
                                        reserve_status = reservation_status
                                        if let reservation = item["reservation"] as? [String: AnyObject]{
                                            
                                            let status = "1"
                                            let voucher = String(describing: reservation["voucher"]!)
                                            let purchase_history_id = String(describing: reservation["purchase_history_id"] as AnyObject)
                                            if purchase_history_id != nil, purchase_history_id.count > 0 {
                                                shareData.shared.purchase_id = purchase_history_id
                                            }
                                            let reservation_status = "reserved"
                                            let reservation_id =  String(describing: reservation["reservation_id"])
                                            let item01 : PRFR = PRFR.init(status: status, voucher: voucher, reservation_status: reservation_status, reservation_id: reservation_id)
                                            shareData.shared.ReservationFinalResponse = item01
                                            
                                            
                                            
                                        }
                                    }
                                    
                                }
                                
                                
                            }else if value == 0 {
                                
                                //                                if let validation =  item["validation"] as? [String: AnyObject]{
                                //                                    if let count = validation["count"] as? [String] {
                                //                                        for item in count {
                                //                                            errorMessage = item
                                //                                        }
                                //                                    }
                                //
                                //                                }
                                
                            }
                        }
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                LoadingOverlay.shared.hideOverlayView()
                self.refreshAllDataVariable = false
                if WSResponseStatus {
                    if reserve_status == "redirect_to_port"  {
                        self.pushNewClass(classIdentifier: "StartReservation")

                    }else if reserve_status == "reserved" {
                        self.openNewClass(classIdentifier: "ResponseConfirmOrder")
                    }
                }else{
                    // self.showDropMessage(errorMessage)
                    self.showAlert("fail")
                    
                }
            })
            
        }
        
        
    }
    
    
    func showAlert(_ type: String) {
        
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "IRANSansMobile", size: 17)!,
            kTextFont: UIFont(name: "IRANSansMobile", size: 14)!,
            kButtonFont: UIFont(name: "IRANSansMobile", size: 14)!,
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        
        switch type {
        case "fail":
            alertView.addButton("بستن") {
            }
            alertView.showError("اخطار:", subTitle: "درخواست رزرو بلیت شما با خطا مواجه شد. لطفامجددا تلاش کنید .")
            break
        default:
            print("")
        }
        
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
