//
//  ReservationVCPart2.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/13/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import SCLAlertView

class ReservationVCPart2:UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate  {
   
    var refreshAllDataVariable = false
    var list: [AnyObject] = []
    var tableTypeList: [String] = [];
    var saensCartModelObject: saensCartModel?
    var packCartModelObject: packCartModel?
    var courseCartModelObject: courseCartModel?
    var type = ""
    var adultCount = 1
    var childCount = 0
    var payment_type = "0"
    
    @IBAction func submitCopounCode(_ sender: Any){
        var copoun = ""
        let ndx = IndexPath(row:0, section: 0)
        let cell = tableView.cellForRow(at:ndx) as! ReservationCustomCell3
        if let value = cell.copounCode.text as? String {
            copoun = value
        }
        
        if shareData.shared.selectedSaensRow != nil {
            self.type = "saens"
            self.list.append(shareData.shared.selectedSaensRow!)
            if let value = shareData.shared.selectedSaensRow?.saens_row_id as? String {
                self.CalculateReservation(value,String(self.adultCount),String(self.childCount),copoun);
            }
        }else if shareData.shared.selectedPackRow != nil{
            self.type = "pack"
            self.list.append(shareData.shared.selectedPackRow!)
            if let value = shareData.shared.selectedPackRow?.pack_id as? String {
                self.CalculatePackReservation(value,copoun);
            }
        }else if shareData.shared.selectedCourseRow != nil{
            self.type = "course"
            self.list.append(shareData.shared.selectedCourseRow!)
            if let value = shareData.shared.selectedCourseRow?.course_id as? String, let adultCount = shareData.shared.selectedAdultCount as? String {
                self.CalculateCourseReservation(value,String(self.adultCount),copoun);
            }
        }
        
    }
    
    @IBAction func startReservation(_ sender: Any){
        
        var copoun = ""
        let ndx = IndexPath(row:0, section: 0)
        let cell = tableView.cellForRow(at:ndx) as! ReservationCustomCell3
        if let value = cell.copounCode.text as? String {
              copoun = value
        }
        
        if shareData.shared.selectedSaensRow != nil {
            if let value = shareData.shared.selectedSaensRow?.saens_row_id as? String {
                let  object: reservationObject = reservationObject.init(type: "seans",objectId: value, objectAdultCount: String(self.adultCount), objectChildCount: String(self.childCount), payment_type: payment_type, coupon: copoun )
                shareData.shared.reservationObject = object
                self.StartReservation(value,String(self.adultCount),String(self.childCount),copoun, payment_type);
            }
        }else if shareData.shared.selectedPackRow != nil{
            if let value = shareData.shared.selectedPackRow?.pack_id as? String {
                let  object: reservationObject = reservationObject.init(type: "pack",objectId: value, objectAdultCount: "", objectChildCount: "", payment_type: payment_type, coupon: copoun )
                shareData.shared.reservationObject = object
                self.StartPackReservation(value,copoun, payment_type);
            }
        }else if shareData.shared.selectedCourseRow != nil{
            if let value = shareData.shared.selectedCourseRow?.course_id as? String, let adultCount = shareData.shared.selectedAdultCount as? String {
                let  object: reservationObject = reservationObject.init(type: "course",objectId: value, objectAdultCount: String(self.adultCount), objectChildCount: "", payment_type: payment_type, coupon: copoun )
                shareData.shared.reservationObject = object
                self.StartCourseReservation(value,String(self.adultCount),copoun,payment_type);
            }
        }
        
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableTypeList.count
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.font.rawValue: UIFont(name: "IRANSansMobile", size: 14),NSAttributedString.Key.foregroundColor.rawValue:UIColor.white])
            self.title = "پرداخت"
        
        
        if let value = shareData.shared.selectedAdultCount as? String {
          if let count = Int(value) as? Int {
                self.adultCount = count
            }
        }
        
        if let value = shareData.shared.selectedChildCount as? String {
            if let count = Int(value) as? Int {
                self.childCount = count
            }
        }
        
        
        if shareData.shared.selectedSaensRow != nil {
            self.type = "saens"
            self.list.append(shareData.shared.selectedSaensRow!)
            if let value = shareData.shared.selectedSaensRow?.saens_row_id as? String {
                self.CalculateReservation(value,String(self.adultCount),String(self.childCount),"");
            }
        }else if shareData.shared.selectedPackRow != nil{
            self.type = "pack"
            self.list.append(shareData.shared.selectedPackRow!)
            if let value = shareData.shared.selectedPackRow?.pack_id as? String {
                self.CalculatePackReservation(value,"");
            }
        }else if shareData.shared.selectedCourseRow != nil{
            self.type = "course"
            self.list.append(shareData.shared.selectedCourseRow!)
            if let value = shareData.shared.selectedCourseRow?.course_id as? String, let adultCount = shareData.shared.selectedAdultCount as? String {
                self.CalculateCourseReservation(value,String(self.adultCount),"");
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let value = shareData.shared.selectedChoosenServices as? String {
        navigationController?.navigationItem.title = "خرید بلیط"
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        switch tableTypeList[indexPath.row] {
        case "ReservationCustomCell1":
            let identifier = "ReservationCustomCell1"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ReservationCustomCell1
            let data = self.list[indexPath.row]
            if let value = data as? saensModel {
                cell.pool_name?.text = shareData.shared.selectedPoolName
                cell.fuldate?.text = self.replaceString(value.start_day! + " " + value.start_date!)
                cell.time?.text = self.replaceString(value.time!)
            }else if let value = data as? courseModel {
                cell.pool_name?.text = shareData.shared.selectedPoolName
                cell.fuldate?.text = self.replaceString(value.start_datetime!)
                cell.time?.text = ""//self.replaceString(value.from! + " الی " + value.to!)
            }else if let value = data as? packsModel {
                cell.pool_name?.text = shareData.shared.selectedPoolName
                cell.fuldate?.text = self.replaceString(value.session_expire_start! + " الی " + value.session_expire_finish!)
                cell.time?.text = ""//self.replaceString(value.from! + " الی " + value.to!)
            }
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell;
            
      
            
        case "ReservationCustomCell3":
            let identifier = "ReservationCustomCell3"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ReservationCustomCell3
            cell.copounCode.delegate = self
            doneWithKeyboard(cell.copounCode)
            if let value = getCopounMessage() {
                cell.copounMessage.text = value
            }else{
                cell.copounMessage.text = ""
                cell.copounMessage.bounds.size.height = 0.0
                cell.cellView.bounds.size.height = 91.0
            }
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell;
            
        case "ReservationCustomCell4":
            let identifier = "ReservationCustomCell4"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ReservationCustomCell4
            
            switch self.type {
            case "saens":
                if let value = self.saensCartModelObject  {
                    cell.price?.text = self.setSeprator(value.total_sale!) //+ " تومان"
                    cell.discount?.text = " تخفیف: " + self.setSeprator(self.calculateDiscountPrice(value.total_market!, value.total_sale!)) //+ " تومان"
                    if let value1 = value.coupon_message  {
                        if value1.count > 0 {
                            cell.discountMessage?.text = "( " + value1 + " )"
                        }else{
                            cell.discountMessage?.text = ""
                        }
                    }
                    if let value1 = value.coupon_credit  {
                        if value1.count > 0 {
                            cell.sodeShoma?.text = " سود شما (با استفاده از کوپن تخفیف)" + self.setSeprator(value1)// + " تومان"
                        }else{
                            cell.sodeShoma?.text = "فاقد کوپن تخفیف"
                        }
                        
                    }
                }
                break;
            case "pack":
                if let value = self.packCartModelObject  {
                    cell.price?.text = self.setSeprator(value.total_sale!)// + " تومان"
                    cell.discount?.text = " تخفیف: " + self.setSeprator(self.calculateDiscountPrice(value.total_market!, value.total_sale!))// + " تومان"
                    if let value1 = value.coupon_message as? String {
                        if value1.count > 0 {
                            cell.discountMessage?.text = "( " + value1 + " )"
                        }else{
                            cell.discountMessage?.text = ""
                        }
                    }
                    if let value1 = value.coupon_credit  {
                        if value1.count > 0 {
                            cell.sodeShoma?.text = " سود شما (با استفاده از کوپن تخفیف)" + self.setSeprator(value1)// + " تومان"
                        }else{
                            cell.sodeShoma?.text = "فاقد کوپن تخفیف"
                        }
                        
                    }
                }
                
                break;
            case "course":
                if let value = self.courseCartModelObject  {
                    cell.price?.text = self.setSeprator(value.total_sale!) //+ " تومان"
                    cell.discount?.text = " تخفیف: " + self.setSeprator(self.calculateDiscountPrice(value.total_market!, value.total_sale!)) //+ " تومان"
                    if let value1 = value.coupon_message  {
                        if value1.count > 0 {
                            cell.discountMessage?.text = "( " + value1 + " )"
                        }else{
                            cell.discountMessage?.text = ""
                        }
                    }
                    if let value1 = value.coupon_credit  {
                        if value1.count > 0 {
                            cell.sodeShoma?.text = " سود شما (با استفاده از کوپن تخفیف)" + self.setSeprator(value1)// + " تومان"
                        }else{
                            cell.sodeShoma?.text = "فاقد کوپن تخفیف"
                        }
                        
                    }
                }
                break;
            default:
                print("")
            }
            
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell;
      
        case "ReservationCustomCell8":
            let identifier = "ReservationCustomCell8"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ReservationCustomCell8
            cell.rowTitle.text = "مبلغ کل:"
            if let value = self.saensCartModelObject  {
                cell.rowValue.text = self.setSeprator(value.total_market!)
            }else if let value = self.packCartModelObject {
                cell.rowValue.text = self.setSeprator(value.total_market!)
            }else if let value = self.courseCartModelObject  {
                cell.rowValue.text = self.setSeprator(value.total_market!)
            }
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell;
        case "ReservationCustomCell9":
            let identifier = "ReservationCustomCell9"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ReservationCustomCell8
            cell.rowTitle.text = "تخفیف هدیه:"
            if let value = self.saensCartModelObject  {
                cell.rowValue.text = self.setSeprator(self.calculateDiscountPrice(value.total_market!, value.total_sale!))
            }else if let value = self.packCartModelObject  {
                cell.rowValue.text = self.setSeprator(self.calculateDiscountPrice(value.total_market!, value.total_sale!))
            }else if let value = self.courseCartModelObject {
                cell.rowValue.text = self.setSeprator(self.calculateDiscountPrice(value.total_market!, value.total_sale!))
            }
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell;
        case "ReservationCustomCell10":
            let identifier = "ReservationCustomCell10"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ReservationCustomCell8
            cell.rowTitle.text = "مبلغ پرداختی:"
            if let value = self.saensCartModelObject  {
                cell.rowValue.text = self.setSeprator(value.total_sale!)
            }else if let value = self.packCartModelObject  {
                cell.rowValue.text = self.setSeprator(value.total_sale!)
            }else if let value = self.courseCartModelObject  {
                cell.rowValue.text = self.setSeprator(value.total_sale!)
            }
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell;
       case "ReservationCustomCell6":
            let identifier = "ReservationCustomCell6"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ReservationCustomCell6
            
            if self.payment_type == "0" {
                cell.sectionStatus.image = UIImage(named: "ic_radio_button_green")
            }else{
                cell.sectionStatus.image = UIImage(named: "ic_radio_button_dark")
            }
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell;
        case "ReservationCustomCell7":
            let identifier = "ReservationCustomCell7"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ReservationCustomCell7
            if self.payment_type == "1" {
                cell.sectionStatus.image = UIImage(named: "ic_radio_button_green")
            }else{
                cell.sectionStatus.image = UIImage(named: "ic_radio_button_dark")
            }
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell;
        default:
            
            print("")
        }
        
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 2:
            self.payment_type = "0"
            self.tableView.reloadData()
            break
        case 3:
            self.payment_type = "1"
            self.tableView.reloadData()
            break
        default:
            print("")
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
      
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func prepareDataForTheView()->Void{
        
        self.tableTypeList = []

        self.tableTypeList.append("ReservationCustomCell3")
//      self.tableTypeList.append("ReservationCustomCell4")
        self.tableTypeList.append("ReservationCustomCell8")
        self.tableTypeList.append("ReservationCustomCell9")
        self.tableTypeList.append("ReservationCustomCell10")
        self.tableTypeList.append("ReservationCustomCell6")
        self.tableTypeList.append("ReservationCustomCell7")
        
        tableView.reloadData()
        
        self.getProfile()

    }
    
    // get data from profile
    func getProfile() -> Void {
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.PROFILE,"client_id=\(client_id)&access_token=\(access_token)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            var mCredit = ""
            //            var mTelephone = ""
            //            var mName = ""
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    //print(json)
                    
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                if let profile = item["profile"] as? [String: AnyObject] {
                                    //                                    if let value = profile["name"] as? String {
                                    //                                        mName = value
                                    //                                    }
                                    //                                    if let value = profile["mobile"] as? String {
                                    //                                        mTelephone = value
                                    //                                    }
                                    if let value = profile["credit"] as? Int {
                                        mCredit = String(value)
                                    }
                                }
                            }
                        }
                    }
                    
                    
                }
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                
                if WSResponseStatus {
                    let ndx = IndexPath(row:5, section: 0)
                    if let cell = self.tableView.cellForRow(at:ndx) as? ReservationCustomCell7 {
                        cell.cellTitle.setTitle("کسر از اعتبار ("+self.setSeprator(mCredit) + ")", for: .normal)
                    }
                }
            })
            
        }
    }
    
    func calculateDiscountPrice(_ totalPrice: String, _ discountPrice: String) -> String {
        var total : Int64 = 0
        var discount : Int64 = 0
        
        do {
            
            if let value = Int64(totalPrice) as? Int64 {
                total = value
            }
            if let value = Int64(discountPrice) as? Int64 {
                discount = value
            }
            total = total - discount
            return String(total)
            
        }catch{
            fatalError("this is error; \(total)")
        }
        
        return "0"
    }
    
    func CalculateReservation(_ saens_row_id:String, _ count_adult: String, _ count_child:String, _ coupon: String) -> Void {
        LoadingOverlay.shared.showOverlay(view: self.view)
        self.refreshAllDataVariable = true
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.CALCULATE_RESERVATION,"client_id=\(client_id)&access_token=\(access_token)&saens_row_id=\(saens_row_id)&count_adult=\(count_adult)&count_child=\(count_child)&coupon=\(coupon)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            do{
                
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                if let cart =  item["cart"] as? [String: AnyObject]{
                                    self.saensCartModelObject =  saensCartModel.prepare(cart as AnyObject)
                                }
                                
                                
                            }else{
                                if let mError = item["error"] as? String {
                                    if mError == "coupon_error" {
                                    if let cart = item["cart"] as? [String: AnyObject] {
                                        if let coupon_message = cart["coupon_message"] as? String {
                                             self.saensCartModelObject?.coupon_message = coupon_message
                                        }else{
                                             self.saensCartModelObject?.coupon_message = ""
                                        }

                                    }
                                    }
                                    
                                }
                            }
                        }
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                LoadingOverlay.shared.hideOverlayView()
                self.refreshAllDataVariable = false
                self.prepareDataForTheView();
//                if WSResponseStatus {
//                    self.prepareDataForTheView();
//                    //set the response ticket Count
////                    let ndx = IndexPath(row:1, section: 0)
////                    let cell = self.tableView.cellForRow(at:ndx) as! ReservationCustomCell2
////                    cell.number.text = "1"
//
//                    }else{
//
//                }
            })
            
        }
        
        
    }
    
    func CalculatePackReservation(_ pack_id:String, _ coupon: String) -> Void {
        LoadingOverlay.shared.showOverlay(view: self.view)
        self.refreshAllDataVariable = true
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.CALCULATE_PACK,"client_id=\(client_id)&access_token=\(access_token)&pack_id=\(pack_id)&coupon=\(coupon)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            do{
                
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    //print(json)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                if let cart =  item["cart"] as? [String: AnyObject]{
                                    self.packCartModelObject =  packCartModel.prepare(cart as AnyObject)
                                }
                            }else{
                                if let mError = item["error"] as? String {
                                    if mError == "coupon_error" {
                                        if let cart = item["cart"] as? [String: AnyObject] {
                                            if let coupon_message = cart["coupon_message"] as? String {
                                                 self.packCartModelObject?.coupon_message = coupon_message
                                            }else{
                                                 self.packCartModelObject?.coupon_message = ""
                                            }
                                            
                                        }
                                    }
                                    
                                }
                            }
                        }
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                LoadingOverlay.shared.hideOverlayView()
                self.refreshAllDataVariable = false
                self.prepareDataForTheView();
//                if WSResponseStatus {
//
//
//                }else{
//
//                }
            })
            
        }
        
        
    }
    
    func CalculateCourseReservation(_ course_id:String, _ count_adult: String, _ coupon: String) -> Void {
        LoadingOverlay.shared.showOverlay(view: self.view)
        self.refreshAllDataVariable = true
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.CALCULATE_COURSE_RESERVATION,"client_id=\(client_id)&access_token=\(access_token)&course_id=\(course_id)&count=\(count_adult)&coupon=\(coupon)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            var errorMessage = ""
            do{
                
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    //print(json)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                if let cart =  item["cart"] as? [String: AnyObject]{
                                    self.courseCartModelObject =  courseCartModel.prepare(cart as AnyObject)
                                }
                                
                                
                            }else if value == 0 {
                                
                                if let validation =  item["validation"] as? [String: AnyObject]{
                                    if let count = validation["count"] as? [String] {
                                        for item in count {
                                            errorMessage = item
                                        }
                                    }
                                    
                                }
                                
                                if let mError = item["error"] as? String {
                                    if mError == "coupon_error" {
                                        if let cart = item["cart"] as? [String: AnyObject] {
                                            if let coupon_message = cart["coupon_message"] as? String {
                                                self.courseCartModelObject?.coupon_message = coupon_message
                                            }else{
                                                self.courseCartModelObject?.coupon_message = ""
                                            }
                                            
                                        }
                                    }
                                    
                                }
                                
                            }
                        }
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                LoadingOverlay.shared.hideOverlayView()
                self.refreshAllDataVariable = false
                self.prepareDataForTheView();

                if WSResponseStatus {
                    // set the response ticket Count
//                    let ndx = IndexPath(row:1, section: 0)
//                    let cell = self.tableView.cellForRow(at:ndx) as! ReservationCustomCell2
//                    if self.courseCartModelObject != nil {
//                        cell.number.text = self.courseCartModelObject?.count
//                    }
                }else{
                    self.showDropMessage(errorMessage)
                }
            })
            
        }
        
        
    }
    
    func StartReservation(_ saens_row_id:String, _ count_adult: String, _ count_child:String, _ coupon: String,_ payment_type:String) -> Void {
        LoadingOverlayV2.sharedV2.showOverlayV2(view: self.view, message: "در حال ثبت درخواست...")
        self.refreshAllDataVariable = true
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.START_SAENS_RESERVATION,"client_id=\(client_id)&access_token=\(access_token)&saens_row_id=\(saens_row_id)&count_adult=\(count_adult)&count_child=\(count_child)&coupon=\(coupon)&payment_type=\(payment_type)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            
            var reserve_status = ""
            
            do{
                
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    if let item = json as? [String: AnyObject]{
                        //print(json)
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                if let reservation_status = item["reservation_status"] as? String{
                                    
                                    if reservation_status == "redirect_to_port" {
                                        reserve_status = reservation_status
                                        if let url = item["url"] as? String {
                                            shareData.shared.startReservationURL = url
                                        }
                                        
                                    }else if reservation_status == "reserved"  {
                                             reserve_status = reservation_status
                                        if let reservation = item["reservation"] as? [String: AnyObject]{
                                        
                                            let status = "1"
                                            let voucher = String(describing: reservation["voucher"] as AnyObject)
                                            
                                            let purchase_history_id = String(describing: reservation["purchase_history_id"] as AnyObject)
                                            if purchase_history_id != nil, purchase_history_id.count > 0 {
                                                 shareData.shared.purchase_id = purchase_history_id
                                            }
                                            let reservation_status = "reserved"
                                            let reservation_id =  String(describing: reservation["reservation_id"])
                                            let item01 : PRFR = PRFR.init(status: status, voucher: voucher, reservation_status: reservation_status, reservation_id: reservation_id)
                                            shareData.shared.ReservationFinalResponse = item01
                                           


                                            
                                        }
                                    }
                                    
                                }
                                
                            }
                        }
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                LoadingOverlayV2.sharedV2.hideOverlayView()
                self.refreshAllDataVariable = false
                if WSResponseStatus {
                    if reserve_status == "redirect_to_port"  {
                        self.pushNewClass(classIdentifier: "StartReservation")
                    }else if reserve_status == "reserved" {
                        
                        // track Reservation Payment success event in ios
//                        let event = ADJEvent.init(eventToken: "8blg24")
//                        Adjust.trackEvent(event)
                        
                        self.openNewClass(classIdentifier: "ResponseConfirmOrder")
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                }else{
                    self.showAlert("fail")
                }
            })
            
        }
        
        
    }
 
    func StartPackReservation(_ pack_id:String, _ coupon: String, _ payment_type:String) -> Void {
        LoadingOverlay.shared.showOverlay(view: self.view)
        self.refreshAllDataVariable = true
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.START_PACK_RESERVATION,"client_id=\(client_id)&access_token=\(access_token)&pack_id=\(pack_id)&coupon=\(coupon)&payment_type=\(payment_type)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            var reserve_status = ""

            do{
                
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    //print(json)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                if let reservation_status = item["reservation_status"] as? String{
                                    
                                    if reservation_status == "redirect_to_port" {
                                        reserve_status = reservation_status
                                        if let url = item["url"] as? String {
                                            shareData.shared.startReservationURL = url
                                        }
                                        
                                    }else if reservation_status == "reserved"  {
                                         reserve_status = reservation_status
                                        if let reservation = item["reservation"] as? [String: AnyObject]{
                                            
                                            let status = "1"
                                            let voucher = String(describing: reservation["voucher"]!)
                                            let purchase_history_id = String(describing: reservation["purchase_history_id"] as AnyObject)
                                            if purchase_history_id != nil, purchase_history_id.count > 0 {
                                                shareData.shared.purchase_id = purchase_history_id
                                            }
                                            let reservation_status = "reserved"
                                            let reservation_id =  String(describing: reservation["reservation_id"])
                                            let item01 : PRFR = PRFR.init(status: status, voucher: voucher, reservation_status: reservation_status, reservation_id: reservation_id)
                                            shareData.shared.ReservationFinalResponse = item01
                                            
                                        }
                                    }
                                    
                                }

                                
                            }
                        }
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                LoadingOverlay.shared.hideOverlayView()
                self.refreshAllDataVariable = false
                if WSResponseStatus {
                    if reserve_status == "redirect_to_port"  {
                        self.pushNewClass(classIdentifier: "StartReservation")
                    }else if reserve_status == "reserved" {
                        
                            // track Pack Payment success event in ios
//                            let event = ADJEvent.init(eventToken: "xpfjrr")
//                            Adjust.trackEvent(event)
                        
                        self.openNewClass(classIdentifier: "ResponseConfirmOrder")
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                }else{
                    self.showAlert("fail")
                }
            })
            
        }
        
        
    }
    
    func StartCourseReservation(_ course_id:String, _ count_adult: String, _ coupon: String, _ payment_type:String) -> Void {
        LoadingOverlay.shared.showOverlay(view: self.view)
        self.refreshAllDataVariable = true
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.START_COURSE_RESERVATION,"client_id=\(client_id)&access_token=\(access_token)&course_id=\(course_id)&count=\(count_adult)&coupon=\(coupon)&payment_type=\(payment_type)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            var reserve_status = ""
            var errorMessage = ""
            do{
                
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    //print(json)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                if let reservation_status = item["reservation_status"] as? String{
                                    
                                    if reservation_status == "redirect_to_port" {
                                        reserve_status = reservation_status
                                        if let url = item["url"] as? String {
                                            shareData.shared.startReservationURL = url
                                        }
                                        
                                    }else if reservation_status == "reserved"  {
                                         reserve_status = reservation_status
                                        if let reservation = item["reservation"] as? [String: AnyObject]{
                                            
                                            let status = "1"
                                            let voucher = String(describing: reservation["voucher"]!)
                                            let purchase_history_id = String(describing: reservation["purchase_history_id"] as AnyObject)
                                            if purchase_history_id != nil, purchase_history_id.count > 0 {
                                                shareData.shared.purchase_id = purchase_history_id
                                            }
                                            let reservation_status = "reserved"
                                            let reservation_id =  String(describing: reservation["reservation_id"])
                                            let item01 : PRFR = PRFR.init(status: status, voucher: voucher, reservation_status: reservation_status, reservation_id: reservation_id)
                                            shareData.shared.ReservationFinalResponse = item01
                                        
                                        }
                                    }
                                    
                                }
                                
                                
                            }else if value == 0 {
                                
//                                if let validation =  item["validation"] as? [String: AnyObject]{
//                                    if let count = validation["count"] as? [String] {
//                                        for item in count {
//                                            errorMessage = item
//                                        }
//                                    }
//
//                                }
                                
                            }
                        }
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                LoadingOverlay.shared.hideOverlayView()
                self.refreshAllDataVariable = false
                if WSResponseStatus {
                    if reserve_status == "redirect_to_port"  {
                        self.pushNewClass(classIdentifier: "StartReservation")
                    }else if reserve_status == "reserved" {
                        
                        // track Course payment success event in ios
//                        let event = ADJEvent.init(eventToken: "8b89yi")
//                        Adjust.trackEvent(event)
                        
                        self.openNewClass(classIdentifier: "ResponseConfirmOrder")
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                }else{
                   // self.showDropMessage(errorMessage)
                    self.showAlert("fail")

                }
            })
            
        }
        
        
    }
    
    @IBAction func payWithCredit(){
        
        self.payment_type = "1"
        self.tableView.reloadData()
        
        
    }
    
    @IBAction func onlinePayment(){
        
        self.payment_type = "0"
        self.tableView.reloadData()
        
        
    }

    func getCopounMessage() -> String? {
        do{
        if let value = saensCartModelObject as? saensCartModel{
            if value.coupon_message!.count > 0 {
                return value.coupon_message!
            }
        }else if let value = packCartModelObject as? packCartModel{
            if value.coupon_message!.count > 0 {
                return value.coupon_message!
            }
        }else if let value = courseCartModelObject as? courseCartModel{
            if value.coupon_message!.count > 0 {
                return value.coupon_message!
            }
        }
        }catch{
            fatalError("get copoun message error \(error)")
        }
        return nil
    }
    //************** softkeyboard tool bar **********************//
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField
        {
            
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
    func doneWithKeyboard(_ sender: UITextField){
        
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        keyboardToolbar.isTranslucent = true
        keyboardToolbar.barTintColor = UIColor.white
        
      
        
        let buttonConfirm = UIButton(type: .system)
        buttonConfirm.setTitle("بستن", for: .normal)
        buttonConfirm.sizeToFit()
        buttonConfirm.titleLabel?.font = UIFont(name: "IRANSansMobile", size: 12)
        buttonConfirm.tintColor = UIColor.black
        buttonConfirm.addTarget(self, action: #selector(closeKeyboard(_:)), for: .touchUpInside)
        
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        keyboardToolbar.items = [flexBarButton,UIBarButtonItem(customView: buttonConfirm)]
        
        if let value = sender as? UITextField {
            value.inputAccessoryView = keyboardToolbar
        }else if let value = sender as? UITextView {
            value.inputAccessoryView = keyboardToolbar
        }
        
    }
   
    func showAlert(_ type: String) {
        
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "IRANSansMobile", size: 17)!,
            kTextFont: UIFont(name: "IRANSansMobile", size: 14)!,
            kButtonFont: UIFont(name: "IRANSansMobile", size: 14)!,
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        
        switch type {
        case "fail":
            alertView.addButton("بستن") {
            }
            alertView.showError("اخطار:", subTitle: "درخواست رزرو بلیت شما با خطا مواجه شد. لطفامجددا تلاش کنید .")
            break
        default:
            print("")
        }
        
    }

}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
