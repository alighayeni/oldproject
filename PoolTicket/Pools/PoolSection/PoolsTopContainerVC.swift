//
//  PoolsTopContainerVC.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 3/22/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import ImageSlideshow
import Cosmos

class PoolsTopContainerVC: UIViewController {
    
    @IBOutlet weak var bannerTopView: ImageSlideshow!
    @IBOutlet weak var poolDiscountBg: UIImageView!
    @IBOutlet weak var poolDiscount: UILabel!
    @IBOutlet var poolRateBar: CosmosView!
    @IBOutlet weak var poolImage: UIImageView!
    @IBOutlet weak var availableForWomen: UIImageView!
    @IBOutlet weak var availableForMan: UIImageView!
    @IBOutlet weak var poolAddress: UILabel!
    @IBOutlet weak var poolName: UILabel!
    @IBOutlet weak var poolPriceWithDiscount: UILabel!
    @IBOutlet weak var poolPrice: UILabel!
    @IBOutlet weak var poolPriceRedLine: UIImageView!
    @IBOutlet weak var topImagesHeight: NSLayoutConstraint!
    @IBOutlet weak var poolPriceInGisheTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    poolRateBar.settings.fillMode = .half
        
        
    }
    
    
}
