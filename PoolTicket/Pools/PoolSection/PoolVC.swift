//
//  PoolVC.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 3/21/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import ImageSlideshow
import Cosmos
import AudioToolbox
import SCLAlertView
import Firebase

class PoolVC: UIViewController{

    @IBOutlet weak var tableview:UITableView!
    var PoolsTopContainerVC: PoolsTopContainerVC?
    var poolsByIdModel: poolsById?
    var gallery: [String] = []
    var tableListCount = 0
    var ratingList: [ratingsObject] = []
    
    var list : [String] = []
    var itemInQueue = 0

    
    @IBOutlet weak var chooseOption:UIView!
    @IBOutlet weak var chooseOption_resevePack:UIView!
    @IBOutlet weak var chooseOption_reserve:UIView!
    @IBOutlet weak var chooseOption_swimming:UIView!

    @IBOutlet weak var CO_resevePack:UIButton!
    @IBOutlet weak var CO_reserve:UIButton!
    @IBOutlet weak var CO_swimming:UIButton!
    
    @IBOutlet weak var chooseServicesButtonView: UIView!
    @IBOutlet weak var choseServicesButtonHeight: NSLayoutConstraint!
    
    @IBOutlet weak var ic_reserve_ticket: UIImageView!
    @IBOutlet weak var ic_reserve_pack: UIImageView!
    @IBOutlet weak var ic_reserve_course: UIImageView!

    var BUTTOM_BAR_OPEN = false
    
    @IBAction func reservePackAction(_ sender: Any){
        if let value = self.poolsByIdModel?.status_sell_pack as? String{
            if value == "1" {
                self.pushNewClass(classIdentifier: "Pools_Pack_list")
            }
        }
    }
    
    @IBAction func reserveAction(_ sender: Any){
        if let value = self.poolsByIdModel?.status_sell_ticket as? String{
            if value == "1" {
               self.pushNewClass(classIdentifier: "Pools_Saens_list")
            }
        }
    }
    
    @IBAction func swimmingCourseAction(_ sender: Any){
        if let value = self.poolsByIdModel?.status_sell_course as? String{
            if value == "1" {
                self.pushNewClass(classIdentifier: "Pool_Course_list")
            }
        }
    }
    
    @IBAction func closeButtomBar(_ sender: Any){
        self.showButtomBar(false)
        AudioServicesPlaySystemSound(1521)
    }
    
    @IBAction func openButtomBar(_ sender: Any){
        self.showButtomBar(true)
        AudioServicesPlaySystemSound(1521)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PoolsTopContainerVC" {
            if let vc = segue.destination as? PoolsTopContainerVC {
                PoolsTopContainerVC = vc
                
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.delegate = self
        tableview.dataSource = self
        self.title = ""
        self.getPoolDetail();
        
        if self.PoolsTopContainerVC?.bannerTopView != nil {
            let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapOnSlideShow))
            self.PoolsTopContainerVC?.bannerTopView.addGestureRecognizer(gestureRecognizer)
            self.PoolsTopContainerVC?.bannerTopView.slideshowInterval = 5
        }
        
        // track Pool Visit in ios
//        let event = ADJEvent.init(eventToken: "9qkwk4")
//        Adjust.trackEvent(event)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        self.showButtomBar(false)
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage() //remove pesky 1 pixel line
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.barTintColor = UIColor.white
        
        
//        let closeTopBarBtn = UIBarButtonItem(image: UIImage(named: "ic_arrow_left_white"),style: .plain ,target: self, action: #selector(self.back(img:)))
//        self.navigationItem.leftBarButtonItem = closeTopBarBtn
        
        
//        self.navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "IRANSansMobile", size: 14),NSForegroundColorAttributeName:UIColor.white]
//        let customFont = UIFont(name: "IRANSansMobile", size: 12.0)!
//        UIBarButtonItem.appearance().setTitleTextAttributes([NSFontAttributeName: customFont], for: .normal)
//        self.title = ""
        
        
        // make ui perfect for iphone plus
        if  DeviceType.IS_IPHONE_6P_6SP_7P_8P{
            if let value = PoolsTopContainerVC?.topImagesHeight as? NSLayoutConstraint {
                value.constant = 277
            }
        }
        
        self.chooseOption_resevePack.dropShadow();
        self.chooseOption_reserve.dropShadow();
        self.chooseOption_swimming.dropShadow();
    }
    
    func getPoolDetail(){
        if let value = Network.reachability?.isConnectedToNetwork {
            if value {
                self.getPoolById(DataSourceManagement.selectedPoolID)
            }else{
                showAlert("internetNotAvailable")
            }
        }else{
            showAlert("internetNotAvailable")
        }
    }
    
    @objc func didTapOnSlideShow() {
        self.PoolsTopContainerVC?.bannerTopView?.presentFullScreenController(from: self)
        
    }
    
    func back(img: AnyObject)
    {

        if !BUTTOM_BAR_OPEN {
            self.dismiss(animated: true, completion: nil)
        }else{
            showButtomBar(false)
        }
    }
    
    @objc func share(img: AnyObject)
    {

        AppDelegate()._manager?.track("share", data: ["pool_id":"\(DataSourceManagement.selectedPoolID)"])
        Analytics.logEvent("share", parameters: ["pool_id":"\(DataSourceManagement.selectedPoolID)"])
        let shareString = ((self.poolsByIdModel?.share_description) ?? "") + "\n\n" + ((self.poolsByIdModel?.share_link) ?? "")
        let activityViewController = UIActivityViewController(activityItems:
            [shareString], applicationActivities: nil)
        let excludeActivities = [
            UIActivity.ActivityType.print,
            UIActivity.ActivityType.assignToContact,
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType.addToReadingList,
            UIActivity.ActivityType.postToFlickr,
            UIActivity.ActivityType.postToTencentWeibo,
            UIActivity.ActivityType.airDrop]
        activityViewController.excludedActivityTypes = excludeActivities;
        
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            activityViewController.popoverPresentationController?.sourceView = super.view
        }
        
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    
    @objc func setAsFavorite(button: UIBarButtonItem)
    {
        // check if user get logged in or not
        if SwiftUtil.getUserAccessToken() == ""{
            openNewClass(classIdentifier: "CheckPhoneNumberRegistrationVC")
        }else{
            if let value = self.poolsByIdModel?.is_favorite {
                if value != "1" {
                    if let poolValue = self.poolsByIdModel as? poolsById {
                        self.setFavoritePool(poolValue.pool_id!)
                    }
                    button.image = UIImage(named: "ic_favorite_full_red")
                }else if value == "1" {
                    if let poolValue = self.poolsByIdModel as? poolsById {
                        self.usetFavoritePool(poolValue.pool_id!)
                    }
                    button.image = UIImage(named: "ic_favorite_empty_white")
                }
            }
        }
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func getPoolById(_ pool_id: String) -> Void {
        LoadingOverlayV2.sharedV2.showOverlayV2(view: self.view, message: "در حال دریافت اطلاعات...")
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let pool_image_width  = "800"//String(describing: self.view.frame.width);
        let index = wsPOST.init(Constants.POOLS_BY_ID + pool_id,"client_id=\(client_id)&access_token=\(access_token)&pool_image_width=\(pool_image_width)&pool_id=\(pool_id)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            do{
                
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                            WSResponseStatus = true
                                if let pool = item["pool"] as? [String: AnyObject] {
                                  
                                self.poolsByIdModel = poolsById.prepare(pool as AnyObject)
                               
                                if let gallery = pool["gallery"] as? [[String: AnyObject]] {
                                   
                                    for object in gallery {
                                       
                                        if let type = object["type"] as? String{
                                           
                                            if type == "video" {
                                               
                                                if let value = object["poster"] as? String {
                                                    self.gallery.append(value)
                                                }
                                                
                                            }else if type == "image" {
                                               
                                                if let value = object["url"] as? String {
                                                    self.gallery.append(value)
                                                }
                                                
                                            }
                                            
                                        }
                                    }
                                    
                                    }
                                    //self.poolsByIdModel = poolsById.prepare(pool as! AnyObject)
                                }
                            }
                        }
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                 LoadingOverlayV2.sharedV2.hideOverlayView()
                if WSResponseStatus {
                    self.setData()
                    AppDelegate()._manager?.track("pool_visit", data: [:])
                    Analytics.logEvent("pool_visit", parameters: nil)
                }else{
                    self.showAlert("webServiceResultfail")
                }
            })
            
        }
        
        
    }
    
    func setData()->Void {
        
        do {
            
            if PoolsTopContainerVC != nil {
                
                // set gallery
                if self.gallery.count > 0 {
                    self.setPoolImages(self.poolsByIdModel?.default_image, images: self.gallery)
                }
                
                // set name
                PoolsTopContainerVC?.poolName.text = self.poolsByIdModel?.name
                shareData.shared.selectedPoolName = self.poolsByIdModel?.name
                
                // set address
                PoolsTopContainerVC?.poolAddress.text = self.poolsByIdModel?.area_name
                
                // set the pool rate
                if let value = self.poolsByIdModel?.rating as? String {
                    PoolsTopContainerVC?.poolRateBar.rating = setRatingStar(value)
                }
                
                // set prices and sale
                if let value = self.poolsByIdModel?.minimum_sale_price as? String{
                    if value != "<null>" {
                        PoolsTopContainerVC?.poolPriceWithDiscount.text = self.replaceString(value) + " " + "تومان"
                        if let price = self.poolsByIdModel?.minimum_market_price as? String{
                            if price != "<null>" {
                                PoolsTopContainerVC?.poolPrice.text = self.replaceString(price) + " " + "تومان"
                            }
                        }
                        PoolsTopContainerVC?.poolPriceRedLine.alpha = 1
                    }else{
                        PoolsTopContainerVC?.poolPriceWithDiscount.text = ""
                        PoolsTopContainerVC?.poolPriceRedLine.alpha = 0
                        if let price = self.poolsByIdModel?.minimum_market_price as? String{
                            if price != "<null>" {
                                PoolsTopContainerVC?.poolPrice.text = self.replaceString(price) + " " + "تومان"
                            }
                        }
                    }
                }
                
        list.removeAll()
        if let value = self.poolsByIdModel?.comment {
            if value == "1"{
                list.append("CommentCell")
            }
        }
        list.append("DetailCell")

        if let value = self.poolsByIdModel?.terms_of_use_enable {
            if value == "1"{
                list.append("TermOfServices")
            }
        }
        if let value = self.poolsByIdModel?.pool_introducing_enable {
            if value == "1"{
                list.append("InformationCell")
            }
        }
          
        
                
      

      //pool attrebute if available
      if let ratings = self.poolsByIdModel?.ratings as? [String:AnyObject] {
        
         if let values = ratings["values"] as? [[String:AnyObject]] {
            if values.count > 0 {
                self.itemInQueue = list.count
            }
            for item in values {
                var tName = ""
                var tRatingValue = "0.0"
                var tCount = "0"
                if let tValue = String(describing: item["name"] as AnyObject) as? String {
                    tName = tValue
                }
                if let tValue = String(describing: item["value"] as AnyObject) as? String {
                    tRatingValue = tValue
                }
                if let tValue = String(describing: item["rated_count"] as AnyObject) as? String {
                    tCount = tValue
                }
                
                let attributeItem = ratingsObject.init(name: tName, value: tRatingValue, rated_count: tCount)
                ratingList.append(attributeItem)
                list.append("pool_attribute_cell")
                
            }
         }
                    
       }
           
      list.append("pool_time_detail_customCell")
      list.append("pool_address_customCell")
                
                
      // hide the buttom bar if sell status is zero
        if let value = self.poolsByIdModel?.status_sell {
            if value == "0" {
                self.chooseServicesButtonView.isHidden = true
                self.choseServicesButtonHeight.constant = 0
            }else if "0" == self.poolsByIdModel?.status_sell_ticket, "0" == self.poolsByIdModel?.status_sell_course,  "0" == self.poolsByIdModel?.status_sell_pack {
                self.chooseServicesButtonView.isHidden = true
                self.choseServicesButtonHeight.constant = 0
            }
        }
        
        
       
                
                
                
//                if let stringURL = URL.init(string: data.default_image!) as? URL {
//                    let imageView = cell.poolImage
//                    let URL = stringURL//Foundation.URL(string: data["thumbnail"]!)!
//                    let placeholderImage = UIImage(named: "main_top_bg.png")!
//                    imageView?.af_setImage(withURL: URL, placeholderImage: placeholderImage)
//                }else{
//                    cell.poolImage.image = UIImage(named: "main_top_bg.png")
//                }
                
               
                
                switch self.poolsByIdModel?.gender {
                case "0"?:
                    PoolsTopContainerVC?.availableForMan.alpha = 1
                    break
                case "1"?:
                    PoolsTopContainerVC?.availableForWomen.alpha = 1
                    break
                case "2"?:
                    PoolsTopContainerVC?.availableForWomen.alpha = 1
                    PoolsTopContainerVC?.availableForMan.alpha = 1
                    break
                default:
                    printLog(m: "")
                }
                
                
                var tMaxDisCount = self.poolsByIdModel?.max_discount
                if tMaxDisCount != nil , tMaxDisCount != "0", tMaxDisCount!.count > 0 {
                    PoolsTopContainerVC?.poolDiscountBg.alpha = 1
                    PoolsTopContainerVC?.poolDiscount.text = "%"+replaceString(tMaxDisCount!)
                }else{
                    PoolsTopContainerVC?.poolDiscountBg.alpha = 0
                    PoolsTopContainerVC?.poolDiscount.alpha = 0
                }
            


                if let value = self.poolsByIdModel?.status_sell_pack as? String{
                    if value == "1" {
                        self.CO_resevePack.setTitleColor(self.setBgColor( 20, 199, 87), for: .normal)
                        self.ic_reserve_pack.image = UIImage.init(named: "ic_pack_green")
                    }else{
                        self.CO_resevePack.setTitleColor(self.setBgColor( 110, 114, 118), for: .normal)
                    }
                }
                
                if let value = self.poolsByIdModel?.status_sell_ticket as? String{
                    if value == "1" {
                        self.CO_reserve.setTitleColor(self.setBgColor( 20, 199, 87), for: .normal)
                        self.ic_reserve_ticket.image = UIImage.init(named: "ic_reserve_green")
                    }else{
                        self.CO_reserve.setTitleColor(self.setBgColor( 110, 114, 118), for: .normal)
                    }
                }
                
                if let value = self.poolsByIdModel?.status_sell_course as? String{
                    if value == "1" {
                        self.CO_swimming.setTitleColor(self.setBgColor( 20, 199, 87), for: .normal)
                        self.ic_reserve_course.image = UIImage.init(named: "ic_swimming_class_green")
                    }else{
                        self.CO_swimming.setTitleColor(self.setBgColor( 110, 114, 118), for: .normal)
                    }
                }
            
                //self.tableListCount = 5
                self.tableview.reloadData()
                
                
            }
            
          // show the pool favorite status -> empty or full
          showTopBarRightSection()
        }catch{
            fatalError("PoolById; setData; error is \(error)")
        }
        
    }
    
    func setBgColor(_ red:CGFloat,_ green:CGFloat,_ blue:CGFloat, _ alpha: CGFloat = 1) ->UIColor {
        
        return UIColor(red: red/256, green: green/256, blue: blue/256, alpha: alpha)
    }
    
    func setPoolImages(_ image:String?, images:[String]?)->Void{
        
        do{
            var tempImageSource = [AlamofireSource]()
            var tempImageSourceV2 = [ImageSource]()
            
            if image != nil {
                var tFileAddress = image
                //tFileAddress = tFileAddress?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                if let value = tFileAddress {
                    tempImageSource.append(AlamofireSource(urlString: value)!)
                }
            }
            if let value = images as? [String]{
                for item in value  {
                    var tFileAddress = item
                    //tFileAddress = tFileAddress.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                    self.printLog(m:tFileAddress)
                    tempImageSource.append(AlamofireSource(urlString: tFileAddress)!)
                }
                
                if value.count > 0 {
                    PoolsTopContainerVC?.bannerTopView.setImageInputs(tempImageSource)
                }else{
                    tempImageSourceV2.append(ImageSource(image: UIImage(named: "main_top_bg.png")!))
                    PoolsTopContainerVC?.bannerTopView.setImageInputs(tempImageSourceV2)
                }
                
            }
        }catch{
        }
    }
    
    //delete the item from this list
    func setFavoritePool(_ pool_id: String) -> Void {
        LoadingOverlayV2.sharedV2.showOverlayV2(view: self.view, message: "اضافه کردن استخر به لیست علاقه مندی ها")
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.SET_FAVORITE,"client_id=\(client_id)&access_token=\(access_token)&pool_id=\(pool_id)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            do{
                
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                            }
                        }
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                LoadingOverlayV2.sharedV2.hideOverlayView()
                if WSResponseStatus {
                    self.showDropMessage("استخر مورد نظر با موفقیت به لیست علاقه مندی ها اضافه شد.")
                    self.poolsByIdModel?.is_favorite = "1"
                }else{
                    self.showDropMessage("درخواست اضافه کردن به علاقه مندی ها با خطا مواجه شده است، لطفا مجدد تلاش کنید.")
                }
            })
            
        }
        
        
    }
    
    //delete the item from this list
    func usetFavoritePool(_ pool_id: String) -> Void {
        LoadingOverlayV2.sharedV2.showOverlayV2(view: self.view, message: "ثبت درخواست جدید")
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.UNSET_FAVORITE,"client_id=\(client_id)&access_token=\(access_token)&pool_id=\(pool_id)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            do{
                
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                            }
                        }
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                LoadingOverlayV2.sharedV2.hideOverlayView()
                if WSResponseStatus {
                    self.poolsByIdModel?.is_favorite = "0"
                }else{
                }
            })
            
        }
        
        
    }
    
    
    func showButtomBar(_ status: Bool) {
        if status {
            BUTTOM_BAR_OPEN = true
            UIView.transition(with: chooseOption, duration: 0.4, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                self.chooseOption.isHidden = false;
            }, completion: nil)
            
        }else{
            BUTTOM_BAR_OPEN = false
            UIView.transition(with: chooseOption, duration: 0.1, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                self.chooseOption.isHidden = true;
            }, completion: nil)
            
        }

    }
    
    @IBAction func showPoolOnTheMap(_ sender: Any){
    
        //shareData.shared.selectedPoolObject = self.poolsByIdModel
        //self.pushNewClass(classIdentifier: "PoolOnTheMap")
        
        //https://maps.google.com/
        if let lat = self.poolsByIdModel?.lat as? String, let lng = self.poolsByIdModel?.lng as? String {
            if let checkURL = URL(string: "comgooglemaps://?q="+lat+","+lng) {
                open(url: checkURL)
            }
        }
    
    }
    
    func open(url: URL) {
        if #available(iOS 10, *) {
            UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: { (success) in
                print("Open \(url): \(success)")
            })
        } else if UIApplication.shared.openURL(url) {
            print("Open \(url)")
        }
    }
    
    @IBAction func MoreDetaileAboutPool(_ sender: Any){

        shareData.shared.poolDescription = self.poolsByIdModel?.pDescription
        self.pushNewClass(classIdentifier: "Pool_description_more_detailVC")
    }
    
    func showAlert(_ type: String) {
        
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "IRANSansMobile", size: 17)!,
            kTextFont: UIFont(name: "IRANSansMobile", size: 14)!,
            kButtonFont: UIFont(name: "IRANSansMobile", size: 14)!,
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        
        switch type {
        case "internetNotAvailable":
            alertView.addButton("تلاش مجدد") {
                self.getPoolDetail();
            }
            alertView.showWarning("هشدار", subTitle: "خطا در ارتباط با شبکه اینترنت، لطفا اتصال به اینترنت را بررسی کنید .")
            break
        case "webServiceResultfail":
            alertView.addButton("تلاش مجدد") {
                self.getPoolDetail();
            }
            alertView.showNotice("خطا در ارتباط با پول تیکت", subTitle: "در ارتباط با سامانه خطایی رخ داده است . لطفا مجددا تلاش کنید .")
            break
        default:
            print("")
        }
        
    }
    
    func showTopBarRightSection(){
        
        
        var favImage = UIImage(named: "ic_favorite_empty_white")
        if let value = self.poolsByIdModel?.is_favorite {
            if value == "1" {
             favImage = UIImage(named: "ic_favorite_full_red")
            }
        }
        let favoriteTopBarBtn = UIBarButtonItem(image: favImage,style: .plain ,target: self, action: #selector(self.setAsFavorite(button:)))

        let shareTopBarBtn = UIBarButtonItem(image: UIImage(named: "ic_share_white"),style: .plain ,target: self, action: #selector(self.share(img:)))
        self.navigationItem.rightBarButtonItems = [favoriteTopBarBtn, shareTopBarBtn]
        
    }
}

extension PoolVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let data = list[indexPath.row]
        
        switch data {
        case "CommentCell":
            var cellIdentifier =  "CommentCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        case "DetailCell":
            var cellIdentifier =  "DetailCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        case "TermOfServices":
            var cellIdentifier =  "TermOfServices"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        case "InformationCell":
            var cellIdentifier =  "InformationCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        case "pool_time_detail_customCell":
            var cellIdentifier =  "pool_time_detail_customCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! pool_time_detail_customCell
            var tData = self.poolsByIdModel?.pDescription
            /*  exprimental code begin  */
            tData = tData?.replacingOccurrences(of: "331\\", with: "")
            tData = tData?.trimmingCharacters(in: .whitespacesAndNewlines)
            /*  exprimental code end  */
            cell.poolTimeDetail.text = tData
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        case "pool_address_customCell":
            var cellIdentifier =  "pool_address_customCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! pool_address_customCell
            cell.poolAddress.text = self.poolsByIdModel?.address
            cell.poolTell.text = self.replaceString((self.poolsByIdModel?.tel)!)
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
            
        case "pool_attribute_cell":
            var cellIdentifier =  "pool_attribute_cell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! pool_attribute_cell
            
            var tValue = indexPath.row - self.itemInQueue
            if let rowData = ratingList[tValue] as? ratingsObject{
                //                var rated_count = ""
                //                if let value = rowData.rated_count as? String {
                //                    if value != "0", value != "", value != "<null>" {
                //                        rated_count = " (" + replaceString("\(value) نظر") + ")"
                //                    }
                //                }
                cell.attributeName.text = rowData.name //+ rated_count
                if let value = Double(rowData.value) as? Double {
                    cell.attributeRateBar.rating = value
                }
            }
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
            
        default:
            return UITableViewCell()
        }
        
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func scrollToRow(at indexPath: IndexPath, at scrollPosition: UITableView.ScrollPosition, animated: Bool){
        
        if indexPath.row == 4 {
            //printLog(m: "4")
        }
    }
    
}

extension PoolVC: UITableViewDelegate {
    
//    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//
//        if indexPath.row == 0 {
//            self.navigationController?.navigationBar.backgroundColor = self.setBgColor(32, 161, 218, 0.5)
//            self.title = shareData.shared.selectedPoolName
//        }
//
//    }
//
//    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//
//        if indexPath.row == 1 {
//            self.navigationController?.navigationBar.backgroundColor = UIColor.clear
//            self.title = ""
//        }
//
//    }
    
}

extension PoolVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let value = self.tableview.contentOffset.y
//        if value >= 220.0, value <= 225.0 {
//            self.navigationController?.navigationBar.backgroundColor = self.setBgColor(32, 161, 218, 0.1)
//            self.title = shareData.shared.selectedPoolName
//        } else if value >= 226.0, value <= 232.0 {
//            self.navigationController?.navigationBar.backgroundColor = self.setBgColor(32, 161, 218, 0.2)
//            self.title = shareData.shared.selectedPoolName
//        } else if value >= 233.0, value <= 242.0 {
//            self.navigationController?.navigationBar.backgroundColor = self.setBgColor(32, 161, 218, 0.3)
//            self.title = shareData.shared.selectedPoolName
//        } else if value >= 243.0, value <= 249.0 {
//            self.navigationController?.navigationBar.backgroundColor = self.setBgColor(32, 161, 218, 0.4)
//            self.title = shareData.shared.selectedPoolName
//        } else if value >= 250.0 {
//            self.navigationController?.navigationBar.backgroundColor = self.setBgColor(32, 161, 218, 0.5)
//            self.title = shareData.shared.selectedPoolName
//        } else {
//            self.navigationController?.navigationBar.backgroundColor = UIColor.clear
//            self.title = ""
//        }
    }
    
}



// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
