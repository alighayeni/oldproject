//
//  Pools_Pack_list.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/2/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import SCLAlertView


class Pools_Pack_list: UIViewController {
   
    @IBOutlet weak var tableView:UITableView!
    var list01 : [ExpandableObjectPacks] = []
    var select_pack_id: String = ""
    var selected_section_id: Int = -1
    @objc func handleReservePack(button: UIButton){
       
//        if SwiftUtil.getUserAccessToken() == ""{
//            openNewClass(classIdentifier: "CheckPhoneNumberRegistrationVC")
//            return
//        }
        var tSection = button.tag / 100
        var tRow = button.tag - (tSection*100)
        
        shareData.shared.selectedChoosenServices = self.list01[tSection].packObjects[0].pack_name
        shareData.shared.selectedChoosenPoolsRulesAndDetails = self.list01[tSection].packObjects[0].terms_of_use

        if let value = self.list01[tSection].packObjects[tRow] as? packsModel{
            if value.status_sell != "0" {
            shareData.shared.selectedSaensRow = nil
            shareData.shared.selectedPackRow = value
            shareData.shared.selectedCourseRow = nil
            pushNewClass(classIdentifier: "ReservationVC")
            }else{
                 showAlert("notAvailAble")
            }
        }
        
        
    }
    
    @objc func handleExpandClose(button: UIButton){

        self.openSelectedItem(button.tag)
//        let section = button.tag
//        var indexPaths = [IndexPath]()
//        for row in self.list01[section].packObjects.indices {
//            let indexPath = IndexPath(row: row, section : section)
//            indexPaths.append(indexPath)
//        }
//        let isExpanded = self.list01[section].isExpanded
//        self.list01[section].isExpanded = !isExpanded
//
//        if !isExpanded {
//            tableView.insertRows(at: indexPaths, with: .fade)
//        }else{
//            tableView.deleteRows(at: indexPaths, with: .fade)
//        }
    }
    
    func openSelectedItem(_ section_id: Int) {
        
        let section = section_id
        var indexPaths = [IndexPath]()
        for row in self.list01[section].packObjects.indices {
            let indexPath = IndexPath(row: row, section : section)
            indexPaths.append(indexPath)
        }
        let isExpanded = self.list01[section].isExpanded
        self.list01[section].isExpanded = !isExpanded
        
        if !isExpanded {
            tableView.insertRows(at: indexPaths, with: .fade)
        }else{
            tableView.deleteRows(at: indexPaths, with: .fade)
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.font.rawValue: UIFont(name: "IRANSansMobile", size: 14),NSAttributedString.Key.foregroundColor.rawValue:UIColor.white])
        self.getPoolPackList(DataSourceManagement.selectedPoolID)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "مشاهده و رزرو پک"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = "رزرو پک"
    }
    
    func getPoolPackList(_ pool_id: String) -> Void {
        let client_id = Constants.clientId
        printLog(m: client_id)
        let access_token = SwiftUtil.getUserAccessToken()
        printLog(m: access_token)
        printLog(m: pool_id)
        
        let index = wsPOST.init(Constants.POOL_PACK_LIST ,"client_id=\(client_id)&pool_id=\(pool_id)&access_token=\(access_token)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            do{
                
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                if let packs = item["packs"] as? [[String:AnyObject]] {
                                    
                                    for pack in packs {
                                        
                                        var tList = [packsModel]()
                                        
                                        let model = packsModel.prepare(pack as AnyObject, "begin")
                                        tList.append(model)
                                        if model.pack_id! == self.select_pack_id {
                                            self.selected_section_id = self.list01.count
                                        }
                                        
                                        if let patterns = pack["pattern_days"] as? [[String:AnyObject]] {
                                            print(String(patterns.count))
                                            for itemObject in patterns {
                                                tList.append(packsModel.preparePattern(itemObject as AnyObject, "pattern"))
                                            }
                                            
                                        }
                                        tList.append(packsModel.prepare(pack as AnyObject, "end"))
                                        
                                        self.list01.append(ExpandableObjectPacks.init(isExpanded: false, packObjects: tList))
                                    }
                                    
                                }
                                
                            }
                        }
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                
                //self.refreshAllDataVariable = false
                if WSResponseStatus {
                    self.printLog(m: String(self.list01.count))
                    self.tableView.reloadData()
                    if self.selected_section_id >= 0 {
                        self.openSelectedItem(self.selected_section_id)
                    }
                }
            })
            
        }
        
        
    }
    
    func setBgColor(_ red:CGFloat,_ green:CGFloat,_ blue:CGFloat) ->UIColor {
        
        return UIColor(red: red/256, green: green/256, blue: blue/256, alpha: 1)
    }
 
    func returnString(theString: String?)->String{
        var tString = ""
        if let value = theString as? String {
            tString = value
        }
        
        if tString == "0" {
            return ""
        }
        
        return tString
    }
    
    func showAlert(_ type: String) {
        
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "IRANSansMobile", size: 17)!,
            kTextFont: UIFont(name: "IRANSansMobile", size: 14)!,
            kButtonFont: UIFont(name: "IRANSansMobile", size: 14)!,
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        
        switch type {
        case "notAvailAble":
            alertView.addButton("بستن") {
            }
            alertView.showNotice("اخطار:", subTitle: "رزرو پک انتخاب شده غیر فعال می باشد.")
            break
        default:
            print("")
        }
        
    }
}

extension Pools_Pack_list: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.list01.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if !self.list01[section].isExpanded {
            return 0
        }
        return self.list01[section].packObjects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data =  self.list01[indexPath.section].packObjects[indexPath.row]
        self.printLog(m: data.type!)
        if data.type == "begin" {
            var cellIdentifier = "reservePackBeginCustomRow"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! reservePackBeginCustomRow
            
            switch data.gender {
            case "0"?:
                cell.availableForMan.alpha = 1
                break
            case "1"?:
                cell.availableForWomen.alpha = 1
                break
            case "2"?:
                cell.availableForMan.alpha = 1
                cell.availableForWomen.alpha = 1
                break
            default:
                printLog(m: "")
            }
            cell.start?.text = "شروع:" + " " + self.replaceString(data.session_expire_start!)
            cell.end?.text = "پایان:" + " " + self.replaceString(data.session_expire_finish!)
            if let value = self.removeHtmlCharacters(data.pdescription!) as? String, value.count > 0 {
                cell.descriptionContent?.text = value
                cell.descriptionContent.isHidden = false
                cell.descriptionTitle.isHidden = false
                cell.descriptionTitle.text = "توضیحات دوره: "
            } else {
                cell.descriptionTitle.text = ""
                cell.descriptionContent?.text = ""
                cell.descriptionContent.isHidden = true
                cell.descriptionContentHeight.constant = 0
                cell.descriptionTitle.isHidden = true
                cell.descriptionTitleHeight.constant = 0
            }
            
            if let value = data.session_count, value.count > 0 {
                cell.sessionCount.text = "تعداد جلسات: " + self.replaceString(value) + " جلسه"
                cell.sessionCountHeight.constant = 30
                cell.sessionCount.isHidden = false
            } else {
                cell.sessionCount.text = ""
                cell.sessionCountHeight.constant = 0
                cell.sessionCount.isHidden = true
            }
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        }else if data.type == "pattern" {
            
            let cellIdentifier = "reservePackPatternCustomRow"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! reservePackPatternCustomRow
            cell.packPatternDayOfWeek?.text = self.replaceString(data.week_day!)
            cell.packPatternTime?.text = "از " + self.replaceString(data.from!) + " الی " + self.replaceString(data.to!)
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        }else if data.type == "end" {
            
            let cellIdentifier = "reservePackEndCustomRow"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! reservePackEndCustomRow
            cell.packPatternPrice?.text = self.setSeprator(data.price_market!)//self.replaceString(data.price_market!) + " " + "تومان"
            cell.packPatternPriceWithDiscount?.text = self.setSeprator(data.price_sale!)//self.replaceString(data.price_sale!)  + " " + "تومان"
            if let value = Int(data.pack_id!) as? Int {
                if let value = Int(data.pack_id!) as? Int {
                    var tTag = indexPath.section * 100
                    tTag = tTag + indexPath.row
                    cell.packPatternReservePack?.tag = tTag
                }
                
                //cell.packPatternReservePack.tag = value
                cell.packPatternReservePack.addTarget(self, action: #selector(handleReservePack) , for: .touchUpInside)
            }
            
            if let value = data.price_sale?.count as? Int {
                if value > 0 {
                    let lineView = UIView(
                        frame: CGRect(x: 0,
                                      y: cell.packPatternPrice.bounds.size.height / 2,
                                      width: cell.packPatternPrice.bounds.size.width,
                                      height: 1
                        )
                    )
                    lineView.backgroundColor = UIColor.lightGray;
                    cell.packPatternPrice.addSubview(lineView)
                }
            }
            
            //discount rate Adult
            if self.returnString(theString: data.discount_percent).count > 0 {
                cell.packPatternDiscountRate?.text = self.replaceString("%"+data.discount_percent!)
                cell.packPatternTitle?.isHidden = false
                cell.packPatternPrice?.isHidden = false
            }else{
                cell.packPatternDiscountRate?.isHidden = true
                cell.packPatternTitle?.isHidden = true
                cell.packPatternPrice?.isHidden = true
            }
            
            //check the status cell
            if data.status_sell == "0" {
                cell.packPatternReservePack.backgroundColor = .lightGray
            }else{
                cell.packPatternReservePack.backgroundColor = getObjectColor("greenBtn")
            }
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
            
        }
        
        return UITableViewCell()
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?  {
        
        if let infoView = TheHeaderView.instanceFromNib() as? TheHeaderView {
            
            
            
            //            switch self.list01[section].packObjects[0].gender {
            //            case "0"?:
            //                infoView.gender.image = UIImage(named:"ic_boyShift_blue")
            //                break
            //            case "1"?:
            //                infoView.gender.image = UIImage(named:"ic_girlShift_red")
            //                break
            //            case "2"?:
            //                infoView.gender.image = UIImage(named:"ic_bothgender")
            //                break
            //            default:
            //                printLog(m: "")
            //            }
            
            if self.list01[section].isExpanded {
                infoView.theTitle.textColor = self.setBgColor( 34, 169, 228)
                infoView.arrow.image = UIImage(named:"ic_arrow_down_blue")
            }else{
                infoView.theTitle.textColor = self.setBgColor( 110, 114, 118)
                infoView.arrow.image = UIImage(named:"ic_arrow_left_gray")
            }
            let space = " ";
            infoView.collapseViewHeader.setTitle(space+self.list01[section].packObjects[0].pack_name!, for: .normal)
            infoView.mainView.dropShadow();
            infoView.collapseViewHeader.tag = section
            self.selected_section_id = section
            infoView.collapseViewHeader.addTarget(self, action: #selector(handleExpandClose), for: .touchUpInside)
            return infoView
        }
        
        //        let label = UILabel()
        //        label.text = "header"
        //        label.backgroundColor = .red
        return UIView()
    }
    
}

extension Pools_Pack_list: UITableViewDelegate {
    
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
