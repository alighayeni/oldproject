//
//  ResponseCommentsVC.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/18/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import Cosmos

class ResponseCommentsVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, UITableViewCellUpdateDelegate {
   
    @IBOutlet weak var tableView: UITableView!
    var refreshAllDataVariable = false
    var list : [Dictionary<String,String>] = []
    var ratingList : [Dictionary<String,String>] = []
    var arrayOfCells: [UITableViewCell] = []

    var posetivePoints: [String] = []
    var negativePoints: [String] = []


    var parentRowId = false
    var parnetId = ""
    var content = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
        
        if let value = shareData.shared.selectedCommentPoolId as? String {
            parentRowId = true
            parnetId = value
        }
        
        let dic0 : [String:String] = ["type": "title"]
        self.list.append(dic0)
        let dic1 : [String:String] =  ["type":"custom1" ]
        self.list.append(dic1)
        if !parentRowId {
            let dic2 : [String:String] =  ["type":"custom2" ]
            self.list.append(dic2)
            let dic3 : [String:String] =  ["type":"custom3" ]
            self.list.append(dic3)
            self.gerRatingOptionList()
        }else{
            let dic2 : [String:String] =  ["type":"button" ]
            self.list.append(dic2)
            self.tableView.reloadData()
        }
        
       
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // add observer
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

       
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)

    }
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = self.list[indexPath.row]
        if let value = item["type"] as? String {
            switch value {
            case "title":
                let cellIdentifier = "ResponseComments0CustomRow"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ResponseComments0CustomRow
                if !parentRowId {
                    cell.content.text = "ارسال نظر جدید"
                }else{
                    if let value = shareData.shared.selectedCommentWriterPoolId as? String {
                     cell.content.text = "در پاسخ به نظر " + value
                    }else{
                      cell.content.text = "در پاسخ به نظر دیگران"
                    }
                }
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                return cell
                
            case "custom1":
                let cellIdentifier = "ResponseComments01CustomRow"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ResponseComments01CustomRow
                cell.content.delegate = self
//                cell.content.text = self.content
                self.doneWithKeyboard(cell.content)
                cell.delegate = self
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                return cell
                
            case "custom2":
                let cellIdentifier = "ResponseComments02CustomRow"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ResponseComments02CustomRow
                cell.content.delegate = self
                self.doneWithKeyboard(cell.content)
//                if posetivePoints.count > 0 {
//                    cell.content.text = self.posetivePoints[0]
//                }
                cell.delegate = self
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                return cell
                
            case "custom3":
                let cellIdentifier = "ResponseComments03CustomRow"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ResponseComments03CustomRow
                cell.content.delegate = self
                self.doneWithKeyboard(cell.content)
//                if negativePoints.count > 0 {
//                cell.content.text = self.negativePoints[0]
//                }
                cell.delegate = self
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                return cell
            case "rating":
                let cellIdentifier = "ResponseComments04CustomRow"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ResponseComments04CustomRow
                if let value = item["title"] as? String {
                    cell.cellTitle.text = value
                }
                if let value = item["pool_rating_id"] as? String, value == "-1" {
                    cell.starRate.isHidden = true
                    cell.cellTitle.font = UIFont.init(name: "IRANSansMobile", size: 15.0)
                } else {
                    cell.starRate.isHidden = false
                    cell.cellTitle.font = UIFont.init(name: "IRANSansMobile", size: 13.0)
                }
                
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                return cell
            case "button": //ResponseComments05CustomRow
                let cellIdentifier = "ResponseComments05CustomRow"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ResponseComments05CustomRow
                cell.submitComment.addTarget(self, action: #selector(submitComment), for: .touchUpInside)
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                return cell
                break
            default:
                print("")
            }
        }
        return UITableViewCell()
    }
   
    
    func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?) {
        
        if let row = indexPath?.row as? Int {
        switch row {
        case 1:
            if let cell = self.tableView(self.tableView, cellForRowAt: indexPath!) as? ResponseComments01CustomRow{
                guard let content00 = cell.content.text as? String else {
                    self.showDropMessage("خطا محتوای نظر کد ۱۰۰")
                    return
                }
                content = content00
            }
            break
        case 2:
            if let cell = tableView.cellForRow(at:indexPath!) as? ResponseComments02CustomRow {
                guard let content01 = cell.content.text as? String else {
                    self.showDropMessage("خطا محتوای نظر کد ۱۰۰")
                    return
                }
                posetivePoints.removeAll()
                posetivePoints.append(content01)
            }
            break
        case 3:
            if let cell = tableView.cellForRow(at:indexPath!) as? ResponseComments03CustomRow {
                guard let content02 = cell.content.text as? String else {
                    self.showDropMessage("خطا محتوای نظر کد ۱۰۰")
                    return
                }
                negativePoints.removeAll()
                negativePoints.append(content02)
            }
            break
        default:
            printLog(m: "")
        }
        }
        
    }


   
    
    public func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
            switch indexPath.row {
            case 1:
                if let cell = self.tableView(self.tableView, cellForRowAt: indexPath) as? ResponseComments01CustomRow{
                    guard let content00 = cell.content.text as? String else {
                        self.showDropMessage("خطا محتوای نظر کد ۱۰۰")
                        return
                    }
                    content = content00
                }
                break
            case 2:
                if let cell = tableView.cellForRow(at:indexPath) as? ResponseComments02CustomRow {
                    guard let content01 = cell.content.text as? String else {
                        self.showDropMessage("خطا محتوای نظر کد ۱۰۰")
                        return
                    }
                    posetivePoints.removeAll()
                    posetivePoints.append(content01)
                }
                break
            case 3:
                if let cell = tableView.cellForRow(at:indexPath) as? ResponseComments03CustomRow {
                    guard let content02 = cell.content.text as? String else {
                        self.showDropMessage("خطا محتوای نظر کد ۱۰۰")
                        return
                    }
                    negativePoints.removeAll()
                    
                    negativePoints.append(content02)
                }
                break
            default:
                printLog(m: String(indexPath.row))
            }
            
        
        
    }
    
  
    
//    func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?) {
//        if (indexPath?.row)! > 0 , (indexPath?.row)! < 4 {
//            switch (indexPath?.row)! {
//            case 1:
//                if let cell = self.tableView(self.tableView, cellForRowAt: indexPath!) as? ResponseComments01CustomRow{
//                    guard let content00 = cell.content.text as? String else {
//                        self.showDropMessage("خطا محتوای نظر کد ۱۰۰")
//                        return
//                    }
//                   content = content00
//                }
//                break
//            case 2:
//                if let cell = tableView.cellForRow(at:indexPath!) as? ResponseComments02CustomRow {
//                    guard let content01 = cell.content.text as? String else {
//                        self.showDropMessage("خطا محتوای نظر کد ۱۰۰")
//                        return
//                    }
//                    posetivePoints.append(content01)
//                }
//                break
//            case 3:
//                if let cell = tableView.cellForRow(at:indexPath!) as? ResponseComments03CustomRow {
//                    guard let content02 = cell.content.text as? String else {
//                        self.showDropMessage("خطا محتوای نظر کد ۱۰۰")
//                        return
//                    }
//                    negativePoints.append(content02)
//                }
//                break
//            default:
//                printLog(m: "")
//            }
//
//        }
//    }
    
    func gerRatingOptionList() -> Void {
        self.refreshAllDataVariable = true
        LoadingOverlay.shared.showOverlay(view: self.view)
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.POOLS_RATING_ITEMS,"client_id=\(client_id)&access_token=\(access_token)")//&access_token=\(access_token)
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            do{
                
                if let indexValue = indexData {
                    self.ratingList = []
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    //print(json)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                if let ratings = item["ratings"] as? [[String: AnyObject]] {
                                    if ratings.count > 0 {
                                        let dic : [String:String] =  ["type":"rating" , "title": "امتیازها:", "pool_rating_id": "-1"]
                                        self.list.append(dic)
                                        self.ratingList.append(dic)
                                    }
                                    for item in ratings {
                                        
                                        var pool_rating_id = ""
                                        var rTitle = ""
                                        if let value = item["pool_rating_id"] as? Int {
                                            pool_rating_id = String(value)
                                        }
                                        if let value = item["title"] as? String {
                                            rTitle = value
                                        }
                                        let dic : [String:String] =  ["type":"rating" , "title": rTitle, "pool_rating_id": pool_rating_id]
                                        if !self.parentRowId {
                                        self.list.append(dic)
                                        self.ratingList.append(dic)
                                        }
                                    }
                                    
                                }

                                
                            }
                        }
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                LoadingOverlay.shared.hideOverlayView()
                self.refreshAllDataVariable = false
                let dic2 : [String:String] =  ["type":"button" ]
                self.list.append(dic2)
                self.tableView.reloadData()
//                if WSResponseStatus {
//
//                }else{
//
//                }
            })
            
        }
        
        
    }
    
    func submitCommentData(_ comment: String,_ reply_to: String, _ pool_id: String, _ ratings_id: String, _ ratings_value: String, _ positive_points: String, _ negative_points : String) -> Void {
        self.refreshAllDataVariable = true
        LoadingOverlay.shared.showOverlay(view: self.view)
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        var packOfData = "client_id=\(client_id)&pool_id=\(pool_id)&access_token=\(access_token)&comment=\(comment)&positive_points[]=\(positive_points)&negative_points[]=\(negative_points)"//&ratings_id[]=\(ratings_id)&ratings_value[]=\(ratings_value)
        let index = wsPOST.init(Constants.SUBMIT_COMMENT,packOfData)
        var WSResponseStatus = false
        index.start{ (indexData) in

            do{

                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    if let item = json as? [String: AnyObject]{

                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true


                            }
                        }
                    }
                }

            }catch{
                print("this is the error: \(error)")
            }

            DispatchQueue.main.async(execute: {
                LoadingOverlay.shared.hideOverlayView()
                self.refreshAllDataVariable = false
                if WSResponseStatus {
                   self.showDropMessage("نظر شما با موفقیت ثبت شد.")
                    shareData.shared.selectedCommentPoolId = nil
                   self.dismiss(animated: true, completion: nil)
                }else{
                    self.showDropMessage("خطا در ثبت نظر.")
                }
            })

        }


    }
    
    func submitReply(_ comment: String,_ reply_to: String, _ pool_id: String) -> Void {
        self.refreshAllDataVariable = true
        LoadingOverlay.shared.showOverlay(view: self.view)
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.SUBMIT_COMMENT,"client_id=\(client_id)&pool_id=\(pool_id)&access_token=\(access_token)&comment=\(comment)&reply_to=\(reply_to)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            do{
                
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                
                            }
                        }
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                LoadingOverlay.shared.hideOverlayView()
                self.refreshAllDataVariable = false
                if WSResponseStatus {
                    self.showDropMessage("نظر شما با موفقیت ثبت شد.")
                    shareData.shared.selectedCommentPoolId = nil
                    self.dismiss(animated: true, completion: nil)
                }else{
                    self.showDropMessage("خطا در ثبت نظر.")
                }
            })
            
        }
        
        
    }
    
    @objc func submitComment(){
        
        var pool_id = ""
        if let value = DataSourceManagement.selectedPoolID as? String {
            pool_id = value
        }else{
            self.showDropMessage("خطا ثبت یک نظر، کد ۱۰۱")
        }
        
        if !parentRowId {
            
//            let ndx1 = IndexPath(row:1, section: 0)
//            if let cell = self.tableView(self.tableView, cellForRowAt: ndx1) as? ResponseComments01CustomRow{
//                guard let content00 = cell.content.text as? String else {
//                    self.showDropMessage("خطا محتوای نظر کد ۱۰۰")
//                    return
//                }
//                content = content00
//                }


            //posetive points
            var ndx2 = IndexPath(row:2, section: 0)
                if let cell = tableView.cellForRow(at:ndx2) as? ResponseComments02CustomRow {
                guard let content01 = cell.content.text as? String else {
                    self.showDropMessage("خطا محتوای نظر کد ۱۰۰")
                    return
                }
                posetivePoints.removeAll()
                posetivePoints.append(content01)
                }


           // negative points
            var ndx3 = IndexPath(row:3, section: 0)
                if let cell = tableView.cellForRow(at:ndx3) as? ResponseComments03CustomRow {
                guard let content02 = cell.content.text as? String else {
                    self.showDropMessage("خطا محتوای نظر کد ۱۰۰")
                    return
                }
                negativePoints.removeAll()
                negativePoints.append(content02)
                }
            
            
            var arrayId: [String] = []
            var arrayValue: [String] = []
            var i = 0
            for item in self.list {
                
                if let value = item["type"] as? String {
                    if value == "rating" {
                     
                        let ndx = IndexPath(row: i, section: 0)
                        if let cell = tableView.cellForRow(at:ndx) as? ResponseComments04CustomRow {
                        if let value = cell.starRate as? CosmosView? {
                            if let value01 = item["pool_rating_id"] as? String{
                                arrayValue.append(String(value!.rating))
                                //print(value01)
                                arrayId.append(value01)
                            }
                        }
                        }
                        
                    }
                }
                i += 1
            }
        
            if content.count < 1 {
                showDropMessage("متن نظر خود را بنویسید.")
                return
            }
       
            let arrayIdString = self.arrayToString(arrayId.description)
           
            let arrayValueString = self.arrayToString(arrayValue.description)
          
            let posetivePointsString = self.arrayToString(posetivePoints.description)
         
            let negativePointsString = self.arrayToString(negativePoints.description)
           
            
            self.submitCommentData(content, parnetId, pool_id, arrayIdString, arrayValueString, posetivePointsString, negativePointsString)
         }else{
            
            var ndx1 = IndexPath(row:1, section: 0)
            if let cell = tableView.cellForRow(at:ndx1) as? ResponseComments01CustomRow{
                guard let content00 = cell.content.text as? String else {
                    self.showDropMessage("خطا محتوای نظر کد ۱۰۰")
                    return
                }
                content = content00
            }
            
            
             self.submitReply(content, parnetId, pool_id)
         }
        
    }
    
    func arrayToString(_ arr: String)-> String{
        var arrayString = arr
        arrayString = arrayString.replacingOccurrences(of: "\"[", with: "")
        arrayString = arrayString.replacingOccurrences(of: "]\"", with: "")
        return arrayString
    }
    
    //************** softkeyboard tool bar **********************//
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField
        {
            //        case personalID:
            //            personalID.resignFirstResponder()
            //            password.becomeFirstResponder()
            //            break
            //        case password:
            //            password.resignFirstResponder()
            //            break
        //
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
    func doneWithKeyboard(_ sender: UITextView){
        
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        keyboardToolbar.isTranslucent = true
        keyboardToolbar.barTintColor = UIColor.white
        
        let button = UIButton(type: .system)
        button.setTitle("بستن", for: .normal)
        button.sizeToFit()
        button.titleLabel?.font = UIFont(name: "IRANSansMobile", size: 12)
        button.tintColor = UIColor.black
        button.addTarget(self, action: #selector(closeKeyboard(_:)), for: .touchUpInside)
        
        
        //        let btnUp = UIButton(type: .custom)
        //        btnUp.setImage(UIImage(named: "ic_keyboard_arrow_up"), for: .normal)
        //        btnUp.frame = CGRect(x: 0, y: 0, width: 13, height: 13)
        //        btnUp.tag = sender.tag
        //        btnUp.addTarget(self, action: #selector(textFieldUp), for: .touchUpInside)
        //        let itemUp = UIBarButtonItem(customView: btnUp)
        //
        //        let btnDown = UIButton(type: .custom)
        //        btnDown.setImage(UIImage(named: "ic_keyboard_arrow_down"), for: .normal)
        //        btnDown.frame = CGRect(x: 0, y: 0, width: 13, height: 13)
        //        btnDown.tag = sender.tag
        //        btnDown.addTarget(self, action: #selector(textFieldDown), for: .touchUpInside)
        //        let itemDown = UIBarButtonItem(customView: btnDown)
        
        
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        keyboardToolbar.items = [flexBarButton,UIBarButtonItem(customView: button)]
        //keyboardToolbar.items = [itemDown,itemUp,flexBarButton,UIBarButtonItem(customView: button)]
        
        if let value = sender as? UITextField {
            value.inputAccessoryView = keyboardToolbar
        }else if let value = sender as? UITextView {
            value.inputAccessoryView = keyboardToolbar
        }
        
    }
    
    //************** softkeyboard tool bar **********************//
   
    func cellDidChangeValue(cell: UITableViewCell) {
        guard let indexPath = self.tableView.indexPath(for: cell) else {
            return
        }
        print("thiiis")
        /// Update data source - we have cell and its indexPath
    }
    
    @objc func keyboardWillShow(_ notification:Notification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            tableView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
        }
    }
    @objc func keyboardWillHide(_ notification:Notification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            tableView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    @IBAction func close(){
        self.dismiss(animated: true, completion: nil)
        shareData.shared.selectedCommentPoolId = nil
    }
}


protocol UITableViewCellUpdateDelegate {
    func cellDidChangeValue(cell: UITableViewCell)
}

