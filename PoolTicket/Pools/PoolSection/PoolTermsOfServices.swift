//
//  PoolTermsOfServices.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 6/4/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class PoolTermsOfServices: ViewController, WKUIDelegate, WKNavigationDelegate {
    

    @IBOutlet weak var contentView:UIView!
    
    var pool_detaile = ""
    var pool_introducing = ""
    var pool_terms_of_use = ""
    
    var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.font.rawValue: UIFont(name: "IRANSansMobile", size: 14),NSAttributedString.Key.foregroundColor.rawValue:UIColor.white])
        
        self.title = "شرایط استفاده"
        
        self.webView = WKWebView(frame: self.contentView.bounds, configuration: WKWebViewConfiguration())
        self.webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //self.webView.translatesAutoresizingMaskIntoConstraints = false
        self.webView.navigationDelegate = self
        self.contentView.addSubview(self.webView)
        
        self.getPoolsTermsOfUse(DataSourceManagement.selectedPoolID)
        
    }
    
   
    
    func getPoolsTermsOfUse(_ pool_id: String) -> Void {
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.POOL_TERMS_OF_USE + pool_id ,"client_id=\(client_id)&pool_id=\(pool_id)&access_token=\(access_token)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            do{
                
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                if let pool_introducing_content = item["terms_of_use"] as? [String:AnyObject] {
                                    if let value = pool_introducing_content["content"] as? String {
                                        self.pool_terms_of_use = value
                                    }
                                }
                                
                            }
                        }
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                //                self.refreshAllDataVariable = false
                //                if WSResponseStatus {
                //                    //                    self.tableView.reloadData()
                //                }else{
                //
                //                }
                var tData = self.pool_terms_of_use
                if tData.count > 10 {
                    //self.contentDetaile.text = self.pool_introducing  + "\n" + self.pool_terms_of_use
                    self.webView.loadHTMLString(tData, baseURL: nil)
                    
                }else{
                    //self.contentDetaile.text = "فیلد معرفی مجموعه تکمیل نشده است."
                }
                
            })
            
        }
        
        
    }
    
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
