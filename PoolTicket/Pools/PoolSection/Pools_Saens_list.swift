//
//  Pools_Saeson_list.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/2/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import SCLAlertView


class Pools_Saens_list: UIViewController {
    
    var list01 : [ExpandableObjectSaens] = []
    @IBOutlet weak var tableView:UITableView!
    var select_saens_id: String = ""
    var selected_section_id: Int = -1
    func returnString(theString: String?)->String{
        var tString = ""
        if let value = theString as? String {
            tString = value
        }
        
        if tString == "0" {
            return ""
        }
        
        return tString
    }
    
    @objc func handleReserveTicket(button: UIButton){
        
        var tSection = button.tag / 100
        var tRow = button.tag - (tSection*100)
        shareData.shared.selectedChoosenServices = self.list01[tSection].seansObjects[0].saens_name
        if let value = self.list01[tSection].seansObjects[tRow] as? saensModel{
            if value.status_sell != "0" {
            shareData.shared.selectedSaensRow = value
            shareData.shared.selectedPackRow = nil
            shareData.shared.selectedCourseRow = nil
            shareData.shared.selectedChoosenPoolsRulesAndDetails = value.terms_of_use
            pushNewClass(classIdentifier: "ReservationVC")
            }else{
               showAlert("notAvailAble")
            }
        }
        
        
    }
    
    @objc func handleExpandClose(button: UIButton){
        
        self.openSelectedItem(button.tag)
//        let section = button.tag
//        var indexPaths = [IndexPath]()
//        for row in self.list01[section].seansObjects.indices {
//            let indexPath = IndexPath(row: row, section : section)
//            indexPaths.append(indexPath)
//        }
//        let isExpanded = self.list01[section].isExpanded
//        self.list01[section].isExpanded = !isExpanded
//
//        //                if self.list01[section].isExpanded {
//        //                    infoView.theTitle.textColor = self.setBgColor( 34, 169, 228)
//        //                    infoView.arrow.image = UIImage(named:"ic_arrow_down_blue")
//        //                }else{
//        //                    infoView.theTitle.textColor = self.setBgColor( 110, 114, 118)
//        //                    infoView.arrow.image = UIImage(named:"ic_arrow_left_gray")
//        //                }
//
//        if !isExpanded {
//            tableView.insertRows(at: indexPaths, with: .fade)
//        }else{
//            tableView.deleteRows(at: indexPaths, with: .fade)
//        }
    }
    
    func openSelectedItem(_ section_id: Int) {
        
        let section = section_id
        var indexPaths = [IndexPath]()
        for row in self.list01[section].seansObjects.indices {
            let indexPath = IndexPath(row: row, section : section)
            indexPaths.append(indexPath)
        }
        let isExpanded = self.list01[section].isExpanded
        self.list01[section].isExpanded = !isExpanded
        
        if !isExpanded {
            tableView.insertRows(at: indexPaths, with: .fade)
        }else{
            tableView.deleteRows(at: indexPaths, with: .fade)
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.font.rawValue: UIFont(name: "IRANSansMobile", size: 14),NSAttributedString.Key.foregroundColor.rawValue:UIColor.white])


        self.getPoolSaensList(DataSourceManagement.selectedPoolID)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "رزرو بلیت: انتخاب سانس"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = "رزرو بلیت"
    }

    func getPoolSaensList(_ pool_id: String) -> Void {
        LoadingOverlayV2.sharedV2.showOverlayV2(view: self.view, message: "در حال بارگذاری")

        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()

        let index = wsPOST.init(Constants.POOL_SEASNS_LIST ,"client_id=\(client_id)&pool_id=\(pool_id)&access_token=\(access_token)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            do{
                
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                 if let saens = item["saens"] as? [[String:AnyObject]] {
                                    
                                    for saen in saens {
                                        
                                        var tList = [saensModel]()
                                        let model = saensModel.prepare(saen as AnyObject, "begin")
                                        tList.append(model)

                                        if model.saens_id == self.select_saens_id {
                                            self.selected_section_id = self.list01.count
                                        }
                                        
                                        if let rows = saen["rows"] as? [[String:AnyObject]] {
                                            //print(String(rows.count))
                                            for itemObject in rows {
                                                tList.append(saensModel.prepareRows(itemObject as AnyObject, "row"))
                                            }
                                        }
                                        self.list01.append(ExpandableObjectSaens.init(isExpanded: false, seansObjects: tList))
                                    }
                                    
                                }
                                
                            }
                        }
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                 LoadingOverlayV2.sharedV2.hideOverlayView()
                //self.refreshAllDataVariable = false
                if WSResponseStatus {
                    self.tableView.reloadData()
                    if self.selected_section_id >= 0 {
                        self.openSelectedItem(self.selected_section_id)
                    }
                }else{
                    
                }
                
            })
            
        }
        
        
    }

    func setBgColor(_ red:CGFloat,_ green:CGFloat,_ blue:CGFloat) ->UIColor {
        
        return UIColor(red: red/256, green: green/256, blue: blue/256, alpha: 1)
    }
    
    func showAlert(_ type: String) {
        
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "IRANSansMobile", size: 17)!,
            kTextFont: UIFont(name: "IRANSansMobile", size: 14)!,
            kButtonFont: UIFont(name: "IRANSansMobile", size: 14)!,
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        
        switch type {
        case "notAvailAble":
            alertView.addButton("بستن") {
            }
            alertView.showNotice("اخطار:", subTitle: "امکان رزرو بلیت برای این سانس امکان پذیر نمی باشد.")
            break
        default:
            print("")
        }
        
    }
}

extension Pools_Saens_list: UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?  {
        
        if let infoView = TheHeaderView.instanceFromNib() as? TheHeaderView {
            
            
            switch self.list01[section].seansObjects[0].gender {
            case "0"?:
                infoView.gender.image = UIImage(named:"ic_boyShift_blue")
                break
            case "1"?:
                infoView.gender.image = UIImage(named:"ic_girlShift_red")
                break
            case "2"?:
                infoView.gender.image = UIImage(named:"ic_bothgender")
                break
            default:
                printLog(m: "")
            }
            
            infoView.theTitle.text = "انتخاب سانس"
            
            if self.list01[section].isExpanded {
                infoView.theTitle.textColor = self.setBgColor( 34, 169, 228)
                infoView.arrow.image = UIImage(named:"ic_arrow_down_blue")
            }else{
                infoView.theTitle.textColor = self.setBgColor( 110, 114, 118)
                infoView.arrow.image = UIImage(named:"ic_arrow_left_gray")
            }
            
            var space = "           ";
            infoView.collapseViewHeader.setTitle(space+self.list01[section].seansObjects[0].saens_name!, for: .normal)
            infoView.mainView.dropShadow();
            infoView.collapseViewHeader.tag = section
            infoView.collapseViewHeader.addTarget(self, action: #selector(handleExpandClose), for: .touchUpInside)
            return infoView
        }
        
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0 {
            return 0
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.list01.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if !self.list01[section].isExpanded {
            return 0
        }
        let theRowCount = self.list01[section].seansObjects.count//-1;
        return theRowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //print(String(indexPath.row))
        let tRow = indexPath.row
        let data =  self.list01[indexPath.section].seansObjects[tRow]
        
        if data.type == "row" {
            //print(data.child_sale_status)
            if data.child_sale_status != nil, data.child_sale_status == "1", data.adult_sale_status != nil, data.adult_sale_status == "1"  {
                
                let identifier = "reserveSaensCustomRowV1"
                let cell =  tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! reserveSaensCustomRowV1
                
                cell.dateAndDay?.text = self.replaceString(data.start_day! + " - " + data.start_date!)
                cell.times?.text = self.replaceString(data.time!)
                cell.price1?.text = setSeprator(data.price_market_adult!)
                cell.price1WithDiscount?.text = setSeprator(data.price_sale_adult!)
                cell.titlePrice1?.text = "بزرگسال:"
                
                //discount rate Adult
                if self.returnString(theString: data.discount_percent_adult).count > 0 {
                    cell.discountRate1?.text = self.replaceString("%"+data.discount_percent_adult!)
                }else{
                    cell.discountRate1?.isHidden = true
                }
                
                // seans description
                if self.returnString(theString: data.pDescription).count > 4 {
                    cell.descriptionTitle?.text = "توضیحات:"
                    cell.descriptionContent?.text = data.pDescription
                }else{
                    cell.descriptionContent?.frame.size.height = 0.0
                    cell.descriptionTitle?.frame.size.height = 0.0
                    cell.descriptionTitle?.isHidden = true
                    cell.descriptionContent?.isHidden = true
                }
                
                cell.price2?.text = setSeprator(data.price_market_child!)
                cell.price2WithDiscount?.text = setSeprator(data.price_sale_child!)
                cell.titlePrice2?.text = "خردسال:"
                
                
                //discount rate child
                if self.returnString(theString: data.discount_percent_child).count > 0 {
                    cell.discountRate2?.text = self.replaceString("%"+data.discount_percent_child!)
                }else{
                    cell.discountRate2?.isHidden = true
                }
                
                if let value = Int(data.saens_row_id!) {
                    var tTag = indexPath.section * 100
                    tTag = tTag + indexPath.row
                    cell.reserveSaens?.tag = tTag
                }
                cell.reserveSaens?.addTarget(self, action: #selector(handleReserveTicket) , for: .touchUpInside)
                
                if let value = data.price_sale_adult?.count {
                    if value > 0 {
                        let lineView = UIView(
                            frame: CGRect(x: 0,
                                          y: cell.price1.bounds.height / 2,
                                          width: cell.price1.bounds.width,
                                          height: 1
                            )
                        )
                        lineView.backgroundColor = UIColor.lightGray;
                        cell.price1.addSubview(lineView)
                    }
                }
                
                if let value = data.price_sale_child?.count as? Int {
                    if value > 0 {
                        let lineView = UIView(
                            frame: CGRect(x: 0,
                                          y: cell.price2.bounds.height / 2,
                                          width: cell.price2.bounds.width,
                                          height: 1
                            )
                        )
                        lineView.backgroundColor = UIColor.lightGray;
                        cell.price2.addSubview(lineView)
                    }
                }
                
                //check the status cell
                if data.status_sell == "0" {
                    cell.reserveSaens.backgroundColor = .lightGray
                    cell.dateAndDay?.textColor = UIColor.lightGray;
                    
                }else{
                    cell.reserveSaens.backgroundColor = getObjectColor("greenBtn")
                }
                
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                
                return cell
                
            } else if data.adult_sale_status != nil, data.adult_sale_status == "1"  {
                
                let identifier = "reserveSaensCustomRowV2"
                let cell =  tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! reserveSaensCustomRowV2
                
                cell.dateAndDay?.text = self.replaceString(data.start_day! + " - " + data.start_date!)
                cell.times?.text = self.replaceString(data.time!)
                cell.price1?.text = setSeprator(data.price_market_adult!)
                cell.price1WithDiscount?.text = setSeprator(data.price_sale_adult!)//self.replaceString(data.price_sale_adult!)  + " " + "تومان"
                cell.titlePrice1?.text = "بزرگسال:"
                
                //discount rate Adult
                if self.returnString(theString: data.discount_percent_adult).count > 0 {
                    cell.discountRate1?.text = self.replaceString("%"+data.discount_percent_adult!)
                }else{
                    cell.discountRate1?.isHidden = true
                }
                
                // seans description
                if self.returnString(theString: data.pDescription).count > 4 {
                    cell.descriptionTitle?.text = "توضیحات:"
                    cell.descriptionContent?.text = data.pDescription
                }else{
                    cell.descriptionTitle?.frame.size.height = 0.0
                    cell.descriptionContent?.frame.size.height = 0.0
                    cell.descriptionTitle?.isHidden = true
                    cell.descriptionContent?.isHidden = true
                }
                if let value = Int(data.saens_row_id!) as? Int {
                    var tTag = indexPath.section * 100
                    tTag = tTag + indexPath.row
                    cell.reserveSaens?.tag = tTag
                }
                cell.reserveSaens?.addTarget(self, action: #selector(handleReserveTicket) , for: .touchUpInside)
                
                if let value = data.price_sale_adult?.count as? Int {
                    if value > 0 {
                        let lineView = UIView(
                            frame: CGRect(x: 0,
                                          y: cell.price1.bounds.height / 2,
                                          width: cell.price1.bounds.width,
                                          height: 1
                            )
                        )
                        lineView.backgroundColor = UIColor.lightGray;
                        cell.price1.addSubview(lineView)
                    }
                }
                
                //check the status cell
                if data.status_sell == "0" {
                    cell.reserveSaens.backgroundColor = .lightGray
                    cell.dateAndDay?.textColor = UIColor.lightGray;
                    
                }else{
                    cell.reserveSaens.backgroundColor = getObjectColor("greenBtn")
                }
                
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                
                return cell
                
            }else if data.child_sale_status != nil, data.child_sale_status == "1" {
                
                let identifier = "reserveSaensCustomRowV2"
                let cell =  tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! reserveSaensCustomRowV2
                
                cell.dateAndDay?.text = self.replaceString(data.start_day! + " - " + data.start_date!)
                cell.times?.text = self.replaceString(data.time!)
                cell.price1?.text = setSeprator(data.price_market_adult!)
                cell.price1WithDiscount?.text = setSeprator(data.price_sale_adult!)
                cell.titlePrice1?.text = "خردسال:"
                
                //discount rate Adult
                if self.returnString(theString: data.discount_percent_adult).count > 0 {
                    cell.discountRate1?.text = self.replaceString("%"+data.discount_percent_adult!)
                }else{
                    cell.discountRate1?.isHidden = true
                }
                
                // seans description
                if self.returnString(theString: data.pDescription).count > 4 {
                    cell.descriptionTitle?.text = "توضیحات:"
                    cell.descriptionContent?.text = data.pDescription
                }else{
                    //cell.descriptionContent?.text = "فاقد توضیحات"
                    cell.descriptionTitle?.frame.size.height = 0.0
                    cell.descriptionContent?.frame.size.height = 0.0
                    cell.descriptionTitle?.isHidden = true
                    cell.descriptionContent?.isHidden = true
                }
                if let value = Int(data.saens_row_id!) as? Int {
                    var tTag = indexPath.section * 100
                    tTag = tTag + indexPath.row
                    cell.reserveSaens?.tag = tTag
                }
                cell.reserveSaens?.addTarget(self, action: #selector(handleReserveTicket) , for: .touchUpInside)
                
                if let value = data.price_sale_adult?.count as? Int {
                    if value > 0 {
                        let lineView = UIView(
                            frame: CGRect(x: 0,
                                          y: cell.price1.bounds.height / 2,
                                          width: cell.price1.bounds.width,
                                          height: 1
                            )
                        )
                        lineView.backgroundColor = UIColor.lightGray;
                        cell.price1.addSubview(lineView)
                    }
                }
                
                //check the status cell
                if data.status_sell == "0" {
                    cell.reserveSaens.backgroundColor = .lightGray
                    cell.dateAndDay?.textColor = UIColor.lightGray;
                    
                }else{
                    cell.reserveSaens.backgroundColor = getObjectColor("greenBtn")
                }
                
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                
                return cell
            }
            
        }
        
        return UITableViewCell()
    }
}

extension Pools_Saens_list: UITableViewDelegate {
    

    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
