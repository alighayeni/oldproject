//
//  Pool_description_more_detailVC.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 6/1/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit

class Pool_description_more_detailVC: ViewController{
    
    
    @IBOutlet weak var poolDescription: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.font.rawValue: UIFont(name: "IRANSansMobile", size: 14),NSAttributedString.Key.foregroundColor.rawValue:UIColor.white])
        
        self.title = "توضیحات"

        if let value = shareData.shared.poolDescription as? String {
            self.poolDescription.text = value
            self.poolDescription.font = UIFont(name: "IRANSansMobile", size: 14)
        }
        
    }
    
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
