//
//  Pool_specificationVC.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 3/30/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage


class Pool_specificationVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var list = [NSObject]()
    var tableRowCount = 0
    var tableRowCountAttribute = 0
    var tableRowCountSpecification = 0

    @IBOutlet weak var tableView:UITableView!
  
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return tableRowCount
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableRowCountAttribute > indexPath.row {
            
            if indexPath.row == 0 {
                var cellIdentifier =  "pool_attribute_customCell_Top"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                return cell
            }else if (tableRowCountAttribute-1) ==  indexPath.row{
                var cellIdentifier =  "pool_specification_customCell_buttom"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                return cell
            }
            var cellIdentifier =  "pool_specification_customCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! pool_specification_customCell
            
            var tCount = indexPath.row
            tCount  = tCount * 3
            if DataSourceManagement.pAttributes.count > (tCount-3) {
            if let value = DataSourceManagement.pAttributes[tCount-3] as? poolsAttributes{
                 cell.btn1.setTitle(DataSourceManagement.pAttributes[tCount-3].name, for: .normal)
                  cell.btn1.tag = (tCount-3)

                if let stringURL = URL.init(string: DataSourceManagement.pAttributes[tCount-3].icon!) as? URL {
                    let imageView = cell.btn1_ic
                    let URL = stringURL//Foundation.URL(string: data["thumbnail"]!)!
                    let placeholderImage = UIImage()
                    imageView?.af_setImage(withURL: URL, placeholderImage: placeholderImage)
                }else{
                    cell.btn1_ic.image = UIImage()//UIImage(named: "main_top_bg.png")
                }
            }
            }
          
            if DataSourceManagement.pAttributes.count > (tCount-2) {
            if let value = DataSourceManagement.pAttributes[tCount-2] as? poolsAttributes {
                 cell.btn2.setTitle(DataSourceManagement.pAttributes[tCount-2].name, for: .normal)
                 cell.btn2.tag = (tCount-2)

                if let stringURL = URL.init(string: DataSourceManagement.pAttributes[tCount-2].icon!) as? URL {
                    let imageView = cell.btn2_ic
                    let URL = stringURL//Foundation.URL(string: data["thumbnail"]!)!
                    let placeholderImage = UIImage()
                    imageView?.af_setImage(withURL: URL, placeholderImage: placeholderImage)
                }else{
                    cell.btn1_ic.image = UIImage()//UIImage(named: "main_top_bg.png")
                }
            }
            }
            
            if DataSourceManagement.pAttributes.count > (tCount-1) {
             if let value = DataSourceManagement.pAttributes[tCount-1] as? poolsAttributes{
                cell.btn3.setTitle(DataSourceManagement.pAttributes[tCount-1].name, for: .normal)
                cell.btn3.tag = (tCount-1)
                if let stringURL = URL.init(string: DataSourceManagement.pAttributes[tCount-1].icon!) as? URL {
                    let imageView = cell.btn3_ic
                    let URL = stringURL//Foundation.URL(string: data["thumbnail"]!)!
                    let placeholderImage = UIImage()
                    imageView?.af_setImage(withURL: URL, placeholderImage: placeholderImage)
                }else{
                    cell.btn1_ic.image = UIImage()//UIImage(named: "main_top_bg.png")
                }
            }
            }

        
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        return cell
       
        }else if tableRowCountAttribute <= indexPath.row{
           
            let tRow = indexPath.row - tableRowCountAttribute//tableRowCountAttribute
            if tRow == 0 {
                var cellIdentifier =  "pool_specification_customCell_Top"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                return cell
            
            }else if tRow != (self.tableRowCountSpecification-1) {
                //if tRow == tableRowCountAttribute {
                    let tRow1 = tRow - 1
                    var cellIdentifier =  "pool_specification_customCellV2"
                    let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! pool_specification_customCellV2
                    cell.rowKey?.text = DataSourceManagement.pSpecification[tRow1].name
                    cell.rowValue?.text = replaceString(DataSourceManagement.pSpecification[tRow1].value!)
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    return cell
            }else{
                var cellIdentifier =  "pool_specification_customCell_buttom"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                return cell
            }
            
//            pool_specification_customCellV2
            
        }
        
        return UITableViewCell()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.font.rawValue: UIFont(name: "IRANSansMobile", size: 14),NSAttributedString.Key.foregroundColor.rawValue:UIColor.white])
         self.title = "مشخصات مجموعه"
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.getPoolAttributes(DataSourceManagement.selectedPoolID)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    
    func getPoolAttributes(_ pool_id: String) -> Void {
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.POOL_ATTRIBUTES ,"client_id=\(client_id)&pool_id=\(pool_id)&access_token=\(access_token)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            do{
                
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                DataSourceManagement.cleanPoolSpecification()
                                DataSourceManagement.cleanPoolAttribute()
                                
                                if let attributes = item["attributes"] as? [[String:AnyObject]] {
                                    for item in attributes {
                                        DataSourceManagement.addPoolAttribute(poolsAttributes.prepare(item as AnyObject))
                                    }
                                }
                                
                                if let specifications = item["specifications"] as? [[String:AnyObject]] {
                                    for item in specifications {
                                        DataSourceManagement.addPoolSpecification(poolsSpecification.prepare(item as AnyObject))
                                    }
                                }
                                
                            }
                        }
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
               
                //self.refreshAllDataVariable = false
                if WSResponseStatus {
                    if DataSourceManagement.pAttributes.count > 0 {
                        var tData = DataSourceManagement.pAttributes.count / 3
                        if (tData*3) == DataSourceManagement.pAttributes.count {
                            
                        }else{
                            tData += 1
                        }
                        self.tableRowCount = tData+2
                        self.tableRowCountAttribute = tData+2
                        
                        self.list.append(DataSourceManagement.pAttributes as NSObject)
                        
                    }
                     if DataSourceManagement.pSpecification.count > 0 {
                        
                        self.tableRowCount = self.tableRowCount + 2
                        self.tableRowCount = self.tableRowCount + DataSourceManagement.pSpecification.count
                        self.tableRowCountSpecification = DataSourceManagement.pSpecification.count + 2

                    }
                    self.tableView.reloadData()
                }else{
                
                }
                
            })
            
        }
        
        
    }
    
    @IBAction func getTheTag(_ sender: Any){
        if let value = (sender as AnyObject).tag as? Int {
            printLog(m: String(value))
            printLog(m: DataSourceManagement.pAttributes[value].name!)
        }
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
