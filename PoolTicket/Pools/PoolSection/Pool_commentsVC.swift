//
//  Pool_commentsVC.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 3/30/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyDrop

class Pool_commentsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var sortBtn:UIButton!
    @IBOutlet weak var newCommentButtonICRightSpace: NSLayoutConstraint!
    
    var tableListCount = 5
    var commentsPage  = 1
    var sortType = "newest"
    var refreshAllDataVariable = false
    var lastPageLoaded = 0
    var sortTitle = [String]()
    
    let likeImageGray = UIImage(named: "ic_comment_like_gray") as UIImage?
    let likeImage = UIImage(named: "ic_comment_like") as UIImage?
    
    let disLikeImageGray = UIImage(named: "ic_comment_dislike_gray") as UIImage?
    let disLikeImage = UIImage(named: "ic_comment_dislike") as UIImage?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        // make ui perfect for iphone plus
        if  DeviceType.IS_IPHONE_6_6S_7_8{
            self.newCommentButtonICRightSpace.constant = 100
        }
        
       self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.font.rawValue: UIFont(name: "IRANSansMobile", size: 14),NSAttributedString.Key.foregroundColor.rawValue:UIColor.white])
        self.title = "دیدگاه ها"

       self.cleanStart_GetComment()
        
    }
    
    func cleanStart_GetComment(){
        self.commentsPage =  1
        cleanTheList()
        self.getComments(DataSourceManagement.selectedPoolID,sortType,String(commentsPage))
    }
    
    @IBAction func theSortAction(_ sender: Any){
        
        if let value = sender as? UIButton {
            showDurationAlert(sender,self.sortTitle, value)
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage() //remove pesky 1 pixel line
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor.white
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getListCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
       

        
        let data = DataSourceManagement.commentsList[indexPath.row] 

        if data.type == "comment" {
            if self.getCommentsAsAnString(data.negative_points!) != "-1" || self.getCommentsAsAnString(data.positive_points!) != "-1" {
                var cellIdentifier =  "comment_customCellV1"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! comment_customCellV1
                cell.commentName.text = data.user_name
                cell.commentPublishDate.text = replaceString(returnJalaliDate(data.created_at!))
                cell.commentDetail.text = data.content
                cell.commentLikeCount.text = self.replaceString(data.count_like!)
                cell.commentDisLikeCount.text = self.replaceString(data.count_dislike!)
                cell.commentPosetivePoint?.text = self.getCommentsAsAnString(data.positive_points!)
                cell.commentNegativePoint?.text = self.getCommentsAsAnString(data.negative_points!)
                cell.dislikeBtn.tag = indexPath.row
                cell.dislikeBtn.addTarget(self, action: #selector(submitDisLike), for: .touchUpInside)
                cell.likeBtn.tag = indexPath.row
                cell.likeBtn.addTarget(self, action: #selector(submitLike), for: .touchUpInside)
                cell.responseBtn.tag = indexPath.row
                cell.responseBtn.addTarget(self, action: #selector(responseToComment), for: .touchUpInside)
                
                if data.is_liked == "<null>" {
                    cell.likeBtn.setImage(likeImageGray, for: .normal)
                    cell.dislikeBtn.setImage(disLikeImageGray, for: .normal)
                }else if data.is_liked == "1" {
                     cell.likeBtn.setImage(likeImage, for: .normal)
                     cell.dislikeBtn.setImage(disLikeImageGray, for: .normal)
                }else if data.is_liked == "0" {
                    cell.dislikeBtn.setImage(disLikeImage, for: .normal)
                    cell.likeBtn.setImage(likeImageGray, for: .normal)
                }
                
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                return cell
            }else{
                var cellIdentifier =  "comment_customCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! comment_customCell
                cell.commentName.text = data.user_name
                cell.commentPublishDate.text = replaceString(returnJalaliDate(data.created_at!))
                cell.commentDetail.text = data.content
                cell.commentLikeCount.text = self.replaceString(data.count_like!)
                cell.commentDisLikeCount.text = self.replaceString(data.count_dislike!)
                cell.dislikeBtn.tag = indexPath.row
                cell.dislikeBtn.addTarget(self, action: #selector(submitDisLike), for: .touchUpInside)
                cell.likeBtn.tag = indexPath.row
                cell.likeBtn.addTarget(self, action: #selector(submitLike), for: .touchUpInside)
                cell.responseBtn.tag = indexPath.row
                cell.responseBtn.addTarget(self, action: #selector(responseToComment), for: .touchUpInside)
               
                if data.is_liked == "<null>" {
                    cell.likeBtn.setImage(likeImageGray, for: .normal)
                    cell.dislikeBtn.setImage(disLikeImageGray, for: .normal)
                }else if data.is_liked == "1" {
                    cell.likeBtn.setImage(likeImage, for: .normal)
                    cell.dislikeBtn.setImage(disLikeImageGray, for: .normal)
                }else if data.is_liked == "0" {
                    cell.dislikeBtn.setImage(disLikeImage, for: .normal)
                    cell.likeBtn.setImage(likeImageGray, for: .normal)
                }
                
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                return cell
                
            }
        }else if data.type == "child" {
            
            var cellIdentifier =  "comment_customCellResponse"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! comment_customCellResponse
            cell.commentName.text = data.user_name
            cell.commentPublishDate.text = replaceString(returnJalaliDate(data.created_at!))
            cell.commentDetail.text = data.content
            cell.commentLikeCount.text = self.replaceString(data.count_like!)
            cell.commentDisLikeCount.text = self.replaceString(data.count_dislike!)
            cell.dislikeBtn.tag = indexPath.row
            cell.dislikeBtn.addTarget(self, action: #selector(submitDisLike), for: .touchUpInside)
            cell.likeBtn.tag = indexPath.row
            cell.likeBtn.addTarget(self, action: #selector(submitLike), for: .touchUpInside)
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.responseBtn.setTitle("در پاسخ به " + data.inResponseWith!, for: .normal)
            cell.responseBtn.tag = indexPath.row
            //cell.responseBtn.addTarget(self, action: #selector(responseToComment), for: .touchUpInside)
            
            if data.is_liked == "<null>" {
                cell.likeBtn.setImage(likeImageGray, for: .normal)
                cell.dislikeBtn.setImage(disLikeImageGray, for: .normal)
            }else if data.is_liked == "1" {
                cell.likeBtn.setImage(likeImage, for: .normal)
                cell.dislikeBtn.setImage(disLikeImageGray, for: .normal)
            }else if data.is_liked == "0" {
                cell.likeBtn.setImage(likeImageGray, for: .normal)
                cell.dislikeBtn.setImage(disLikeImage, for: .normal)
            }
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
            
        }
        
        return UITableViewCell()
    }
    
    @objc func submitLike(button: UIButton) {
        if SwiftUtil.getUserAccessToken() == ""{
            openNewClass(classIdentifier: "CheckPhoneNumberRegistrationVC")
        }else{
            let tData = DataSourceManagement.commentsList[button.tag]
            if tData.is_liked != "1",  tData.is_liked != "0"{
                self.commentAction(tData.pool_comment_id!,"1",button.tag)
            }
        }
        
//        self.productListArray[rowId] = cartProductModel.prepare(value01 as AnyObject)
//        let indexPath = IndexPath(item: rowId, section: 0)
//        self.cartTable.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
    }
    
    @objc func submitDisLike(button: UIButton) {
        if SwiftUtil.getUserAccessToken() == ""{
            openNewClass(classIdentifier: "CheckPhoneNumberRegistrationVC")
        }else{
            let tData = DataSourceManagement.commentsList[button.tag]
            if tData.is_liked != "1",  tData.is_liked != "0"{
                self.commentAction(tData.pool_comment_id!,"0",button.tag)
            }
        }
    }
    
    func getCommentsAsAnString(_ dataItem : AnyObject) ->String{
        var returnString = ""
        if let value  =  dataItem as? [String] {
            for item in value  {
                returnString = item + "\n"
            }
            if value.count>0{
                return returnString
            }
        }
      return "-1"
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let lastElement = DataSourceManagement.commentsList.count - 1
        if indexPath.row == lastElement, !refreshAllDataVariable {
            if lastPageLoaded < commentsPage {
                self.getComments(DataSourceManagement.selectedPoolID,sortType,String(commentsPage))
            }
        }
    }
    
    func getListCount()->Int {
        return DataSourceManagement.commentsList.count
    }
    
    func getComments(_ pool_id: String, _ sort_by:String, _ page:String) -> Void {
        //if getListCount() <= 0 {
            LoadingOverlayV2.sharedV2.showOverlayV2(view: self.view, message: "در حال دریافت اطلاعات...")
        //}
        self.refreshAllDataVariable = true
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        var comment_depth = 2
        let limit  = "100"
        var sortBy = ""
        let index = wsPOST.init(Constants.POOLS_COMMENT + page + "/" + limit,"client_id=\(client_id)&access_token=\(access_token)&pool_id=\(pool_id)&page=\(page)&limit=\(limit)&sort_by=\(sortType)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            do{
                
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    //print(json)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true

                                if let value = item["page"] as? Int {
                                    self.lastPageLoaded = value
                                }
                                
                                if let value = item["comment_depth"] as? Int {
                                    comment_depth = value
                                }
                                
                                if let value = item["sorted_by"] as? String {
                                    sortBy = value
                                }
                                
                                if let comments = item["comments"] as? [[String: AnyObject]] {
                                    for comment in comments{
                                        DataSourceManagement.addPoolsComment(poolsComments.prepare(comment as AnyObject, "comment"))
                                        if let childs = comment["childs"] as? [[String: AnyObject]] {
                                            for child in childs {
                                                DataSourceManagement.addPoolsComment(poolsComments.prepareChild(child as AnyObject, "child", comment["user_name"] as! String))
                                                if comment_depth > 2 {
                                                     if let child1 = child["childs"] as? [[String: AnyObject]] {
                                                        for item1 in child1  {
                                                        DataSourceManagement.addPoolsComment(poolsComments.prepareChild(item1 as AnyObject, "child", child["user_name"] as! String))
                                                             if comment_depth > 3 {
                                                                if let child2 = item1["childs"] as? [[String: AnyObject]] {
                                                                  for item2 in child2  {
                                                                     DataSourceManagement.addPoolsComment(poolsComments.prepareChild(item2 as AnyObject, "child", item1["user_name"] as! String))
                                                                    
                                                                    }
                                                                }
                                                                
                                                            }
                                                            
                                                        }
                                                    
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if comments.count == 10 {
                                        self.commentsPage += 1
                                    }
                                }
                                
                                if let sort_list_items = item["sort_list"] as? [[String: AnyObject]] {
                                    
                                    DataSourceManagement.cleanCommentSortList()
                                    for item in sort_list_items {
                                        DataSourceManagement.addCommentSortList(sort_list.prepare(item as AnyObject))
                                    }
                                    
                                    self.sortTitle = []
                                    for item in DataSourceManagement.poolCommentSortList {
                                        if let value = item.title as? String {
                                            self.sortTitle.append(value)
                                        }
                                    }
                                    
                                }
                                
                            }
                        }
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                LoadingOverlayV2.sharedV2.hideOverlayView()
                self.refreshAllDataVariable = false
                if sortBy.count > 0 {
                    self.sortBtn.setTitle(self.sortTitle(sortBy), for: .normal)
                    self.sortType = sortBy
                }
                if WSResponseStatus {
                    self.tableView.reloadData()
                }else{
                    
                }
            })
            
        }
        
        
    }

    func commentAction(_ pool_comment_id: String, _ like:String, _ row: Int) -> Void {
        self.refreshAllDataVariable = true
        LoadingOverlay.shared.showOverlay(view: self.view)
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.LIKE_AND_DISLIKE_COMMENT,"client_id=\(client_id)&access_token=\(access_token)&pool_comment_id=\(pool_comment_id)&like=\(like)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            do{
                
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    //print(json)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                            }
                        }
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                LoadingOverlay.shared.hideOverlayView()
                self.refreshAllDataVariable = false
                if WSResponseStatus {
                    if like == "1" {
                        
                        let value = DataSourceManagement.commentsList[row]
                        if var value01 = Int(value.count_like!) as? Int {
                            value01 = value01 + 1
                            value.count_like = String(value01)
                        }
                        DataSourceManagement.commentsList[row] = value
                        let ndx = IndexPath(row:row, section: 0)
                        DataSourceManagement.commentsList[row].is_liked = "1"
                        self.tableView.reloadRows(at: [ndx], with: UITableView.RowAnimation.fade)
                        self.showDropMessage("لایک شما ثبت شد.")
                        
                    }else if like == "0"{
                        
                        let value = DataSourceManagement.commentsList[row]
                        if var value01 = Int(value.count_dislike!) as? Int {
                            value01 = value01 + 1
                            value.count_dislike = String(value01)
                        }
                        DataSourceManagement.commentsList[row] = value
                        let ndx = IndexPath(row:row, section: 0)
                        DataSourceManagement.commentsList[row].is_liked = "0"
                        self.showDropMessage("انجام شد.")
                        self.tableView.reloadRows(at: [ndx], with: UITableView.RowAnimation.fade)

                    }
                }else{
                    if like == "1" {
                        self.showDropMessage("لطفا دوباره تلاش کنید.")
                    }else{
                        self.showDropMessage("لطفا دوباره تلاش کنید.")
                    }
                }
            })
            
        }
        
        
    }
    
    func cleanTheList(){
        DataSourceManagement.cleanPoolsComment()
        self.tableView.reloadData()
    }
    
    //*************** drop down **************************//
    func showDurationAlert(_ sender: Any, _ strArray: [String], _ btn: UIButton) {
        // var durations = ["bank", "doctor", "restaurant"]
        let actions = strArray.map { seconds in
            return UIAlertAction(title: "\(seconds)", style: .default) { _ in
              
                btn.setTitle( seconds, for: .normal)
                self.sortType = self.getSortItemTitle(seconds)
                self.cleanStart_GetComment()
                
              
            }
        }
        
        
        let cancelAction = UIAlertAction(title: "بستن", style: .cancel, handler: nil)
        
        let controller = UIAlertController(title: "مرتب سازی بر اساس", message: nil, preferredStyle: .actionSheet)
        for action in [cancelAction] + actions {
            controller.addAction(action)
            //            self.selectedGenderId = ""
        }
        showAlert(controller, sourceView: sender as? UIView)
    }
    
    func showAlert(_ controller: UIAlertController, sourceView: UIView? = nil) {
        
        if UIDevice.current.userInterfaceIdiom == .pad  {
            if let sourceView = sourceView {
                let rect = sourceView.convert(sourceView.bounds, to: view)
                controller.popoverPresentationController?.sourceView = view
                controller.popoverPresentationController?.sourceRect = rect
            }
        }
        
        present(controller, animated: true, completion: nil)
    }
    
    func upAllDrops(_ sender: AnyObject) {
        if let hidden = navigationController?.isNavigationBarHidden {
            navigationController?.setNavigationBarHidden(!hidden, animated: true)
        }
        
        Drop.upAll()
    }
    
    //    func sampleText() -> String {
    //        let text = ""
    //
    //        return text
    //    }
    //*************** drop down **************************//
    
    @objc func responseToComment(button: UIButton)->Void{
        
        let data = DataSourceManagement.commentsList[button.tag] 
        shareData.shared.selectedCommentWriterPoolId = data.user_name!
        shareData.shared.selectedCommentPoolId = data.pool_comment_id!
        openSubmitNewCommentPage()
        //printLog(m: data.pool_comment_id!)
//        if SwiftUtil.getUserAccessToken() == ""{
//            openNewClass(classIdentifier: "CheckPhoneNumberRegistrationVC")
//        }else{
//        shareData.shared.selectedPoolId = data.pool_comment_id!
//        openNewClass(classIdentifier: "ResponseCommentsVC")
//        }
    }
    
    @IBAction func submitNewComment(){
        
        openSubmitNewCommentPage()
    }
    
    func openSubmitNewCommentPage() {
        if SwiftUtil.getUserAccessToken() == ""{
            openNewClass(classIdentifier: "CheckPhoneNumberRegistrationVC")
        }else{
            openNewClass(classIdentifier: "ResponseCommentsVC")
        }
    }
    
    func sortTitle(_ sortString: String)->String{
        var returnString = ""
        do{
            
            switch sortString {
            case "favorite":
                returnString = "مجبوب ترین"
                break;
            case "newest":
                returnString = "جدید ترین"
                break;
    
            default:
                print("")
            }
            
        }catch{
            fatalError("this is error \(error)");
        }
        
        return returnString;
    }
    
    func getSortItemTitle(_ sortString: String)->String{
        var returnString = ""
        do{
            
            switch sortString {
            case "مجبوب ترین":
                returnString = "favorite"
                break;
            case "جدید ترین":
                returnString = "newest"
                break;
                
            default:
                print("")
            }
            
        }catch{
            fatalError("this is error \(error)");
        }
        
        return returnString;
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
