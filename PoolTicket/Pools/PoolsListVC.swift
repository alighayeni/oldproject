//
//  PoolsListVC.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/28/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyDrop
import Firebase

class PoolsListVC: UIViewController, UITextFieldDelegate {
   

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sortBtn:UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var filterViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var pageTitle1: UILabel!
    @IBOutlet weak var pageTitleBtn: UIButton!
    var selectedCity: cityModel?
    var access_token = ""
    var extraQuery = ""
    var sortTitle = [String]()
    var pools : [poolsListModel] = []
    var pageIndex = 1
    var lastPageLoaded = 0
    var refreshAllDataVariable = false
    var lat = ""
    var lng = ""
    var QueryString = ""
    var defaultSort_by = "name"
    static var nearme = false
    static var attachedCityId : filterObject?
    
    @objc
    func openFilter() {
        
         //setSearchPoolNameParameter()
         PoolFiltersNew.filterConfigType = "allpools"
         self.pushNewClass(classIdentifier: "PoolFiltersNew")
        
    }
    
    @IBAction func close(_ sender:Any){
        
        PoolFilters.clearFiltersView()
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func searchAction(_ sender: Any){
          searchThePool()
    }
    
    @IBAction func theSortAction(_ sender: Any){
        
        if let value = sender as? UIButton {
            showDurationAlert(sender,self.sortTitle, value)
        }
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DataSourceManagement.filterList = []
        tableView.delegate = self
        tableView.dataSource = self
        collectionView.delegate = self
        collectionView.dataSource = self
        pageTitle1.text = "مجموعه های آبی"
        
        
        // clear the filter
        PoolFiltersNew.poolName = ""
        PoolFiltersNew.selectedPoolTypeName = ""
        PoolFiltersNew.selectedPoolTypeId = ""
        PoolFiltersNew.selectedGenderTypePosition = 0
        
        if PoolsListVC.nearme {
            
            let object = filterObject.init(name: "استخر های نزدیک من", key: "nearMe", value1: "true", value2: nil)
            DataSourceManagement.filterList.append(object)
            //pageTitle1.text = "استخر های نزدیک"
            pageTitleBtn.setTitle("مشاهده بر روی نقشه", for: UIControl.State.normal)
            // add lat lang to extra query
            extraQuery += "&lng=\(lng)&lat=\(lat)"
            if DataSourceManagement.filterList.count > 0 {
                self.collectionView.reloadData()
                self.filterViewHeightConstraint.constant = 50
            }
            AppDelegate()._manager?.track("open_near_me_pool_list", data: [:])
            Analytics.logEvent("open_near_me_pool_list", parameters: nil)
        } else {
            AppDelegate()._manager?.track("open_pool_list", data: [:])
            Analytics.logEvent("open_pool_list", parameters: nil)
            extraQuery = ""
            
            // if the user load this page from main page and has attached city Id
            // check the custom query as well
            if PoolsListVC.attachedCityId != nil {
               DataSourceManagement.filterList.append(PoolsListVC.attachedCityId!)
               PoolsListVC.attachedCityId = nil
            }
            
            //pageTitle1.text = "مجموعه های آبی"
            pageTitleBtn.setTitle("تغییر شهر", for: UIControl.State.normal)
            
        }


        access_token = SwiftUtil.getUserAccessToken()

        self.lat = DataSourceManagement.lat
        self.lng = DataSourceManagement.lng
        self.title = ""
        if !AppDelegate.changeFilterValue {
            prepareQuery()
        }
        getDefaultData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        let closeTopBarBtn = UIBarButtonItem(image: UIImage(named: "ic_filters"),style: .plain ,target: self, action: #selector(self.openFilter))
        self.navigationItem.rightBarButtonItem = closeTopBarBtn
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //segmentSection.selectedSegmentIndex = 1
        
        if PoolsListVC.nearme {
            pageTitleBtn.setTitle("مشاهده بر روی نقشه", for: UIControl.State.normal)
        } else {
            pageTitleBtn.setTitle("تغییر شهر", for: UIControl.State.normal)
        }
        
        if AppDelegate.changeFilterValue {
            
            if DataSourceManagement.filterList.count > 0 {
                self.collectionView.reloadData()
                self.filterViewHeightConstraint.constant = 50
            } else {
                self.filterViewHeightConstraint.constant = 0
            }

            prepareQuery()
            AppDelegate.changeFilterValue = false
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
       clearTheQueryFilter()
    }
   
    func getDefaultData() {
        
        if !AppDelegate.changeFilterValue {
            if DataSourceManagement.filterList.count > 0 {
                self.collectionView.reloadData()
                self.filterViewHeightConstraint.constant = 50
            } else {
                emptyExtraQuery()
                self.filterViewHeightConstraint.constant = 0
            }
            self.prepareQuery()
        }
        
    }
    
    func emptyExtraQuery(){
        extraQuery = ""
    }
    
    func prepareQuery(){
        creatCustomQuery()
        pageIndex = 1
        let client_id = Constants.clientId

        extraQuery = "sort_by=\(defaultSort_by)&access_token=\(access_token)&client_id=\(client_id)\(extraQuery)"
        callGetDataFromServer(Page: String(pageIndex),Query: extraQuery)

    }
    
    //MARK: Prepare a custom query "filter"
    func creatCustomQuery() -> Void {
        let filterList = DataSourceManagement.filterList
        if filterList.count > 0 {
             extraQuery = ""
        }
        for item in filterList {
            
            switch item.key {
            case "has_discount":
                extraQuery += "&\(item.key!)=\(item.value1!)"
            case "gender":
                if item.value1 != "2" {
                    extraQuery += "&\(item.key!)=\(item.value1!)"
                }
            case "selectedCity":
                selectedCity = DataSourceManagement.getSelectedStateAndCity()
                if let value = selectedCity, let cityId = value.city_id {
                    extraQuery += "&city_id=\(String(cityId))"
                }
            case "priceSlider":
                if let minPrice = item.value1, let maxPrice = item.value2 {
                    extraQuery += "&min_price=\(minPrice)&max_price=\(maxPrice)"
                }
            case "poolName":
                if let poolName = item.value1, poolName.count > 0 {
                    extraQuery += "&pool_name=\(poolName)"
                }
            case "nearMe":
                // find index of old selected city in the filter
                var index = -1
                if DataSourceManagement.filterList.count > 0 {
                    for i in 0..<DataSourceManagement.filterList.count {
                        if let value = DataSourceManagement.filterList[i] as? filterObject {
                            if value.key == "selectedCity" {
                                index = i
                            }
                        }
                    }
                    // remove the old city data
                    if index != -1 {
                        DataSourceManagement.filterList.remove(at: index)
                        creatCustomQuery()
                        continue
                    }
                    // add lat lang to extra query
                    extraQuery += "&lng=\(lng)&lat=\(lat)"
                }
            case "poolType":
                if let pool_typeId = item.value1, pool_typeId.count > 0 {
                    extraQuery += "&pool_type=\(pool_typeId)"
                }
            case "attachedCityId":
                // if the user load this page from main page and has attached city Id
                if let cityId = item.value2 {
                    extraQuery += "&city_id=\(cityId)"
                }
            case .none:
                break
            case .some(_):
                break
            }
        }
        
    }
    
    //MARK: Fetch The Data
    func callGetDataFromServer(Page index: String, Query: String) {
        getPoolsListV2(index,Query)
    }
    
    //get the list Version 2
    func getPoolsListV2(_ index: String,_ qString: String) -> Void {
        
        if getListCount() <= 0 {
            LoadingOverlayV2.sharedV2.showOverlayV2(view: self.view, message: "در حال دریافت اطلاعات...")
        }
        self.refreshAllDataVariable = true
        let currentIndex:Int? = Int(index)
        let count = 10
        var sortBy = ""
        if index == "1" {
            DataSourceManagement.cleanPoolsList()
            tableView.reloadData()
        }
        
        let index = wsPOST.init(Constants.POOLS_LIST + index + "/" + String(count),qString)
        //var WSResponseStatus = false
        
        index.start{ (indexData) in
            
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                  //print(json)
                    if let value = currentIndex as? Int{
                        self.lastPageLoaded = value
                    }
                    
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                //WSResponseStatus = true
                                
                                //map settings
                                if let value = item["map_settings"] as? [String:AnyObject] {
                                    if let tLat = String(describing: value["lat"] as AnyObject) as? String {
                                        shareData.shared.Pool_list_map_Lat = tLat
                                    }
                                    if let tLng = String(describing: value["lng"] as AnyObject) as? String {
                                        shareData.shared.Pool_list_map_Lng = tLng
                                    }
                                    if let tZoom = String(describing: value["zoom"] as AnyObject) as? String {
                                        shareData.shared.Pool_list_map_zoom = tZoom
                                    }
                                }
                                
                                if let value = item["sorted_by"] as? String {
                                    sortBy = value
                                }
                                
                                if let pools = item["pools"] as? [[String: AnyObject]] {
                                    
                                    for pool in pools {
                                        DataSourceManagement.addPoolsList(poolsListModel.prepare(pool as AnyObject))
                                    }
                                    if pools.count == count {
                                        self.pageIndex += 1
                                    }
                                    
                                }
                                if let sort_list_items = item["sort_list"] as? [[String: AnyObject]] {
                                    
                                    DataSourceManagement.cleanSortList()
                                    for item in sort_list_items {
                                        DataSourceManagement.addSortList(sort_list.prepare(item as AnyObject))
                                    }
                                    
                                    self.sortTitle = []
                                    for item in DataSourceManagement.poolSortList {
                                        if let value = item.title {
                                            self.sortTitle.append(value)
                                        }
                                    }
                                    
                                }
                                
                            }
                        }
                    }
                    
                    
                }
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                LoadingOverlayV2.sharedV2.hideOverlayView()
                self.refreshAllDataVariable = false
                if sortBy.count > 0 {
                    self.sortBtn.setTitle(self.getSortConfig(sortBy), for: .normal)
                }
                self.tableView.reloadData()
                
            })
            
        }
        
    }
    
    func getListCount()->Int {
      return DataSourceManagement.searchPoolList.count
    }
    
    func clearTheQueryFilter() {
        // clear the filter
        shareData.shared.filterPoolListEnable = nil
        shareData.shared.filterPoolListQuery = nil
    }
    
    func searchThePool(){
        
        clearTheQueryFilter()
        DataSourceManagement.setOpenNearLocationPool(false)
        if !self.refreshAllDataVariable {
            prepareQuery()
        }
        
    }
    
    //MARK: The Drop Down
    func showDurationAlert(_ sender: Any, _ strArray: [String], _ btn: UIButton) {
        // var durations = ["bank", "doctor", "restaurant"]
        let actions = strArray.map { seconds in
            return UIAlertAction(title: "\(seconds)", style: .default) { _ in
                //Drop.down(self.sampleText(), state: .default, duration: seconds)
                //print(seconds)
                //self.selectedGenderId = seconds
                //self.tSelectedCategory = seconds
                btn.setTitle( seconds, for: .normal)
                self.defaultSort_by = self.getSortConfig(seconds)
                self.prepareQuery()
                //selectedCategoryId = self.getCategoryId(name: seconds)
                //print(selectedCategoryId)
            }
        }
        
        
        let cancelAction = UIAlertAction(title: "بستن", style: .cancel, handler: nil)
        
        let controller = UIAlertController(title: "مرتب سازی بر اساس", message: nil, preferredStyle: .actionSheet)
        for action in [cancelAction] + actions {
            controller.addAction(action)
            //self.selectedGenderId = ""
        }
        showAlert(controller, sourceView: sender as? UIView)
    }
    
    func showAlert(_ controller: UIAlertController, sourceView: UIView? = nil) {
        
        if UIDevice.current.userInterfaceIdiom == .pad  {
            if let sourceView = sourceView {
                let rect = sourceView.convert(sourceView.bounds, to: view)
                controller.popoverPresentationController?.sourceView = view
                controller.popoverPresentationController?.sourceRect = rect
            }
        }
        
        present(controller, animated: true, completion: nil)
    }
    
    func upAllDrops(_ sender: AnyObject) {
        if let hidden = navigationController?.isNavigationBarHidden {
            navigationController?.setNavigationBarHidden(!hidden, animated: true)
        }
        
        Drop.upAll()
    }
    
    //MARK: Handle Soft Keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField
        {
        default:
            searchThePool()
            textField.resignFirstResponder()
        }
        return true
    }
    
    func doneWithKeyboard(_ sender: UITextField){
        
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        keyboardToolbar.isTranslucent = true
        keyboardToolbar.barTintColor = UIColor.white
        
        let button = UIButton(type: .system)
        button.setTitle("بستن", for: .normal)
        button.sizeToFit()
        button.titleLabel?.font = UIFont(name: "IRANSansMobile", size: 12)
        button.tintColor = UIColor.black
        button.addTarget(self, action: #selector(closeKeyboard(_:)), for: .touchUpInside)
        
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        keyboardToolbar.items = [flexBarButton,UIBarButtonItem(customView: button)]
        //keyboardToolbar.items = [itemDown,itemUp,flexBarButton,UIBarButtonItem(customView: button)]
        
        if let value = sender as? UITextField {
            value.inputAccessoryView = keyboardToolbar
        }
//        else if let value = sender as? UITextView {
//            value.inputAccessoryView = keyboardToolbar
//        }
        
    }
    
    func getSortConfig(_ sortString: String)->String{
        var returnString = ""
        do{
        
        switch sortString {
           
        case "favorite":
             returnString = "مجبوب ترین"
            break;
            case "مجبوب ترین":
            returnString = "favorite"
            break
            
            case "visit_count":
            returnString = "پربازدید ترین"
            break;
            case "پربازدید ترین":
            returnString = "visit_count"
            break;
            
            case "comment":
            returnString =  "بیشترین نظر"
            break;
            case "بیشترین نظر":
            returnString = "comment"
            break;
            
            case "discount":
            returnString = "بیشترین تخفیف"
            break;
            case "بیشترین تخفیف":
            returnString = "discount"
            break;
            
            case "name":
            returnString = "حروف الفبا"
            case "حروف الفبا":
            returnString = "name"
            break;
            
            default:
                print("")
        }
            
        }catch{
            fatalError("this is error \(error)");
        }
        
        return returnString;
    }
  
   
    @IBAction func chooseCityAndState() {
        //self.pushNewClass(classIdentifier: "PoolFiltersNew")
        DataSourceManagement.fromInbox = true
        AppDelegate.changeFilterValue = true
        if PoolsListVC.nearme {
            pushNewClass(classIdentifier: "PoolsOnTheMap")
        } else {
            DataSourceManagement.fromInbox = true
            openNewClass(classIdentifier: "ChooseStateAndCity")

        }
    }
}

extension PoolsListVC: UITableViewDelegate, UITableViewDataSource {

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return DataSourceManagement.searchPoolList.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        var cellIdentifier =  "poolCustomRowTable"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! poolCustomRowTable
        
        let data = DataSourceManagement.searchPoolList[indexPath.row] 
        
        
        //clear objcet items
        cell.availableForMan.alpha = 0
        cell.poolPrice.text = ""
        cell.poolPriceInGishe.text = ""
        cell.poolPriceInGisheTitle.alpha = 0
        cell.availableForWomen.alpha = 0
        cell.poolDiscountBg.alpha = 0
        
        cell.poolName.text = data.name
        if "" != data.area_name, "<null>" != data.area_name {
            cell.poolAddress.text = data.area_name
        }else{
            cell.poolAddress.text = data.city_name
        }
        if let value = data.rating as? String {
            cell.poolRateBar.rating = setRatingStar(value)
        }
        
        if let stringURL = URL.init(string: data.default_image!) as? URL {
            let imageView = cell.poolImage
            let URL = stringURL//Foundation.URL(string: data["thumbnail"]!)!
            let placeholderImage = UIImage(named: "main_top_bg.png")!
            imageView?.af_setImage(withURL: URL, placeholderImage: placeholderImage)
        }else{
            cell.poolImage.image = UIImage(named: "main_top_bg.png")
        }
        
        switch data.gender! {
        case "0":
            cell.availableForMan.alpha = 1
            break
        case "1":
            cell.availableForWomen.alpha = 1
            break
        case "2":
            cell.availableForWomen.alpha = 1
            cell.availableForMan.alpha = 1
            break
        default:
            printLog(m: "")
            cell.availableForWomen.alpha = 0
            cell.availableForMan.alpha = 0
        }
        
        
        // if the discount qual to zero, all discount section should be hidden
        if data.max_discount != "null", data.max_discount != "0" {
            if data.max_discount != nil , data.max_discount!.count > 0 {
                cell.poolDiscountBg.alpha = 1
                cell.poolDiscount.text = "%\(replaceString(data.max_discount!)) تخفیف"
            }else{
                cell.poolDiscount.text = ""
                cell.poolDiscountBg.alpha = 0
            }
        }else{
            cell.poolDiscount.text = ""
            //cell.poolDiscountBg.alpha = 0
        }
        
        
        
        //        if let value = data.minimum_sale_price as? String{
        //
        //            if value != "<null>" {
        //                cell.poolPriceInGishe.text = self.replaceString(value) + " " + "تومان"
        //                if let price = data.minimum_market_price as? String{
        //                    if price != "<null>" {
        //                        cell.poolPrice.text = self.replaceString(price) + " " + "تومان"
        //                    }
        //                }
        //
        //            }else{
        //
        //                cell.poolPriceInGishe.text = ""
        //                cell.poolRedLine.alpha = 0
        //                if let price = data.minimum_market_price as? String{
        //                    if price != "<null>" {
        //                        cell.poolPrice.text = self.replaceString(price) + " " + "تومان"
        //                    }
        //                }
        //            }
        //        }
        
        if data.minimum_market_price != data.minimum_sale_price, data.minimum_market_price != "<null>",  data.minimum_sale_price != "<null>"  {
            
            // gishe has the price
            // sale has the price
            // thei are not equal
            
            
            // set the gishe price
            if let price = data.minimum_market_price as? String {
                cell.poolPriceInGishe.text = self.replaceString(price) + " " + "تومان"
                cell.poolPriceInGisheTitle.alpha = 1
                cell.poolPriceInGishe.alpha = 1
                // if discount is not zero, the red line show be shown on the price
                let lineView = UIView(
                    frame: CGRect(x: 0,
                                  y: cell.poolPriceInGishe.bounds.size.height / 2,
                                  width: cell.poolPriceInGishe.bounds.size.width,
                                  height: 1
                    )
                )
                lineView.backgroundColor = UIColor.red;
                cell.poolPriceInGishe.addSubview(lineView)
            }
            
            // set sale price if it is available
            if let salePrice = data.minimum_sale_price as? String{
                cell.poolPrice.text = self.replaceString(salePrice) + " " + "تومان"
            }
            
            
        }else if data.minimum_market_price == "<null>", data.minimum_sale_price == "<null>"{
            /*
             for resolve tasked that defined in team work
             https://magnait.teamwork.com/#tasks/16911154
             */
            cell.poolPriceInGisheTitle.alpha = 0
            cell.poolPriceInGishe.alpha = 0
            cell.poolPrice.text = "مشاهده"
            
            
        }else if data.minimum_market_price != "<null>", data.minimum_sale_price == "<null>"{
            /*
             for resolve tasked that defined in team work
             https://magnait.teamwork.com/#tasks/16917606
             */
            cell.poolPriceInGisheTitle.alpha = 0
            cell.poolPriceInGishe.alpha = 0
            
            if let salePrice = data.minimum_sale_price as? String{
                if salePrice != "<null>" {
                    cell.poolPrice.text = self.replaceString(salePrice) + " " + "تومان"
                }
            }
            
        }else if data.minimum_market_price == data.minimum_sale_price, data.minimum_market_price != "<null>",
            data.minimum_sale_price != "<null>"  {
            
            cell.poolPriceInGisheTitle.alpha = 0
            cell.poolPriceInGishe.alpha = 0
            
            if let salePrice = data.minimum_sale_price as? String{
                if salePrice != "<null>" {
                    cell.poolPrice.text = self.replaceString(salePrice) + " " + "تومان"
                }
            }
            
        }
        
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = DataSourceManagement.searchPoolList[indexPath.row] 
        DataSourceManagement.setSelectedPoolId(data.pool_id!)
        pushNewClass(classIdentifier: "PoolVC")
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let lastElement = DataSourceManagement.searchPoolList.count - 1
        if indexPath.row == lastElement, !refreshAllDataVariable {
            if lastPageLoaded < pageIndex {
//                let sort = "sort_by=" + defaultSort_by + "&"
              //  getPoolsListV2(String(pageIndex),sort+QueryString+"&access_token=\(access_token)")
               // extraQuery += "&sort_by=\(defaultSort_by)&access_token=\(access_token)"
                callGetDataFromServer(Page: String(pageIndex),Query: extraQuery)
                print(">>>>>>>>>> new page")
            }
        }
    }
    
}
extension PoolsListVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DataSourceManagement.filterList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "filterCollectionViewCell", for: indexPath) as! filterCollectionViewCell
        let data = DataSourceManagement.filterList[indexPath.row]
        cell.labelName.text = data.name
        cell.removeItemFromFilter.tag = indexPath.row
        cell.removeItemFromFilter.addTarget(self, action: #selector(self.removeThefilter(_:)), for: .touchUpInside)
        return cell
    }
    
    @objc
    func removeThefilter(_ button: UIButton) -> Void {
        if let index = button.tag as? Int{
            if DataSourceManagement.filterList.count > index {
                switch DataSourceManagement.filterList[index].key {
                case "nearMe":
                    PoolsListVC.nearme = false
                    pageTitleBtn.setTitle("تغییر شهر", for: UIControl.State.normal)
                case "poolName":
                    PoolFiltersNew.poolName = ""
                case "poolType":
                    PoolFiltersNew.selectedPoolTypeName = ""
                    PoolFiltersNew.selectedPoolTypeId = ""
                case "gender":
                    PoolFiltersNew.selectedGenderTypePosition = 0
                default:
                    break
                }
                DataSourceManagement.filterList.remove(at: index)
                collectionView.reloadData()
                getDefaultData()
                //prepareQuery()
            }
        }
    }
    
}

extension PoolsListVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let data = DataSourceManagement.filterList[indexPath.row].name
        let widthlenght = data?.width(withConstrainedHeight: 50.0, font: UIFont.init(name: "IRANSansMobile", size: 17.5)!) ?? 0.0
        return CGSize.init(width: (widthlenght + 70), height: 50)
    }
}


