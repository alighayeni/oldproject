//
//  PoolOnTheMap.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/2/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps

class PoolOnTheMap: UIViewController, GMSMapViewDelegate {
    
    @IBOutlet weak var theMap:GMSMapView!
    var selectedPoolObject: poolsById?
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        theMap.delegate = self
        theMap.accessibilityElementsHidden = false
        
        self.selectedPoolObject = shareData.shared.selectedPoolObject
        
        self.showPoolsOnMap()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    
    private func showPoolsOnMap()-> Void{
        
        
       // for item in allItems {
            
            var lat = 0.0
            var lng = 0.0
            var status_sell = "0"
            
            
            // the 1 means ready for sale in pool ticket, the 0 is not available
            if let value = self.selectedPoolObject?.status_sell as? String {
                status_sell = value
            }
            
            // lat and lng of the pool in the map area
            if let tLat = self.selectedPoolObject?.lat as? String{
               if let value = Double(tLat) as? Double {
                    lat = value
                }
            }
            if let tLng = self.selectedPoolObject?.lng as? String{
                if let value = Double(tLng) as? Double{
                    lng = value
                }
            }
            
            
            if lat != 0.0 && lng != 0.0 {
                
                let marker = GMSMarker()
                
                // set the icon marker
                var markerImage = UIImage(named: "itemMarkerBlue")!.withRenderingMode(.alwaysOriginal)
                if status_sell == "1" {
                    markerImage = UIImage(named: "itemMarkerBlue")!.withRenderingMode(.alwaysOriginal)
                }else{
                    markerImage = UIImage(named: "itemMarkerRed")!.withRenderingMode(.alwaysOriginal)
                }
                let markerView = UIImageView(image: markerImage)//markerImage)
                marker.iconView = markerView
                
                // marker stand degree
                var degrees = 0.0
                marker.rotation = degrees
                marker.position = CLLocationCoordinate2D(latitude:  lat, longitude: lng)
                let data = markerCustomDetaile01(item: self.selectedPoolObject!)
                marker.userData = data
                marker.map = theMap
                let camera = GMSCameraPosition.camera(withLatitude:  lat, longitude: lng, zoom: 12)
                self.theMap.camera = camera
                
            }
        }
        
//    if allItems.count > 0 {
//            var lat = 0.0
//            var lng = 0.0
//            if let tLat = self.selectedPoolObject?.lat as? String {
////                if let value = Double(tLat) as? Double {
////                    lat = value
////                }
//            }
//            if let tLng = self.selectedPoolObject?.lng as? String {
////                if let value = Double(tLng1) as? Double {
////                    lng = value
////                }
//            }
//            if lat != 0.0 && lng != 0.0 {
//
//                let camera = GMSCameraPosition.camera(withLatitude:  lat, longitude: lng, zoom: 12)
//                self.theMap.camera = camera
//
//            }
// }
//
//
//    }
    
    
    public func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        //print("didtapat")
        
    }
    
    public func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        
        //print("didlongpress")
        
    }
    
    
    public func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker){
        
        //print("didtapinfo")
        
//        if let object = marker.userData as? markerCustomDetaile01 {
//            if let item = object.item  {
//                self.printLog(m: item.name!+" "+item.pool_id!)
//
//                DataSourceManagement.setSelectedPoolId(item.pool_id!)
//                self.openNewClass(classIdentifier: "PoolVC")
//
//            }
//        }
        //        self.showDropMessage("قابلیت نمایش استخر در نسخه آینده فعال خواهد بود.")
        //        shareData.shared.lastCarName = (marker.userData as! markerCustomDetaile).item?.Name
        //        if let value = (marker.userData as! markerCustomDetaile).item {
        //            shareData.shared.lastCarsModel = value
        //            shareData.shared.ApllicationIconId = value.Application
        //
        //        }
        //
        //        shareData.shared.lastCarTime = ""
        //        openNewClass(classIdentifier: "carTabView")
        
        
        
        
    }
    
    
    
    public func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        
        if let object = marker.userData as? markerCustomDetaile01 {
            if let item = object.item  {
                var poolTitle = ""
                var status_sell = ""
                
                if let value = item.name {
                    poolTitle = value
                }
                
                if let value = item.status_sell {
                    status_sell = value
                }
                if status_sell == "0" {
                    if let infoView = TheMarkerDetailInActive.instanceFromNib() as? TheMarkerDetailInActive {
                        infoView.pTitle?.text = poolTitle
                        return infoView
                    } else{
                        return nil
                    }
                }else if status_sell == "1" {
                    if let infoView01 = TheMarkerDetailActive.instanceFromNib() as? TheMarkerDetailActive {
                        infoView01.pTitle?.text = poolTitle
                        return infoView01
                    } else{
                        return nil
                    }
                }
            }
        }
        return nil
    }
    
    @IBAction func close(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

struct markerCustomDetaile01 {
    var item:poolsById?
}

