//
//  FilterGenderCustomCell.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/15/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
class FilterCustomCell: UITableViewCell {
    
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var selectedItemImage: UIImageView!

}
