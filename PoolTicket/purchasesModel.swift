//
//  purchasesModel.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/2/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation

class purchasesModel: NSObject {
    
    var purchase_history_id:String?
    var service_type:String?
    var pool_name:String?
    var pool_id:String?
    var voucher:String?
    var status_text:String?
    var status_color:String?
    var start_date:String?
    var start_day:String?
    var time:String?
    var sean_name:String?
    var ticket_count:String?
    var course_name: String?
    var pack_name: String?
    var remain_session:String?

    init(_ purchase_history_id:String,_ service_type:String,_ pool_name:String,_ pool_id:String,_ voucher:String,_ status_text:String,_ status_color:String,_ start_date:String,_ start_day:String,_ time:String, _ sean_name:String, _ ticket_count:String, _ course_name:String, _ pack_name:String, _ remain_session:String) {
        self.purchase_history_id = purchase_history_id
        self.service_type = service_type
        self.pool_name = pool_name
        self.pool_id = pool_id
        self.voucher = voucher
        self.status_text = status_text
        self.service_type = service_type
        self.status_color = status_color
        self.start_date = start_date
        self.start_day = start_day
        self.time = time
        self.sean_name = sean_name
        self.ticket_count = ticket_count
        self.course_name = course_name
        self.pack_name = pack_name
        self.remain_session = remain_session
    }
    
    static func prepareReservation(_ item:AnyObject) -> purchasesModel{
        let purchase_history_id = String(describing: item["purchase_history_id"] as AnyObject)
        let service_type = String(describing: item["service_type"] as AnyObject)
        let pool_name = String(describing: item["pool_name"] as AnyObject)
        let pool_id = String(describing: item["pool_id"] as AnyObject)
        let voucher = String(describing: item["voucher"] as AnyObject)
        let status_color = String(describing: item["status_color"] as AnyObject)
        let start_date = String(describing: item["start_date"] as AnyObject)
        let time = String(describing: item["time"] as AnyObject)
        let sean_name = String(describing: item["saens_name"] as AnyObject)
        let ticket_count = String(describing: item["ticket_count"] as AnyObject)
        let status_text = String(describing: item["status_text"] as AnyObject)
        let start_day = String(describing: item["start_day"] as AnyObject)
        let course_name = "-1"//String(describing: item["course_name"] as AnyObject)
        let pack_name = "-1"//String(describing: item["pack_name"] as AnyObject)
        let remain_session  = "-1"
        let instance = purchasesModel(purchase_history_id, service_type, pool_name, pool_id, voucher, status_text, status_color, start_date, start_day, time, sean_name, ticket_count,course_name,pack_name, remain_session)
        return instance
    }
    
    static func preparePack(_ item:AnyObject) -> purchasesModel{
        let purchase_history_id = String(describing: item["purchase_history_id"] as AnyObject)
        let service_type = String(describing: item["service_type"] as AnyObject)
        let pool_name = String(describing: item["pool_name"] as AnyObject)
        let pool_id = String(describing: item["pool_id"] as AnyObject)
        let voucher = String(describing: item["voucher"] as AnyObject)
        let status_text = String(describing: item["status_text"] as AnyObject)
        let status_color = String(describing: item["status_color"] as AnyObject)
        let start_date = "-1"//String(describing: item["start_date"] as AnyObject)
        let start_day = "-1"//String(describing: item["start_day"] as AnyObject)
        let time = "-1"//String(describing: item["time"] as AnyObject)
        let sean_name =  "-1"//String(describing: item["sean_name"] as AnyObject)
        let ticket_count =  "-1"//String(describing: item["ticket_count"] as AnyObject)
        let course_name = "-1"//String(describing: item["course_name"] as AnyObject)
        let pack_name = String(describing: item["pack_name"] as AnyObject)
        let remain_session = String(describing: item["remain_session"] as AnyObject)

    let instance = purchasesModel(purchase_history_id, service_type, pool_name, pool_id, voucher, status_text, status_color, start_date, start_day, time, sean_name, ticket_count,course_name,pack_name, remain_session)
        return instance
    }
    
    static func prepareCourse(_ item:AnyObject) -> purchasesModel{
        let purchase_history_id = String(describing: item["purchase_history_id"] as AnyObject)
        let service_type = String(describing: item["service_type"] as AnyObject)
        let pool_name = String(describing: item["pool_name"] as AnyObject)
        let pool_id = String(describing: item["pool_id"] as AnyObject)
        let voucher = String(describing: item["voucher"] as AnyObject)
        let status_text = String(describing: item["status_text"] as AnyObject)
        let status_color = String(describing: item["status_color"] as AnyObject)
        let start_date = "-1"//String(describing: item["start_date"] as AnyObject)
        let start_day = "-1"//String(describing: item["start_day"] as AnyObject)
        let time = "-1"//String(describing: item["time"] as AnyObject)
        let sean_name =  "-1"//String(describing: item["sean_name"] as AnyObject)
        let ticket_count =  "-1"//String(describing: item["ticket_count"] as AnyObject)
        let course_name = String(describing: item["course_name"] as AnyObject)
        let pack_name = "-1"//String(describing: item["pack_name"] as AnyObject)
        let remain_session = "-1"
       let instance = purchasesModel(purchase_history_id, service_type, pool_name, pool_id, voucher, status_text, status_color, start_date, start_day, time, sean_name, ticket_count,course_name,pack_name, remain_session)
        return instance
    }
}
