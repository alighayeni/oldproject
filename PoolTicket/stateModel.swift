//
//  stateModel.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/13/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation

class stateModel: NSObject {
    
    
    
    var state_id:String?
    var state_name:String?
 
    
    
    init(_ state_id:String,_ state_name:String) {
        self.state_id = state_id
        self.state_name = state_name
       
    }
    
    static func prepare(_ item:AnyObject) -> stateModel{
        
        
        
        let state_id = String(describing: item["state_id"] as AnyObject)
        let state_name = String(describing: item["state_name"] as AnyObject)
      
        let instance = stateModel(state_id, state_name)
        return instance
    }
    
    
    
}
