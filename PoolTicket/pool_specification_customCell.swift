//
//  File.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/2/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit

class pool_specification_customCell: UITableViewCell {
    
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn1_ic: UIImageView!

    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn2_ic: UIImageView!

    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn3_ic: UIImageView!
    
}
