//
//  TextFieldWithPadding.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 5/28/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit


class TextFieldWithPadding: UITextField {
    
    let padding = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8);
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
