//
//  textModel.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/26/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import UIKit

class textModel: NSObject {
    
    
    var slide_type:String?
    var title:String?
    var content:String?
    var image:String?
    var url:String?
   
    
    
    init(slide_type:String, title:String, content:String, image:String, url:String) {
        self.slide_type=slide_type
        self.title=title
        self.content=content
        self.image=image
        self.url=url
        
       
    }
    
    static func prepare(_ item:AnyObject) -> textModel{
        
        let slide_type = String(describing: item["slide_type"] as AnyObject)
        let title = String(describing: item["title"] as AnyObject)
        let content = String(describing: item["content"] as AnyObject)
        let image = String(describing: item["image"] as AnyObject)
        let url = String(describing: item["url"] as AnyObject)

        let instance = textModel(slide_type:slide_type, title:title, content:content, image:image, url:url)
        
        return instance
    }
    
    
    
}
