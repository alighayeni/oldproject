//
//  ReviewOrders.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/2/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import EFQRCode

class PurchasesHistoryVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
   
    
    var phPage  = 1
    var lastPageLoaded = 0

    var refreshAllDataVariable = false
    @IBOutlet weak var tableview:UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        tableview.dataSource = self
        tableview.delegate = self
        
        DataSourceManagement.cleanPurchasesHistory()
        self.purchasesHistory(String(phPage))
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage() //remove pesky 1 pixel line
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        
//        let closeTopBarBtn = UIBarButtonItem(image: UIImage(named: "ic_arrow_left_white"),style: .plain ,target: self, action: #selector(self.back(img:)))
//        self.navigationItem.leftBarButtonItem = closeTopBarBtn
        
         self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.font.rawValue: UIFont(name: "IRANSansMobile", size: 14),NSAttributedString.Key.foregroundColor.rawValue:UIColor.white])
        self.title = "تاریخچه خرید"
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataSourceManagement.userPurchaseHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = DataSourceManagement.userPurchaseHistory[indexPath.row] 

        if data.service_type == "reservation" {
            let cellIdentifier =  "purchaseCustomCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! purchaseCustomCell
            if let value  = data.pool_name as? String {
                cell.pool_name.text = value
            }
        
            if let value  = data.voucher as? String {
                cell.voucher.text = self.replaceString(value)
            }
        
            if let value  = data.start_day as? String, let value1 = data.start_date as? String {
                cell.start_date.text = self.replaceString(value + " " + value1)
            }
        
            if let value  = data.time as? String {
                cell.time.text = self.replaceString(value)
            }

            if let value  = data.ticket_count as? String {
                 cell.ticket_count.text = value + " نفر"
            }
            
            if let value  = data.sean_name as? String {
                cell.saens_name.text = "نام سانس: " + value
            }
            
             cell.total_price.text = "رزرو بلیط"
        
            if let value  = data.status_text as? String {
                cell.status.setTitle(value, for: .normal)
                cell.status.setBackgroundColor(color: self.hexStringToUIColor(data.status_color!), forState: UIControl.State.normal)
                cell.status.layer.cornerRadius = 3
                cell.status.clipsToBounds = true
            }
           cell.show_specification.tag = indexPath.row
           cell.status.tag = indexPath.row
           cell.status.addTarget(self, action: #selector(showQr(button:)), for: .touchUpInside)
           cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
     
        }else  if data.service_type == "course_reservation" {
            
            let cellIdentifier =  "purchaseCustomCellTypeCourse"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! purchaseCustomCell
            if let value  = data.pool_name as? String {
                cell.pool_name.text = value
            }
            
            if let value  = data.voucher as? String {
                cell.voucher.text = self.replaceString(value)
            }
            
            if let value  = data.course_name as? String {
                cell.start_date.text = self.replaceString(value)
            }
            
            
            
             cell.total_price.text = "کلاس شنا"
            
            if let value  = data.status_text as? String {
                cell.status.setTitle(value, for: .normal)
                cell.status.setBackgroundColor(color: self.hexStringToUIColor(data.status_color!), forState: UIControl.State.normal)
                cell.status.layer.cornerRadius = 3
                cell.status.clipsToBounds = true
            }
            
            cell.show_specification.tag = indexPath.row
            cell.status.tag = indexPath.row
            cell.status.addTarget(self, action: #selector(showQr(button:)), for: .touchUpInside)
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        }else if  data.service_type == "pack_reservation" {
            let cellIdentifier =  "purchaseCustomCellTypePack"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! purchaseCustomCell
            if let value  = data.pool_name as? String {
                cell.pool_name.text = value
            }
            
            if let value  = data.voucher as? String {
                cell.voucher.text = self.replaceString(value)
            }
            
            if let value  = data.pack_name as? String {
                cell.start_date.text = self.replaceString(value)
            }
            
            if let value  = data.remain_session as? String {
                cell.ticket_count.text = self.replaceString(value) + " جلسه"
            }
            
            cell.total_price.text = "رزرو پک"
            
            if let value  = data.status_text as? String {
                cell.status.setTitle(value, for: .normal)
                cell.status.setBackgroundColor(color: self.hexStringToUIColor(data.status_color!), forState: UIControl.State.normal)
                cell.status.layer.cornerRadius = 3
                cell.status.clipsToBounds = true
            }
            
            cell.show_specification.tag = indexPath.row
            cell.status.tag = indexPath.row
            cell.status.addTarget(self, action: #selector(showQr(button:)), for: .touchUpInside)
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        }
        
    
        return UITableViewCell()
    }
    
    @objc func showQr(button: UIButton){
    
        let data = DataSourceManagement.userPurchaseHistory[button.tag] 
        if let value  = data.voucher as? String {
            shareData.shared.selectedPurchaseQRString = value
            self.pushNewClass(classIdentifier: "PurchaseQRViewer")
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let lastElement = DataSourceManagement.userPurchaseHistory.count - 1
        if indexPath.row == lastElement, !refreshAllDataVariable {
            if lastPageLoaded < phPage {
                purchasesHistory(String(phPage))
            }
        }
    }
    
    func back(img: AnyObject)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func purchasesHistory(_ page:String) -> Void {
        self.refreshAllDataVariable = true
        if page == "1" {
            LoadingOverlayV2.sharedV2.showOverlayV2(view: self.view, message: "در حال دریافت اطلاعات...")
        }
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let limit  = "10"
        let index = wsPOST.init(Constants.PURCHASES_HISTORY + page + "/" + limit,"client_id=\(client_id)&access_token=\(access_token)&page=\(page)&limit=\(limit)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            do{
                
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    
                    
                    if let value = Int(page) as? Int{
                        self.lastPageLoaded = value
                    }
                    
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                if let purchases = item["purchases"] as? [[String: AnyObject]] {
                                    for item in purchases{
                                        if let itemValue = item as? [String: AnyObject]{
                                            if let value = itemValue["service_type"] as? String {
                                                
                                                switch value {
                                                case "reservation":
                                                DataSourceManagement.addPurchasesHistory(purchasesModel.prepareReservation(item as AnyObject))
                                                    break;
                                                case "course_reservation":
                                                   DataSourceManagement.addPurchasesHistory(purchasesModel.prepareCourse(item as AnyObject))
                                                    break;
                                                case "pack_reservation":
                                                  DataSourceManagement.addPurchasesHistory(purchasesModel.preparePack(item as AnyObject))
                                                    break;
                                                default:
                                                    self.printLog(m: "")
                                                }
                                                
                                            }
                                            
                                            //DataSourceManagement.addPurchasesHistory(purchasesModel.pre)
                                        }
                                    }
                                    
                                    if purchases.count == 10 {
                                        self.phPage += 1
                                    }
                                }
                            }
                        }
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                self.refreshAllDataVariable = false
                LoadingOverlayV2.sharedV2.hideOverlayView()
                if WSResponseStatus {
                   self.tableview.reloadData()
                }else{
                    
                }
            })
            
        }
        
        
    }
    
    func hexStringToUIColor (_ hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    @IBAction func checkSpecification(_ sender: Any){
        
        if let value = (sender as AnyObject).tag as? Int {
         
            shareData.shared.purchase_id = DataSourceManagement.userPurchaseHistory[value].purchase_history_id!
            openNewClass(classIdentifier: "ShowTransactionSpecificationVC")
        }
        
    }
}

extension UIButton {
    func setBackgroundColor(color: UIColor, forState: UIControl.State) {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()!.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        self.setBackgroundImage(colorImage, for: forState)
    }}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
