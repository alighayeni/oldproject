//
//  ReservationCustomCell4.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/9/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
class ReservationCustomCell4: UITableViewCell {
    
    @IBOutlet weak var discount: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var discountMessage: UILabel!
    @IBOutlet weak var sodeShoma: UILabel!
    
}
