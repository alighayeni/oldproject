//
//  packCartModel.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/12/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
class packCartModel: NSObject {
    
    var pack_id:String?
    var total_sale:String?
    var total_market:String?
    var coupon:String?
    var coupon_message:String?
    var coupon_credit:String?
    var discount_chosen:String?
   
    
    init(_ pack_id:String,_ total_sale:String, _ total_market:String, _ coupon:String, _ coupon_message:String, _ coupon_credit:String, _ discount_chosen:String) {
        
        self.pack_id = pack_id
        self.total_sale = total_sale
        self.total_market = total_market
        self.coupon = coupon
        self.coupon_message = coupon_message
        self.coupon_credit = coupon_credit
        self.discount_chosen = discount_chosen
        
    }
    
    static func prepare(_ item:AnyObject) -> packCartModel{
        
        let pack_id = String(describing: item["pack_id"] as AnyObject)
        let total_sale = String(describing: item["total_sale"] as AnyObject)
        let total_market = String(describing: item["total_market"] as AnyObject)
        let coupon = String(describing: item["coupon"] as AnyObject)
        let coupon_message = String(describing: item["coupon_message"] as AnyObject)
        let coupon_credit = String(describing: item["coupon_credit"] as AnyObject)
        let discount_chosen = String(describing: item["discount_chosen"] as AnyObject)
        
        let instance = packCartModel(pack_id, total_sale, total_market, coupon, coupon_message, coupon_credit, discount_chosen)
        return instance
    }
}
