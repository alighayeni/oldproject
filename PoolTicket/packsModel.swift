//
//  packsModel.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/4/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
class packsModel: NSObject {
    
    var type:String?
    var pack_id:String?
    var pack_name:String?
    var gender:String?
    var age_type:String?
    var session_count:String?
    var total_capacity:String?
    var remain_capacity:String?
    var pdescription:String?
    var price_sale:String?
    var price_market:String?
    var discount_percent:String?
    var status_sell:String?
    var session_expire_start:String?
    var session_expire_finish:String?
    var pattern_days:AnyObject?

    var week_day:String?
    var from:String?
    var to:String?
    var terms_of_use:String?
    
    init(_ type:String, _ pack_id:String,_ pack_name:String,_ gender:String,_ age_type:String,_ session_count:String,_ total_capacity:String,_ remain_capacity:String,_ description:String,_ price_sale:String,_ price_market:String,_ discount_percent:String,_ status_sell:String,_ session_expire_start:String, _ session_expire_finish:String, _ pattern_days: AnyObject, _ week_day: String, _ from: String, _ to: String, _ terms_of_use:String) {
       
        self.pack_id = pack_id
        self.pack_name = pack_name
        self.gender = gender
        self.age_type = age_type
        self.session_count = session_count
        self.total_capacity = total_capacity
        self.remain_capacity = remain_capacity
        self.pdescription = description
        self.price_sale = price_sale
        self.price_market = price_market
        self.discount_percent = discount_percent
        self.status_sell = status_sell
        self.session_expire_start = session_expire_start
        self.session_expire_finish = session_expire_finish
        self.pattern_days = pattern_days
        self.type = type
        self.week_day = week_day
        self.from = from
        self.to = to
        self.terms_of_use = terms_of_use

    }
    
    static func prepare(_ item:AnyObject, _ itemType: String) -> packsModel{
        let type = itemType
        let pack_id = String(describing: item["pack_id"] as AnyObject)
        let pack_name = String(describing: item["pack_name"] as AnyObject)
        let gender = String(describing: item["gender"] as AnyObject)
        let age_type = String(describing: item["age_type"] as AnyObject)
        let session_count = String(describing: item["session_count"] as AnyObject)
        let total_capacity = String(describing: item["total_capacity"] as AnyObject)
        let remain_capacity = String(describing: item["remain_capacity"] as AnyObject)
        let description = String(describing: item["description"] as AnyObject)
        let price_sale = String(describing: item["price_sale"] as AnyObject)
        let price_market = String(describing: item["price_market"] as AnyObject)
        let discount_percent = String(describing: item["discount_percent"] as AnyObject)
        var status_sell = "0"
        if let value = item["status_sell"] as? Bool {
            if value {
              status_sell = "1"
            }
        }        
        let session_expire_start = String(describing: item["session_expire_start"] as AnyObject)
        let session_expire_finish = String(describing: item["session_expire_finish"] as AnyObject)
        let pattern_days = item["pattern_days"] as AnyObject
        let terms_of_use = String(describing: item["terms_of_use"] as AnyObject)
        
        
        let week_day = "-1"
        let from = "-1"
        let to = "-1"

        let instance = packsModel(type ,pack_id, pack_name,gender,age_type,session_count,total_capacity,remain_capacity, description,price_sale, price_market,discount_percent,status_sell, session_expire_start, session_expire_finish, pattern_days, week_day, from, to, terms_of_use)
        return instance
    }
    
    static func preparePattern(_ item:AnyObject, _ type: String) -> packsModel{
        let type = type
        let pack_id = "-1"//String(describing: item["pack_id"] as AnyObject)
        let pack_name = "-1"//String(describing: item["pack_name"] as AnyObject)
        let gender = "-1"//String(describing: item["gender"] as AnyObject)
        let age_type = "-1"//String(describing: item["age_type"] as AnyObject)
        let session_count = "-1"//String(describing: item["session_count"] as AnyObject)
        let total_capacity = "-1"//String(describing: item["total_capacity"] as AnyObject)
        let remain_capacity = "-1"//String(describing: item["remain_capacity"] as AnyObject)
        let description = "-1"//String(describing: item["description"] as AnyObject)
        let price_sale = "-1"//String(describing: item["price_sale"] as AnyObject)
        let price_market = "-1"//String(describing: item["price_market"] as AnyObject)
        let discount_percent = "-1"//String(describing: item["discount_percent"] as AnyObject)
        let status_sell = "-1"//String(describing: item["status_sell"] as AnyObject)
        let session_expire_start = "-1"//String(describing: item["session_expire_start"] as AnyObject)
        let session_expire_finish = "-1"//String(describing: item["session_expire_finish"] as AnyObject)
        let pattern_days = "" as AnyObject//item["pattern_days"] as AnyObject
        let terms_of_use = ""//String(describing: item["terms_of_use"] as AnyObject)

        let week_day = String(describing: item["week_day"] as AnyObject)
        let from = String(describing: item["from"] as AnyObject)
        let to = String(describing: item["to"] as AnyObject)
        
        let instance = packsModel(type ,pack_id, pack_name,gender,age_type,session_count,total_capacity,remain_capacity, description,price_sale, price_market,discount_percent,status_sell, session_expire_start, session_expire_finish, pattern_days as AnyObject, week_day, from, to, terms_of_use)
        return instance
    }
}
