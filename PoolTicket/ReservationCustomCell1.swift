//
//  ReservationCustomCell1.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/9/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
class ReservationCustomCell1: UITableViewCell {
    
    
    
    @IBOutlet weak var time_ic: UIImageView!
    @IBOutlet weak var pool_name: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var fuldate: UILabel!

    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var endDate: UILabel!

    override func awakeFromNib() {
        
        if shareData.shared.selectedSaensRow != nil {
            self.time_ic.isHidden = false
        }else if shareData.shared.selectedPackRow != nil{
            self.time_ic.isHidden = true
        }else if shareData.shared.selectedCourseRow != nil{
            self.time_ic.isHidden = true
        }
        
    }


}
