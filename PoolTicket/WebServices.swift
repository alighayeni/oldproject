//
//  WebServices.swift
//  PoolTicket
//
//  Created by ali ghayeni hessaroeyeh on 31.10.19.
//  Copyright © 2019 OneSignal. All rights reserved.
//

import Foundation

class WebServices {
    
    static let shared = WebServices()
    
    enum HTTPMethod: String {
        case get = "GET"
        case post = "POST"
    }
    
    
    func getDataFor(url: String,httpMethod: HTTPMethod, postString: String? = nil,_ done : @escaping (_ status:Data?)->Void) -> Void {
        
        let urlString = url
        var request = URLRequest(url: URL(string:  urlString)!)
        request.setValue(wsGET.getPTrackingValue(), forHTTPHeaderField: "P-Tracking")
        request.addValue(wsGET.getPTrackingValue(), forHTTPHeaderField: "P-Tracking")
        request.httpBody = postString!.data(using: String.Encoding.utf8)
        
        let session = URLSession.shared
        session.configuration.timeoutIntervalForRequest = 30
        session.configuration.timeoutIntervalForResource = 30
        
        switch httpMethod {
        case .get:
            request.httpMethod = HTTPMethod.get.rawValue
        case .post:
            request.httpMethod = HTTPMethod.post.rawValue
        }
        
        session.dataTask(with: request) {data, response, err in
            if let httpResponse = response as? HTTPURLResponse {
                if let value = httpResponse.allHeaderFields as? [String: Any] {
                    if let P_Tracking = value["p-tracking"] as? String {
                        if P_Tracking.count > 0 {
                            self.setPTrackingValue(P_Tracking)
                        }
                    }
                }
            }
            
            if let resData = data {
                done(resData)
            }else{
                done(nil)
            }
        }.resume()
        
    }
    
    // setter and getter refresh token
    func setPTrackingValue(_ value: String){
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: UserDefaults.Keys.PTracking)
    }
    
    static func getPTrackingValue()->String{
        let defaults = UserDefaults.standard
        if let value = defaults.string(forKey: UserDefaults.Keys.PTracking) {
            return value
        }
        return ""
    }
}
