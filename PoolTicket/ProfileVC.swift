//
//  ProfileVC.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/28/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import SCLAlertView

class ProfileVC: UIViewController {
    
    @IBOutlet weak var name:UILabel!
    @IBOutlet weak var telephone:UILabel!
    @IBOutlet weak var amountOfMoney:UIButton!
    @IBOutlet weak var buttomView:UIView!
    @IBOutlet weak var profile:UIButton!
    @IBOutlet weak var logoutView:UIView!
    @IBOutlet weak var loginView:UIView!
    @IBOutlet weak var logoutImageView:UIImageView!
    @IBOutlet weak var customerScore:UILabel!
    @IBOutlet weak var historyCustomView: UIView!
    @IBOutlet weak var increaseMCustomView: UIView!
    @IBOutlet weak var editProfileCustomView: UIView!
    @IBOutlet weak var tellAFriendsCustomView: UIView!
    @IBOutlet weak var customerCareCustomView: UIView!

    var mName = ""
    var mTelephone = ""
    var mCredit = ""
    var mEmail = ""
    var points = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.navigationController?.navigationBar.barTintColor = self.setBgColor(43, green: 163, blue: 215)
        self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.font.rawValue: UIFont(name: "IRANSansMobile", size: 16),NSAttributedString.Key.foregroundColor.rawValue:UIColor.white])
        self.title = "پروفایل"
        
        let UIgrayColor = UIColor.init(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
        
        historyCustomView.layer.borderWidth = 1
        historyCustomView.layer.cornerRadius = 2
        historyCustomView.layer.borderColor = UIgrayColor.cgColor
        
        increaseMCustomView.layer.borderWidth = 1
        increaseMCustomView.layer.cornerRadius = 2
        increaseMCustomView.layer.borderColor =  UIgrayColor.cgColor
        
        editProfileCustomView.layer.borderWidth = 1
        editProfileCustomView.layer.cornerRadius = 2
        editProfileCustomView.layer.borderColor =  UIgrayColor.cgColor
        
        tellAFriendsCustomView.layer.borderWidth = 1
        tellAFriendsCustomView.layer.cornerRadius = 2
        tellAFriendsCustomView.layer.borderColor =  UIgrayColor.cgColor
        
        customerCareCustomView.layer.borderWidth = 1
        customerCareCustomView.layer.cornerRadius = 2
        customerCareCustomView.layer.borderColor =  UIgrayColor.cgColor
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkTheView()
    }
    
    func checkTheView() {
        if SwiftUtil.getUserAccessToken() == ""{
            self.loginView.alpha = 1
            //self.profile.setTitle("ورود به حساب کاربری", for: .normal)
            self.logoutView.isHidden = true
            self.logoutImageView.isHidden = true
            //self.buttomView.isHidden = true
            
        }else{
            self.loginView.alpha = 0
            //self.profile.setTitle("ویرایش پروفایل", for: .normal)
            self.getProfile()
            self.logoutView.isHidden = false
            self.logoutImageView.isHidden = false
            //self.buttomView.isHidden = false
            
        }
    }
    // get data from profile
    func getProfile() -> Void {
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.PROFILE,"client_id=\(client_id)&access_token=\(access_token)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                if let profile = item["profile"] as? [String: AnyObject] {
                                    if let value = profile["name"] as? String {
                                        self.mName = value
                                        UserDefaults.standard.set(value, forKey: UserDefaults.Keys.profileName)
                                    }
                                    if let value = profile["mobile"] as? String {
                                        self.mTelephone = value
                                        UserDefaults.standard.set(value, forKey: UserDefaults.Keys.profileMobile)
                                    }
                                    if let value = profile["email"] as? String {
                                        self.mEmail = value
                                        UserDefaults.standard.set(value, forKey: UserDefaults.Keys.profileEmail)
                                    }
                                    if let value = profile["credit"] as? Int {
                                        self.mCredit = String(value)
                                    }
                                    if let value = profile["points"] as? Int {
                                        self.points = String(value)
                                    }                                }
                    }
              }
           }
                    
                    
                }
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                
                if WSResponseStatus {

                    self.customerScore.text = "امتیاز: \(self.replaceString(self.points))"
                    self.name.text = self.mName
                    self.telephone.text = self.mTelephone
//                  self.amountOfMoney.setTitle(self.replaceString(self.mCredit), for: .normal)
                   
                    var mCreditNSNumber : NSNumber?
                    let someString = self.mCredit
                    if let myInteger = Int(someString) as? Int{
                        mCreditNSNumber = NSNumber(value:myInteger)
                    }
                    let formatter = NumberFormatter()
                    formatter.locale = Locale.current // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
                    formatter.numberStyle = .currency
                    if let value = mCreditNSNumber as? NSNumber {
                        if let formattedTipAmount = formatter.string(from: value as NSNumber) {
                            //tipAmountLabel.text = "Tip Amount: \(formattedTipAmount)"
                            var tString = formattedTipAmount.replacingOccurrences(of: ".00", with:"")
                            tString = "اعتبار: " + tString
                            tString = tString.replacingOccurrences(of: "$", with:"")
                            tString = tString.replacingOccurrences(of: "IRR", with:"")
                            tString = tString.replacingOccurrences(of: "irr", with:"")
                            tString = tString + " تومان "
                             self.amountOfMoney.setTitle(self.replaceString(tString), for: .normal)
                        }
                    }
                    
                    
 //                   self.buttomView.alpha = 1
                    
                }else{
//                    //try again
//                    Drop.down("username or password is wrong! please try again")
                }
            })
            
        }
    }
    
    
    func showLoginAlert(_ type: String) {
        
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "IRANSansMobile", size: 17)!,
            kTextFont: UIFont(name: "IRANSansMobile", size: 14)!,
            kButtonFont: UIFont(name: "IRANSansMobile", size: 14)!,
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        
        switch type {
        case "success":
            alertView.addButton("بستن") {
            }
            alertView.showSuccess("پیام:", subTitle: "با موفقیت از حساب کاربری خارج شده اید.")
            break
        default:
            print("")
        }
        
    }
    
    
    @IBAction func logout(){
        
        self.setUserRefreshToken("")
        self.setUserAccessToken("")
        // clear the username for Onsignal and Analytics Tag
        self.setUserName("")
        
        self.name.text = ""
        self.telephone.text = ""
       // self.profile.setTitle("ورود به حساب کاربری", for: .normal)
        self.amountOfMoney.setTitle("", for: .normal)
        // open the main tab
        self.showLoginAlert("success")
        self.tabBarController?.selectedIndex = 2//required value
        
        self.checkTheView()
    }
    
    @IBAction func openEditProfile(){
        
        if SwiftUtil.getUserAccessToken() == ""{
            openNewClass(classIdentifier: "CheckPhoneNumberRegistrationVC")
        }else{
        openNewClass(classIdentifier: "EditProfileVC")
        }
        
    }
    
    @IBAction func creditIncrease(){
        if SwiftUtil.getUserAccessToken() == ""{
            openNewClass(classIdentifier: "CheckPhoneNumberRegistrationVC")
        }else{
            openNewClass(classIdentifier: "StartCreditIncreaseVC")
        }
    }

    @IBAction func purchaseHistory(){
        
        if SwiftUtil.getUserAccessToken() == ""{
            openNewClass(classIdentifier: "CheckPhoneNumberRegistrationVC")
        }else{
            self.pushNewClass(classIdentifier: "PurchasesHistoryVC")
        }
        
    }
    
    @IBAction func recommendedFriend(){
        
        if SwiftUtil.getUserAccessToken() == ""{
            openNewClass(classIdentifier: "CheckPhoneNumberRegistrationVC")
        }else{
            self.pushNewClass(classIdentifier: "TellAFriendAndRecommender")
        }
        
    }
    
    @IBAction func login() {
        openNewClass(classIdentifier: "CheckPhoneNumberRegistrationVC")
    
    }
    
    @IBAction func checkCustomerCare() {
        self.showAlert()
    }
    
    func showAlert() {
        
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "IRANSansMobile", size: 17)!,
            kTextFont: UIFont(name: "IRANSansMobile", size: 14)!,
            kButtonFont: UIFont(name: "IRANSansMobile", size: 14)!,
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        alertView.addButton("بستن") {
        }
        alertView.showNotice(":اطلاعیه", subTitle: "این ویژگی به زودی فعال میشود.")
        
        
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
