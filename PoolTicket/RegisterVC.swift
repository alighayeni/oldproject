//
//  RegisterVC.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/13/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyDrop
import SCLAlertView
import FirebaseAnalytics


class RegisterVC: UIViewController, UITextFieldDelegate {
    
    var state_id : String = ""
    var city_id : String = ""
    
    @IBOutlet weak var nameAndLastName: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var chooseStateAndCity:UIButton!
    @IBAction func chooseStateAndCityAction(){
        
        openNewClass(classIdentifier: "ChooseStateAndCity")
    }
    
    @IBAction func registerNewAccount(){
        
        guard let nameAndLastName = nameAndLastName.text else{
            self.showDropMessage("name and last name is empty!")

            return
        }
        do {
            if nameAndLastName.count <= 2 {
                self.showAlert("NameAndLastNameValidation")
                return
            }
        }catch{
            fatalError("this is the error in register \(error)")
        }
        
        guard let email = email.text else{
            self.showDropMessage("email is empty!")

            return
        }
        do {
        if email.count > 0, !self.isValidEmail(email) {
            self.showAlert("EmailValidationFail")
            return
        }
        }catch{
            fatalError("this is the error in register \(error)")
        }
        
        guard let password = password.text else{
            self.showDropMessage("password is empty!")
            return
        }
        if password.count <= 5 {
            self.showAlert("PasswordValidationFail")
            return
        }
        
        guard  let registerToken = shareData.shared.register_token else {
            //self.showDropMessage("register token fail!")
            self.showAlert("RegisterTokenFail")
            return
        }
        
        if let selectedCity = shareData.shared.selectedBirthModel {

            if let value = selectedCity.state_id {
                self.state_id = value
            }

            if let value = selectedCity.city_id {
                self.city_id = value
            }

        }
//        else{
//            showAlert("CityAndStateNotSelected")
//            return
//        }
        
        if self.state_id.isEmpty || self.city_id.isEmpty{
            Analytics.logEvent("registerUser_defaultValue_cityState", parameters: ["cityName": self.selectedCityAndState?.city_name ?? "empty", "cityId": self.city_id, "stateName": self.selectedCityAndState?.state_name ?? "empty" , "stateId": self.state_id  ])
            self.state_id = "8"
            self.city_id = "117"
        }
//        if self.state_id != "" , self.city_id != "" {
        self.register(nameAndLastName, email, password, self.state_id, self.city_id, registerToken)
//        }else{
//            self.showAlert("cityFail")
//        }
        
    }

    var selectedStateId = ""
    var selectedCityId = ""
    var selectedCityAndState : cityModel?
    
    @IBOutlet weak var showAndHidePassword: UIButton!
    
    @IBAction func showAndHidePasswordAction(_ Sender: Any){
        
        if !password.isSecureTextEntry  {
            password.isSecureTextEntry = true
            self.showAndHidePassword.setImage(UIImage(named: "ic_pass_show")!, for: UIControl.State.normal)
        }else{
            password.isSecureTextEntry = false
            self.showAndHidePassword.setImage(UIImage(named: "ic_pass_hide")!, for: UIControl.State.normal)
            
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.font.rawValue: UIFont(name: "IRANSansMobile", size: 14),NSAttributedString.Key.foregroundColor.rawValue:UIColor.white])
        self.title = "عضویت"
        
        nameAndLastName.delegate = self
        nameAndLastName.tag = 1
        doneWithKeyboard(nameAndLastName)
        
        email.delegate = self
        email.tag = 2
        doneWithKeyboard(email)
        
        password.delegate = self
        password.tag = 3
        doneWithKeyboard(password)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
       setTheSelectedCityAndState()
    }
    
    func setTheSelectedCityAndState(){
        if shareData.shared.selectedBirthModel != nil {
        self.selectedCityAndState = shareData.shared.selectedBirthModel
            if let selectedCity = self.selectedCityAndState {
                if let value = selectedCity.state_id {
                    self.state_id = value
                }
                if let value = selectedCity.city_id {
                    self.city_id = value
                }
            }
        let tData = (self.selectedCityAndState?.city_name)! + " (" + (self.selectedCityAndState?.state_name)! + ")"
        if tData.count > 0 {
            chooseStateAndCity.setTitle(tData, for: .normal)
            chooseStateAndCity.setTitleColor(UIColor.darkGray, for: .normal)
        }
    
        }
    }
    
    func register(_ name: String, _ email: String, _ password: String, _ selectedState_id: String, _ selectedCity_id: String, _ register_token: String) -> Void {
        LoadingOverlayV2.sharedV2.showOverlayV2(view: self.view, message: "در حال ارسال اطلاعات")

        let client_id = Constants.clientId
        var WSResponseStatus = false

        let index = wsPOST.init(Constants.REGISTER,"client_id=\(client_id)&name=\(name)&email=\(email)&password=\(password)&state_id=\(selectedState_id)&city_id=\(selectedCity_id)&register_token=\(register_token)")
        index.start{ (indexData) in
            
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                if let value = item["access_token"] as? String {
                                    self.setUserAccessToken(value)
                                }

                                if let value = item["refresh_token"] as? String {
                                    self.setUserRefreshToken(value)
                                }

                            }
                        }
                    }
                    
                }
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                LoadingOverlayV2.sharedV2.hideOverlayView()
                if WSResponseStatus {
                   
                    // track search event in ios
//                    let event = ADJEvent.init(eventToken: "gzx99y")
//                    Adjust.trackEvent(event)
                    AppDelegate()._manager?.track("register", data: [:])
                    Analytics.logEvent("register", parameters: nil)
                    self.showAlert("success")
                    self.createMenuView()
                    
                }else{
                    //try again
                    self.showAlert("RegisterFail")
                }
            })
            
        }
        
        
    }
    
    
    //************** softkeyboard tool bar **********************//
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField
        {
        case nameAndLastName:
            nameAndLastName.resignFirstResponder()
            email.becomeFirstResponder()
            break
            
        case email:
            email.resignFirstResponder()
            password.becomeFirstResponder()
            break
            
        case password:
            password.resignFirstResponder()
            nameAndLastName.becomeFirstResponder()
            break
        
            
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
    func doneWithKeyboard(_ sender: UITextField){
        
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        keyboardToolbar.isTranslucent = true
        keyboardToolbar.barTintColor = UIColor.white
        
        let button = UIButton(type: .system)
        button.setTitle("بستن", for: .normal)
        button.sizeToFit()
        button.titleLabel?.font = UIFont(name: "IRANSansMobile", size: 13)
        button.tintColor = UIColor.black
        button.addTarget(self, action: #selector(closeKeyboard(_:)), for: .touchUpInside)
        
        
        let btnUp = UIButton(type: .custom)
        btnUp.setImage(UIImage(named: "ic_keyboard_arrow_up"), for: .normal)
        btnUp.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        btnUp.tag = sender.tag
        btnUp.addTarget(self, action: #selector(textFieldUp), for: .touchUpInside)
        let itemUp = UIBarButtonItem(customView: btnUp)
        
        let btnDown = UIButton(type: .custom)
        btnDown.setImage(UIImage(named: "ic_keyboard_arrow_down"), for: .normal)
        btnDown.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        btnDown.tag = sender.tag
        btnDown.addTarget(self, action: #selector(textFieldDown), for: .touchUpInside)
        let itemDown = UIBarButtonItem(customView: btnDown)
        
        
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        //keyboardToolbar.items = [flexBarButton,UIBarButtonItem(customView: button)]
        keyboardToolbar.items = [itemDown,itemUp,flexBarButton,UIBarButtonItem(customView: button)]
        
        if let value = sender as? UITextField {
            value.inputAccessoryView = keyboardToolbar
        }else if let value = sender as? UITextView {
            value.inputAccessoryView = keyboardToolbar
        }
        
    }
    
    @objc func textFieldUp(_ sender: Any)-> Void {
        
        let tObject = (sender as AnyObject).tag
        if let value = tObject as? Int {
            switch value
            {
            case 1:
                nameAndLastName.resignFirstResponder()
                email.becomeFirstResponder()
                break
                
            case 2:
                email.resignFirstResponder()
                password.becomeFirstResponder()
                break
                
            case 3:
                password.resignFirstResponder()
                nameAndLastName.becomeFirstResponder()
                break
                
            default:
                print("")
            }
        }
    }
    
    @objc func textFieldDown(_ sender: Any)-> Void {
        
        let tObject = (sender as AnyObject).tag
        if let value = tObject as? Int {
            switch value
            {
            case 1:
                nameAndLastName.resignFirstResponder()
                email.becomeFirstResponder()
                break
                
            case 2:
                email.resignFirstResponder()
                password.becomeFirstResponder()
                break
                
            case 3:
                password.resignFirstResponder()
                nameAndLastName.becomeFirstResponder()
                break
                
            default:
                print("")
            }
        }
    }
    
    
    //************** softkeyboard tool bar **********************//
    
    fileprivate func createMenuView() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "TabBarMainVC") as! TabBarMainVC
        
        let rightViewController = storyboard.instantiateViewController(withIdentifier: "rightView") as! RightDrawerVC
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        UINavigationBar.appearance().tintColor = UIColor.brown
        
        
        let slideMenuController = SlideMenuController(mainViewController: mainViewController, rightMenuViewController: rightViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        
        self.present(slideMenuController, animated: false, completion: nil)
    }
    
    //*****
    
    func showAlert(_ type: String) {
        
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "IRANSansMobile", size: 17)!,
            kTextFont: UIFont(name: "IRANSansMobile", size: 14)!,
            kButtonFont: UIFont(name: "IRANSansMobile", size: 14)!,
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        
        switch type {
        case "success":
            alertView.addButton("بستن") {
            }
            alertView.showSuccess("به پول تیکت خوش آمدید", subTitle: "ثبت نام موفقیت آمیز بود.")
            break
        case "cityFail":
            alertView.addButton("بستن") {
            }
            alertView.showError("اخطار:", subTitle: "لطفا شهر محل سکونت خود را انتخاب کنید")
            break
        case "RegisterFail":
            alertView.addButton("بستن") {
            }
            alertView.showError("اخطار:", subTitle: "ثبت نام شما با خطا مواجه شد. لطفا فیلد های ثبت نام را بررسی کرده و مجددا تلاش کنید.")
            break
        case "RegisterTokenFail":
            alertView.addButton("بستن") {
            }
            alertView.showError("اخطار:", subTitle: "خطا در ثبت نام. لطفا مجدد ثبت نام را انجام دهید.")
            break
        case "EmailValidationFail":
            alertView.addButton("بستن") {
            }
            alertView.showError("اخطار:", subTitle: "ایمیل وارد شده صحیح نمی باشد، لطفا آن را بررسی کنید.")
            break
        case "NameAndLastNameValidation":
            alertView.addButton("بستن") {
            }
            alertView.showError("اخطار:", subTitle: "فیلد نام و نام خانوادگی اشتباه است. (حد اقل ۴ کاراکتر)")
            break
        case "CityAndStateNotSelected":
            alertView.addButton("بستن") {
            }
            alertView.showError("اخطار:", subTitle: "لطفا شهر و استان محل سکونت خود را انتخاب کنید.")
            break
        case "PasswordValidationFail":
            alertView.addButton("بستن") {
            }
            alertView.showError("اخطار:", subTitle: "حد اقل تعداد کاراکتر مجاز برای رمز عبور ۶ کاراکتر می باشد.")
            break
        default:
            print("")
        }
        
    }
    
    func isValidEmail(_ testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
   
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
