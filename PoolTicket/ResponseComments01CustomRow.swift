//
//  ResponseComments01CustomRow.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/18/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit

class ResponseComments01CustomRow: UITableViewCell {
    
    
    @IBOutlet weak var content: UITextView!
    var delegate: UITableViewCellUpdateDelegate?
    
    override func awakeFromNib() {
         super.awakeFromNib()
        content.target(forAction: #selector(didCHangeTextFieldValue), withSender: self)
//            .addTarget(self, action: Selector("didCHangeTextFieldValue:"), forControlEvents: UIControlEvents.EditingChanged)
    }
    
    @objc func didCHangeTextFieldValue(sender: AnyObject?) {
        self.delegate?.cellDidChangeValue(cell: self)
    }
    
    
}


