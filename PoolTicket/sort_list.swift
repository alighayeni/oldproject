//
//  sort_list.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 3/20/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation

class sort_list: NSObject {
    
    var title:String?
    var code:String?

    init(_ title:String,_ code:String) {
        self.title = title
        self.code = code
    }
    
    static func prepare(_ item:AnyObject) -> sort_list{
        let title = String(describing: item["title"] as AnyObject)
        let code = String(describing: item["code"] as AnyObject)
        let instance = sort_list(title, code)
        return instance
    }
}
