//
//  TabBarMain.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/11/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import OneSignal

class TabBarMainVC: UITabBarController {
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
      
        self.setTabBarfont()
       
        if let slideMenuController = self.slideMenuController() {
            self.slideMenuController()?.closeLeft()        }
        
        // open the main tab
        self.tabBarController?.selectedIndex = 2//required value
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedIndex = 4
        
        self.setNavigationBarItem()
    }
    
    func UIColorMake(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat)->UIColor{
        return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: 1.0)
    }
    
    
    
    func setTabBarfont(){
        
        //tab bar font and color!
        let colorNormal : UIColor = UIColor.lightGray
        let colorSelected : UIColor = UIColorMake(32, 161, 218)//setBgColor(34,green: 192, blue:100)
        let titleFontAll : UIFont = UIFont(name: "IRANSansMobile", size: 11.0)!
        
        let attributesNormal = [
            convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : colorNormal,
            convertFromNSAttributedStringKey(NSAttributedString.Key.font) : titleFontAll
        ]
        
        let attributesSelected = [
            convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : colorSelected,
            convertFromNSAttributedStringKey(NSAttributedString.Key.font) : titleFontAll
        ]
        
        
        // UITabBarItem.appearance()
        UITabBarItem.appearance().setTitleTextAttributes(convertToOptionalNSAttributedStringKeyDictionary(attributesNormal), for: UIControl.State())
        UITabBarItem.appearance().setTitleTextAttributes(convertToOptionalNSAttributedStringKeyDictionary(attributesSelected), for: .selected)
        
    }
    
}

extension TabBarMainVC : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("mainView: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("mainView: leftDidOpen")
    }
    
    func leftWillClose() {
        print("mainView: leftWillClose")
    }
    
    func leftDidClose() {
        print("mainView: leftDidClose")
    }
    
    func rightWillOpen() {
        print("mainView: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("mainView: rightDidOpen")
    }
    
    func rightWillClose() {
        print("mainView: rightWillClose")
    }
    
    func rightDidClose() {
        print("mainView: rightDidClose")
        setAction()
    }
    
    class TabBarMain: SlideMenuController {
        
        override func awakeFromNib() {
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "TabBarMainVC") as? UITabBarController{
                self.mainViewController = controller
            }
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "leftView") {
                self.leftViewController = controller
            }
            super.awakeFromNib()
        }
        
    }
    
    func setAction(){
        
        switch DataSourceManagement.selectedDrawerState {
        case "0":
            self.selectedIndex = 0
            DataSourceManagement.addSelectedDrawerState("-1")
            break
        case "1":
            self.selectedIndex = 1
            DataSourceManagement.addSelectedDrawerState("-1")
            break
        case "2":
            self.selectedIndex = 2
            DataSourceManagement.addSelectedDrawerState("-1")
            break
        default:
            printLog(m: "state Zero")
        }
        
        
    }
    
    
    
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
