//
//  BlogPostModel.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/20/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit

class BlogPostModel: NSObject {
    
    var id:String?
    var date:String?
    var title:String?
    var link:String?
    var content:String?

    init(_ id:String,_ date:String, _ title:String, _ link:String, _ content:String) {
        self.id = id
        self.date = date
        self.title = title
        self.link = link
        self.content = content
    }
    
    static func prepare(_ item:AnyObject) -> BlogPostModel{
        
        let id = String(describing: item["id"] as AnyObject)
        let date = String(describing: item["modified"] as AnyObject)
        let link = String(describing: item["link"] as AnyObject)
        
        var title = ""
        if let value = item["title"] as? [String: AnyObject] {
            if let rendered = value["rendered"] as? String {
              title = rendered
            }
        }
        
        var content = ""
        if let excerpt = item["excerpt"] as? [String: AnyObject] {
            if let rendered = excerpt["rendered"] as? String {
                content = rendered
                content = content.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
            }
        }

        let instance = BlogPostModel(id, date, title, link, content)
        return instance
    }
    
    
    
}
