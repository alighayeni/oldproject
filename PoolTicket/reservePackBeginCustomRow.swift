//
//  reservePackBeginCustomRow.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/5/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
class reservePackBeginCustomRow: UITableViewCell {
    
    @IBOutlet weak var start: UILabel!
    @IBOutlet weak var end: UILabel!
    @IBOutlet weak var descriptionTitle: UILabel!
    @IBOutlet weak var descriptionTitleHeight: NSLayoutConstraint!
    @IBOutlet weak var descriptionContent: UILabel!
    @IBOutlet weak var descriptionContentHeight: NSLayoutConstraint!
    @IBOutlet weak var availableForWomen: UIImageView!
    @IBOutlet weak var availableForMan: UIImageView!
    @IBOutlet weak var sessionCount: UILabel!
    @IBOutlet weak var sessionCountHeight: NSLayoutConstraint!

}

