//
//  DrawerComponent.swift
//  EnergyPlus
//
//  Created by Ali Ghayeni on 9/11/17.
//  Copyright © 2017 GreenHost. All rights reserved.
//

import Foundation

extension ViewController : SlideMenuControllerDelegate {
    
  
    func leftWillOpen() {
        print("mainView: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("mainView: leftDidOpen")
    }
    
    func leftWillClose() {
        print("mainView: leftWillClose")
    }
    
    func leftDidClose() {
        print("mainView: leftDidClose")
    }
    
    func rightWillOpen() {
        print("mainView: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("mainView: rightDidOpen")
    }
    
    func rightWillClose() {
        print("mainView: rightWillClose")
    }
    
    func rightDidClose() {
        print("mainView: rightDidClose")
    }
    
    class ViewController: SlideMenuController {
        
        override func awakeFromNib() {
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "TabBarMainVC") {
                self.mainViewController = controller
            }
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "rightView") {
                self.rightViewController = controller
            }
            super.awakeFromNib()
        }
        
    }
 
    
    //    override func view(_ animated: Bool) {
    //
    //        func leftDidOpen() {
    //            self.slideMenuController()?.closeLeft()
    //        }
    //    }
    
}
