//
//  ExpandableObject.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/4/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation

struct ExpandableObjectPacks {
    
    var isExpanded : Bool
    let packObjects: [packsModel]
    
}

struct ExpandableObjectSaens {
    
    var isExpanded : Bool
    let seansObjects: [saensModel]
    
}

struct ExpandableObjectCourse {
    
    var isExpanded : Bool
    let courseObjects: [courseModel]
    
}
