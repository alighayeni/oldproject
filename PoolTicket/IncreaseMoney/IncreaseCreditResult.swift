//
//  IncreaseCreditResult.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 5/30/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class IncreaseCreditResult: UIViewController  {
    
    @IBOutlet weak var imgResult: UIImageView!
    @IBOutlet weak var resultTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        if let value = shareData.shared.IncreaseCreditResult_Status as? String {
            if value == "0" {
                self.resultTitle?.text = "افزایش موجودی با خطا مواجه شد ."
                self.imgResult.image = UIImage.init(named: "ic_payment_fail_red")
            }else{
                AppDelegate()._manager?.track("create_increse_payment_success", data: [:])
                Analytics.logEvent("create_increse_payment_success", parameters: nil)
                self.resultTitle?.text = "افزایش اعتبار با موفقیت انجام شد." + "\n" + replaceString(shareData.shared.IncreaseCreditResult_Credit!) + " تومان "
                self.imgResult.image = UIImage.init(named: "ic_payment_confirm_green")
            }
        }
        
        
    }
    
    @IBAction func closeView(){
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
}
