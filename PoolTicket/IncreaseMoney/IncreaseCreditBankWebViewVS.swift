//
//  IncreaseCreditBankWebViewVS.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 3/12/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class IncreaseCreditBankWebViewVS: UIViewController, WKNavigationDelegate {
    
    
    @IBOutlet weak var contentView: UIView!
    var webView: WKWebView!
    var urlObservation: NSKeyValueObservation?
    var status = ""
    var credit = ""
    override func viewDidLoad() {
    super.viewDidLoad()

        shareData.shared.IncreaseCreditResult_Status = nil
        shareData.shared.IncreaseCreditResult_Credit = nil
        
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        let configuration = WKWebViewConfiguration()
        configuration.preferences = preferences
        
        self.webView = WKWebView(frame: self.contentView.bounds, configuration: configuration)
//        self.webView = WKWebView(frame: self.contentView.bounds, configuration: WKWebViewConfiguration())
        self.webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.webView.navigationDelegate = self
        self.contentView.addSubview(self.webView)
        
        
        printLog(m: DataSourceManagement.increaseUrl)
        let url = URL(string: DataSourceManagement.increaseUrl)!
  
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
        // Add observation.
        urlObservation = webView.observe(\.url, changeHandler: { (webView, change) in
            
            print("Web view URL changed to \(webView.url?.absoluteString ?? "Empty")")
           
            var tData = webView.url?.absoluteString ?? "Empty"
            if tData.contains("org.poolticket://credit-increase?") {
                tData = tData.replacingOccurrences(of: "org.poolticket://credit-increase?", with: "")
                var tData2 = tData.split(separator: "&")
                for item in tData2 {
                    var tData3 = item.split(separator: "=")
                    switch tData3[0] {
                    case "status":
                        self.status = String(tData3[1])
                        if self.status == "1" {
                            // track Credit Increase payment success event in ios
//                            let event = ADJEvent.init(eventToken: "cky7ck")
//                            Adjust.trackEvent(event)
                        }
                        break;
                    case "credit":
                        self.credit = String(tData3[1])
                        break;
                    default:
                        print("")
                    }
                }
                
                if self.status != "" {
                shareData.shared.IncreaseCreditResult_Status = self.status
                shareData.shared.IncreaseCreditResult_Credit = self.credit
                shareData.shared.IncreaseCreditResult_setColse = true
                self.openNewClass(classIdentifier: "IncreaseCreditResult")
                }
            }
        })
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.navigationDelegate), options: .new, context: nil)
      
            
     
    }
    
    @IBAction func close(){
        self.dismiss(animated: true, completion: nil)
    }
    

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if self.webView != nil {
        self.webView.removeObserver(self, forKeyPath: #keyPath(WKWebView.navigationDelegate))
        self.webView = nil
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let value = shareData.shared.IncreaseCreditResult_setColse as? Bool {
            if value {
            shareData.shared.IncreaseCreditResult_setColse = nil
            self.dismiss(animated: true, completion: nil)
            }
        }
        
    }

//    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Swift.Void)
//    {
//        print(webView.url?.absoluteString ?? "Empty")
//        if(navigationAction.navigationType == .formSubmitted)
//        {
//
//        }
//        decisionHandler(.allow)
//    }
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Swift.Void)
    {
        print(webView.url?.absoluteURL ?? "Empty")
        switch navigationAction.navigationType {
        case .formSubmitted:
            print("")
        case  .linkActivated:
            print(navigationAction.request.description)
            if let value = navigationAction.request.description as? String {
                if value.contains("jibit://pay.jibit.mobi") {
                    if self.openCustomURLScheme(customURLScheme: value) {
                        // app was opened successfully
                        print("success")
                        self.dismiss(animated: true, completion: nil)
                    } else {
                        // handle unable to open the app, perhaps redirect to the App Store
                        print("fail")
                    }
                }
            }
            print("")
        default:
            print("")
        }
        
        
        decisionHandler(.allow)
    }
    
}
