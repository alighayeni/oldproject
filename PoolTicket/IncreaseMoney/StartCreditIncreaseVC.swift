//
//  StartCreditIncreaseVC.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 3/12/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyDrop
import SCLAlertView


class StartCreditIncreaseVC: UIViewController, UITextFieldDelegate {
    
    var startReservationStatus = false

    
    @IBOutlet weak var amount:UITextField!
    @IBAction func startIncreaseAmount(_ sender:Any){
        
        guard let price = amount.text else{
            printLog(m: "")
            return
        }
        
        if let integerPrice = Int64(price) as? Int64 {
            if integerPrice > 500, !startReservationStatus {
                setIncreasePrice(price)
            }else{
               showAlert("failLessThan500")
            }
        }
        
        
    }

    //CREDIT_INCREASE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        amount.delegate = self
        doneWithKeyboard(amount)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    @IBAction func close(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func setIncreasePrice( _ price: String) -> Void {
        startReservationStatus = true
        LoadingOverlayV2.sharedV2.showOverlayV2(view: self.view, message: "درخواست افزایش اعتبار")
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.CREDIT_INCREASE,"client_id=\(client_id)&price=\(price)&access_token=\(access_token)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            var bURL : String?
            
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                if let value = item["url"] as? String {
                                    DataSourceManagement.addTheIncreaseURL(value)
                                    bURL = value
                                }
                                
                            }
                        }
                    }
                    
                }
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                self.startReservationStatus = false
                LoadingOverlayV2.sharedV2.hideOverlayView()
                if WSResponseStatus {
                    if bURL != nil {
                        self.openNewClass(classIdentifier: "IncreaseCreditBankWebViewVS")
                    }
                }else{
                    self.showAlert("fail")
                }
            })
            
        }
        
        
    }
    //************** softkeyboard tool bar **********************//
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField
        {
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
    func doneWithKeyboard(_ sender: UITextField){
        
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        keyboardToolbar.isTranslucent = true
        keyboardToolbar.barTintColor = UIColor.white
        
        let button = UIButton(type: .system)
        button.setTitle("بستن", for: .normal)
        button.sizeToFit()
        button.titleLabel?.font = UIFont(name: "IRANSansMobile", size: 12)
        button.tintColor = UIColor.black
        button.addTarget(self, action: #selector(closeKeyboard(_:)), for: .touchUpInside)
    
        
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        keyboardToolbar.items = [flexBarButton,UIBarButtonItem(customView: button)]
        //keyboardToolbar.items = [itemDown,itemUp,flexBarButton,UIBarButtonItem(customView: button)]
        
        if let value = sender as? UITextField {
            value.inputAccessoryView = keyboardToolbar
        }else if let value = sender as? UITextView {
            value.inputAccessoryView = keyboardToolbar
        }
        
    }
    
    //************** softkeyboard tool bar **********************//

    func showAlert(_ type: String) {
        
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "IRANSansMobile", size: 17)!,
            kTextFont: UIFont(name: "IRANSansMobile", size: 14)!,
            kButtonFont: UIFont(name: "IRANSansMobile", size: 14)!,
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        
        switch type {
        case "failLessThan500":
            alertView.addButton("بستن") {
            }
            alertView.showError("اخطار:", subTitle: "مبلغ افزایش اعتبار وارد شده از حد مجاز پایین تر می باشد. لطفا مجددا این مورد را بررسی کنید.")
            break
        case "fail":
            alertView.addButton("بستن") {
            }
            alertView.showError("اخطار:", subTitle: "درخواست شما با خطا مواجه شد، لطفا مجددا تلاش کنید .")
            break
        default:
            print("")
        }
        
    }
    
}
