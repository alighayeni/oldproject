//
//  newtestviecontroller.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/18/19.
//  Copyright © 2019 OneSignal. All rights reserved.
//

import Foundation
import UIKit

class ServiceDescriptionController: UIViewController {
    
    @IBOutlet weak var labeltext:UILabel!
    @IBOutlet weak var contentText:UITextView!
    @IBOutlet weak var topImage: UIImageView!
    @IBOutlet weak var topImageIcon: UIImageView!
    static var serviceType: String = "ticket"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = self.setBgColor(43, green: 163, blue: 215)
        self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.font.rawValue: UIFont(name: "IRANSansMobile", size: 16),NSAttributedString.Key.foregroundColor.rawValue:UIColor.white])
        self.title = ""
        
        switch ServiceDescriptionController.serviceType {
        case "ticket":
            self.labeltext.text = "رزرو بلیت؟"
            self.title = "رزرو بلیت"
            self.topImage.image = UIImage.init(named: "service_bilit_top")
            self.topImageIcon.image = UIImage.init(named: "ic_ticket_white")
            self.getServicesDescription("ticket")
        case "course":
            self.labeltext.text = "کلاس شنا؟"
            self.title = "کلاس شنا"
            self.topImage.image = UIImage.init(named: "service_class_top")
            self.topImageIcon.image = UIImage.init(named: "ic_class_white")
            self.getServicesDescription("course")
        case "pack":
            self.labeltext.text = "پک؟"
            self.title = "پک"
            self.topImage.image = UIImage.init(named: "service_pack_top")
            self.topImageIcon.image = UIImage.init(named: "Ic_pack_white")
            self.getServicesDescription("pack")
        default:
            break
        }
        
    }
    
    func getServicesDescription(_ service:String) -> Void {
        let client_id = Constants.clientId
        var WSResponseStatus = false
        var serviceContent = ""
        //let access_token = SwiftUtil.getUserAccessToken()
        //let limit  = "10"
        let index = wsGET.init(Constants.SERVICE_DESCRIPTION + "?client_id=\(client_id)&service=\(service)","")
        index.start{ (indexData) in
            
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    if let item = json as? [String: AnyObject] {
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                if let value = item["content"] as? String {
                                    serviceContent = value
                                }
                            }
                        }
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {

                if WSResponseStatus {
                    self.contentText.text = serviceContent
                }
            })
            
        }
        
        
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
