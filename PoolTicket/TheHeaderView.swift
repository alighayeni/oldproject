//
//  TheHeaderView.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/5/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit

class TheHeaderView: UIView {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var gender: UIImageView!
    @IBOutlet weak var arrow: UIImageView!
    @IBOutlet weak var collapseViewHeader: UIButton!
    @IBOutlet weak var theTitle: UILabel!
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    class func instanceFromNib() -> TheHeaderView {
        return UINib(nibName: "TheHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! TheHeaderView
    }
}

