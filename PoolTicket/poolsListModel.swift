//
//  poolsListModel.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 3/1/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation

class poolsListModel: NSObject {
    
    var pool_id:String?
    var name:String?
    var max_discount:String?
    var default_image:String?
    var area_name: String?
    var gender: String?
    var rating: String?
    var lat: String?
    var lng: String?
    var status_sell: String?
    var status_sell_course: String?
    var status_sell_pack: String?
    var minimum_sale_price: String?
    var minimum_market_price: String?
    var city_name: String?


    
    init(pool_id:String, name:String, max_discount:String, default_image:String, area_name:String, gender:String, rating:String, lat:String, lng:String, status_sell:String, status_sell_course:String, status_sell_pack:String, minimum_sale_price: String, minimum_market_price:String, city_name:String) {
        self.pool_id=pool_id
        self.name=name
        self.max_discount=max_discount
        self.default_image=default_image
        self.area_name=area_name
        self.gender=gender
        self.rating=rating
        self.lat=lat
        self.lng=lng
        self.status_sell=status_sell
        self.status_sell_course=status_sell_course
        self.status_sell_pack=status_sell_pack
        self.minimum_sale_price=minimum_sale_price
        self.minimum_market_price=minimum_market_price
        self.city_name=city_name
    }
    
    static func prepare(_ item:AnyObject) -> poolsListModel{
        
        let pool_id = String(describing: item["pool_id"] as AnyObject)
        let name = String(describing: item["name"] as AnyObject)
        var max_discount = String(describing: item["max_discount"] as AnyObject)
        if let value = Float(max_discount) as? Float {
            if let intValue = Int(value) as? Int {
                max_discount = String(describing: intValue as AnyObject)
            }
        }
        let default_image = String(describing: item["default_image"] as AnyObject)
        let area_name = String(describing: item["area_name"] as AnyObject)
        let city_name = String(describing: item["city_name"] as AnyObject)
        let gender = String(describing: item["gender"] as AnyObject)
        let rating = String(describing: item["rating"] as AnyObject)
        let lat = String(describing: item["lat"] as AnyObject)
        let lng = String(describing: item["lng"] as AnyObject)
        let status_sell = String(describing: item["status_sell"] as AnyObject)
        let status_sell_course = String(describing: item["status_sell_course"] as AnyObject)
        let status_sell_pack = String(describing: item["status_sell_pack"] as AnyObject)
        let minimum_sale_price = String(describing: item["minimum_sale_price"] as AnyObject)
        let minimum_market_price = String(describing: item["minimum_market_price"] as AnyObject)
        
        let instance = poolsListModel(pool_id:pool_id, name:name, max_discount:max_discount, default_image:default_image, area_name:area_name,gender:gender,rating:rating, lat:lat, lng:lng, status_sell:status_sell, status_sell_course:status_sell_course, status_sell_pack:status_sell_pack,minimum_sale_price:minimum_sale_price,minimum_market_price:minimum_market_price, city_name:city_name)
        
        return instance
    }
    
    
    
}
