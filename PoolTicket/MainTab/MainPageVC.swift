//
//  mainPageVC.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/21/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import OneSignal
import SCLAlertView
import ImageSlideshow

class MainPageVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate, CLLocationManagerDelegate {
  
    
    @IBOutlet weak var tableView: UITableView!
    
    var deviceTypeNumber = 0 // zero means iphone 6 or 6s or 7 or 8
    var locationManager: CLLocationManager = CLLocationManager()
    let scrollView = UIScrollView()
    var poolList = [poolsModel]()
    var latestLat = ""
    var latestLng = ""
    var selectedGroupId = "1"
    var selectedGropItemsList: [NSObject] = []
    var selectedCityAndState : cityModel?
    var GET_INDEX_DATA_STATUS = false
    var groupId01 = "";
    var groupIdTitle01 = "";
    var gropItemsList01: [NSObject] = []
    var indexList: [[AnyHashable: Any]] = []
    
    var topBannerWidth = 320
    var topBannerHeight = 170

    
    var groupId02 = "";
    var groupIdTitle02 = "";
    var gropItemsList02: [NSObject] = []

    var groupId03 = "";
    var groupIdTitle03 = "";
    var gropItemsList03: [NSObject] = []

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.deviceType()
        
       
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 35))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "ic_main_logo")
        imageView.image = image
        
        self.navigationItem.titleView = imageView
        
        //supportIconView.
        let support = UIBarButtonItem(image: UIImage(named: "ic_support_white"),style: .plain ,target: self, action: #selector(self.support))
        self.navigationItem.rightBarButtonItems = [support]
        
        let search = UIBarButtonItem(image: UIImage(named: "ic_search_white"),style: .plain ,target: self, action: #selector(self.search))
        self.navigationItem.leftBarButtonItems = [search]

        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.register(UINib.init(nibName: "PTCollectionViewTableViewCell", bundle: nil), forCellReuseIdentifier: "PTCollectionViewTableViewCell")
        self.tableView.register(UINib.init(nibName: "PTShowPoolsCell", bundle: nil), forCellReuseIdentifier: "PTShowPoolsCell")
        self.tableView.register(UINib.init(nibName: "PTTopBannerTableViewCell", bundle: nil), forCellReuseIdentifier: "PTTopBannerTableViewCell")


        getIndex();

    }
    
    
    @objc
    func search() {
        DataSourceManagement.setOpenNearLocationPool(false)
        self.pushNewClass(classIdentifier: "PoolsListVC")
    }
    
    @objc
    func support() {
        self.pushNewClass(classIdentifier: "SupportVC")
    }
    
    func getIndex(){
        if let value = Network.reachability?.isConnectedToNetwork {
            if value {
                getIndexDataV2()
            }else{
                showAlert(.internetNotAvailable)
            }
        }else{
            showAlert(.internetNotAvailable)
        }
    }
    
    func getIndexDataV2() { // V2.1
        self.GET_INDEX_DATA_STATUS = true
        LoadingOverlayV2.sharedV2.showOverlayV2(view: self.view, message: "در حال دریافت اطلاعات...")
        let client_id = Constants.clientId
        let pool_image_width = "900"// String(describing: self.bannerView.frame.width)
        var WSResponseStatus = false
        let access_token = SwiftUtil.getUserAccessToken()
        WebServices.shared.getDataFor(url: Constants.INDEXV2_1, httpMethod: WebServices.HTTPMethod.post, postString: "client_id=\(client_id)&pool_image_width=\(pool_image_width)&access_token=\(access_token)") { (indexData) in
            
//        }
//        let index = wsPOST.init(Constants.INDEXV2_1,"client_id=\(client_id)&pool_image_width=\(pool_image_width)&access_token=\(access_token)")
//        index.start{ (indexData) in
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    debugPrint(json)
                    if let item = json as? [String: AnyObject]{
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                            }
                        }
                        if let groups = item["groups"] as? [[String: AnyObject]] {
                            for group in groups {
                                if let type = group["type"] as? String {
                                    switch type {
                                    case "top-banner":
                                        if let slides = group["slides"] as? [[String: AnyObject]] {
                                            var objectList : [top‌‌Banner] = []
                                            for slide in slides {
                                                objectList.append(top‌‌Banner.prepare(slide as AnyObject))
                                            }
                                            guard let title = group["title"] else {
                                                return
                                            }
                                            if slides.count > 0 {
                                                var object: [AnyHashable: Any] = [:]
                                                object["title"] = title
                                                object["type"] = "top-banner"
                                                object["list"] = objectList
                                                self.indexList.append(object)
                                            }
                                            
                                        }
                                        
                                        // show custom cell with btn
                                        var object: [AnyHashable: Any] = [:]
                                        object["title"] = nil
                                        object["type"] = "show-pool-cell"
                                        object["list"] = nil
                                        self.indexList.append(object)
                                        
                                        
                                    case "banner":
                                        if let slides = group["slides"] as? [[String: AnyObject]] {
                                            var objectList : [top‌‌Banner] = []
                                            for slide in slides {
                                                objectList.append(top‌‌Banner.prepare(slide as AnyObject))
                                            }
                                            guard let title = group["title"] else {
                                                return
                                            }
                                            if slides.count > 0 {
                                                var object: [AnyHashable: Any] = [:]
                                                object["title"] = title
                                                object["type"] = "banner"
                                                object["list"] = objectList
                                                self.indexList.append(object)
                                            }
                                        }
                                        break
                                    case "pool":
                                        if let slides = group["slides"] as? [[String: AnyObject]] {
                                            var objectList : [poolsById] = []
                                            for slide in slides {
                                                objectList.append(poolsById.prepare(slide as AnyObject))
                                            }
                                            guard let title = group["title"] else {
                                                return
                                            }
                                            if slides.count > 0 {
                                                var object: [AnyHashable: Any] = [:]
                                                object["title"] = title
                                                object["type"] = "pool"
                                                object["list"] = objectList
                                                self.indexList.append(object)
                                            }
                                        }
                                        break
                                    case "service":
                                        if let slides = group["slides"] as? [[String: AnyObject]] {
                                            var objectList : [serivce] = []
                                            for slide in slides {
                                                objectList.append(serivce.prepare(slide as AnyObject))
                                            }
                                            guard let title = group["title"]  else {
                                                return
                                            }
                                            if slides.count > 0 {
                                                var object: [AnyHashable: Any] = [:]
                                                object["title"] = title
                                                object["type"] = "service"
                                                object["list"] = objectList
                                                self.indexList.append(object)
                                            }
                                        }
                                        break
                                    case "url":
                                        if let slides = group["slides"] as? [[String: AnyObject]] {
                                            var objectList : [urlObject] = []
                                            for slide in slides {
                                                objectList.append(urlObject.prepare(slide as AnyObject))
                                            }
                                            guard let title = group["title"] else {
                                                return
                                            }
                                            if slides.count > 0 {
                                                var object: [AnyHashable: Any] = [:]
                                                object["title"] = title
                                                object["type"] = "url"
                                                object["list"] = objectList
                                                self.indexList.append(object)
                                            }
                                        }
                                        break
                                        
                                    default:
                                        self.printLog(m: "")
                                    }
                                }
                            }
                        }
                    }
                }
            } catch {
                print("error")
            }
            
            DispatchQueue.main.async(execute: {
                
                if WSResponseStatus {
                    self.GET_INDEX_DATA_STATUS = false
                    LoadingOverlayV2.sharedV2.hideOverlayView()
                    self.tableView.reloadData()
                } else {
                    self.showAlert(.webServiceResultfail)
                }
                
            })
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.navigationBar.barTintColor = self.setBgColor(43, green: 163, blue: 215)
        self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.font.rawValue: UIFont(name: "IRANSansMobile", size: 16)! ,NSAttributedString.Key.foregroundColor.rawValue:UIColor.white])
        self.title = "صفحه اصلی"

        setTheSelectedCityAndState()
    }
    
    func deviceType()->Void{

        if DeviceType.IS_IPHONE_6_6S_7_8 {
            
//            self.deviceTypeNumber = 0
//            self.topImageHeightConstraint.constant = 316
//            self.mainTabTopSpace.constant = 40
//            let cellSize = CGSize(width:343 , height:283)
//            let layout = UICollectionViewFlowLayout()
//            layout.scrollDirection = .horizontal //.horizontal
//            layout.itemSize = cellSize
//            //layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
//            layout.minimumLineSpacing = 0.0
//            layout.minimumInteritemSpacing = 0.0
//            self.customSlider.setCollectionViewLayout(layout, animated: true)
//
           }else if DeviceType.IS_IPHONE_6P_6SP_7P_8P{
//
            self.topBannerWidth = 360
            self.topBannerHeight = 192
//            self.deviceTypeNumber = 1
//            self.topImageHeightConstraint.constant = 349
//            self.mainTabTopSpace.constant = 73
//            let cellSize = CGSize(width:382, height:318)
//            let layout = UICollectionViewFlowLayout()
//            layout.scrollDirection = .horizontal //.horizontal
//            layout.itemSize = cellSize
//            //layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
//            layout.minimumLineSpacing = 0.0
//            layout.minimumInteritemSpacing = 0.0
//            self.customSlider.setCollectionViewLayout(layout, animated: true)
//
        }
    
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      return selectedGropItemsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
       
        
        if let data = selectedGropItemsList[indexPath.row] as? poolsModel {
       
            
        
        var cellIdentifier =  "poolCustomRow"
            if self.deviceTypeNumber == 1 {
                cellIdentifier =  "poolCustomRowV1"
            }

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! poolCustomRow
        
            
            //clear objcet items
            cell.availableForMan.alpha = 0
            cell.poolPrice.text = ""
            cell.poolPriceInGishe.text = ""
            cell.poolPriceInGisheTitle.alpha = 0
            cell.availableForWomen.alpha = 0
            
            
       // let data = self.poolList[indexPath.row]
        cell.poolName.text = data.name
        cell.poolAddress.text = data.area_name
        if let value = data.rating  {
            cell.poolRateBar.rating = setRatingStar(value)
        }
        
        if let stringURL = URL.init(string: data.default_image!)  {
            let imageView = cell.poolImage
            let URL = stringURL//Foundation.URL(string: data["thumbnail"]!)!
            let placeholderImage = UIImage(named: "main_top_bg.png")!
            imageView?.af_setImage(withURL: URL, placeholderImage: placeholderImage)
        }else{
            cell.poolImage.image = UIImage(named: "main_top_bg.png")
        }
        
        
            
        switch data.gender! {
        case "0":
            cell.availableForMan.alpha = 1
            break
        case "1":
            cell.availableForWomen.alpha = 1
            break
        case "2":
            cell.availableForWomen.alpha = 1
            cell.availableForMan.alpha = 1
            break
        default:
            printLog(m: "")
            cell.availableForWomen.alpha = 0
            cell.availableForMan.alpha = 0
        }
    
        // if the discount qual to zero, all discount section should be hidden
        if data.max_discount != "null", data.max_discount != "0" {
            if data.max_discount != nil , data.max_discount!.count > 0 {
                    cell.poolDiscountBg.alpha = 1
                    cell.poolDiscount.text = "%"+replaceString(data.max_discount!)
            }else{
                cell.poolDiscount.text = ""
                cell.poolDiscountBg.alpha = 0
            }
        }else{
            cell.poolDiscount.text = ""
            cell.poolDiscountBg.alpha = 0
        }
         
            
            
       // if cellIdentifier ==  "poolCustomRow" || cellIdentifier == "poolCustomRowV1" {
        
            
            if data.minimum_market_price != data.minimum_sale_price, data.minimum_market_price != "<null>",  data.minimum_sale_price != "<null>"  {

                // gishe has the price
                // sale has the price
                // thei are not equal


                // set the gishe price
                if let price = data.minimum_market_price {
                        cell.poolPriceInGishe.text = setSeprator(price)//self.replaceString(price) + " " + "تومان"
                        cell.poolPriceInGisheTitle.alpha = 1
                        cell.poolPriceInGishe.alpha = 1
                        // if discount is not zero, the red line show be shown on the price
                        let lineView = UIView(
                            frame: CGRect(x: 0,
                                          y: cell.poolPriceInGishe.bounds.size.height / 2,
                                          width: cell.poolPriceInGishe.bounds.size.width,
                                          height: 1
                            )
                        )
                        lineView.backgroundColor = UIColor.red;
                        cell.poolPriceInGishe.addSubview(lineView)
                }

                // set sale price if it is available
                if let salePrice = data.minimum_sale_price {
                        cell.poolPrice.text = setSeprator(salePrice)//self.replaceString(salePrice) + " " + "تومان"
                }


            }else if data.minimum_market_price == "<null>", data.minimum_sale_price == "<null>"{
                /*
                 for resolve tasked that defined in team work
                 https://magnait.teamwork.com/#tasks/16911154
                 */
                cell.poolPriceInGisheTitle.alpha = 0
                cell.poolPriceInGishe.alpha = 0
                cell.poolPrice.text = "مشاهده"


            }else if data.minimum_market_price != "<null>", data.minimum_sale_price == "<null>"{
                /*
                 for resolve tasked that defined in team work
                 https://magnait.teamwork.com/#tasks/16917606
                 */
                cell.poolPriceInGisheTitle.alpha = 0
                cell.poolPriceInGishe.alpha = 0

                if let salePrice = data.minimum_sale_price {
                    if salePrice != "<null>" {
                        cell.poolPrice.text = setSeprator(salePrice)//self.replaceString(salePrice) + " " + "تومان"
                    }
                }

            }else if data.minimum_market_price == data.minimum_sale_price, data.minimum_market_price != "<null>",
                data.minimum_sale_price != "<null>"  {

                cell.poolPriceInGisheTitle.alpha = 0
                cell.poolPriceInGishe.alpha = 0

                if let salePrice = data.minimum_sale_price {
                    if salePrice != "<null>" {
                        cell.poolPrice.text = setSeprator(salePrice)//self.replaceString(salePrice) + " " + "تومان"
                    }
                }

            }
            
       // }
       // print(indexPath.row)
        return cell
            
       }else if let data = selectedGropItemsList[indexPath.row] as? textModel {
        
            
            let cellIdentifier =  "textCustomRow"
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! textCustomRow
            
            cell.content.text = data.content
            cell.title.text = data.title
            
            if let stringURL = URL.init(string: data.image!)  {
                let imageView = cell.image
                let URL = stringURL//Foundation.URL(string: data["thumbnail"]!)!
                let placeholderImage = UIImage(named: "main_top_bg.png")!
                imageView?.af_setImage(withURL: URL, placeholderImage: placeholderImage)
            }else{
                cell.image.image = UIImage(named: "main_top_bg.png")
            }
            
            //print(indexPath.row)
            return cell

        
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let data = selectedGropItemsList[indexPath.row]
        if  let value = data as? poolsModel {
            DataSourceManagement.setSelectedPoolId(value.pool_id!)
            openNewClass(classIdentifier: "PoolVC")
        }else if let value = data as? textModel {
            shareData.shared.selectedIndexTextType = value
            openNewClass(classIdentifier: "TextTypeIndexVC")
        }
    }
    
    
//    func getIndexData() -> Void {
//        self.GET_INDEX_DATA_STATUS = true
//        LoadingOverlayV2.sharedV2.showOverlayV2(view: self.view, message: "در حال دریافت اطلاعات...")
//        let client_id = Constants.clientId
//        let pool_image_width = "900"// String(describing: self.bannerView.frame.width)
//        var WSResponseStatus = false
//        let access_token = SwiftUtil.getUserAccessToken()
//        let index = wsPOST.init(Constants.INDEX,"client_id=\(client_id)&pool_image_width=\(pool_image_width)&access_token=\(access_token)")
//        index.start{ (indexData) in
//
//            do{
//                if let indexValue = indexData {
//                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
//                    if let item = json as? [String: AnyObject]{
//                        if let value = item["status"] as? Int {
//                            if value == 1 {
//                                WSResponseStatus = true
//                            }
//                        }
//                        if let logo = item["logo"] as? String {
//                            UserDefaults.standard.set(logo, forKey: UserDefaults.Keys.drawerLogoUrl)
//                        }
//                        if let indexValue = item["index"] as? [String: AnyObject] {
//                            if let groupsValue = indexValue["groups"] as? [[String: AnyObject]]{
//
//                                var i = 0
//                                for group in groupsValue {
//                                    if let groupItem = group as? [String: AnyObject]{
//                                            if let value = groupItem["index_app_group_id"] as? Int {
//                                            //print(value)
//                                            i+=1
//                                            switch i {
//                                            case 1:
//                                                self.gropItemsList01 = []
//                                                self.groupId01 = "1"
//                                                self.groupIdTitle01 = ""
//                                                if let title = groupItem["title"] as? String{
//                                                self.groupIdTitle01 = title
//                                                }
//
//                                                if let slides = groupItem["slides"] as? [[String: AnyObject]] {
//                                                for slide in slides {
//                                                    if let value = slide["slide_type"] as? String {
//                                                        switch value {
//                                                        case "pool":
//                                                            self.gropItemsList01.append(poolsModel.prepare(slide as AnyObject))
//                                                            break
//                                                        case "text":
//                                                            self.gropItemsList01.append(textModel.prepare(slide as AnyObject))
//                                                            break
//                                                        default:
//                                                            self.printLog(m: "")
//                                                        }
//                                                    }
//                                                }
//                                                }
//                                                break
//
//                                            case 2:
//                                                self.gropItemsList02 = []
//                                                self.groupIdTitle02 = ""
//                                                self.groupId02 = "2"
//                                                if let title = groupItem["title"] as? String{
//                                                    self.groupIdTitle02 = title
//                                                }
//                                                if let slides = groupItem["slides"] as? [[String: AnyObject]] {
//                                                    for slide in slides {
//                                                        if let value = slide["slide_type"] as? String {
//                                                            switch value {
//                                                            case "pool":
//                                                                self.gropItemsList02.append(poolsModel.prepare(slide as AnyObject))
//                                                                break
//                                                            case "text":
//                                                                self.gropItemsList02.append(textModel.prepare(slide as AnyObject))
//                                                                break
//                                                            default:
//                                                                self.printLog(m: "")
//                                                            }
//                                                        }
//                                                    }
//                                                }
//                                                break
//
//                                            case 3:
//                                                self.gropItemsList03 = []
//                                                self.groupId03 = "3"
//                                                self.groupIdTitle03 = ""
//                                                if let title = groupItem["title"] as? String{
//                                                    self.groupIdTitle03 = title
//                                                }
//                                                if let slides = groupItem["slides"] as? [[String: AnyObject]] {
//                                                    for slide in slides {
//                                                        if let value = slide["slide_type"] as? String {
//                                                            switch value {
//                                                            case "pool":
//                                                                self.gropItemsList03.append(poolsModel.prepare(slide as AnyObject))
//                                                                break
//                                                            case "text":
//                                                                self.gropItemsList03.append(textModel.prepare(slide as AnyObject))
//                                                                break
//                                                            default:
//                                                                self.printLog(m: "")
//                                                            }
//                                                        }
//                                                    }
//                                                }
//                                                break
//
//
//                                            default:
//                                                self.printLog(m: "")
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }catch{
//                print("this is the error: \(error)")
//            }
//
//            DispatchQueue.main.async(execute: {
//                self.GET_INDEX_DATA_STATUS = false
//                LoadingOverlayV2.sharedV2.hideOverlayView()
//                if WSResponseStatus {
////                    if self.groupIdTitle01 != "" {
////                        self.group01Btn.setTitle(self.groupIdTitle01, for: .normal)
////                    }
////                    if self.groupIdTitle02 != "" {
////                        self.group02Btn.setTitle(self.groupIdTitle02, for: .normal)
////                    }
////                    if self.groupIdTitle03 != "" {
////                        self.group03Btn.setTitle(self.groupIdTitle03, for: .normal)
////                    }
////                    self.selectedGropItemsList.removeAll()
////                    self.selectedGropItemsList.append(contentsOf: self.gropItemsList01)
////                    self.customSlider.reloadData()
////                    self.setUnderlineView(self.selectedGroupId)
//                }else{
//                   self.showAlert("webServiceResultfail")
//                }
//            })
//
//        }
//
//
//    }
    
    //************** softkeyboard tool bar **********************//
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField
        {
        default:
            //seachPoolList();
            textField.resignFirstResponder()
        }
        return true
    }
    
    func doneWithKeyboard(_ sender: UITextField){
        
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        keyboardToolbar.isTranslucent = true
        keyboardToolbar.barTintColor = UIColor.white
        
        let button = UIButton(type: .system)
        button.setTitle("بستن", for: .normal)
        button.sizeToFit()
        button.titleLabel?.font = UIFont(name: "IRANSansMobile", size: 12)
        button.tintColor = UIColor.black
        button.addTarget(self, action: #selector(closeKeyboard(_:)), for: .touchUpInside)
    
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        keyboardToolbar.items = [flexBarButton,UIBarButtonItem(customView: button)]
        //keyboardToolbar.items = [itemDown,itemUp,flexBarButton,UIBarButtonItem(customView: button)]
        
        if let value = sender as? UITextField {
            value.inputAccessoryView = keyboardToolbar
        }else if let value = sender as? UITextView {
            value.inputAccessoryView = keyboardToolbar
        }
        
    }
    
    //************** softkeyboard tool bar **********************//
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch status {
        case .restricted, .denied:
            // Disable your app's location features
            ///disableMyLocationBasedFeatures()
            break
            
        case .authorizedWhenInUse:
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.delegate = self
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            break
            
        case .authorizedAlways:
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.delegate = self
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            break
            
        case .notDetermined:
            break
        }
        
        
    }

    func locationManager(_ manager: CLLocationManager,
                         didUpdateLocations locations: [CLLocation])
    {
        
     
        if let _ = locations.first {
            let latestLocation: CLLocation = locations[locations.count - 1]
          
            if self.latestLat != String(latestLocation.coordinate.latitude) , self.latestLng != String(latestLocation.coordinate.longitude) {
            
                if let value = String(latestLocation.coordinate.latitude) as? String{
                    DataSourceManagement.setDeviceLat(value)
                    latestLat = value
                }
                if let value = String(latestLocation.coordinate.longitude)  as? String{
                    DataSourceManagement.setDeviceLng(value)
                    latestLng = value
                }
                
                //print(latestLocation.coordinate.latitude)
                //print(latestLocation.coordinate.longitude)
            }
          
        }
        
    }
    
    // after user selected the state and city
    func setTheSelectedCityAndState(){
//        if DataSourceManagement.getSelectedStateAndCity() != nil {
//            self.selectedCityAndState = DataSourceManagement.getSelectedStateAndCity()
//            let tData = (self.selectedCityAndState?.city_name)! + " (" + (self.selectedCityAndState?.state_name)! + ")"
//            shareData.shared.topHeaderLabelPoolList =  (self.selectedCityAndState?.city_name)! + " (" + (self.selectedCityAndState?.state_name)! + ")"
//            selectStateAndCityBtn.setTitle(tData, for: .normal)
//
//        }
    }

    // get Connection type
    func getNetworkType()->String {
        guard let status = Network.reachability?.status else {
            print("not available")
            return ""
        }
        switch status {
        case .unreachable:
            return ""
        case .wifi:
            return "wifi"
        case .wwan:
            return "mobileData"
        }
        return ""
        
        
    }
    

    func showAlert(_ type: InternetAvailability) {
        
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "IRANSansMobile", size: 17)!,
            kTextFont: UIFont(name: "IRANSansMobile", size: 14)!,
            kButtonFont: UIFont(name: "IRANSansMobile", size: 14)!,
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        
        switch type {
        case .internetNotAvailable:
            alertView.addButton("تلاش مجدد") {
               self.getIndex();
            }
            alertView.showWarning("هشدار", subTitle: "خطا در ارتباط با شبکه اینترنت، لطفا اتصال به اینترنت را بررسی کنید .")
        case .webServiceResultfail:
            alertView.addButton("تلاش مجدد") {
                self.getIndex();
            }
            alertView.showNotice("خطا در ارتباط با پول تیکت", subTitle: "در ارتباط با سامانه خطایی رخ داده است . لطفا مجددا تلاش کنید .")
        }
    }
    
    @objc
    func showAllPools() {
        DataSourceManagement.setOpenNearLocationPool(false)
        PoolsListVC.nearme = false
        pushNewClass(classIdentifier: "PoolsListVC")
    }
    
    @objc
    func showNearPools() {
        PoolsListVC.nearme = true
        DataSourceManagement.setOpenNearLocationPool(true)
        pushNewClass(classIdentifier: "PoolsListVC")
//        openNewClass(classIdentifier: "PoolsListVC")
    }
    
    /* this function will handel the item click on the main page*/
    func clickHandler (link_type: String, url_target: String?, url: String?, page: String?, params: AnyObject? = nil) {
        switch link_type {
        case "url":
            if let urlTarget = url_target, let theUrl = url {
                var includeAccessToken = 0
                if let parameter = params as? [String: AnyObject] {
                    if let include_access_token = parameter["include_access_token"] as? Int {
                        includeAccessToken = include_access_token
                    }
                }
                let access_token = SwiftUtil.getUserAccessToken()
                switch urlTarget {
                case "internal":
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let viewController = storyboard.instantiateViewController(withIdentifier: "TextTypeIndexReadMoreVC") as! TextTypeIndexReadMoreVC
                    if includeAccessToken == 0 {
                        viewController.targetUrlAddress = theUrl
                    } else {
                        if theUrl.contains("?") {
                            viewController.targetUrlAddress = theUrl + "&access_token=\(access_token)"
                        } else {
                            viewController.targetUrlAddress = theUrl + "?access_token=\(access_token)"
                        }
                    }
                    viewController.modalPresentationStyle = .fullScreen
                    self.navigationController?.pushViewController(viewController, animated: true)
                case "external":
                    if includeAccessToken == 0 {
                        SwiftUtil.openUrlInPhoneBrowser(theUrl)
                    } else {
                        SwiftUtil.openUrlInPhoneBrowser(theUrl + "&access_token=\(access_token)")
                    }
                default:
                    break
                }
            }
        case "page":
            if let pageValue = page {
                switch pageValue {
                case "pool_list":
                    let paramsValue = params as? [String: AnyObject]
                    // handle the city if attached
                    if let cityId = paramsValue?["city_id"] as? Int, let cityName = paramsValue?["city_name"] as? String {
                        let object = filterObject.init(name: cityName, key: "attachedCityId", value1: cityName, value2: String(cityId))
                        PoolsListVC.attachedCityId = object
                    }
                    DataSourceManagement.setOpenNearLocationPool(false)
                    self.pushNewClass(classIdentifier: "PoolsListVC")
                case "pack_list":
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let viewController = storyboard.instantiateViewController(withIdentifier: "ServiceListTableView") as! ServiceListTableView
                    viewController.type = "pack"
                    viewController.pTitle = "پک"
                    viewController.modalPresentationStyle = .fullScreen
                    self.navigationController?.pushViewController(viewController, animated: true)
                case "course_list":
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let viewController = storyboard.instantiateViewController(withIdentifier: "ServiceListTableView") as! ServiceListTableView
                    viewController.type = "course"
                    viewController.pTitle = "کلاس شنا"
                    viewController.modalPresentationStyle = .fullScreen
                    self.navigationController?.pushViewController(viewController, animated: true)
                case "seans_list":
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let viewController = storyboard.instantiateViewController(withIdentifier: "ServiceListTableView") as! ServiceListTableView
                    viewController.type = "saens"
                    viewController.pTitle = "رزرو بلیت"
                    viewController.modalPresentationStyle = .fullScreen
                    self.navigationController?.pushViewController(viewController, animated: true)
                case "pool_page":
                    guard let paramsValue = params as? [String: AnyObject] else {
                        return
                    }
                    guard let poolId = paramsValue["pool_id"] as? Int else {
                        return
                    }
                    DataSourceManagement.setSelectedPoolId(String(poolId))
                    openNewClass(classIdentifier: "PoolVC")
                default:
                    break
                }
            }
            break
        default:
            break
        }
        
    }
   
}

extension MainPageVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.indexList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let data = self.indexList[indexPath.row]
        if let type = data["type"] as? String {
            
            switch type {
            case "show-pool-cell":
                 /*
                 list tamame majmoeha
                 mojmoe haie abi atrafe man
                 */
                 let cell = tableView.dequeueReusableCell(withIdentifier: "PTShowPoolsCell", for: indexPath) as! PTShowPoolsCell
                 cell.showAllPools.addTarget(self, action: #selector(self.showAllPools), for: .touchUpInside)
                 cell.showNearPools.addTarget(self, action: #selector(self.showNearPools), for: .touchUpInside)
                
                 cell.selectionStyle = .none
                 return cell
                
            case "top-banner":
                /*
                top banner
                */
                 let cell = tableView.dequeueReusableCell(withIdentifier: "PTTopBannerTableViewCell", for: indexPath) as! PTTopBannerTableViewCell
                 var tempImageSource = [AlamofireSource]()
                 if let list = data["list"] as? [top‌‌Banner] {
                    for item in list {
                        tempImageSource.append(AlamofireSource(urlString: item.image!)!)
                    }
                 }
                 cell.topBanner.setImageInputs(tempImageSource)
                 let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTapOn))
                 cell.topBanner.addGestureRecognizer(gestureRecognizer)
                 
                cell.selectionStyle = .none
                 return cell
                
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "PTCollectionViewTableViewCell", for: indexPath) as! PTCollectionViewTableViewCell
                cell.collectionView.reloadData()
                cell.collectionView.collectionViewLayout.invalidateLayout()
                cell.mainUIViewController = self
                cell.topBannerWidth = self.topBannerWidth
                cell.topBannerHeight = self.topBannerHeight
                cell.set(arrayObject: self.indexList[indexPath.row])
                
                cell.selectionStyle = .none
                return cell
            }
        }
        return UITableViewCell()
    }
    
    @objc
    func didTapOn() {
    
        let ndx = IndexPath(row:0, section: 0)
        if let cell = tableView.cellForRow(at:ndx) as? PTTopBannerTableViewCell {
            let position = cell.topBanner.currentPage
            if let data = self.indexList[0] as? [AnyHashable: Any] {
                if let type = data["type"] as? String, type == "top-banner" {
                    if let list = data["list"] as? [top‌‌Banner], list.count > position {
                        print(list[position].link_type)
                        if let linktype = list[position].link_type {
                            switch linktype {
                            case "page":
                                self.clickHandler(link_type: linktype, url_target: nil, url: nil, page: list[position].page ?? "", params: list[position].params)
//                                DataSourceManagement.setOpenNearLocationPool(false)
//                                self.pushNewClass(classIdentifier: "PoolsListVC")
                            case "url":
                                if let url = list[position].url as? String, let url_target = list[position].url_target as? String {
                                    self.clickHandler(link_type: linktype, url_target: url_target, url: url, page: nil, params: list[position].params)
//                                  SwiftUtil.openUrlInPhoneBrowser(url)
                                }
                            default:
                                break
                            }
                        }
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) ->  CGFloat {
        let data = self.indexList[indexPath.row]
        if let value = data["type"] as? String {
            switch value {
            case "banner":
                return 180
            case "url":
                break
            case "service":
                break
            case "pool":
                return 270
            case "top-banner":
                return CGFloat.init(self.topBannerHeight)
            case "show-pool-cell":
                return 122
            default:
                break
            }
        }
        return 284
    }

    
}



//extension MainPageVC: UICollectionViewDataSource


enum groupType: String {
    case topBanner = "top-banner"
    case serivce
    case pool
    case url
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

//MARK: INTERNET AVAILABILITY 
enum InternetAvailability: String {
    case internetNotAvailable = "internetNotAvailable"
    case webServiceResultfail = "webServiceResultfail"
}
