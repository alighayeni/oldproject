//
//  PTTopBannerTableViewCell.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/16/19.
//  Copyright © 2019 OneSignal. All rights reserved.
//

import UIKit
import ImageSlideshow


class PTTopBannerTableViewCell: UITableViewCell {

    @IBOutlet weak var topBanner: ImageSlideshow!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        topBanner.slideshowInterval = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    class func instanceFromNib() -> PTTopBannerTableViewCell {
        return UINib(nibName: "PTTopBannerTableViewCell", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PTTopBannerTableViewCell
    }
    
}
//class func instanceFromNib() -> PTShowPoolsCell {
//    return UINib(nibName: "PTShowPoolsCell", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PTShowPoolsCell
//}
