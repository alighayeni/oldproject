//
//  PTTopBannerCell.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/10/19.
//  Copyright © 2019 OneSignal. All rights reserved.
//

import UIKit

class PTTopBannerCell: UICollectionViewCell {

    @IBOutlet weak var banner: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    class func instanceFromNib() -> PTTopBannerCell {
        return UINib(nibName: "PTTopBannerCell", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PTTopBannerCell
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        banner?.af_cancelImageRequest()
    }
}
