//
//  PTCollectionViewTableViewCell.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/2/19.
//  Copyright © 2019 OneSignal. All rights reserved.
//

import UIKit
import SDWebImage


class PTCollectionViewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    var arrayObject: [AnyHashable: Any]?
    var type: String?
    var title: String?
    var list: [AnyObject]?
    var mainUIViewController: UIViewController?
    var topBannerHeight = 320
    var topBannerWidth = 170

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.collectionView.register(UINib.init(nibName: "PTPoolCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PTPoolCollectionViewCell")
        self.collectionView.register(UINib.init(nibName: "PTMainBannerCell", bundle: nil), forCellWithReuseIdentifier: "PTMainBannerCell")
        self.collectionView.register(UINib.init(nibName: "PTTopBannerCell", bundle: nil), forCellWithReuseIdentifier: "PTTopBannerCell")
    
       
        //let cellSize1 = CGSize(width:200 , height:200)
        //        let layout1 = UICollectionViewFlowLayout()
        //        layout.scrollDirection = .horizontal //.horizontal
        //        layout.itemSize = cellSize1
        //        layout.minimumLineSpacing = 0.0
        //        layout.minimumInteritemSpacing = 0.0
        //        self.collectionView.setCollectionViewLayout(layout, animated: true)
        
        
    }
    
    func set( arrayObject: [AnyHashable: Any]?) {
        
        if let value = arrayObject {
            var layoutHeight = 200
            var layoutWidth = 200
            self.arrayObject = value
            if let value = self.arrayObject?["type"] as? String {
                type = value
                switch value {
                case "banner":
                    layoutHeight = 150
                    layoutWidth = 185
                case "url":
                    break
                case "service":
                    break
                case "pool":
                    layoutHeight = 240
                    layoutWidth = 190
                case "top-banner":
                    layoutHeight = topBannerHeight
                    layoutWidth = topBannerWidth
                case "show-pool-cell":
                    layoutHeight = 133
                    layoutWidth = Int(UIScreen.main.bounds.width)
                default:
                    break
                }
            }
            if let value = self.arrayObject?["title"] as? String {
                title = value
                self.titleLabel.text = title
            }
            if let value = self.arrayObject?["list"] as? [top‌‌Banner] {
                self.list = value
            } else if let value = self.arrayObject?["list"] as? [poolsById] {
                self.list = value
            }  else if let value = self.arrayObject?["list"] as? [serivce] {
                self.list = value
            }  else if let value = self.arrayObject?["list"] as? [urlObject] {
                self.list = value
            }
            
            let cellSize = CGSize(width:layoutWidth , height:layoutHeight)
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal //.horizontal
            layout.itemSize = cellSize
            layout.minimumLineSpacing = 0.0
            layout.minimumInteritemSpacing = 0.0
            self.collectionView.setCollectionViewLayout(layout, animated: true)
            
            self.collectionView.dataSource = self
            self.collectionView.delegate = self
            self.collectionView.reloadData()
            self.collectionView.semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
            self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: false)
//            if let value = self.arrayObject?["type"] as? String {
//                if value == "top-banner" {
//                    self.collectionView.backgroundColor = .gray
//                    self.titleLabel.frame.size.height = 0
//                    self.titleLabel.isHidden = true
//                    if self.list?.count ?? 0 > 2 {
//                        //                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//                        self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: false)
//                        //                        }
//                    }
//                } else {
//                    self.collectionView.backgroundColor = .clear
//                    self.titleLabel.isHidden = false
//                    self.titleLabel.frame.size.height = 26
//                }
//            }
            
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        arrayObject = nil
        type = nil
        title = nil
        list = nil
        
    }
    
}

extension PTCollectionViewTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.type == "show-pool-cell" {
            return 1
        }
        return self.list?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let value = self.arrayObject?["type"] as? String {
            self.type = value
        }
        //        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PTPoolCollectionViewCell", for: indexPath as IndexPath) as! PTPoolCollectionViewCell
        if let data  = self.list?[indexPath.row] as? poolsById {
            if self.type == "pool" {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PTPoolCollectionViewCell", for: indexPath as IndexPath) as! PTPoolCollectionViewCell
                
                cell.frame.size.width = 190
                //clear objcet items
                cell.availableForMan.alpha = 0
                cell.poolPrice.text = ""
                cell.poolPriceInGishe.text = ""
                //cell.poolPriceInGisheTitle.alpha = 0
                cell.availableForWomen.alpha = 0
                
                // let data = self.poolList[indexPath.row]
                cell.poolName.text = data.name
                cell.poolAddress.text = data.area_name
                if let value = data.rating  {
                    cell.poolRateBar.rating = Double(value) ?? 0.0
                }
                
                if let stringURL = URL.init(string: data.default_image!)  {
                    let imageView = cell.poolImage
                    let URL = stringURL//Foundation.URL(string: data["thumbnail"]!)!
                    let placeholderImage = UIImage(named: "main_top_bg.png")!
                    imageView?.af_setImage(withURL: URL, placeholderImage: placeholderImage)
                }else{
                    cell.poolImage.image = UIImage(named: "main_top_bg.png")
                }
                
                
                
                switch data.gender! {
                case "0":
                    cell.availableForMan.alpha = 1
                    break
                case "1":
                    cell.availableForWomen.alpha = 1
                    break
                case "2":
                    cell.availableForWomen.alpha = 1
                    cell.availableForMan.alpha = 1
                    break
                default:
                    cell.availableForWomen.alpha = 0
                    cell.availableForMan.alpha = 0
                }
                
                // if the discount qual to zero, all discount section should be hidden
                //                if data.max_discount != "null", data.max_discount != "0" {
                //                    if data.max_discount != nil , data.max_discount!.count > 0 {
                //                        cell.poolDiscountBg.alpha = 1
                //                        cell.poolDiscount.text = "%"+replaceString(data.max_discount!)
                //                    }else{
                //                        cell.poolDiscount.text = ""
                //                        cell.poolDiscountBg.alpha = 0
                //                    }
                //                }else{
                //                    cell.poolDiscount.text = ""
                //                    cell.poolDiscountBg.alpha = 0
                //                }
                
                // if cellIdentifier ==  "poolCustomRow" || cellIdentifier == "poolCustomRowV1" {
                if data.minimum_market_price != data.minimum_sale_price, data.minimum_market_price != "<null>",  data.minimum_sale_price != "<null>" {
                    
                    // gishe has the price
                    // sale has the price
                    // thei are not equal
                    
                    
                    // set the gishe price
                    if let price = data.minimum_market_price {
                        cell.poolPriceInGishe.text = setSeprator(price)//self.replaceString(price) + " " + "تومان"
                        //cell.poolPriceInGisheTitle.alpha = 1
                        cell.poolPriceInGishe.alpha = 1
                        // if discount is not zero, the red line show be shown on the price
                        let lineView = UIView(
                            frame: CGRect(x: 0,
                                          y: cell.poolPriceInGishe.bounds.size.height / 2,
                                          width: cell.poolPriceInGishe.bounds.size.width,
                                          height: 1
                            )
                        )
                        lineView.backgroundColor = UIColor.red;
                        cell.poolPriceInGishe.addSubview(lineView)
                    }
                    
                    // set sale price if it is available
                    if let salePrice = data.minimum_sale_price {
                        cell.poolPrice.text = setSeprator(salePrice)//self.replaceString(salePrice) + " " + "تومان"
                    }
                    
                    
                }else if data.minimum_market_price == "<null>", data.minimum_sale_price == "<null>"{
                    /*
                     for resolve tasked that defined in team work
                     https://magnait.teamwork.com/#tasks/16911154
                     */
                    //cell.poolPriceInGisheTitle.alpha = 0
                    cell.poolPriceInGishe.alpha = 0
                    cell.poolPrice.text = "مشاهده"
                    
                    
                }else if data.minimum_market_price != "<null>", data.minimum_sale_price == "<null>"{
                    /*
                     for resolve tasked that defined in team work
                     https://magnait.teamwork.com/#tasks/16917606
                     */
                    //cell.poolPriceInGisheTitle.alpha = 0
                    cell.poolPriceInGishe.alpha = 0
                    
                    if let salePrice = data.minimum_sale_price {
                        if salePrice != "<null>" {
                            cell.poolPrice.text = setSeprator(salePrice)//self.replaceString(salePrice) + " " + "تومان"
                        }
                    }
                    
                }else if data.minimum_market_price == data.minimum_sale_price, data.minimum_market_price != "<null>",
                    data.minimum_sale_price != "<null>"  {
                    
                    cell.poolPriceInGishe.alpha = 0
                    if let salePrice = data.minimum_sale_price {
                        if salePrice != "<null>" {
                            cell.poolPrice.text = setSeprator(salePrice)//self.replaceString(salePrice) + " " + "تومان"
                        }
                    }
                    
                }
                return cell
            }
        } else if let data  = self.list?[indexPath.row] as? top‌‌Banner {
            if self.type == "banner" {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PTMainBannerCell", for: indexPath as IndexPath) as! PTMainBannerCell
                cell.frame.size.height = 150
                cell.frame.size.width = 185
                
                cell.banner.image = UIImage(named: "main_top_bg.png")
                if let stringURL = URL.init(string: data.image!)  {
                    let imageViewMainBanner = cell.banner
                    imageViewMainBanner?.image = UIImage(named: "main_top_bg.png")
                    imageViewMainBanner?.sd_setImage(with: stringURL, placeholderImage: UIImage(named: "main_top_bg.png"))
                }else{
                    cell.banner.image = UIImage(named: "main_top_bg.png")
                }
                
                return cell
            } else if self.type == "top-banner" {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PTTopBannerCell", for: indexPath as IndexPath) as! PTTopBannerCell
                cell.frame.size.height = CGFloat.init(topBannerHeight)
                cell.frame.size.width = CGFloat.init(topBannerWidth)
                if let stringURL = URL.init(string: data.image!)  {
                    let imageViewTopBanner = cell.banner
                    imageViewTopBanner?.image = UIImage(named: "main_top_bg.png")
                    imageViewTopBanner?.sd_setImage(with: stringURL, placeholderImage: UIImage(named: "main_top_bg.png"))
                }else{
                    cell.banner.image = UIImage(named: "main_top_bg.png")
                }
                return cell
            }
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.type == "pool" {
            if let value = self.list?[indexPath.row] as? poolsById {
                DataSourceManagement.setSelectedPoolId(value.pool_id!)
                let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PoolVC") as UIViewController
                viewController.modalPresentationStyle = .fullScreen
                self.mainUIViewController?.navigationController?.pushViewController(viewController, animated: true)
            }
        } else if self.type == "banner" {
            if let value = self.list?[indexPath.row] as? top‌‌Banner {
                switch value.link_type ?? "" {
                case "page":
                    if let link_type = value.link_type, let page = value.page {
                        self.clickHandler(link_type: link_type, url_target: nil, url: nil, page: page, params: value.params)
                    }
                case "url":
                     if let link_type = value.link_type, let url = value.url , let url_target = value.url_target  {
                        self.clickHandler(link_type: link_type, url_target: url_target, url: url, page: nil, params: value.params)
                    }
                default:
                    break
                }
                
            }
        }
        
        
        //else if let value = self.arrayObject?["list"] as? [poolsById] {
//            self.list = value
//        }  else if let value = self.arrayObject?["list"] as? [serivce] {
//            self.list = value
//        }  else if let value = self.arrayObject?["list"] as? [urlObject] {
//            self.list = value
//        }
        
        
    }
    
    // persian replace character
    func replaceString(_ stringNumber: String)->String{
        var token = stringNumber
        token =  token.replacingOccurrences(of: "0", with:"۰")
        token =  token.replacingOccurrences(of: "1", with:"۱")
        token =  token.replacingOccurrences(of: "2", with:"۲")
        token =  token.replacingOccurrences(of: "3", with:"۳")
        token =  token.replacingOccurrences(of: "4", with:"۴")
        token =  token.replacingOccurrences(of: "5", with:"۵")
        token =  token.replacingOccurrences(of: "6", with:"۶")
        token =  token.replacingOccurrences(of: "7", with:"۷")
        token =  token.replacingOccurrences(of: "8", with:"۸")
        token =  token.replacingOccurrences(of: "9", with:"۹")
        return token
    }
    
    // set price seprator
    func setSeprator(_ number: String)-> String {
        var mCreditNSNumber : NSNumber?
        let someString = number
        if let myInteger = Int(someString) as? Int{
            mCreditNSNumber = NSNumber(value:myInteger)
        }
        let formatter = NumberFormatter()
        formatter.locale = Locale.current // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
        formatter.numberStyle = .currency
        if let value = mCreditNSNumber as? NSNumber {
            if let formattedTipAmount = formatter.string(from: value as NSNumber) {
                //tipAmountLabel.text = "Tip Amount: \(formattedTipAmount)"
                var tString = formattedTipAmount.replacingOccurrences(of: ".00", with:"")
                tString = tString.replacingOccurrences(of: "$", with:"")
                tString = tString.replacingOccurrences(of: "IRR", with:"")
                tString = tString + " تومان "
                return self.replaceString(tString)
            }
        }
        return self.replaceString(number)
    }
    
    /* this function will handel the item click on the main page*/
    func clickHandler (link_type: String, url_target: String?, url: String?, page: String?, params: AnyObject? = nil) {
        
        var includeAccessToken = 0
        if let parameter = params as? [String: AnyObject] {
            if let include_access_token = parameter["include_access_token"] as? Int {
                includeAccessToken = include_access_token
            }
        }
        let access_token = SwiftUtil.getUserAccessToken()
        
        switch link_type {
        case "url":
            if let urlTarget = url_target, let theUrl = url {
                switch urlTarget {
                case "internal":
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let viewController = storyboard.instantiateViewController(withIdentifier: "TextTypeIndexReadMoreVC") as! TextTypeIndexReadMoreVC
                    //viewController.targetUrlAddress = theUrl
                    if includeAccessToken == 0 {
                        viewController.targetUrlAddress = theUrl
                    } else {
                        if theUrl.contains("?") {
                            viewController.targetUrlAddress = theUrl + "&access_token=\(access_token)"
                        } else {
                            viewController.targetUrlAddress = theUrl + "?access_token=\(access_token)"
                        }
                    }
                    viewController.modalPresentationStyle = .fullScreen
                    self.mainUIViewController?.navigationController?.pushViewController(viewController, animated: true)
                case "external":
                    SwiftUtil.openUrlInPhoneBrowser(theUrl)
                default:
                    break
                }
            }
        case "page":
            if let pageValue = page {
                switch pageValue {
                case "pool_list":
                    let paramsValue = params as? [String: AnyObject]
                    // handle the city if attached
                    if let cityId = paramsValue?["city_id"] as? Int, let cityName = paramsValue?["city_name"] as? String {
                        let object = filterObject.init(name: cityName, key: "attachedCityId", value1: cityName, value2: String(cityId))
                        PoolsListVC.attachedCityId = object
                    }
                    DataSourceManagement.setOpenNearLocationPool(false)
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let viewController = storyboard.instantiateViewController(withIdentifier: "PoolsListVC") as! PoolsListVC
                    viewController.modalPresentationStyle = .fullScreen
                    self.mainUIViewController?.navigationController?.pushViewController(viewController, animated: true)
                case "pack_list":
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let viewController = storyboard.instantiateViewController(withIdentifier: "ServiceListTableView") as! ServiceListTableView
                    viewController.type = "pack"
                    viewController.pTitle = "پک"
                    viewController.modalPresentationStyle = .fullScreen
                    self.mainUIViewController?.navigationController?.pushViewController(viewController, animated: true)
                case "course_list":
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let viewController = storyboard.instantiateViewController(withIdentifier: "ServiceListTableView") as! ServiceListTableView
                    viewController.type = "course"
                    viewController.pTitle = "کلاس شنا"
                    viewController.modalPresentationStyle = .fullScreen
                    self.mainUIViewController?.navigationController?.pushViewController(viewController, animated: true)
                case "seans_list", "ticket_list":
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let viewController = storyboard.instantiateViewController(withIdentifier: "ServiceListTableView") as! ServiceListTableView
                    viewController.type = "saens"
                    viewController.pTitle = "رزرو بلیت"
                    viewController.modalPresentationStyle = .fullScreen
                    self.mainUIViewController?.navigationController?.pushViewController(viewController, animated: true)
                case "pool_page":
                    guard let paramsValue = params as? [String: AnyObject] else {
                        return
                    }
                    guard let poolId = paramsValue["pool_id"] as? Int else {
                        return
                    }
                    DataSourceManagement.setSelectedPoolId(String(poolId))
                    let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PoolVC") as UIViewController
                    viewController.modalPresentationStyle = .fullScreen
                    self.mainUIViewController?.navigationController?.pushViewController(viewController, animated: true)
                default:
                    break
                }
            }
            break
        default:
            break
        }
        
    }
}
