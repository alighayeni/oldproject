//
//  PTShowPoolsCell.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/13/19.
//  Copyright © 2019 OneSignal. All rights reserved.
//

import UIKit

class PTShowPoolsCell: UITableViewCell {

    @IBOutlet weak var showAllPools: UIButton!
    @IBOutlet weak var showNearPools: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    class func instanceFromNib() -> PTShowPoolsCell {
        return UINib(nibName: "PTShowPoolsCell", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PTShowPoolsCell
    }
}
