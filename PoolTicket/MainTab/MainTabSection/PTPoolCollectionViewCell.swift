//
//  PTPoolCollectionViewCell.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/2/19.
//  Copyright © 2019 OneSignal. All rights reserved.
//

import UIKit
import Cosmos
import Alamofire

class PTPoolCollectionViewCell: UICollectionViewCell {

    @IBOutlet var poolRateBar: CosmosView!
    
    @IBOutlet weak var poolImage: UIImageView!
    @IBOutlet weak var availableForWomen: UIImageView!
    @IBOutlet weak var availableForMan: UIImageView!
    @IBOutlet weak var poolAddress: UILabel!
    @IBOutlet weak var poolName: UILabel!
    
    @IBOutlet weak var poolPrice: UILabel!
    @IBOutlet weak var poolPriceInGishe: UILabel!
    
    override func awakeFromNib() {
        poolRateBar.settings.fillMode = .half
    }
    
    class func instanceFromNib() -> PTPoolCollectionViewCell {
        return UINib(nibName: "PTPoolCollectionViewCell", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PTPoolCollectionViewCell
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        poolImage.af_cancelImageRequest()
    }
}
