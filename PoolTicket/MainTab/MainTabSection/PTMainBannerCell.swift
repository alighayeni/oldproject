//
//  CustomCellCV.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/3/19.
//  Copyright © 2019 OneSignal. All rights reserved.
//

import UIKit
import Alamofire

class PTMainBannerCell: UICollectionViewCell {

    @IBOutlet weak var banner: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    class func instanceFromNib() -> PTMainBannerCell {
        return UINib(nibName: "PTMainBannerCell", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PTMainBannerCell
    }
    
    override func prepareForReuse() {
        super.reuseIdentifier
        banner?.af_cancelImageRequest()
    }
}
