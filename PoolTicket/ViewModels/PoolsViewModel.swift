//
//  PoolsViewModel.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 3/17/19.
//  Copyright © 2019 OneSignal. All rights reserved.
//

import UIKit

public final class PoolsViewModel {
    
    // MARK: - Instance Properties
    public let pool: poolsById
    public var pool_id:String? = nil
    public var name:String?  = nil
    public var address:String?  = nil
    public var capacity:String?  = nil
    public var lat:String?  = nil
    public var lng: String?  = nil
    public var status_sell: String? = nil
    public var status_sell_ticket: String? = nil
    public var status_sell_course: String? = nil
    public var status_sell_pack: String? = nil
    public var pDescription: String? = nil
    public var terms_of_use_enable: String? = nil
    public var pool_introducing_enable: String? = nil
    public var tel: String? = nil
    public var gender: String? = nil
    public var comment: String? = nil
    public var state_name: String? = nil
    public var city_name: String? = nil
    public var area_name: String? = nil
    public var city_id: String? = nil
    public var area_id: String? = nil
    public var default_image: String? = nil
    public var state_id: String? = nil
    public var max_discount: String? = nil
    public var is_favorite: String? = nil
    public var rating: [String:AnyObject]? = nil
    public var minimum_sale_price: String? = nil
    public var minimum_market_price: String? = nil
    public var share_link: String? = nil
    public var share_description: String? = nil
    public var ratings: [String:AnyObject]? = nil
    public var poolPriceWithDiscount: String? = nil
    public var poolPrice: String? = nil
    public var tMaxDisCount: String? = nil
    
    // MARK: - Object LifeCycle
    public init(pool: poolsById) {
        self.pool = pool
        
        pool_id = pool.pool_id
        
        default_image = pool.default_image
        
        name = pool.name
        
        if pool.rating != nil {
        //    rating = value
        } else {
        //    rating = "0.0"
        }
        
        if let value = pool.minimum_sale_price {
            if value != "<null>" {
                poolPriceWithDiscount = "\(String(describing: String.replaceString(value))) تومان"
                if let price = pool.minimum_market_price {
                    if price != "<null>" {
                        poolPrice = "\(String(describing: String.replaceString(price))) تومان"
                    }
                }
                //                PoolsTopContainerVC?.poolPriceRedLine.alpha = 1
            }else{
                //                PoolsTopContainerVC?.poolPriceWithDiscount.text = ""
                //                PoolsTopContainerVC?.poolPriceRedLine.alpha = 0
                if let price = pool.minimum_market_price {
                    if price != "<null>" {
                        poolPrice = "\(String(describing: String.replaceString(price))) تومان"
                    }
                }
            }
        }
        
        if let value = pool.comment {
            comment = value
        }
        
        
        if let value = pool.terms_of_use_enable {
            terms_of_use_enable = value
        }
        
        if let value = pool.pool_introducing_enable {
            pool_introducing_enable = value
        }
        
        if let value = pool.ratings as? [String:AnyObject] {
            ratings = value
        }
        
        // hide the buttom bar if sell status is zero
        if let value = pool.status_sell {
            status_sell = value
        }
        
        if let value = pool.status_sell_ticket {
            status_sell_ticket = value
        }
        
        if let value = pool.status_sell_course {
            status_sell_course = value
        }
        
        if let value = pool.status_sell_pack {
            status_sell_pack = value
        }
        
        if let value = pool.gender {
            gender = value
        }
        
        
        if pool.max_discount != nil , pool.max_discount != "0", (pool.max_discount ?? "").count > 0 {
            tMaxDisCount = "%\(String(describing: String.replaceString((pool.max_discount ?? ""))))"
        }
        
        if let value = pool.status_sell_pack {
           status_sell_pack = value
        }
        
        if let value = pool.status_sell_ticket {
           status_sell_ticket = value
        }
        
        if let value = pool.status_sell_course {
           status_sell_course = value
        }
      
        
        
    }
    
    
    
}
