//
//  SubmitActivationNumber.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/12/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import SCLAlertView


class SubmitActivationNumber: UIViewController {
    
    
    var connectionStatus = false
    @IBOutlet weak var recievedToken: UITextField!;
    @IBOutlet weak var scrollView:UIScrollView!
    var activeField: UITextField?
    @IBOutlet weak var getAnotherCodeBtn:UIButton!
    @IBOutlet weak var remainingTime:UILabel!
    @IBAction func getAnotherCode(){

        if self.tryToGetCode < 2 {
            if !connectionStatus, seconds <= 0 {
                if let value = shareData.shared.loginInputPhoneNumber as? String {
                    getMobileActivationCodeAgain(value,"sms")
                }
            }else{
                showDropMessage("لطفا شکیبا باشید.")
            }
        }else if self.tryToGetCode == 2 {
            if !connectionStatus, seconds <= 0 {
                if let value = shareData.shared.loginInputPhoneNumber as? String {
                    getMobileActivationCodeAgain(value,"otp")
                }
            }else{
                showDropMessage("لطفا شکیبا باشید.")
            }
        }else{
            self.showAlert("fail3Time")
        }
        
    }
    
    @IBAction func mobileApprove(_ Sender: Any){
        
        if var mToken = recievedToken.text  as? String {
            mToken = removeWhiteSpace(mToken)
          
            if !connectionStatus {
                if let value = shareData.shared.loginInputPhoneNumber {
                self.getApprovedMobileNumber(value,mToken)
                }
            }else{
                
            }
            
        }
        
    }
    
    var seconds = 0 //This variable will hold a starting value of seconds. It could be any amount above 0.
    var timer = Timer()
    var isTimerRunning = false //This will be used to make sure only one timer is created at a time.
    var tryToGetCode = 1
    
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        seconds -= 1     //This will decrement(count down)the seconds.
        
        if seconds <= 0 {
            pauseTimer()
        }else{
            self.remainingTime.text = "(\(seconds))"
        }
    }
    
    func pauseTimer(){
        timer.invalidate()
        self.remainingTime.text = ""
        seconds = 0
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.font.rawValue: UIFont(name: "IRANSansMobile", size: 14),NSAttributedString.Key.foregroundColor.rawValue:UIColor.white])
        //set the second to a minute
        self.seconds = 58
        doneWithKeyboard(recievedToken)
        
//        if let value = shareData.shared.loginInputPhoneNumber {
//         getMobileActivationCodeAgain(value)
//        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        registerForKeyboardNotifications()
        
        runTimer()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        deregisterFromKeyboardNotifications()
    }
    
    
    // approve the token that recieved with sms
    func getApprovedMobileNumber(_ mobileNo: String,_ token: String){
        self.connectionStatus = true
        LoadingOverlayV2.sharedV2.showOverlayV2(view: self.view, message: "بررسی صحت کد وارد شده")
        var register_token: String?
        var WSStatus = false
        let client_id = Constants.clientId
        
        let index = wsPOST.init(Constants.MOBILE_APPROVE,"mobile=\(mobileNo)&client_id=\(client_id)&token=\(token)")
        index.start{ (indexData) in
            
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    
                    if let item = json as? [String: AnyObject]{
                        if let value = item["status"] as? Int {
                            if value == 1 {
                               WSStatus = true
                            }
                        }
                        if let value = item["error"] as? String {
                            self.printLog(m: String(value))
                        }
                        if let value = item["register_token"] as? Int {
                           register_token = String(value)
                           shareData.shared.register_token = String(value)
                        }
                    }
                }
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                self.connectionStatus = false
                LoadingOverlayV2.sharedV2.hideOverlayView()
                if WSStatus {
                    if register_token != nil {
                        if mobileNo.count > 0 {
                            // register the use in main thread in chabouk push.
                            AppDelegate()._manager?.registerUser(mobileNo)
                        }
                        self.pushNewClass(classIdentifier: "RegisterVC")
                    }
                }else{
                        self.showAlert("fail")
                }
                
            })
            
        }
    }
    
    // if user does not get the token with sms
    func getMobileActivationCodeAgain(_ mobileNo: String, _ type: String){
        connectionStatus = true
        let client_id = Constants.clientId
        LoadingOverlayV2.sharedV2.showOverlayV2(view: self.view, message: "درخواست مجدد کد")
        let approve_send_type = type
        
        let index = wsPOST.init(Constants.RESEND_TOKEN,"mobile=\(mobileNo)&client_id=\(client_id)&approve_send_type=\(approve_send_type)")
         var WSResponseStatus = false
        index.start{ (indexData) in
            var requestTime = 0

            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    
                    if let item = json as? [String: AnyObject]{
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                            }
                        }
                        if let value = item["available_in"] as? Int {
                            requestTime = value
                        }
                    }
                }
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                self.connectionStatus = false
                LoadingOverlayV2.sharedV2.hideOverlayView()
                if WSResponseStatus {
                    self.tryToGetCode += 1

                    if requestTime == 60 {
                        self.showAlert("success")
                        if self.tryToGetCode == 3 {
                            self.getAnotherCodeBtn.setTitle("تعداد مجاز درخواست کد تکمیل شد.", for: .normal)
                        }else{
                            self.seconds = 60
                            self.runTimer()
                        }
                    }else{
                        if let value = requestTime as? Int {
                            self.timer.invalidate()
                            self.seconds = value
                            self.runTimer()
                        }
                    }
                }else{
                    self.showAlert("fail")
                    
                }
                
            })
            
        }
    }
    
    
    //************** softkeyboard tool bar **********************//
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField
        {
            //        case personalID:
            //            personalID.resignFirstResponder()
            //            password.becomeFirstResponder()
            //            break
            //        case password:
            //            password.resignFirstResponder()
            //            break
        //
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
    func doneWithKeyboard(_ sender: UITextField){
        
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        keyboardToolbar.isTranslucent = true
        keyboardToolbar.barTintColor = UIColor.white
        
        let button = UIButton(type: .system)
        button.setTitle("بستن", for: .normal)
        button.sizeToFit()
        button.titleLabel?.font = UIFont(name: "IRANSansMobile", size: 12)
        button.tintColor = UIColor.black
        button.addTarget(self, action: #selector(closeKeyboard(_:)), for: .touchUpInside)
        
        
        //        let btnUp = UIButton(type: .custom)
        //        btnUp.setImage(UIImage(named: "ic_keyboard_arrow_up"), for: .normal)
        //        btnUp.frame = CGRect(x: 0, y: 0, width: 13, height: 13)
        //        btnUp.tag = sender.tag
        //        btnUp.addTarget(self, action: #selector(textFieldUp), for: .touchUpInside)
        //        let itemUp = UIBarButtonItem(customView: btnUp)
        //
        //        let btnDown = UIButton(type: .custom)
        //        btnDown.setImage(UIImage(named: "ic_keyboard_arrow_down"), for: .normal)
        //        btnDown.frame = CGRect(x: 0, y: 0, width: 13, height: 13)
        //        btnDown.tag = sender.tag
        //        btnDown.addTarget(self, action: #selector(textFieldDown), for: .touchUpInside)
        //        let itemDown = UIBarButtonItem(customView: btnDown)
        
        
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        keyboardToolbar.items = [flexBarButton,UIBarButtonItem(customView: button)]
        //keyboardToolbar.items = [itemDown,itemUp,flexBarButton,UIBarButtonItem(customView: button)]
        
        if let value = sender as? UITextField {
            value.inputAccessoryView = keyboardToolbar
        }else if let value = sender as? UITextView {
            value.inputAccessoryView = keyboardToolbar
        }
        
    }
    
    /*keyboard automatic scroll*/
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.isScrollEnabled = true
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: keyboardSize!.height, right: 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.activeField {
            if (!aRect.contains(activeField.frame.origin)){
                self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: -keyboardSize!.height, right: 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollView.isScrollEnabled = false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }

    func showAlert(_ type: String) {
        
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "IRANSansMobile", size: 17)!,
            kTextFont: UIFont(name: "IRANSansMobile", size: 14)!,
            kButtonFont: UIFont(name: "IRANSansMobile", size: 14)!,
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        
        switch type {
        case "fail":
            alertView.addButton("بستن") {
            }
            alertView.showError("اخطار:", subTitle: "کد وارد شده صحیح نمی باشد.")
            break
        case "fail3Time":
            alertView.addButton("بستن") {
            }
            alertView.showError("اخطار:", subTitle: "تعداد مجاز درخواست کد تکمیل شد.")
            break
        default:
            print("")
        }
        
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
