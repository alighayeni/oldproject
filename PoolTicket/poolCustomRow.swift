//
//  poolCustomRow.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/23/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import Cosmos

class poolCustomRow: UICollectionViewCell {
    

    @IBOutlet weak var poolDiscountBg: UIImageView!
    @IBOutlet weak var poolDiscount: UILabel!
    @IBOutlet var poolRateBar: CosmosView!
    
    @IBOutlet weak var poolImage: UIImageView!
    @IBOutlet weak var availableForWomen: UIImageView!
    @IBOutlet weak var availableForMan: UIImageView!
    @IBOutlet weak var poolAddress: UILabel!
    @IBOutlet weak var poolName: UILabel!
    
    @IBOutlet weak var poolPriceInGisheTitle: UILabel!
    @IBOutlet weak var poolPrice: UILabel!
    @IBOutlet weak var poolPriceInGishe: UILabel!
    @IBOutlet weak var poolRedLine: UIImageView!

    override func awakeFromNib() {
        poolRateBar.settings.fillMode = .half
    }
    
}
