//
//  LoadingOverlayV2.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 5/5/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation

import UIKit

public class LoadingOverlayV2{
    
    var overlayView = UIView()
    var activityIndicator = UIActivityIndicatorView()
    var labelView = UILabel()
    var labelViewEn = UILabel()
    
    class var sharedV2: LoadingOverlayV2 {
        struct Static {
            static let instance: LoadingOverlayV2 = LoadingOverlayV2()
        }
        return Static.instance
    }
    
    public func showOverlayV2(view: UIView, message: String) {
        
        overlayView.frame = CGRectMake(0, 0, 300, 100)
        overlayView.center = CGPoint(x: view.bounds.width / 2,y: view.bounds.height / 2) //- 49
        overlayView.backgroundColor = .white //getCgColor(30, 30, 30)
        overlayView.layer.cornerRadius = 2
        overlayView.alpha = 0
        overlayView.dropShadow()
    
        labelView.frame = CGRectMake(0, 0, 200, 30)
        labelView.text = "لطفا شکیبا باشید."
        labelView.textAlignment = .right
        labelView.textColor = .darkGray
        labelView.center = CGPoint(x: overlayView.bounds.width / 2,y: 30)
        labelView.font = UIFont(name: "IRANSansMobile",
                                 size: 16.0)
        overlayView.addSubview(labelView)
        //IranYekanRD
        activityIndicator.frame = CGRectMake(0, 0, 60, 60)
        activityIndicator.style = .gray
        activityIndicator.center = CGPoint(x: 260,y: 65)
        overlayView.addSubview(activityIndicator)
        
        
        labelViewEn.frame = CGRectMake(0, 0, 200, 30)
        labelViewEn.text = message
        labelViewEn.textColor = .gray
        labelView.textAlignment = .center
        labelViewEn.center = CGPoint(x: 170,y: 65)
        labelViewEn.font = UIFont(name: "IRANSansMobile",
                                   size: 14.0)
        overlayView.addSubview(labelViewEn)

        
        
//        labelView.center = CGPoint(x: overlayView.bounds.width / 2,y: overlayView.bounds.height / 2)

        view.addSubview(overlayView)
        
        
        overlayView.fadeIn()
        activityIndicator.startAnimating()
        
    }
    
    public func hideOverlayView() {
        overlayView.fadeOut()
        
        let delayInSeconds = 1.0
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
            self.activityIndicator.stopAnimating()
            self.overlayView.removeFromSuperview()
        }
    }
    
    func CGRectMake(_ x: CGFloat, _ y: CGFloat, _ width: CGFloat, _ height: CGFloat) -> CGRect {
        return CGRect(x: x, y: y, width: width, height: height)
    }
    
    func getCgColor(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat) ->UIColor {
        return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: 1.0)
    }
    
    
    
}




