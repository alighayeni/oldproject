//
//  courseModel.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/9/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
class courseModel: NSObject {
    
    var type:String?
    var course_id:String?
    var course_name:String?
    var gender:String?
    //var age_type:String?
    var session_count:String?
    var price_sale:String?
    var price_market:String?
    var discount_percent:String?
    var total_capacity:String?
    var remain_capacity:String?
    var pdescription:String?
    var status_sell:String?
    var start_datetime:String?
    var finish_datetime:String?
    var pattern_days:AnyObject?
    var week_day:String?
    var from:String?
    var to:String?
    
    var terms_of_use: String?
    
    init(_ type:String, _ course_id:String,_ course_name:String,_ gender:String,_ session_count:String,_ total_capacity:String,_ remain_capacity:String,_ description:String,_ price_sale:String,_ price_market:String,_ discount_percent:String,_ status_sell:String, _ start_datetime:String, _ finish_datetime:String, _ pattern_days: AnyObject, _ week_day: String, _ from: String, _ to: String, _ terms_of_use:String) {
        
        self.course_id = course_id
        self.course_name = course_name
        self.gender = gender
        self.session_count = session_count
        self.total_capacity = total_capacity
        self.remain_capacity = remain_capacity
        self.pdescription = description
        self.price_sale = price_sale
        self.price_market = price_market
        self.discount_percent = discount_percent
        self.start_datetime = start_datetime
        self.finish_datetime = finish_datetime
        self.pattern_days = pattern_days
        self.type = type
        self.week_day = week_day
        self.from = from
        self.to = to
        self.terms_of_use = terms_of_use
        
    }
    
    static func prepare(_ item:AnyObject, _ itemType: String) -> courseModel{
        let type = itemType
        let course_id = String(describing: item["course_id"] as AnyObject)
        let course_name = String(describing: item["course_name"] as AnyObject)
        let gender = String(describing: item["gender"] as AnyObject)
        let session_count = String(describing: item["session_count"] as AnyObject)
        let total_capacity = String(describing: item["total_capacity"] as AnyObject)
        let remain_capacity = String(describing: item["remain_capacity"] as AnyObject)
        let description = String(describing: item["description"] as AnyObject)
        let price_sale = String(describing: item["price_sale"] as AnyObject)
        let price_market = String(describing: item["price_market"] as AnyObject)
        let discount_percent = String(describing: item["discount_percent"] as AnyObject)
        let status_sell = String(describing: item["status_sell"] as AnyObject)
        let start_datetime = String(describing: item["start_datetime"] as AnyObject)
        let finish_datetime = String(describing: item["finish_datetime"] as AnyObject)
        let terms_of_use = String(describing: item["terms_of_use"] as AnyObject)

        let pattern_days = item["pattern_days"] as AnyObject
        
        let week_day = "-1"
        let from = "-1"
        let to = "-1"
        
        let instance = courseModel(type ,course_id, course_name, gender, session_count, total_capacity, remain_capacity, description, price_sale, price_market, discount_percent, status_sell, start_datetime, finish_datetime, pattern_days, week_day, from, to, terms_of_use)
        return instance
    }
    
    static func preparePattern(_ item:AnyObject, _ type: String) -> courseModel{
        let type = type
        let course_id = "-1"//String(describing: item["pack_id"] as AnyObject)
        let course_name = "-1"//String(describing: item["pack_name"] as AnyObject)
        let gender = "-1"//String(describing: item["gender"] as AnyObject)
        let session_count = "-1"//String(describing: item["session_count"] as AnyObject)
        let total_capacity = "-1"//String(describing: item["total_capacity"] as AnyObject)
        let remain_capacity = "-1"//String(describing: item["remain_capacity"] as AnyObject)
        let description = "-1"//String(describing: item["description"] as AnyObject)
        let price_sale = "-1"//String(describing: item["price_sale"] as AnyObject)
        let price_market = "-1"//String(describing: item["price_market"] as AnyObject)
        let discount_percent = "-1"//String(describing: item["discount_percent"] as AnyObject)
        let status_sell = "-1"//String(describing: item["status_sell"] as AnyObject)
        let start_datetime = "-1"//String(describing: item["session_expire_start"] as AnyObject)
        let finish_datetime = "-1"//String(describing: item["session_expire_finish"] as AnyObject)
        let pattern_days = "" as AnyObject//item["pattern_days"] as AnyObject
        let terms_of_use = "-1"//String(describing: item["terms_of_use"] as AnyObject)

        let week_day = String(describing: item["week_day"] as AnyObject)
        let from = String(describing: item["from"] as AnyObject)
        let to = String(describing: item["to"] as AnyObject)
        
        let instance = courseModel(type ,course_id, course_name,gender,session_count,total_capacity,remain_capacity, description,price_sale, price_market,discount_percent,status_sell, start_datetime, finish_datetime, pattern_days as AnyObject, week_day, from, to, terms_of_use)
        return instance
    }
}
