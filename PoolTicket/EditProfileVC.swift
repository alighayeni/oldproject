//
//  EditProfileVC.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 3/10/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyDrop
import PersianDatePicker
import SCLAlertView

class EditProfileVC: UIViewController, UITextFieldDelegate {
    
    var mName = "";
    var mMobileNumber = "";
    var mCredit = "";
    var mPersonalId = "";
    var mEmail = "";
    var mGender = "";
    var mBirth_day = "";
    var mBirth_month = "";
    var mBirth_year = "";
    var newPassword = "";
    var mCity = "";
    var mCity_id = "";
    var mState = "";
    var mState_id = "";
    var allow_sms = false;
    var allow_email = false;
    var type = ""

    
    var genderGroup = ["مرد","زن"]
    
    var selectedGenderId = ""
    var selectedBirthDate = ""

    @IBOutlet weak var persianDatePicker: PersianDatePickerView!
    @IBOutlet weak var buttomToolbar: UIToolbar!

    @IBAction func hideTheDatePicket(_ sender: Any) {
        self.showButtomBar(false)
    }

    var selectedCityAndState : cityModel?
    @IBOutlet weak var profileName:UITextField!
//    @IBOutlet weak var profileCredit:UITextField!
    @IBOutlet weak var profilePersonalId:UITextField!
    @IBOutlet weak var profileEmail:UITextField!
    @IBOutlet weak var profileNewPassword:UITextField!
    @IBOutlet weak var profileSelectedCityAndState:UIButton!
    
    @IBOutlet weak var ic_SmsBtn: UIImageView!
    
    @IBAction func setSendSmsBtn(sender: Any) {
        self.allow_sms = !self.allow_sms
        if self.allow_sms == false {
            self.ic_SmsBtn.image = UIImage.init(named: "ic_check_box_empty_dark")
        }else{
            self.ic_SmsBtn.image = UIImage.init(named: "ic_check_box_full_blue")
        }
    }
    
    @IBOutlet weak var ic_EmailBtn: UIImageView!
    
    @IBAction func setSendEmailBtn (sender: Any) {
        self.allow_email = !self.allow_email
        if self.allow_email == false {
            self.ic_EmailBtn.image = UIImage.init(named: "ic_check_box_empty_dark")
        }else{
            self.ic_EmailBtn.image = UIImage.init(named: "ic_check_box_full_blue")
        }
    }

    
    @IBAction func selectedCityAndStateAction(_ sender:Any) {
       
        openNewClass(classIdentifier: "ChooseStateAndCity")
  
    }
    
    @IBOutlet weak var profileGender:UIButton!
  
    @IBAction func selectedGenderAction(_ sender:Any) {
       
        if let value = sender as? UIButton {
            showDurationAlert(sender,self.genderGroup, value)
        }
        
    }
    
    @IBOutlet weak var profileSelectedBirthDate:UIButton!
    
    @IBAction func selectedBirthDateAction(_ sender:Any) {
        self.showButtomBar(true)
    }
    
    @IBAction func subMitNewData(_ sender:Any) {
        

        profileName.resignFirstResponder()
        profilePersonalId.resignFirstResponder()
        profileEmail.resignFirstResponder()
        
        guard var name = self.profileName.text as? String else{
            self.showDropMessage("لطفا فیلد نام را با مقداری صحیح پر کنید.")
            return
        }
        
        if name.trimmingCharacters(in: .whitespaces).count < 3 {
            self.showAlert("profileNameFail")
        }
        
        guard var personalId = self.profilePersonalId.text as? String else{
            self.showDropMessage("لطفا کد ملی صحیح را وارد کنید")
            return
        }
        
        if personalId.count != 10, personalId.count > 0 {
            self.showAlert("personalIdFail")
        }
        
        guard var email = self.profileEmail.text as? String else{
            self.showDropMessage("لطفا فیلد ایمیل صحیح را وارد کنید")
            return
        }
        
        if email.count > 0, !self.isValidEmail(email) {
            self.showAlert("emailAddressFail")
        }
        
        if let value = self.profileNewPassword.text as? String {
            if value.count > 5 {
            self.newPassword = value
            }
        }

        
        var tCityId = mCity_id
        var tState_Id = mState_id
        
            if let value = self.selectedCityAndState?.city_id {
                tCityId = value
            }
            if let value = self.selectedCityAndState?.state_id {
                tState_Id = value
            }
            
        var tGender = mGender
        if selectedGenderId != "" {
            tGender = selectedGenderId
        }
        
        self.UpdateProfile( name, personalId, email, self.selectedBirthDate, tCityId, tState_Id, tGender, self.newPassword)
        
        
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        profileName.delegate = self
        profilePersonalId.delegate = self
        profileEmail.delegate = self
        
        profileName.tag = 1
        self.doneWithKeyboard(profileName)
        
        profilePersonalId.tag = 2
        self.doneWithKeyboard(profilePersonalId)
        
        profileEmail.tag = 3
        self.doneWithKeyboard(profileEmail)
        
        self.doneWithKeyboard(profileNewPassword)

        persianDatePicker.font = UIFont(name: "IRANSansMobile", size: 18)
        //print(persianDatePicker)
        
        persianDatePicker.onChange = { (year, month, day) in
            self.profileSelectedBirthDate.setTitle(self.replaceString("\(day) \(self.getJalaliMonthName(month-1)) \(year)"), for: .normal)
            self.selectedBirthDate = "\(year)/\(month)/\(day)"
        }
        
        self.showButtomBar(false)
        
        self.getProfile()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setTheSelectedCityAndState()
    }
    
    // get data from profile
    func getProfile() -> Void {
        LoadingOverlay.shared.showOverlay(view: self.view)
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.PROFILE,"client_id=\(client_id)&access_token=\(access_token)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    //print(json)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                if let profile = item["profile"] as? [String: AnyObject] {
                                    if let value = profile["name"] as? String {
                                        self.mName = value
                                    }
                                    if let value = profile["mobile"] as? String {
                                        self.mMobileNumber = value
                                    }
                                    if let value = profile["credit"] as? Int {
                                        self.mCredit = String(value) + " تومان"
                                    }
                                    
                                    if let value = profile["code_meli"] as? String {
                                        self.mPersonalId = value
                                    }
                                    
                                    if let value = profile["email"] as? String {
                                        self.mEmail = value
                                    }
                                    
                                    if let value = profile["gender"] as? Int64 {
                                        self.mGender = String(value)
                                    }
                                    
                                    if let value = profile["birth_day"] as? String {
                                        self.mBirth_day = value
                                    }
                                    
                                    if let value = profile["birth_month"] as? String {
                                        self.mBirth_month = value
                                    }
                                    
                                    if let value = profile["birth_year"] as? String {
                                        self.mBirth_year = value
                                    }
                                    
                                    if let value = profile["city"] as? String {
                                        self.mCity = value
                                    }
                                    
                                    if let value = profile["city_id"] as? Int {
                                        self.mCity_id = String(value)
                                    }
                                    
                                    if let value = profile["state"] as? String {
                                        self.mState = value
                                    }
                                    
                                    if let value = profile["state_id"] as? Int {
                                        self.mState_id = String(value)
                                    }
                                    
                                    if let value = profile["allow_email"] as? Bool {
                                        self.allow_email = value
                                    }
                                    
                                    if let value = profile["allow_sms"] as? Bool {
                                        self.allow_sms = value
                                    }
                                    
                                }
                            }
                        }
                    }
                    
                    
                }
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                LoadingOverlay.shared.hideOverlayView()
                if WSResponseStatus {
                    
                  self.profileName.text =  self.mName
                  self.profilePersonalId.text = self.mPersonalId
                  self.profileEmail.text = self.mEmail
                  let tData = (self.mCity) + " (" + (self.mState) + ")"
                  if tData.count > 0 {
                     self.profileSelectedCityAndState.setTitle(tData, for: .normal)
                     self.profileSelectedCityAndState.setTitleColor(UIColor.darkGray, for: .normal)
                  }
                  if self.mGender != "null" {
                    self.setGender(gender: self.mGender)
                  }
                  
                  if self.mBirth_year != "null", self.mBirth_month != "null", self.mBirth_day != "null" {
                    if let value01 = Int(self.mBirth_year) as? Int,  let value02 = Int(self.mBirth_month) as? Int, let value03 = Int(self.mBirth_month) as? Int {
                        self.persianDatePicker.year = value01
                        self.persianDatePicker.month = value02
                        self.persianDatePicker.day = value03
                        let tData = self.getJalaliMonthName(Int(self.mBirth_month)!-1)
                      self.profileSelectedBirthDate.setTitle(self.replaceString("\(self.mBirth_day) \(tData) \(self.mBirth_year)"), for: .normal)
                    }
                  }
                  
                    if self.allow_sms == false {
                        self.ic_SmsBtn.image = UIImage.init(named: "ic_check_box_empty_dark")
                    }else{
                        self.ic_SmsBtn.image = UIImage.init(named: "ic_check_box_full_blue")
                    }
                    
                    if self.allow_email == false {
                        self.ic_EmailBtn.image = UIImage.init(named: "ic_check_box_empty_dark")
                    }else{
                        self.ic_EmailBtn.image = UIImage.init(named: "ic_check_box_full_blue")
                    }
                    
                    
                }else{
                    //try again
                    //Drop.down("username or password is wrong! please try again")
                }
            })
        }
    }
    
    func setGender(gender : String){
        if gender == "1" {
            self.profileGender.setTitle("زن", for: .normal)
        }else{
            self.profileGender.setTitle("مرد", for: .normal)
        }
    }
    
    // get data from profile
    func UpdateProfile(_ name: String, _ code_meli: String, _ email: String, _ birthday: String, _ city_id:String, _ state_id:String, _ gender:String, _ password: String) -> Void {
        LoadingOverlay.shared.showOverlay(view: self.view)
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        var passQuery = ""
        if password.count > 0 {
            passQuery = "&password=" + password
        }
        let index = wsPOST.init(Constants.UPDATE_PROFILE,"client_id=\(client_id)&access_token=\(access_token)&name=\(name)&code_meli=\(code_meli)&email=\(email)&birth_date=\(birthday)&city_id=\(city_id)&state_id=\(state_id)&gender=\(gender)&allow_sms=\(String(self.allow_sms))&allow_email=\(String(self.allow_email))" + passQuery)
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    
                    
                    
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                            }
                        }
                    }
                    
                    
                }
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                LoadingOverlay.shared.hideOverlayView()
                if WSResponseStatus {
                    self.showDropMessage("اطلاعات کاربری شما با موفقیت ویرایش شد.")
                    self.dismiss(animated: true, completion: nil)
                    //self.getProfile()
                }else{
                }
            })
        }
    }
    
    @IBAction func close(){
        self.dismiss(animated: true, completion: nil)
    }
    
    // after user selected the state and city
    func setTheSelectedCityAndState(){
        if shareData.shared.selectedBirthModel != nil {
            self.selectedCityAndState = shareData.shared.selectedBirthModel
            let tData = (self.selectedCityAndState?.city_name)! + " (" + (self.selectedCityAndState?.state_name)! + ")"
            self.profileSelectedCityAndState.setTitle(tData, for: .normal)
            
        }
    }
    
    //*************** drop down **************************//
    func showDurationAlert(_ sender: Any, _ strArray: [String], _ btn: UIButton) {
        // var durations = ["bank", "doctor", "restaurant"]
        let actions = strArray.map { seconds in
            return UIAlertAction(title: "\(seconds)", style: .default) { _ in
                //Drop.down(self.sampleText(), state: .default, duration: seconds)
                //print(seconds)
                self.selectedGenderId = String(strArray.firstIndex(of: seconds)!)
                //self.tSelectedCategory = seconds
                btn.setTitle( seconds, for: .normal)
                //selectedCategoryId = self.getCategoryId(name: seconds)
                //print(selectedCategoryId)
            }
        }
        
        
        let cancelAction = UIAlertAction(title: "بستن", style: .cancel, handler: nil)
        
        let controller = UIAlertController(title: "جنسیت", message: nil, preferredStyle: .actionSheet)
        for action in [cancelAction] + actions {
            controller.addAction(action)
            self.selectedGenderId = ""
        }
        showAlert(controller, sourceView: sender as? UIView)
    }
    
    func showAlert(_ controller: UIAlertController, sourceView: UIView? = nil) {
        
        if UIDevice.current.userInterfaceIdiom == .pad  {
            if let sourceView = sourceView {
                let rect = sourceView.convert(sourceView.bounds, to: view)
                controller.popoverPresentationController?.sourceView = view
                controller.popoverPresentationController?.sourceRect = rect
            }
        }
        
        present(controller, animated: true, completion: nil)
    }
    
    func upAllDrops(_ sender: AnyObject) {
        if let hidden = navigationController?.isNavigationBarHidden {
            navigationController?.setNavigationBarHidden(!hidden, animated: true)
        }
        
        Drop.upAll()
    }
    
//    func sampleText() -> String {
//        let text = ""
//
//        return text
//    }
    //*************** drop down **************************//
    
    func showButtomBar(_ status: Bool) {
        
        if status {
            
            UIView.transition(with: persianDatePicker, duration: 0.4, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                self.persianDatePicker.isHidden = false;
                
                UIView.transition(with: self.buttomToolbar, duration: 0.1, options: UIView.AnimationOptions.transitionFlipFromBottom, animations: {
                    self.buttomToolbar.isHidden = false;
                }, completion: nil)
                
                
            }, completion: nil)
            
            
            
        }else{
            UIView.transition(with: buttomToolbar, duration: 0.1, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                self.buttomToolbar.isHidden = true;
                
                UIView.transition(with: self.persianDatePicker, duration: 0.4, options: UIView.AnimationOptions.transitionFlipFromBottom, animations: {
                    self.persianDatePicker.isHidden = true;
                }, completion: nil)
                
            }, completion: nil)
            
            self.type = ""
        }
        
    }

    
    // ****************** SoftKeyBoard ************************
  
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField
        {

        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
    
    func doneWithKeyboard(_ sender: UITextField){
        
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        keyboardToolbar.isTranslucent = true
        keyboardToolbar.barTintColor = UIColor.white
        
        let button = UIButton(type: .system)
        button.setTitle("بستن", for: .normal)
        button.sizeToFit()
        button.titleLabel?.font = UIFont(name: "IRANSansMobile", size: 12)
        button.tintColor = UIColor.black
        button.addTarget(self, action: #selector(closeKeyboard(_:)), for: .touchUpInside)
        
        
        let btnUp = UIButton(type: .custom)
        btnUp.setImage(UIImage(named: "ic_keyboard_arrow_up"), for: .normal)
        btnUp.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        btnUp.tag = sender.tag
        btnUp.addTarget(self, action: #selector(textFieldUp), for: .touchUpInside)
        let itemUp = UIBarButtonItem(customView: btnUp)
        
        let btnDown = UIButton(type: .custom)
        btnDown.setImage(UIImage(named: "ic_keyboard_arrow_down"), for: .normal)
        btnDown.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        btnDown.tag = sender.tag
        btnDown.addTarget(self, action: #selector(textFieldDown), for: .touchUpInside)
        let itemDown = UIBarButtonItem(customView: btnDown)
        
        
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        //keyboardToolbar.items = [flexBarButton,UIBarButtonItem(customView: button)]
        keyboardToolbar.items = [itemDown,itemUp,flexBarButton,UIBarButtonItem(customView: button)]
        
        if let value = sender as? UITextField {
            value.inputAccessoryView = keyboardToolbar
        }else if let value = sender as? UITextView {
            value.inputAccessoryView = keyboardToolbar
        }
        
    }
    
    

    
    @objc func textFieldUp(_ sender: Any)-> Void {
        
        let tObject = (sender as AnyObject).tag
        if let value = tObject as? Int {
            switch value
            {
            case 1:
                profileName.resignFirstResponder()
                break
            case 2:
                profileName.becomeFirstResponder()
                profilePersonalId.resignFirstResponder()
                break
            case 3:
                profilePersonalId.becomeFirstResponder()
                profileEmail.resignFirstResponder()
                break
            default:
                print("")
            }
        }
    }
    
    @objc func textFieldDown(_ sender: Any)-> Void {
        
        let tObject = (sender as AnyObject).tag
        if let value = tObject as? Int {
            switch value
            {
            case 1:
                profileName.resignFirstResponder()
                profilePersonalId.becomeFirstResponder()
                break
            case 2:
                profilePersonalId.resignFirstResponder()
                profileEmail.becomeFirstResponder()
                break
            case 3:
                profileEmail.resignFirstResponder()
                break
            default:
                print("")
            }
        }
    }
    
    func showAlert(_ type: String) {
        
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "IRANSansMobile", size: 17)!,
            kTextFont: UIFont(name: "IRANSansMobile", size: 14)!,
            kButtonFont: UIFont(name: "IRANSansMobile", size: 14)!,
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        
        switch type {
        case "fail":
            alertView.addButton("بستن") {
            }
            alertView.showError("اخطار:", subTitle: "لطفا اطلاعات وارد شده را بررسی کنید و دوباره تلاش کنید")
            break
        case "profileNameFail":
            alertView.addButton("بستن") {
            }
            alertView.showNotice("اخطار:", subTitle: "فیلد نام اشتباه وارد شده است . لطفا بررسی بفرمائید.")
            break
        case "personalIdFail":
            alertView.addButton("بستن") {
            }
            alertView.showNotice("اخطار:", subTitle: "کد ملی وارد شده استباه است")
            break
            
        case "emailAddressFail":
            alertView.addButton("بستن") {
            }
            alertView.showNotice("اخطار:", subTitle: "ایمیل آدرس وارد شده اشتباه است")
            break
            
        case "failPasswordCount":
            alertView.addButton("بستن") {
            }
            alertView.showError("اخطار:", subTitle: "تعداد کاراکتر های مجاز برای رمز عبور جدید بیشتر از ۵ کاراکتر می باشد")
            break
        default:
            print("")
        }
        
    }
    
    func isValidEmail(_ testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}
