//
//  poolsById.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 3/22/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation

//class poolsById: NSObject {
//
//
//    var pool_id:String?
//    var name:String?
//    var address:String?
//    var capacity:String?
//    var lat:String?
//    var lng: String?
//    var status_sell: String?
//    var status_sell_ticket: String?
//    var status_sell_course: String?
//    var status_sell_pack: String?
//    var pDescription: String?
//    var terms_of_use_enable: String?
//    var pool_introducing_enable: String?
//    var tel: String?
//    var gender: String?
//    var comment: String?
//    var state_name: String?
//    var city_name: String?
//    var area_name: String?
//    var city_id: String?
//    var area_id: String?
//    var default_image: String?
//    var state_id: String?
//    var max_discount: String?
//    var is_favorite: String?
//    var rating: String?
//    var minimum_sale_price: String?
//    var minimum_market_price: String?
//    var share_link: String?
//    var share_description: String?
//    var ratings: AnyObject?
//
//    required init(_ pool_id: String, _ name: String, _ address:String,_ capacity:String,_ lat:String,_ lng:String,_ status_sell:String,_ status_sell_ticket:String,_ status_sell_course:String,_ status_sell_pack:String,_ pDescription:String,_ terms_of_use_enable:String, _ pool_introducing_enable:String, _ tel:String, _ gender:String, _ comment:String, _ state_name:String, _ city_name:String, _ area_name:String, _ city_id:String, _ area_id:String, _ default_image:String, _ state_id:String, _ max_discount:String, _ is_favorite:String, _ rating:String, _ minimum_sale_price: String, _ minimum_market_price:String, _ share_link:String, _ share_description:String,_ ratings: AnyObject?) {
//        self.pool_id = pool_id
//        self.name = name
//        self.address = address
//        self.capacity = capacity
//        self.lat = lat
//        self.lng = lng
//        self.status_sell = status_sell
//        self.status_sell_ticket = status_sell_ticket
//        self.status_sell_course = status_sell_course
//        self.status_sell_pack = status_sell_pack
//        self.pDescription = pDescription
//        self.terms_of_use_enable = terms_of_use_enable
//        self.pool_introducing_enable = pool_introducing_enable
//        self.tel = tel
//        self.gender = gender
//        self.comment = comment
//        self.state_name = state_name
//        self.city_name = city_name
//        self.area_name = area_name
//        self.city_id = city_id
//        self.area_id = area_id
//        self.default_image = default_image
//        self.state_id = state_id
//        self.max_discount = max_discount
//        self.is_favorite = is_favorite
//        self.minimum_sale_price = minimum_sale_price
//        self.is_favorite = is_favorite
//        self.minimum_sale_price = minimum_sale_price
//        self.minimum_market_price = minimum_market_price
//        self.share_link = share_link
//        self.rating = rating
//        self.share_description = share_description
//        self.ratings = ratings
//
//    }
//
//    static func prepare(_ item:AnyObject) -> poolsById{
//
//        let pool_id = String(describing: item["pool_id"] as AnyObject)
//        let name = String(describing: item["name"] as AnyObject)
//        let address = String(describing: item["address"] as AnyObject)
//        let capacity = String(describing: item["capacity"] as AnyObject)
//        let lat = String(describing: item["lat"] as AnyObject)
//        let lng = String(describing: item["lng"] as AnyObject)
//        let status_sell = String(describing: item["status_sell"] as AnyObject)
//        let status_sell_ticket = String(describing: item["status_sell_ticket"] as AnyObject)
//        let status_sell_course = String(describing: item["status_sell_course"] as AnyObject)
//        let status_sell_pack = String(describing: item["status_sell_pack"] as AnyObject)
//        let pDescription = String(describing: item["description"] as AnyObject)
//        let terms_of_use_enable = String(describing: item["terms_of_use_enable"] as AnyObject)
//        let pool_introducing_enable = String(describing: item["pool_introducing_enable"] as AnyObject)
//        let tel = String(describing: item["tel"] as AnyObject)
//        let gender = String(describing: item["gender"] as AnyObject)
//        let comment = String(describing: item["comment"] as AnyObject)
//        let state_name = String(describing: item["state_name"] as AnyObject)
//        let city_name = String(describing: item["city_name"] as AnyObject)
//        let area_name = String(describing: item["area_name"] as AnyObject)
//        let city_id = String(describing: item["city_id"] as AnyObject)
//        let area_id = String(describing: item["area_id"] as AnyObject)
//        let is_favorite = String(describing: item["is_favorite"] as AnyObject)
//        let default_image = String(describing: item["default_image"] as AnyObject)
//        let state_id = String(describing: item["state_id"] as AnyObject)
//        var max_discount = String(describing: item["max_discount"] as AnyObject)
//        if let value = Float(max_discount) as? Float {
//            if let intValue = Int(value) as? Int {
//                max_discount = String(describing: intValue as AnyObject)
//            }
//        }
//        let rating = String(describing: item["rating"] as AnyObject)
//        let minimum_sale_price = String(describing: item["minimum_sale_price"] as AnyObject)
//        let minimum_market_price = String(describing: item["minimum_market_price"] as AnyObject)
//        let share_link = String(describing: item["share_link"] as AnyObject)
//        let share_description = String(describing: item["share_description"] as AnyObject)
//        let ratings = item["ratings"] as? AnyObject
//
//
//
//        let instance = poolsById(pool_id, name, address, capacity, lat, lng, status_sell, status_sell_ticket, status_sell_course, status_sell_pack, pDescription, terms_of_use_enable, pool_introducing_enable, tel, gender, comment, state_name, city_name, area_name, city_id, area_id, default_image, state_id, max_discount, is_favorite, rating, minimum_sale_price, minimum_market_price, share_link, share_description, ratings)
//
//        return instance
//    }
//
//
//
//
////    "gender": Number, // 0 => male, 1 => female, 2 => all
////    "comment": Boolean,
////    "state_name": String,
////    "city_name": String,
////    "area_name": String|Null,
////    "state_id": Number,
////    "city_id": Number,
////    "area_id": Number|null,
////    "default_image": Url|null,
////    "max_discount": String,
////    "is_favorite": Boolean,
////    "rating": Float,
//
//}
