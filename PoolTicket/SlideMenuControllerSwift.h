//
//  SlideMenuControllerSwift.h
//  drawerSample
//
//  Created by Ali Ghayeni on 11/30/16.
//  Copyright © 2016 Ali Ghayeni. All rights reserved.
//

#ifndef SlideMenuControllerSwift_h
#define SlideMenuControllerSwift_h


#endif /* SlideMenuControllerSwift_h */




//
//  SlideMenuControllerSwift.h
//  SlideMenuControllerSwift
//
//  Created by Kosuke Matsuda on 2015/10/31.
//  Copyright © 2015年 Yuji Hato. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SlideMenuControllerSwift.
FOUNDATION_EXPORT double SlideMenuControllerSwiftVersionNumber;

//! Project version string for SlideMenuControllerSwift.
FOUNDATION_EXPORT const unsigned char SlideMenuControllerSwiftVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SlideMenuControllerSwift/PublicHeader.h>
