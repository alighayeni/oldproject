//
//  TextTypeIndexReadMoreVC.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/18/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class TextTypeIndexReadMoreVC: UIViewController, WKNavigationDelegate {
    
    @IBOutlet weak var contentView:UIView!
    var webView: WKWebView!
    var theData = ""
    var urlObservation: NSKeyValueObservation?
    var tData: textModel?
    var targetUrlAddress: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        let configuration = WKWebViewConfiguration()
        configuration.preferences = preferences
        
        self.webView = WKWebView(frame: self.contentView.bounds, configuration: configuration)
        self.webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.webView.navigationDelegate = self
        
        self.contentView.addSubview(self.webView)
        
        if let value = targetUrlAddress as? String {
            let url = URL(string: value)!
            webView.allowsBackForwardNavigationGestures = true
            webView.load(URLRequest(url: url))
            urlObservation = webView.observe(\.url, changeHandler: { (webView, change) in
                print("Web view URL changed to \(webView.url?.absoluteString ?? "Empty")")
            })
            webView.addObserver(self, forKeyPath: #keyPath(WKWebView.navigationDelegate), options: .new, context: nil)
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.font.rawValue: UIFont(name: "IRANSansMobile", size: 14),NSAttributedString.Key.foregroundColor.rawValue:UIColor.white])
        self.title = ""
        self.navigationController?.navigationItem.leftBarButtonItem?.setTitleTextAttributes(convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont(name: "IRANSansMobile", size: 16)! ,convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor):UIColor.white]), for: UIControl.State.normal)
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.webView.removeObserver(self, forKeyPath: #keyPath(WKWebView.navigationDelegate))
        self.webView = nil
        
    }
    
    @IBAction func close(){
        self.dismiss(animated: true, completion: nil)
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}
