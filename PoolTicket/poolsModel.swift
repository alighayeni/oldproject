//
//  ViewController.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 1/24/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import UIKit

class poolsModel: NSObject {

    
    var slide_type:String?
    var pool_id:String?
    var name:String?
    var max_discount:String?
    var default_image:String?
    var area_name: String?
    var gender: String?
    var rating: String?
    var minimum_sale_price: String?
    var minimum_market_price: String?

    
    
    init(slide_type:String, pool_id:String, name:String, max_discount:String, default_image:String, area_name:String, gender:String, rating:String, minimum_sale_price: String, minimum_market_price:String) {
        self.pool_id=pool_id
        self.name=name
        self.slide_type=slide_type
        self.max_discount=max_discount
        self.default_image=default_image
        self.area_name=area_name
        self.gender=gender
        self.rating=rating
        self.minimum_sale_price=minimum_sale_price
        self.minimum_market_price=minimum_market_price
    }
    
    static func prepare(_ item:AnyObject) -> poolsModel{
        
        let pool_id = String(describing: item["pool_id"] as AnyObject)
        let slide_type = String(describing: item["slide_type"] as AnyObject)
        let name = String(describing: item["name"] as AnyObject)
        var max_discount = String(describing: item["max_discount"] as AnyObject)
        if let value = Float(max_discount) {
            if let intValue = Int(value) as? Int {
               max_discount = String(describing: intValue as AnyObject)
            }
        }
        let default_image = String(describing: item["default_image"] as AnyObject)
        let area_name = String(describing: item["area_name"] as AnyObject)
        let gender = String(describing: item["gender"] as AnyObject)
        let rating = String(describing: item["rating"] as AnyObject)
        let minimum_sale_price = String(describing: item["minimum_sale_price"] as AnyObject)
        let minimum_market_price = String(describing: item["minimum_market_price"] as AnyObject)

        
        let instance = poolsModel(slide_type:slide_type,pool_id:pool_id, name:name, max_discount:max_discount, default_image:default_image, area_name:area_name,gender:gender,rating:rating,minimum_sale_price:minimum_sale_price,minimum_market_price:minimum_market_price)
        
        return instance
    }

    
   
}

