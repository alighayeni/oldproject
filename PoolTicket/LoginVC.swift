//
//  LoginVC.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/14/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import SCLAlertView

class LoginVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var username:UITextField!
    @IBOutlet weak var password:UITextField!
    var connectionStatus = false
    @IBOutlet weak var scrollView:UIScrollView!
    var activeField: UITextField?

    @IBOutlet weak var showAndHidePassword: UIButton!
    
    @IBAction func showAndHidePasswordAction(_ Sender: Any){
        
        if !password.isSecureTextEntry  {
            password.isSecureTextEntry = true
            self.showAndHidePassword.setImage(UIImage(named: "ic_pass_show")!, for: UIControl.State.normal)
        }else{
            password.isSecureTextEntry = false
            self.showAndHidePassword.setImage(UIImage(named: "ic_pass_hide")!, for: UIControl.State.normal)

        }
        
    }
    
    @IBAction func loginAction(_ Sender: Any){
       
        if !connectionStatus {

            guard let username = username.text else {
                self.showDropMessage("missing username")
                return
            }
            guard let password = password.text else {
                self.showDropMessage("missing password")
                return
            }
            self.getLogin(username, password)
            
        }else{
            showDropMessage("لطفا کمی صبر کنید.")
        }
    }
    
    @IBAction func forgetPassword(_ Sender: Any){
        pushNewClass(classIdentifier: "ForgetPassVC")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.font.rawValue: UIFont(name: "IRANSansMobile", size: 14),NSAttributedString.Key.foregroundColor.rawValue:UIColor.white])
        let customFont = UIFont(name: "IRANSansMobile", size: 13.0)!
        UIBarButtonItem.appearance().setTitleTextAttributes(convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.font): customFont]), for: .normal)
        self.title = "ورود"

        
        username.delegate = self
        username.tag = 1
        password.delegate = self
        password.tag = 2
        doneWithKeyboard(username)
        doneWithKeyboard(password)
        
        if let value = shareData.shared.loginInputPhoneNumber as? String {
            self.username.text = value
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        registerForKeyboardNotifications()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        deregisterFromKeyboardNotifications()
    }
    
    func getLogin(_ username: String, _ password: String) -> Void {
        
        self.connectionStatus = true
        LoadingOverlayV2.sharedV2.showOverlayV2(view: self.view, message: "بررسی تلفن همراه و رمز عبور")

        var deviceId = ""
        if let value = KeychainService.loadDeviceId(), value != nil {
            deviceId = String(value)
        }
        
        let params : [String : String]  = ["platform_type" : "mobile_application", "os_type" : "ios", "os_version": getOSVersion(), "code_version": self.getBuildVersion(), "build_version": self.getCodeVersion(), "device_name": UIDevice().type.rawValue, "imei": deviceId]

        
        let client_id = Constants.clientId
        let front = "front";
        let grant_type = "password";
        let index = wsPOST.init(Constants.TOKEN,"client_id=\(client_id)&grant_type=\(grant_type)&username=\(username)&password=\(password)&scope=\(front)&metadata=\(jsonToString(json: (params as? AnyObject)!))&device_id=\(deviceId)")

       
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                // set the username for Onsignal and Analytics Tag
                                self.setUserName(username)

                                if let value = item["access_token"] as? String {
                                    self.setUserAccessToken(value)
                                }
                                
                                if let value = item["refresh_token"] as? String {
                                    self.setUserRefreshToken(value)
                                }
                                
                                if let value = item["id"] as? Int {
                                    self.setUserId(String(value))
                                }
                                
                                do {
                                    shareData.shared.RTUModel  = refreshTokenUsersModel.prepare(item["user"] as AnyObject)
                                }catch{
                                    fatalError("this is error \(error)")
                                }
                                
                                if let user = item["user"] as? [String: AnyObject] {
                                    
                                    if let value = user["name"] as? String {
                                        UserDefaults.standard.set(value, forKey: UserDefaults.Keys.profileName)
                                    }
                                    if let value = user["mobile"] as? String {
                                        UserDefaults.standard.set(value, forKey: UserDefaults.Keys.profileMobile)
                                    }
                                    if let value = user["email"] as? String {
                                        UserDefaults.standard.set(value, forKey: UserDefaults.Keys.profileEmail)
                                    }
                                }
                                
                            }
                        }
                    }
                    
                }
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                self.connectionStatus = false
                LoadingOverlayV2.sharedV2.hideOverlayView()
                

                if WSResponseStatus {
                    if username.count > 0 {
                        // register the use in main thread in chabouk push.
                        //AppDelegate()._manager?.registerUser(username)
                    }
                    self.showAlert("success")
                    self.dismiss(animated: true, completion: nil)
                }else{
                    self.showAlert("fail")
                }
            })
            
        }
        
        
    }
    

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField
        {
                    case username:
                        username.resignFirstResponder()
                        password.becomeFirstResponder()
                        break
                    case password:
                        password.resignFirstResponder()
                        username.becomeFirstResponder()
                        break
        
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
    func doneWithKeyboard(_ sender: UITextField){
        
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        keyboardToolbar.isTranslucent = true
        keyboardToolbar.barTintColor = UIColor.white
        
        let button = UIButton(type: .system)
        button.setTitle("بستن", for: .normal)
        button.sizeToFit()
        button.titleLabel?.font = UIFont(name: "IRANSansMobile", size: 12)
        button.tintColor = UIColor.black
        button.addTarget(self, action: #selector(closeKeyboard(_:)), for: .touchUpInside)
        
        
                let btnUp = UIButton(type: .custom)
                btnUp.setImage(UIImage(named: "ic_keyboard_arrow_up"), for: .normal)
                btnUp.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
                btnUp.tag = sender.tag
                btnUp.addTarget(self, action: #selector(textFieldUp), for: .touchUpInside)
                let itemUp = UIBarButtonItem(customView: btnUp)
        
                let btnDown = UIButton(type: .custom)
                btnDown.setImage(UIImage(named: "ic_keyboard_arrow_down"), for: .normal)
                btnDown.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
                btnDown.tag = sender.tag
                btnDown.addTarget(self, action: #selector(textFieldDown), for: .touchUpInside)
                let itemDown = UIBarButtonItem(customView: btnDown)
        
        
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        //keyboardToolbar.items = [flexBarButton,UIBarButtonItem(customView: button)]
        keyboardToolbar.items = [itemDown,itemUp,flexBarButton,UIBarButtonItem(customView: button)]
        
        if let value = sender as? UITextField {
            value.inputAccessoryView = keyboardToolbar
        }else if let value = sender as? UITextView {
            value.inputAccessoryView = keyboardToolbar
        }
        
    }
    
     @objc func textFieldUp(_ sender: Any)-> Void {
     
     let tObject = (sender as AnyObject).tag
         if let value = tObject as? Int {
             switch value
             {
             case 1:
             username.resignFirstResponder()
             password.becomeFirstResponder()
             break
             case 2:
             password.resignFirstResponder()
             username.becomeFirstResponder()
             break
             default:
             print("")
             }
         }
     }
     
     @objc func textFieldDown(_ sender: Any)-> Void {
     
     let tObject = (sender as AnyObject).tag
         if let value = tObject as? Int {
             switch value
             {
             case 1:
             username.resignFirstResponder()
             password.becomeFirstResponder()
             break
             case 2:
             password.resignFirstResponder()
             username.becomeFirstResponder()
             break
             default:
             print("")
             }
         }
     }
 
    
    //************** softkeyboard tool bar **********************//
    /*keyboard automatic scroll*/
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.isScrollEnabled = true
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: keyboardSize!.height, right: 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.activeField {
            if (!aRect.contains(activeField.frame.origin)){
                self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: -keyboardSize!.height, right: 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollView.isScrollEnabled = false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }

    func showAlert(_ type: String) {
        
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "IRANSansMobile", size: 17)!,
            kTextFont: UIFont(name: "IRANSansMobile", size: 14)!,
            kButtonFont: UIFont(name: "IRANSansMobile", size: 14)!,
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        
        switch type {
        case "success":
            alertView.addButton("بستن") {
            }
            alertView.showInfo("پیام:", subTitle: "شما با موفقیت وارد شده اید.")
            break
        case "fail":
            alertView.addButton("بستن") {
            }
            alertView.showError("اخطار:", subTitle: "لطفا اطلاعات وارد شده را بررسی کنید و دوباره تلاش کنید")
            break
        default:
            print("")
        }
       
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}
