//
//  AppDelegate.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 1/24/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import UIKit
import CoreData
import OneSignal
import GoogleMaps
import Firebase
import Fabric
import UserNotifications
import AdpPushClient

//import Adjust

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, PushClientManagerDelegate {

    var window: UIWindow?
    static var changeFilterValue: Bool = false
    let _manager = PushClientManager.default()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        stylizeNavigationFont()
        
        // google map token
        GMSServices.provideAPIKey("AIzaSyAP4tutLmWg_4Vl8sDsdj14mZjVMUZK6xg")//AIzaSyC1xhrkAxbYCFnAj5_TrT9HH00_JPsWquc

        // set firebase
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
        Fabric.sharedSDK().debug = false
      
        
        // internet connection
        do {
            Network.reachability = try Reachability(hostname: "www.google.com")
            do {
                try Network.reachability?.start()
            } catch let error as Network.Error {
                print(error)
            } catch {
                print(error)
            }
        } catch {
            print(error)
        }
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .sound, .badge]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: {(_ granted: Bool, _ error: Error?) -> Void in
            })
            UIApplication.shared.registerForRemoteNotifications()
        } else {
            // Fallback on earlier versions
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
      
        application.registerForRemoteNotifications()
        
        // set adjust for the app
        setAdjustConfig()
        
        
        //********************Chabok Push**********************//
        //true connects to Sandbox environment
        //false connects to Production environment
        PushClientManager.setDevelopment(false)
        //Reset badge and clear notification when app launched.
        PushClientManager.resetBadge()
        _manager?.addDelegate(self)
        _manager?.enableLog = true
        //Initialize with credential keys
        let state = _manager?.registerApplication("poolticket",                    //based on your environment
            apiKey: "ad4655fbbb1f362ff5502efe44c1c38cf9852db0",         //based on your environment
            userName: "po0lT!ck3tCh@Bok",  //based on your environment
            password: "cH@b0k+p00ltIck3t")  //based on your environment
        if _manager?.application(application, didFinishLaunchingWithOptions: launchOptions) == true {
            print("Launched by tapping on notification")
        }
        if state == true {
            print("Initialized")
        } else {
            print("Not initialized")
        }
        // register in pushkit with User id OR as a Guest 
        if let value = AppDelegate()._manager?.userId {
            AppDelegate()._manager?.registerUser(value)
        } else {
            AppDelegate()._manager?.registerAsGuest()
        }
        //********************Chabok Push**********************//

      
        return true
    }
    
    
    func stylizeNavigationFont() {
        //custom font
        let customFont = UIFont(name: "IRANSansMobile", size: 14)!  //note we're force unwrapping here
        
        //navigation bar coloring:
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().barTintColor = UIColor(red: 43/256, green: 163/256, blue: 215/256, alpha: 1)
        
        //unique text style:
        var fontAttributes: [String: Any]
        fontAttributes = [convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.white, convertFromNSAttributedStringKey(NSAttributedString.Key.font): customFont]
        
        //navigation bar & navigation buttons font style:
        UINavigationBar.appearance().titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary(fontAttributes)
        UIBarButtonItem.appearance().setTitleTextAttributes(convertToOptionalNSAttributedStringKeyDictionary(fontAttributes), for: .normal)
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
//        print(url.absoluteString)
        
        var urlObservation: NSKeyValueObservation?
        var pReservationResponse: PRFR?
        var purchase_history_id: String?
        
        var status = ""
        var voucher = ""
        var reservation_status = ""
        var reservation_id = ""
        
        var tData = url.absoluteString ?? "Empty"
        if tData.contains("org.poolticket://pack-reservation?") {
            tData = tData.replacingOccurrences(of: "org.poolticket://pack-reservation?", with: "")
            let tData2 = tData.split(separator: "&")
            for item in tData2 {
                var tData3 = item.split(separator: "=")
                switch tData3[0] {
                case "status":
                    status = String(tData3[1])
                    
                    if status == "1" {
                        // track Pack Payment success event in ios
//                        let event = ADJEvent.init(eventToken: "xpfjrr")
//                        Adjust.trackEvent(event)
                    }
                    
                    break;
                case "voucher":
                    voucher = String(tData3[1])
                    break;
                case "reservation_status":
                    reservation_status = String(tData3[1])
                    break;
                case "reservation_id":
                    reservation_id = String(tData3[1])
                    break;
                case "purchase_history_id":
                    purchase_history_id = String(tData3[1])
                    
                    if purchase_history_id != nil, (purchase_history_id?.count)! > 0 {
                        shareData.shared.purchase_id = purchase_history_id
                    }else{
                        shareData.shared.purchase_id = nil
                    }
                    break
                default:
                    print("")
                }
                
            }
            let item01 : PRFR = PRFR.init(status: status, voucher: voucher, reservation_status: reservation_status, reservation_id: reservation_id)
            shareData.shared.ReservationFinalResponse = item01
            if status == "1" {
                self.openNewClass(classIdentifier: "ResponseConfirmOrder")
            }else if status == "0" {
                self.openNewClass(classIdentifier: "ResponseConfirmOrderVFail")
            }
            
        }else if tData.contains("org.poolticket://course-reservation?"){
            tData = tData.replacingOccurrences(of: "org.poolticket://course-reservation?", with: "")
            var tData2 = tData.split(separator: "&")
            for item in tData2 {
                var tData3 = item.split(separator: "=")
                switch tData3[0] {
                case "status":
                    status = String(tData3[1])
                    
                    if status == "1" {
                        // track Course payment success event in ios
//                        let event = ADJEvent.init(eventToken: "8b89yi")
//                        Adjust.trackEvent(event)
                    }
                    
                    break;
                case "voucher":
                    voucher = String(tData3[1])
                    break;
                case "reservation_status":
                    reservation_status = String(tData3[1])
                    break;
                case "reservation_id":
                    reservation_id = String(tData3[1])
                    break;
                    
                case "purchase_history_id":
                    purchase_history_id = String(tData3[1])
                    
                    if purchase_history_id != nil, (purchase_history_id?.count)! > 0 {
                        shareData.shared.purchase_id = purchase_history_id
                    }else{
                        shareData.shared.purchase_id = nil
                    }
                    break
                default:
                    print("")
                }
            }
            let item01 : PRFR = PRFR.init(status: status, voucher: voucher, reservation_status: reservation_status, reservation_id: reservation_id)
            shareData.shared.ReservationFinalResponse = item01
            if status == "1" {
                self.openNewClass(classIdentifier: "ResponseConfirmOrder")
            }else if status == "0" {
                self.openNewClass(classIdentifier: "ResponseConfirmOrderVFail")
            }
            
        }else if tData.contains("org.poolticket://reservation?"){
            tData = tData.replacingOccurrences(of: "org.poolticket://reservation?", with: "")
            let tData2 = tData.split(separator: "&")
            for item in tData2 {
                var tData3 = item.split(separator: "=")
                switch tData3[0] {
                case "status":
                    status = String(tData3[1])
                    if status == "1" {
                        // track Reservation Payment success event in ios
//                        let event = ADJEvent.init(eventToken: "8blg24")
//                        Adjust.trackEvent(event)
                    }
                    break;
                case "voucher":
                    voucher = String(tData3[1])
                    break;
                case "reservation_status":
                    reservation_status = String(tData3[1])
                    break;
                case "reservation_id":
                    reservation_id = String(tData3[1])
                    break;
                case "purchase_history_id":
                    purchase_history_id = String(tData3[1])
                    
                    if purchase_history_id != nil, (purchase_history_id?.count)! > 0 {
                        shareData.shared.purchase_id = purchase_history_id
                    }else{
                        shareData.shared.purchase_id = nil
                    }
                    break
                default:
                    print("")
                }
            }
            let item01 : PRFR = PRFR.init(status: status, voucher: voucher, reservation_status: reservation_status, reservation_id: reservation_id)
            shareData.shared.ReservationFinalResponse = item01
            if status == "1" {
                self.openNewClass(classIdentifier: "ResponseConfirmOrder")
            }else if status == "0" {
                self.openNewClass(classIdentifier: "ResponseConfirmOrderVFail")
            }
            
        }else if tData.contains("org.poolticket://credit-increase?") {
            var status = ""
            var credit = ""
            tData = tData.replacingOccurrences(of: "org.poolticket://credit-increase?", with: "")
            let tData2 = tData.split(separator: "&")
            for item in tData2 {
                var tData3 = item.split(separator: "=")
                switch tData3[0] {
                case "status":
                    status = String(tData3[1])
                    if status == "1" {
                        // track Credit Increase payment success event in ios
//                        let event = ADJEvent.init(eventToken: "cky7ck")
//                        Adjust.trackEvent(event)
                    }
                    break;
                case "credit":
                    credit = String(tData3[1])
                    break;
                default:
                    print("")
                }
            }
            
            if status != "" {
                shareData.shared.IncreaseCreditResult_Status = status
                shareData.shared.IncreaseCreditResult_Credit = credit
                shareData.shared.IncreaseCreditResult_setColse = true
                self.openNewClass(classIdentifier: "IncreaseCreditResult")
            }
        }
        

        return true
    }
    
    func openNewClass(classIdentifier: String){
        let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: classIdentifier) as UIViewController
        viewController.modalPresentationStyle = .fullScreen
        if let topController = UIApplication.topViewController() {
            topController.present(viewController, animated: true)
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let deviceTokenString = deviceToken.reduce("", { $0 + String(format: "%02X", $1) })
        UserDefaults.standard.set(deviceTokenString, forKey: UserDefaults.Keys.PushNotificationToken)
        UserDefaults.standard.synchronize()
        
        // Handle receive Device Token From APNS Server
        _manager?.application(application, didRegisterForRemoteNotificationsWithDeviceToken: deviceToken)
    }

//    @available(iOS 10.0, *)
//    func userNotificationCenter(_ center: UNUserNotificationCenter,
//                                willPresent notification: UNNotification,
//                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//        print(">>>>>>>>>>>>>>>>>>>>>>>test")
//
//    }
    
    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if let userinfo = response.notification.request.content.userInfo as? [AnyHashable : Any] {
            if let aps = userinfo["aps"] as? [String: Any] {
                let notification_id = String(describing: aps["notification_id"] as AnyObject)
                if let category = aps["category"] as? String {
                    if let push_token = UserDefaults.standard.value(forKey: UserDefaults.Keys.PushNotificationToken) as? String {
                        self.readNotification(push_token: push_token, notification_id: notification_id)
                    }
                    switch category {
                    case notificationCategory.OPEN_APP:
                        break
                    case notificationCategory.OPEN_POOL_PAGE:
                          if let acmeUrl = userinfo["acme-pool-id"] as? Int {
                            DispatchQueue.main.async {
                                DataSourceManagement.setSelectedPoolId(String(describing: acmeUrl))
                                let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PoolVC") as UIViewController
                                viewController.modalPresentationStyle = .fullScreen
                                if let topController = UIApplication.topViewController() {
                                    topController.present(viewController, animated: true)
                                }
                            }
                          }
                        break
                    case notificationCategory.OPEN_URL:
                        if let acmeUrl = userinfo["acme-url"] as? String {
                             SwiftUtil.openUrlInPhoneBrowser(acmeUrl)
                        }
                    case notificationCategory.OPEN_PURCHASE_HISTORY_DETAILS:
                        if let purchaseId = userinfo["acme-purchase-history-id"] as? Int {
                            shareData.shared.purchase_id = "\(purchaseId)"
                            let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ShowTransactionSpecificationVC") as UIViewController
                            viewController.modalPresentationStyle = .fullScreen
                            if let topController = UIApplication.topViewController() {
                                topController.present(viewController, animated: true)
                            }
                        }
                    default:
                        break
                    }
                    
                }
            }
        }
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
    }

    func setAdjustConfig(){
        
        
//        let appToken = "e60lbpthc1s0"
//        let environment = ADJEnvironmentProduction //ADJEnvironmentSandbox
//        //let environment = ADJEnvironmentProduction
//        let adjustConfig = ADJConfig(appToken: appToken, environment: environment)
//        // change the log level
//        adjustConfig?.logLevel = ADJLogLevelVerbose
        
        // Enable event buffering.
        // adjustConfig.eventBufferingEnabled = true
        // Set default tracker.
        // adjustConfig.defaultTracker = "{TrackerToken}"
        // Send in the background.
        // adjustConfig.sendInBackground = true
        // set an attribution delegate
        //adjustConfig?.delegate = self as? AdjustDelegate & NSObjectProtocol

        // first SDK Signiture
        //adjustConfig?.setAppSecret(4, info1: 156683929, info2: 620909385, info3: 1190098470, info4: 516173774);
        // sdk 0.4.4 / build 83
        //adjustConfig?.setAppSecret(11, info1: 1441605393, info2: 198578829, info3: 1756361850, info4: 1360150846);
        // sdk 0.4.8 / build 95
        //adjustConfig?.setAppSecret(20, info1: 971990495, info2: 449398308, info3: 1134667267, info4: 1794633930);
        // sdk 0.5 / build 99
       // adjustConfig?.setAppSecret(21, info1: 690632778, info2: 1864597163, info3: 1447726203, info4: 533181656);
        // Initialise the SDK.
       // Adjust.appDidLaunch(adjustConfig!)
        
    

        // Put the SDK in offline mode.
        // Adjust.setOfflineMode(true);
        
        // Disable the SDK
        //Adjust.setEnabled(true);
        
        
    }
    
//    func adjustAttributionChanged(_attribution: ADJAttribution) {
//        NSLog("adjust attribution %@", _attribution)
//    }
//    
//    func adjustEventTrackingSucceeded(_eventSuccessResponseData: ADJEventSuccess) {
//        NSLog("adjust event success %@", _eventSuccessResponseData)
//    }
//    
//    func adjustEventTrackingFailed(_eventFailureResponseData: ADJEventFailure) {
//        NSLog("adjust event failure %@", _eventFailureResponseData)
//    }
//    
//    func adjustSessionTrackingSucceeded(_sessionSuccessResponseData: ADJSessionSuccess) {
//        NSLog("adjust session success %@", _sessionSuccessResponseData)
//    }
//    
//    func adjustSessionTrackingFailed(_sessionFailureResponseData: ADJSessionFailure) {
//        NSLog("adjust session failure %@", _sessionFailureResponseData)
//    }
//    
//    @objc func adjustDeeplinkResponse(_deeplink: NSURL!) -> Bool {
//        return true
//    }

    func readNotification(push_token: String, notification_id: String) {
        let client_id = Constants.clientId
        let scope = "front";
        let params = "client_id=\(client_id)&notification_id=\(notification_id)&push_token=\(push_token)&access_token=\(SwiftUtil.getUserAccessToken())&scope=\(scope)"
        let postReadNotification = wsPOST.init(Constants.READ_NOTIFICATION,params)
        postReadNotification.start { (Data) in
            // result of the request
        }
    }
    
    
    //MARK : Notification AppDelegation
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Handle failure of get Device token from Apple APNS Server
        _manager?.application(application, didFailToRegisterForRemoteNotificationsWithError: error)
    }
    
    @available(iOS 8.0, *)
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        // Handle iOS 8 remote Notificaiton Settings
        _manager?.application(application, didRegister: notificationSettings)
    }
    
}

extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}

extension AppDelegate {
    enum notificationCategory {
        static let OPEN_APP = "OPEN_APP"
        static let OPEN_POOL_PAGE = "OPEN_POOL_PAGE"
        static var OPEN_URL = "OPEN_URL"
        static var OPEN_PURCHASE_HISTORY_DETAILS = "OPEN_PURCHASE_HISTORY_DETAILS"
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
