//
//  Constants.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/11/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation

enum Constants {
    
    //MARK: Static Token
    static let clientId = "cwmoedaql4cqrj82";

    //MARK: Base URL (Test and Final)
    private static let MAIN_URL = "https://www.poolticket.org/api/";
    //private static let MAIN_URL = "https://www.poolticket.org/gim248/public_html/api/";

    //MARK: End points
    /* check if Sign up before and get Token
     -> OAuth2 - Get access and refresh token */
    static let TOKEN = MAIN_URL + "v2/token"
    static let CHECK_REGISTRATION = MAIN_URL + "v2/check-register"
    static let RESEND_TOKEN = MAIN_URL + "v2/resend-mobile-approve"
    static let MOBILE_APPROVE = MAIN_URL + "v2/mobile-approve"
    static let ALL_STATES = MAIN_URL + "v2/states"
    static let ALL_CITIES_AND_STATES = MAIN_URL + "v2/cities"
    static let PASSWORD = MAIN_URL + "v2/password"
    static let REGISTER = MAIN_URL + "v2/register"
    static let INDEX = MAIN_URL + "v2/page/index"
    static let INDEXV2_1 = MAIN_URL + "v2.1/page/index"
    static let PROFILE = MAIN_URL + "v2/user/profile"
    static let UPDATE_PROFILE = MAIN_URL + "v2/user/profile/update"
    static let FAVORITE = MAIN_URL + "v2/user/pool/favorites"
    static let POOLS_LIST = MAIN_URL + "v2/pools/"
    static let CREDIT_INCREASE = MAIN_URL + "v2/user/credit-increase/start"
    static let POOLS_BY_ID = MAIN_URL + "v2/pool/"
    static let UNSET_FAVORITE = MAIN_URL + "v2/user/pool/unset-favorite"
    static let SET_FAVORITE = MAIN_URL + "v2/user/pool/set-favorite"
    static let POOLS_COMMENT = MAIN_URL +  "v2/pool/comments/"
    static let POOL_INTRODUCING = MAIN_URL +  "v2/pool/introducing/"
    static let POOL_TERMS_OF_USE = MAIN_URL +  "v2/pool/terms-of-use/"
    static let POOL_ATTRIBUTES = MAIN_URL +  "v2/pool/attributes"
    static let POOL_SEASNS_LIST = MAIN_URL + "v2/saens/list"
    static let POOL_PACK_LIST = MAIN_URL + "v2/pack/list"
    static let POOL_COURSE_LIST = MAIN_URL + "v2/course/list"
    static let PURCHASES_HISTORY = MAIN_URL + "v2/user/purchases-history/"
    static let SERVICE_DESCRIPTION = MAIN_URL + "v2/page/services-description"
    static let PURCHASE_BY_ID = MAIN_URL + "v2/user/purchase-history/"
    static let CALCULATE_RESERVATION = MAIN_URL + "v2/reservation/calculate"
    static let CALCULATE_PACK = MAIN_URL + "v2/pack-reservation/calculate"
    static let LIKE_AND_DISLIKE_COMMENT = MAIN_URL + "v2/user/pool/comment/like"
    static let CALCULATE_COURSE_RESERVATION = MAIN_URL + "v2/course-reservation/calculate"
    static let START_SAENS_RESERVATION = MAIN_URL + "v2/user/reservation/start"
    static let START_PACK_RESERVATION = MAIN_URL + "v2/user/pack-reservation/start"
    static let START_COURSE_RESERVATION = MAIN_URL + "v2/user/course-reservation/start"
    static let CONTACT_US = MAIN_URL + "v2/contact"
    static let CONTACT_US_TELL = MAIN_URL + "v2/page/contact-us"
    static let GET_ALL_AREA = MAIN_URL + "v2/areas"
    static let GET_POOL_TYPE = MAIN_URL + "v2/pool/types"
    static let RECOMMENDER_LINK = MAIN_URL + "v2/user/recommender-link"
    static let ABOUT_US = MAIN_URL + "v2/page/about-us"
    static let FAQ = MAIN_URL + "v2/page/faq"
    static let POOLS_RATING_ITEMS = MAIN_URL + "v2/pool/ratings"
    static let GET_CONTACT_US = MAIN_URL + "v2/page/contact-us"
    static let SUBMIT_COMMENT = MAIN_URL + "v2/user/pool/send-comment"
    static let READ_NOTIFICATION = MAIN_URL + "v2/notification/read"
    static let COURSE_LIST = MAIN_URL + "v2/courses/"
    static let PACK_LIST = MAIN_URL + "v2/packs/"
    static let SAENS_LIST = MAIN_URL + "v2/saens/"

    
    //MARK: Keys
    static let APP_SETTING = MAIN_URL + "v2/settings/app"
    static var Access_Token = "";
    static var AccessTokenTitle = "Access_Token";
    static var UserName = "";
    static var UserNameTitle = "Username";
    static var NextTimeUpdate =  "next_time_update"
    static var AllCitiesUpdate =  "all_cities_update"
    static var Refresh_Token = "";
    static var RefreshTokenTitle = "Refresh_Token";
    static var PTracking = "p-tracking";
    static var User_Id = "";
    static var UserIdTitle = "User_Id";
}


