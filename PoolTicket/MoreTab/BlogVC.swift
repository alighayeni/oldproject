//
//  BlogVC.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/20/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit


class BlogVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableview: UITableView!
    var list : [BlogPostModel] = []
    var refreshAllDataVariable = false
    var pageIndex = 1
    var lastPageLoaded = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.delegate = self
        tableview.dataSource = self
        
        self.getBlogPosts(String(pageIndex))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    @IBAction func close(){
        self.dismiss(animated: true, completion: nil)
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var indentifier = "blogCustomCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: indentifier, for: indexPath) as! blogCustomCell
        
        let data = self.list[indexPath.row]
        var tLink = "https://www.poolticket.org/blog/wp-content/uploads/2018/04/600_402_%D9%86%D9%85%D8%A7%DB%8C%DB%8C-%D8%A7%D8%B2-%D9%BE%D8%A7%D8%B1%DA%A9-%D8%A2%D8%A8%DB%8C-%DA%A9%D9%88%D8%AB%D8%B1-%D8%B4%DB%8C%D8%B1%D8%A7%D8%B2.jpeg"
        
        if let stringURL = URL.init(string: tLink) as? URL {
            let imageView = cell.postImg
            let URL = stringURL//Foundation.URL(string: data["thumbnail"]!)!
            let placeholderImage = UIImage(named: "main_top_bg.png")!
            imageView?.af_setImage(withURL: URL, placeholderImage: placeholderImage)
        }else{
            cell.postImg.image = UIImage(named: "main_top_bg.png")
        }
        
        
        cell.blogPost?.text = data.content
        cell.nTitle?.text = data.title
        cell.blogPost?.text = data.content
        cell.publishDate?.text = returnJalaliDate(data.date!)
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        return cell
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.list.count
    }
    
    func getBlogPosts(_ inx: String) -> Void {
        self.refreshAllDataVariable = true
        if inx == "1" {
            LoadingOverlay.shared.showOverlay(view: self.view)
        }
        let index = wsGET.init(Constants.POOLS_RATING_ITEMS + inx,"")//&access_token=\(access_token)
        var WSResponseStatus = false
        index.start{ (indexData) in
            do{
                if let value = Int(inx) as? Int {
                    self.lastPageLoaded = value
                }
                
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    
                    if let items = json as? [[String: AnyObject]]{
                        
                        if items.count == 10 {
                            self.pageIndex += 1
                        }
                        
                        for item in items {
                           self.list.append(BlogPostModel.prepare(item as AnyObject))
                        }
                        
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
        DispatchQueue.main.async(execute: {
                LoadingOverlay.shared.hideOverlayView()
                self.refreshAllDataVariable = false
                self.tableview.reloadData()
                     
        })
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let lastElement = self.list.count - 1
        if indexPath.row == lastElement, !refreshAllDataVariable {
            if lastPageLoaded < pageIndex {
                getBlogPosts(String(pageIndex))
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = self.list[indexPath.row]
        shareData.shared.selectedBlogLink = data.link
        openNewClass(classIdentifier: "BlogById")
        
    }
    
    
    
}
