//
//  MoreVC.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/13/19.
//  Copyright © 2019 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import SafariServices

class MoreVC: UIViewController {
    
    let USERDefault = UserDefaults.standard
    @IBOutlet weak var appVersionLabel: UILabel!
    @IBOutlet weak var updateTheNewVersion: UIButton!
    
    @IBOutlet weak var InstagramView: UIView!
    @IBOutlet weak var twitterBtn: UIButton!
    @IBOutlet weak var facebookBtn: UIButton!
    @IBOutlet weak var telegramBtn: UIButton!

    var latest_app_download_link = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = self.setBgColor(43, green: 163, blue: 215)
        self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.font.rawValue: UIFont(name: "IRANSansMobile", size: 16)!,NSAttributedString.Key.foregroundColor.rawValue:UIColor.white])
        self.title = "بیشتر"
        
        self.appVersionLabel.text = "نسخه \(self.replaceString(self.getCodeVersion()))"
        self.getAppSetting()
        
      
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let value = USERDefault.value(forKey: UserDefaults.Keys.facebook) as? String, value.count > 0 {
            self.facebookBtn.alpha = 1
        } else {
            self.facebookBtn.alpha = 0
        }
        if let value = USERDefault.value(forKey: UserDefaults.Keys.instagram) as? String, value.count > 0 {
            self.InstagramView.alpha = 1
        } else {
            self.InstagramView.alpha = 0
        }
        if let value = USERDefault.value(forKey: UserDefaults.Keys.telegram) as? String, value.count > 0 {
            self.telegramBtn.alpha = 1
        } else {
            self.telegramBtn.alpha = 0
        }
        if let value = USERDefault.value(forKey: UserDefaults.Keys.twitter) as? String, value.count > 0 {
            self.twitterBtn.alpha = 1
        } else {
            self.twitterBtn.alpha = 0
        }
        
    }
    
    @IBAction func purchaseHistory(){
        
        if SwiftUtil.getUserAccessToken() == ""{
            openNewClass(classIdentifier: "CheckPhoneNumberRegistrationVC")
        }else{
            self.pushNewClass(classIdentifier: "PurchasesHistoryVC")
        }
        
    }
    
    @IBAction func supportAction(){
        
        self.pushNewClass(classIdentifier: "SupportVC")
        
    }
    
    @IBAction func recommendedFriend(){
        
        if SwiftUtil.getUserAccessToken() == ""{
            openNewClass(classIdentifier: "CheckPhoneNumberRegistrationVC")
        }else{
            self.pushNewClass(classIdentifier: "TellAFriendAndRecommender")
        }
        
    }
    
    @IBAction func openAboutUs(){
        self.pushNewClass(classIdentifier: "AboutUS_VC")
        
    }
    
    @IBAction func openFAQ(){
        
        self.pushNewClass(classIdentifier: "FAQ_VC")
        
    }
    
    @IBAction func openBlogPost(){
        if let url = URL( string: "https://www.poolticket.org/blog")  {
            let svc = SFSafariViewController(url: url)
            present(svc, animated: true, completion: nil)
        }
    }
    
    @IBAction func openTelegram () {
        if let value = USERDefault.value(forKey: UserDefaults.Keys.telegram) as? String {
            SwiftUtil.openUrlInPhoneBrowser(value)
        }
    }
    
    @IBAction func openInstagram () {
        if let value = USERDefault.value(forKey: UserDefaults.Keys.instagram) as? String {
            SwiftUtil.openUrlInPhoneBrowser(value)
        }
    }
    
    @IBAction func openFacebook () {
        if let value = USERDefault.value(forKey: UserDefaults.Keys.facebook) as? String {
            SwiftUtil.openUrlInPhoneBrowser(value)
        }
    }
    
    @IBAction func openCart () {
               
        SwiftUtil.openUrlInPhoneBrowser("https://www.poolticket.org/shop")

    }
 
    @IBAction func downloadUpdate(){
        
        guard let url = URL(string: latest_app_download_link) else {
            return //be safe
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }
    /*  get the App Setting */
    func getAppSetting() -> Void {
//        var latest_all_cities_update = ""
        var latest_build = ""
//        var latest_build_support = ""
        var latest_version = ""
//        var timestamp = ""
        let access_token = SwiftUtil.getUserAccessToken()
        let client_id = Constants.clientId
        let scope = "front";
        var pushNotificationExtendQuery = ""
        if let value = UserDefaults.standard.value(forKey: UserDefaults.Keys.PushNotificationToken) as? String {
            pushNotificationExtendQuery = "&push_token=" + value
        }
        let index = wsPOST.init(Constants.APP_SETTING,"client_id=\(client_id)&package_name=org.poolticket.customer.ios&access_token=\(access_token)&scope=\(scope)&appClassPosition=DrawerMenu\(pushNotificationExtendQuery)")
        index.start{ (indexData) in
            
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                
                                if let app = item["app"] as? [String: AnyObject] {
                                    
                                    
                                    if let value = app["latest_app_download_link"] as? String {
                                        self.latest_app_download_link = value
                                    }
                                    if let value = app["latest_build"] as? Int {
                                        latest_build = String(value)
                                    }
//                                    if let value = app["latest_build_support"] as? String {
//                                        latest_build_support = value
//                                    }
                                    if let value = app["latest_version"] as? String {
                                        latest_version = value
                                    }
                                    
                                }
                                
                            }else{
                                // handle if it is not true
                            }
                        }
                        
                        
                    }
                }
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                
                // check if the app need to update
                var currentBuildVersion = 0
                var latestBuildVersion = 0
                if let value = Int(self.getBuildVersion()) {
                    currentBuildVersion = value
                }
                if let value = Int(latest_build) {
                    latestBuildVersion = value
                }
                if currentBuildVersion < latestBuildVersion {
                    self.updateTheNewVersion?.setTitle("دریافت \(self.replaceString(latest_version))" + " ", for: .normal)
                    self.updateTheNewVersion?.alpha = 1
                }else{
                    self.updateTheNewVersion.alpha = 0
                }
            
            })
            
        }
    }
    
    
}




// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
