//
//  BlogById.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/20/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class BlogById: UIViewController, WKUIDelegate, WKNavigationDelegate  {
    
    @IBOutlet weak var contentView:UIView!
    var webView: WKWebView!
    var theData = ""
    var urlObservation: NSKeyValueObservation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        self.webView = WKWebView(frame: self.contentView.bounds, configuration: WKWebViewConfiguration())
        self.webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //self.webView.translatesAutoresizingMaskIntoConstraints = false
        self.webView.navigationDelegate = self
        self.contentView.addSubview(self.webView)
        
        var tLink = shareData.shared.selectedBlogLink
        if let value = tLink as? String {
            let url = URL(string: value)!
            webView.load(URLRequest(url: url))
            webView.allowsBackForwardNavigationGestures = true
            urlObservation = webView.observe(\.url, changeHandler: { (webView, change) in
                print("Web view URL changed to \(webView.url?.absoluteString ?? "Empty")")
            })
            webView.addObserver(self, forKeyPath: #keyPath(WKWebView.navigationDelegate), options: .new, context: nil)
        }
        
        
    }
    
    @IBAction func close(){
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.webView.removeObserver(self, forKeyPath: #keyPath(WKWebView.navigationDelegate))
        self.webView = nil
        
    }
    
}
