//
//  AboutUS_VC.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/14/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import SCLAlertView
import WebKit

class SupportVC: UIViewController, UITextFieldDelegate, UITextViewDelegate, WKNavigationDelegate {
    
    @IBOutlet weak var mainViewHeight: NSLayoutConstraint!
    var activeField: UITextField?
    var refreshAllDataVariable = false
    @IBOutlet weak var view01:UIView!
//    @IBOutlet weak var view02:UIView!
   
    @IBOutlet weak var contentView:UIView!
    var content = ""
    var webView: WKWebView!
    var urlObservation: NSKeyValueObservation?


    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var nameAndLastName:UITextField!
    @IBOutlet weak var phoneNumber:UITextField!
    @IBOutlet weak var message:UITextView!
    @IBOutlet weak var email:UITextField!

    var pNumber = ""

    var submitOnce = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = self.setBgColor(43, green: 163, blue: 215)
        self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.font.rawValue: UIFont(name: "IRANSansMobile", size: 16),NSAttributedString.Key.foregroundColor.rawValue:UIColor.white])
        self.title = "تماس با ما"
        
        navigationController?.navigationBar.tintColor = UIColor.white
        
        // create call icon
        let callBtnIcon = UIBarButtonItem(image: UIImage(named: "ic_call_contact_us"),style: .plain ,target: self, action: #selector(self.callANumber))
        self.navigationItem.rightBarButtonItem = callBtnIcon

    
        
        nameAndLastName.delegate = self
        nameAndLastName.tag = 1
        nameAndLastName.text = returnProfileValue("name") ?? ""
        doneWithKeyboard(nameAndLastName)
        
        phoneNumber.delegate = self
        phoneNumber.tag = 2
        phoneNumber.text = returnProfileValue("mobile") ?? ""
        doneWithKeyboard(phoneNumber)
        
        email.delegate = self
        email.tag = 3
        email.text = returnProfileValue("email") ?? ""
        doneWithKeyboard(email)
        
        message.delegate = self
        message.tag = 4
        doneWithKeyboard(message)
        
        view01.dropShadow()
        //view02.dropShadow()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        registerForKeyboardNotifications()
        getContactNumber();
        
    }
    
    @IBAction func closeVC(_ sender:Any) -> Void {
       self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitComment(_ sender: UIButton) -> Void {

        guard let name = self.nameAndLastName.text as? String else{
            showDropMessage("")
            return
        }
        
        if name.count < 3 {
            showAlert("failNameSurname")
            return
        }
        
        guard let phoneNumber = self.phoneNumber.text as? String else{
            showDropMessage("")
            return
        }
        if phoneNumber.count < 7 {
            showAlert("failPhoneNumber")
            return
        }
        
        guard let message = self.message.text as? String else{
            showDropMessage("")
            return
        }
        if message.count < 10 {
            showAlert("failMessage")
            return
        }
        
        
        guard let email = self.email.text as? String else{
            showDropMessage("")
            return
        }
        
        
        if !refreshAllDataVariable{
            if !submitOnce {
                self.submitComment(name, phoneNumber, email, message)
            }else{
                self.showAlert("submitOnce")
            }
        }
        
    }
    
    func getContactNumber() -> Void {
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.CONTACT_US_TELL,"client_id=\(client_id)&access_token=\(access_token)")
        index.start{ (contact_us_detail) in
            do{
                
                if let rawData = contact_us_detail {
                    let json = try JSONSerialization.jsonObject(with: rawData, options: JSONSerialization.ReadingOptions.allowFragments)
                    //print(json)
                    
                    if let items = json as? [String: AnyObject]{
                     
                        if let contact_us = items["contact_us"] as? [String: AnyObject] {
                            if let value = contact_us["phone"] as? String  {
                                self.pNumber = value
                            }
                            if let value = contact_us["content"] as? String  {
                                self.content = value
                            }
                        }
                        
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            DispatchQueue.main.async(execute: {
               
                if self.content.count > 0 {
                    self.showContent()
                }
                
            })
            
        }
        
        
    }
    
    func showContent(){
        
        self.webView = WKWebView(frame: self.contentView.bounds, configuration: WKWebViewConfiguration())
        self.webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //self.webView.translatesAutoresizingMaskIntoConstraints = false
        self.webView.navigationDelegate = self
        self.contentView.addSubview(self.webView)
        
        let header = "<html><head><link rel='stylesheet' href='style.css' type='text/css'><meta name='viewport' content='initial-scale=1.0'/></head><body>"
        let finalHtml = header + self.content + "</body></html>"
        webView.loadHTMLString(finalHtml, baseURL: nil)
        webView.allowsBackForwardNavigationGestures = true
        urlObservation = webView.observe(\.url, changeHandler: { (webView, change) in
            print("Web view URL changed to \(webView.url?.absoluteString ?? "Empty")")
        })
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.navigationDelegate), options: .new, context: nil)
        

    }
    
    func submitComment(_ name:String, _ mobile: String, _ email:String, _ content: String) -> Void {
        LoadingOverlayV2.sharedV2.showOverlayV2(view: self.view, message: "در حال ارسال پیام...")
        self.refreshAllDataVariable = true
        let access_token = SwiftUtil.getUserAccessToken()
        let client_id = Constants.clientId
        let index = wsPOST.init(Constants.CONTACT_US,"client_id=\(client_id)&name=\(name)&mobile=\(mobile)&content=\(content)&email=\(email)&access_token=\(access_token)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            
            var reserve_status = ""
            
            do{
                
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                
                            }
                        }
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                LoadingOverlayV2.sharedV2.hideOverlayView()
                self.refreshAllDataVariable = false
                if WSResponseStatus {
                    self.showAlert("success")
                    self.submitOnce = true
                }else{
                    self.showAlert("fail")
                }
            })
            
        }
        
        
    }
    
    @objc
    func callANumber() {
    
    if let url = URL(string: "tel://\(pNumber)"), UIApplication.shared.canOpenURL(url) {
        if #available(iOS 10, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
}
    

    
    /*keyboard automatic scroll*/
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.isScrollEnabled = true
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: keyboardSize!.height, right: 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.activeField {
            if (!aRect.contains(activeField.frame.origin)){
                self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        var info = notification.userInfo!
        //let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0) //-keyboardSize!.height
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollView.isScrollEnabled = true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
    
    
    //************** softkeyboard tool bar **********************//
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField
        {
        case nameAndLastName:
            nameAndLastName.resignFirstResponder()
            phoneNumber.becomeFirstResponder()
            break
        case phoneNumber:
            phoneNumber.resignFirstResponder()
            email.becomeFirstResponder()
            break
        case email:
            email.resignFirstResponder()
            message.becomeFirstResponder()
            break
        case message:
            message.resignFirstResponder()
            break
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
    func doneWithKeyboard(_ sender: Any){
        
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        keyboardToolbar.isTranslucent = true
        keyboardToolbar.barTintColor = UIColor.white
        
        let button = UIButton(type: .system)
        button.setTitle("بستن", for: .normal)
        button.sizeToFit()
        button.titleLabel?.font = UIFont(name: "IRANSansMobile", size: 12)
        button.tintColor = UIColor.black
        button.addTarget(self, action: #selector(closeKeyBoard), for: .touchUpInside)
        
        
        let btnUp = UIButton(type: .custom)
        btnUp.setImage(UIImage(named: "ic_keyboard_arrow_up"), for: .normal)
        btnUp.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        btnUp.tag = (sender as AnyObject).tag
        btnUp.addTarget(self, action: #selector(textFieldUp), for: .touchUpInside)
        let itemUp = UIBarButtonItem(customView: btnUp)
        
        let btnDown = UIButton(type: .custom)
        btnDown.setImage(UIImage(named: "ic_keyboard_arrow_down"), for: .normal)
        btnDown.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        btnDown.tag = (sender as AnyObject).tag
        btnDown.addTarget(self, action: #selector(textFieldDown), for: .touchUpInside)
        let itemDown = UIBarButtonItem(customView: btnDown)
        
        
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        //keyboardToolbar.items = [flexBarButton,UIBarButtonItem(customView: button)]
        keyboardToolbar.items = [itemDown,itemUp,flexBarButton,UIBarButtonItem(customView: button)]
        
        if let value = sender as? UITextField {
            value.inputAccessoryView = keyboardToolbar
        }else if let value = sender as? UITextView {
            value.inputAccessoryView = keyboardToolbar
        }
        
    }
    
    @objc func closeKeyBoard(){
        self.view.endEditing(true)
    }
    
    @objc func textFieldUp(_ sender: Any)-> Void {
        
        let tObject = (sender as AnyObject).tag
        if let value = tObject as? Int {
            switch value
            {
            case 1:
                nameAndLastName.resignFirstResponder()
                break
            case 2:
                phoneNumber.resignFirstResponder()
                nameAndLastName.becomeFirstResponder()
                break
            case 3:
                email.resignFirstResponder()
                phoneNumber.becomeFirstResponder()
                break
            case 4:
                message.resignFirstResponder()
                email.becomeFirstResponder()
                break
            default:
                print("")
            }
        }
    }
    
    @objc func textFieldDown(_ sender: Any)-> Void {
        
        let tObject = (sender as AnyObject).tag
        if let value = tObject as? Int {
            switch value
            {
            case 1:
                nameAndLastName.resignFirstResponder()
                phoneNumber.becomeFirstResponder()
                break
            case 2:
                phoneNumber.resignFirstResponder()
                email.becomeFirstResponder()
                break
            case 3:
                email.resignFirstResponder()
                message.becomeFirstResponder()
                break
            case 4:
                message.resignFirstResponder()
                break
            default:
                print("")
            }
        }
    }
    
    
    //************** softkeyboard tool bar **********************//
    
    func showAlert(_ type: String) {
        
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "IRANSansMobile", size: 17)!,
            kTextFont: UIFont(name: "IRANSansMobile", size: 14)!,
            kButtonFont: UIFont(name: "IRANSansMobile", size: 14)!,
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        
        switch type {
        case "success":
            alertView.addButton("بستن") {
            }
            alertView.showInfo("پیام:", subTitle: "پیام شما با موفقیت ارسال شد.")
            break
        case "fail":
            alertView.addButton("بستن") {
            }
            alertView.showError("اخطار:", subTitle: "لطفا اطلاعات وارد شده را بررسی کنید و دوباره تلاش کنید")
            break
        case "failNameSurname":
            alertView.addButton("بستن") {
            }
            alertView.showNotice("اخطار:", subTitle: "نام و نام خانوادگی را تکمیل بفرمائید")
            break
        case "failPhoneNumber":
            alertView.addButton("بستن") {
            }
            alertView.showNotice("اخطار:", subTitle: "شماره همراه وارد شده صحیح نمی باشد")
            break
            
        case "failMessage":
            alertView.addButton("بستن") {
            }
            alertView.showNotice("اخطار:", subTitle: "متن پیام مورد نظر را یادداشت بفرمائید.")
            break
            
        case "submitOnce":
            alertView.addButton("بستن") {
            }
            alertView.showError("اخطار:", subTitle: "پیام شما با موفقیت ارسال شده است. شما مجاز به ثبت درخواست محدد نیستید.")
            break
        default:
            print("")
        }
        
    }
    
    func returnProfileValue(_ category: String) -> String? {
        switch category {
        case "name":
            if let value = UserDefaults.standard.value(forKey: UserDefaults.Keys.profileName) as? String {
                return value
            }
        case "email":
            if let value = UserDefaults.standard.value(forKey: UserDefaults.Keys.profileEmail) as? String {
                return value
            }
        case "mobile":
            if let value = UserDefaults.standard.value(forKey: UserDefaults.Keys.profileMobile) as? String {
                return value
            }
        default:
            break
        }
        return nil
    }
    
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
