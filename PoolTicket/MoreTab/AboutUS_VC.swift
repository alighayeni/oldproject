//
//  ContactUS_VS.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/17/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class AboutUS_VC: UIViewController, WKUIDelegate, WKNavigationDelegate {
   
    @IBOutlet weak var contentView:UIView!
    var webView: WKWebView!
    var theData = ""
    var urlObservation: NSKeyValueObservation?

    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.navigationController?.navigationBar.barTintColor = self.setBgColor(43, green: 163, blue: 215)
        self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.font.rawValue: UIFont(name: "IRANSansMobile", size: 16),NSAttributedString.Key.foregroundColor.rawValue:UIColor.white])
        self.title = "درباره ما"
        navigationController?.navigationBar.tintColor = UIColor.white
        
        self.webView = WKWebView(frame: self.contentView.bounds, configuration: WKWebViewConfiguration())
        self.webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //self.webView.translatesAutoresizingMaskIntoConstraints = false
        self.webView.navigationDelegate = self
        self.contentView.addSubview(self.webView)
        var tLink = Constants.ABOUT_US + "?client_id=\(Constants.clientId)"
        if let value = tLink as? String {
            let url = URL(string: value)!
            webView.load(URLRequest(url: url))
            webView.allowsBackForwardNavigationGestures = true
            urlObservation = webView.observe(\.url, changeHandler: { (webView, change) in
                print("Web view URL changed to \(webView.url?.absoluteString ?? "Empty")")
            })
            webView.addObserver(self, forKeyPath: #keyPath(WKWebView.navigationDelegate), options: .new, context: nil)
        }
       // self.getAboutUS()
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.webView.removeObserver(self, forKeyPath: #keyPath(WKWebView.navigationDelegate))
        self.webView = nil
        
    }
    
    @IBAction func close(){
        self.dismiss(animated: true, completion: nil)
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
