//
//  TellAFriendAndRecommender.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/17/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class TellAFriendAndRecommender: UIViewController {
    
    var coupon_code = ""
    var link = ""
    var coupon_text = ""
    var refreshAllDataVariable = false
    
    @IBOutlet weak var codeLable: UILabel!
    @IBOutlet weak var copounText: UILabel!
    @IBAction func copyTheLink(){
        
        UIPasteboard.general.string = coupon_code
        showDropMessage("کد دعوت به دوستان با موفقیت کپی شد.")
    
    }
    
    @IBAction func shareTheCode(){
        AppDelegate()._manager?.track("free_charge", data: [:])
        Analytics.logEvent("free_charge", parameters: nil)
       // var shareString = "Chappar iOS Application https://goo.gl/Mtd66A"
        let activityViewController = UIActivityViewController(activityItems:
            [link], applicationActivities: nil)
        let excludeActivities = [
            UIActivity.ActivityType.mail,
            UIActivity.ActivityType.print,
            UIActivity.ActivityType.assignToContact,
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType.postToFlickr,
            UIActivity.ActivityType.postToTencentWeibo,
            UIActivity.ActivityType.airDrop]
        activityViewController.excludedActivityTypes = excludeActivities;
        
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            activityViewController.popoverPresentationController?.sourceView = super.view
        }
        
        self.present(activityViewController, animated: true, completion: nil)
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = self.setBgColor(43, green: 163, blue: 215)
        self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.font.rawValue: UIFont(name: "IRANSansMobile", size: 16)!,NSAttributedString.Key.foregroundColor.rawValue:UIColor.white])
        self.title = "دریافت شارژ رایگان"
        
        
        
        // track Free charge in ios
//        let event = ADJEvent.init(eventToken: "kvgiqg")
//        Adjust.trackEvent(event)
        
        self.recommenderLink()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    @IBAction func closeTheApp(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func recommenderLink() -> Void {
        self.refreshAllDataVariable = true
        LoadingOverlay.shared.showOverlay(view: self.view)
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.RECOMMENDER_LINK,"client_id=\(client_id)&access_token=\(access_token)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            do{
                
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                               
                                if let value = item["coupon_code"] as? String {
                                    self.coupon_code = value
                                }
                              
                                if let value = item["link"] as? String {
                                    self.link = value
                                }
                                
                                if let value = item["coupon_text"] as? String {
                                    self.coupon_text = value
                                }
                                
                            }
                        }
                    }
                }
                
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                LoadingOverlay.shared.hideOverlayView()
                self.refreshAllDataVariable = false
                
                if WSResponseStatus {
                    self.codeLable.text = self.replaceString(self.coupon_code)
                    self.copounText.text = self.coupon_text
                }else{
                    
                }
            })
            
        }
        
        
    }
    
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
