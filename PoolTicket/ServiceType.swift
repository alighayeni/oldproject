//
//  ServiceType.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 6/28/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit

class ServiceType: NSObject {
    
    var type_id:String?
    var name:String?
    
    init(_ type_id:String,_ name:String) {
        self.type_id = type_id
        self.name = name
        
    }
    
    static func prepare(_ item:AnyObject) -> ServiceType{
        
        let type_id = String(describing: item["type_id"] as AnyObject)
        let name = String(describing: item["name"] as AnyObject)
        
        let instance = ServiceType(type_id, name)
        return instance
    }
    
}
