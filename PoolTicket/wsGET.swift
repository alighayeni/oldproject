//
//  wsGET.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 2/13/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
class wsGET:NSObject {
    
    var url: String?
    var postString: String
    init(_ url: String,_ postString: String) {
        self.url = url
        self.postString = postString
//        if postString.count > 0 {
//            self.postString = postString + "&P-Tracking=" + wsGET.getPTrackingValue()
//        }else{
//            self.postString = "P-Tracking=" + wsGET.getPTrackingValue()
//        }
    }
    
    func start(_ done : @escaping (_ status:Data?)->Void) -> Void {
        
        let urlString = self.url
        var request = URLRequest(url: URL(string:  urlString!)!)
        request.httpMethod = "GET"
        request.setValue(wsGET.getPTrackingValue(), forHTTPHeaderField: "P-Tracking")
        request.addValue(wsGET.getPTrackingValue(), forHTTPHeaderField: "P-Tracking")
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let session = URLSession.shared
        session.configuration.timeoutIntervalForRequest = 30
        session.configuration.timeoutIntervalForResource = 30
        session.dataTask(with: request) {data, response, err in
           
            if let httpResponse = response as? HTTPURLResponse {
                if let value = httpResponse.allHeaderFields as? [String: Any] {
                    if let P_Tracking = value["p-tracking"] as? String {
                        if P_Tracking.count > 0 {
                        self.setPTrackingValue(P_Tracking)
                        }
                    }
                }
            }
            
            if let resData = data {
                done(resData)
            }else{
                done(nil)
            }
            }.resume()
    }
    
    // setter and getter refresh token
    func setPTrackingValue(_ value: String){
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: UserDefaults.Keys.PTracking)
    }
    
    static func getPTrackingValue()->String{
        let defaults = UserDefaults.standard
        if let value = defaults.string(forKey: UserDefaults.Keys.PTracking){
            return value
        }
        return ""
    }
    
    
}
