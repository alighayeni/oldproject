//
//  LeftDrawer.swift
//  drawerSample
//
//  Created by Ali Ghayeni on 11/30/16.
//  Copyright © 2016 Ali Ghayeni. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import AlamofireImage

class RightDrawerVC: UIViewController {
 
    var mName = "";
    var mTelephone = "";
    var mCredit = "";
    var latest_app_download_link = ""
    @IBOutlet weak var drawerLogo:UIImageView!
    @IBOutlet weak var appVersionLabel: UILabel!
    @IBOutlet weak var updateTheNewVersion: UIButton!
    @IBAction func openTheLink(){
        
        guard let url = URL(string: latest_app_download_link) else {
            return //be safe
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }
    
    @IBOutlet weak var loginTopSpace: NSLayoutConstraint!
    @IBOutlet weak var loginAndRegister: UIView!
    
    @IBOutlet weak var bottomTopSpace: NSLayoutConstraint!
    @IBOutlet weak var bottomHeaderHeight: NSLayoutConstraint!
    
    @IBOutlet weak var topSectionStackView: UIStackView!
    @IBOutlet weak var topHeaderHeight: NSLayoutConstraint!
    @IBOutlet weak var profileNameBtn:UIButton!
    @IBOutlet weak var profileAmountBtn:UIButton!
    
    @IBAction func chargeTheAccount(){
        
        if SwiftUtil.getUserAccessToken() == ""{
                openNewClass(classIdentifier: "CheckPhoneNumberRegistrationVC")
        }else{
         openNewClass(classIdentifier: "StartCreditIncreaseVC")
        }
         self.slideMenuController()?.closeRight()

    }
    
    @IBAction func itemloginAndRegister(){
        
        openNewClass(classIdentifier: "CheckPhoneNumberRegistrationVC")
        self.slideMenuController()?.closeRight()
        
    }
    
    @IBAction func showFavorite(){
        
        DataSourceManagement.addSelectedDrawerState("1")
        self.slideMenuController()?.closeRight()
        
    }
    
    @IBAction func purchaseHistory(){
        
        if SwiftUtil.getUserAccessToken() == ""{
            openNewClass(classIdentifier: "CheckPhoneNumberRegistrationVC")
        }else{
            self.openNewClass(classIdentifier: "PurchasesHistoryVC")
        }
        self.slideMenuController()?.closeRight()
        
    }
    
    @IBAction func supportAction(){
        
        self.openNewClass(classIdentifier: "SupportVC")
        self.slideMenuController()?.closeRight()
        
    }
    
    @IBAction func recommendedFriend(){
    
        if SwiftUtil.getUserAccessToken() == ""{
            openNewClass(classIdentifier: "CheckPhoneNumberRegistrationVC")
        }else{
            self.openNewClass(classIdentifier: "TellAFriendAndRecommender")
        }
        self.slideMenuController()?.closeRight()
    
    }

    @IBAction func openAboutUs(){
        
        self.openNewClass(classIdentifier: "AboutUS_VC")
        self.slideMenuController()?.closeRight()
        
    }
    
    @IBAction func openFAQ(){
        
        self.openNewClass(classIdentifier: "FAQ_VC")
        self.slideMenuController()?.closeRight()
        
    }
    
    @IBAction func openBlogPost(){
        
        self.openNewClass(classIdentifier: "BlogVC")
        self.slideMenuController()?.closeRight()
        
    }
    
    
    
    
//    AboutUS_VC
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.appVersionLabel.text = "پول تیکت نسخه \(self.replaceString(self.getCodeVersion()))"
        
        self.getProfile()
        self.getAppSetting()
        
        if SwiftUtil.getUserAccessToken() == ""{
            self.topSectionStackView.isHidden = true
            self.loginAndRegister.isHidden = false
            self.bottomTopSpace.constant = 52
            self.loginTopSpace.constant = 8
        }else{
            self.topSectionStackView.isHidden = false
            self.loginAndRegister.isHidden = true
            self.bottomTopSpace.constant = 138
        }
        
       
        
        if let value = UserDefaults.standard.value(forKeyPath: UserDefaults.Keys.drawerLogoUrl) as? String {
            Alamofire.request(value).responseImage { response in
                debugPrint(response)
                debugPrint(response.result)
                if let image = response.result.value {
                    self.drawerLogo.image = image
                }
            }
        } else {
            self.drawerLogo.image = UIImage.init(named: "drawer_to_logo")
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.updateTheNewVersion.alpha = 0

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // get data from profile
    func getProfile() -> Void {
        let client_id = Constants.clientId
        let access_token = SwiftUtil.getUserAccessToken()
        let index = wsPOST.init(Constants.PROFILE,"client_id=\(client_id)&access_token=\(access_token)")
        var WSResponseStatus = false
        index.start{ (indexData) in
            
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    
                    
                    
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                WSResponseStatus = true
                                
                                if let profile = item["profile"] as? [String: AnyObject] {
                                    if let value = profile["name"] as? String {
                                        self.mName = value
                                    }
                                    if let value = profile["mobile"] as? String {
                                        self.mTelephone = value
                                    }
                                    if let value = profile["credit"] as? Int {
                                        self.mCredit = String(value)
                                    }
                                }
                            }
                        }
                    }
                    
                    
                }
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                
                if WSResponseStatus {
                    self.profileNameBtn.setTitle(self.mName, for: .normal)
                    self.profileAmountBtn.setTitle(self.setSeprator(self.mCredit), for: .normal)
                }
            })
            
        }
        
        
    }
    
    /*  get the App Setting */
    func getAppSetting() -> Void {
        var latest_all_cities_update = ""
        var latest_build = ""
        var latest_build_support = ""
        var latest_version = ""
        var timestamp = ""
        let access_token = SwiftUtil.getUserAccessToken()
        let client_id = Constants.clientId
        let scope = "front";
        var pushNotificationExtendQuery = ""
        if let value = UserDefaults.standard.value(forKey: UserDefaults.Keys.PushNotificationToken) as? String {
            pushNotificationExtendQuery = "&push_token=" + value
        }
        let index = wsPOST.init(Constants.APP_SETTING,"client_id=\(client_id)&package_name=org.poolticket.customer.ios&access_token=\(access_token)&scope=\(scope)&appClassPosition=DrawerMenu\(pushNotificationExtendQuery)")
        index.start{ (indexData) in
            
            do{
                if let indexValue = indexData {
                    let json = try JSONSerialization.jsonObject(with: indexValue, options: JSONSerialization.ReadingOptions.allowFragments)
                    if let item = json as? [String: AnyObject]{
                        
                        if let value = item["status"] as? Int {
                            if value == 1 {
                                
                                if let app = item["app"] as? [String: AnyObject] {
                                    
                                   
                                    if let value = app["latest_app_download_link"] as? String {
                                        self.latest_app_download_link = value
                                    }
                                    if let value = app["latest_build"] as? Int {
                                        latest_build = String(value)
                                    }
                                    if let value = app["latest_build_support"] as? String {
                                        latest_build_support = value
                                    }
                                    if let value = app["latest_version"] as? String {
                                        latest_version = value
                                    }
                                    
                                }
                                
                            }else{
                                // handle if it is not true
                            }
                        }
                        
                        
                    }
                }
            }catch{
                print("this is the error: \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                
                // check if the app need to update
                    var currentBuildVersion = 0
                    var latestBuildVersion = 0
                    if let value = Int(self.getBuildVersion()) {
                        currentBuildVersion = value
                    }
                    if let value = Int(latest_build) {
                        latestBuildVersion = value
                    }
                    if currentBuildVersion < latestBuildVersion {
                      self.updateTheNewVersion.setTitle("دریافت نسخه \(self.replaceString(latest_version))", for: .normal)
                      self.updateTheNewVersion.alpha = 1
                    }else{
                      self.updateTheNewVersion.alpha = 0
                    }
                
            })
            
        }
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
