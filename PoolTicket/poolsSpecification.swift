//
//  poolsSpecification.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/1/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation

class poolsSpecification: NSObject {
    
    var name:String?
    var value:String?
    var icon:String?

    
    init(_ name:String,_ value:String, _ icon:String) {
        self.name = name
        self.value = value
        self.icon = icon
    }
    
    static func prepare(_ item:AnyObject) -> poolsSpecification{
        let name = String(describing: item["name"] as AnyObject)
        let icon = String(describing: item["icon"] as AnyObject)
        let value = String(describing: item["value"] as AnyObject)

        let instance = poolsSpecification(name, value, icon)
        return instance
    }
}
