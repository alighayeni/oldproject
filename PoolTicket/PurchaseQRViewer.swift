//
//  PurchaseQRViewer.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 5/3/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
import EFQRCode


class PurchaseQRViewer: UIViewController {
    
    @IBOutlet weak var qrImage:UIImageView!
    @IBOutlet weak var qrId:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "QR Code"
        
        if let value = shareData.shared.selectedPurchaseQRString as? String {
            self.qrId.text  = "کد رزرو بلیط: " + replaceString(value)
                if let tryImage = EFQRCode.generate(
                    content: value
                    ) {
                    print("Create QRCode image success: \(tryImage)")
                    self.qrImage?.image = UIImage(cgImage: tryImage)
                } else {
                    print("Create QRCode image failed!")
                }
        }
        
    }
    
    
}
