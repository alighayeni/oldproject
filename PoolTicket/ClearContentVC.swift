//
//  RulesOfUseingPools.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/22/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit

class ClearContentVC: UIViewController {
    
    @IBOutlet weak var topLable:UILabel!
    @IBOutlet weak var contentView:UITextView!
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if shareData.shared.selectedChoosenPoolsRulesAndDetails != nil {
            self.topLable.text = "شرایط استفاده"
            self.contentView?.text = shareData.shared.selectedChoosenPoolsRulesAndDetails
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    @IBAction func closeVC(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }
    
}


