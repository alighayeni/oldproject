//
//  reserveSaensCustomRowV1.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/6/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit
class reserveSaensCustomRowV1: UITableViewCell {
    
    
    
    @IBOutlet weak var dateAndDay: UILabel!
    @IBOutlet weak var times: UILabel!

    @IBOutlet weak var descriptionTitle: UILabel!
    @IBOutlet weak var descriptionContent: UILabel!

    @IBOutlet weak var titlePrice1: UILabel!
    @IBOutlet weak var price1WithDiscount: UILabel!
    @IBOutlet weak var price1: UILabel!
    @IBOutlet weak var discountRate1: UILabel!

    
    @IBOutlet weak var titlePrice2: UILabel!
    @IBOutlet weak var price2WithDiscount: UILabel!
    @IBOutlet weak var price2: UILabel!
    @IBOutlet weak var discountRate2: UILabel!

    
    
    @IBOutlet weak var mainView1: UIView!
    @IBOutlet weak var mainView2: UIView!

    @IBOutlet weak var reserveSaens: UIButton!

    override func awakeFromNib() {
        mainView1.layer.borderWidth = 1.0
        mainView1.layer.borderColor = UIColor.lightGray.cgColor
        mainView1.clipsToBounds = true
        
        mainView2.layer.borderWidth = 1.0
        mainView2.layer.borderColor = UIColor.lightGray.cgColor
        mainView2.clipsToBounds = true
    }
    
}
