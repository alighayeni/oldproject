//
//  blogCustomCell.swift
//  PoolTicket
//
//  Created by Ali Ghayeni on 4/20/18.
//  Copyright © 2018 OneSignal. All rights reserved.
//

import Foundation
import UIKit

class blogCustomCell: UITableViewCell {
    
    @IBOutlet weak var postImg: UIImageView!
    @IBOutlet weak var nTitle: UILabel!
    @IBOutlet weak var publishDate: UILabel!
    @IBOutlet weak var blogPost: UILabel!

}
